package parallel;
import static org.junit.Assert.assertTrue;

import java.sql.ResultSet;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


   public class JavaScriptControls extends PageBase {
//	public static final String base=null;
	
	
	 //Java Script EjsExecutor=(JavascriptExecutor)driver;
	
	public static void waitforJStoLoad() {
		
		if(jsExecutor.executeScript("return document.readyState").toString().equals("Complete")) {
			System.out.println("Page is Loaded");
		}
		for(int i=0;i<25;i++) {
			try {
				Thread.sleep(2000);
			}
			catch(Exception e) {
				}
			
			if(jsExecutor.executeScript("return document.ready state").toString().equals("Complete")) {
				
				break;
				}
			}
			}
		
		
	

	public static void waitForPageLoad() {
		String header="document.getElementsByTagName('h1')[0].innerText";
		String title="document.getElementByClassName('title')[0].innerText";
		
		for(int i=0;i<25;i++) {
			
			try {
				Thread.sleep(2000);
				
				
				if(jsExecutor.executeScript("return document.getElementbyTagName('h1')[0]")!=null) {
					if(jsExecutor.executeScript("return" +header).toString()!=null);
				
				{
						System.out.println("Page is loaded :" +jsExecutor.executeScript("return" +header).toString());
						break;
					
				
				}}
				
				else if (jsExecutor.executeScript("return document.getElementbyTagName('h1')[0]")!=null) {
					if(jsExecutor.executeScript("return" +header).toString()!=null);{
						
						System.out.println("Page is loaded :" +jsExecutor.executeScript("return" +title).toString());
						break;
					}
					}
				}
				
		catch(Exception e) {
			}
		}
}
	
	public static void WaitUntilFieldIsReady(String fieldname) {
		System.out.println("Waiting for normal field");
		PredicateStepandWaitToComplete("PageObject.find("+jsBuilder(fieldname)+")!=undefined");
		//PredicateStepandWaitToComplete("PageObject.find("+jsBuilder(fieldname)+").stencilled==false";)
		
		
	}
	
	public static void WaitUntilControlIsReady(String ControlKey) {
		try {
			PredicateStepandWaitToComplete("PageObjects.find({"+buildcriteria(ControlKey)+"})!=undefined");
			PredicateStepandWaitToComplete("PageObjects.find({"+buildcriteria(ControlKey)+").stencilled==undefined|| PageObjects.find({"+buildcriteria(ControlKey)+"}).stencilled==false");
			
		}
		catch(Exception e) {}
	}
	

public static void PredicateStepandWaitToComplete(String predicateScriptToBeExecuted) {
	
	//WebDriverWait jswait=new WebDriverWait(driver,30);
	try {
		Thread.sleep(200);
		//ExpectedCondition<Boolean> predicateComplete=((JavaScriptExecutor)browser).executeScript("return" +predicateScriptToBeExecuted).equals(true);
	}
	catch(Exception e) {}
}

public static String jsBuilder(String key) {
	key="{'caption':"+key+"'}";
	return key;
	
}

public static String jsBuilderForDataKey(String key) {
	key="{'dataKey':"+key+"'}";
	return key;
}

public static String jsBuilderForControls(String key) {
	key="{'name':"+key+"'}";
	return key;
}

public static String jsBuilderNew(String caption,String dataKey) {
	
	StringBuilder stringbuilder=new StringBuilder();
	if(caption!=null) {
		if(!caption.equals("")) {
			stringbuilder.append("'caption':'"+caption+"'");
			
		}
		
		
	}
	
	if(dataKey!=null) {
		if(stringbuilder.length()!=0) {
			stringbuilder.append("'");
		}
		stringbuilder.append("'dataKey':'"+dataKey+"'");
	}
	caption="{" + stringbuilder + "}";
	System.out.println(caption);
	return caption; 
}


public static String jsBuilderBasedOnLabelName(String labelName,String value) {
	StringBuilder stringbuilder=new StringBuilder();
	switch(labelName) {
	case "caption":
		if(labelName!=null) {
			stringbuilder.append("'caption':'" +value+"'");
			
		}
		
		break;
	case "dataKey":
		if(labelName!=null) {
			stringbuilder.append("'dataKey':'" +value+"'");
			break;
		}
	case "gridColumnName":
		if(labelName!=null) {
			stringbuilder.append("'gridColumnName':'" +value+"'");
			break;
			
		}
		
	case "cellValue":
		if(labelName!=null) {
			stringbuilder.append("'cellValue':'" +value+"'");
			break;
			
		}
		
		
		
	}
	return labelName;
}

public static String buildCriteriaForGrid(String key) {
	
	try {
		SqliteConnection sql=new SqliteConnection();
		String query="Select * from GridData where column Name='TaskName' AND ObjectKey='"+key+"'";
		ResultSet rst=sql.GetDataSet(query);
		if(rst.getString("Value")!=null) {
			
			key="cellValue:'"  + rst.getString("Value")+"'";
		}
		else {
			throw new Exception("No value found in DB for key" +key);
		}
		
		
	}
	catch(Exception e) {
		assertTrue(e.getMessage(),false);
	}
	
	return key;
	
}


public static String buildcriteria(String key) throws Exception{
	StringBuilder stringbuilder=new StringBuilder();
	SqliteConnection sql=new SqliteConnection();
	String sqlsquery="Select * from GridData where ObjectKey='"+key+"'";
	ResultSet rst=sql.GetDataSet(sqlsquery);
	if(!rst.next()) {
		throw new Exception("Control informaion new found for key"+key);
		
	}
	else if 
		(rst.getString("metaDataUrl")!=null){
			stringbuilder.append("metaDataUrl:'"+rst.getString("metaDataUrl")+"'");
			
		}
	else if 
	(rst.getString("metaDataUrl")!=null){
		stringbuilder.append("metaDataUrl:'"+rst.getString("metaDataUrl")+"'");
		
	}else if 
	(rst.getString("Type")!=null){
		if(stringbuilder.length()!=0){
			stringbuilder.append(",");
			
		}
		stringbuilder.append("metaDataUrl:'"+rst.getString("metaDataUrl")+"'");
		
	}
		
		return key;
}




public boolean IsMandatory(String fieldName) {
	// TODO Auto-generated method stub
	return false;
}




public boolean IsDisabled(String fieldName) {
	// TODO Auto-generated method stub
	return false;
}




public boolean IsEnabled(String fieldName) {
	// TODO Auto-generated method stub
	return false;
}




public void clear(String fieldName) {
	// TODO Auto-generated method stub
	
}




public Object performAndReturn(WebDriver driver2, String string) {
	// TODO Auto-generated method stub
	return null;
}




public String performJsAndReturn(WebDriver driver2, String jsArg1) {
	// TODO Auto-generated method stub
	return null;
}


public  String jsBuilderName(String jsArg1) {
	// TODO Auto-generated method stub
	return null;
}


}
