#Test Case: TC7_CLM_CIB_TC_162
#User Story ID: SCN_RR_084
#Designed by: Vibhav Kumar  TC7_CLM_CIB_TC_162_RR Very High Risk
#Last Edited by: Vibhav Kumar
Feature: Test

  @Automation
  Scenario: To verify whether Risk gets calculated properly for High risk and is overridden
    Given I login to Fenergo Application with "RM:IBG-GOVT ENTITY"
    When I complete "NewRequest" screen with key "SCN_CRA_084"
	   When I complete "Product" screen with key "CallAccount"
	   When I complete "Relationship" screen with key "C1"
    And I complete "CaptureNewRequest" screen with key "SCN_CRA_077"
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "SCN_CRA_084"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "SCN_CRA_084"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Individual"
    When I complete "AssociationDetails" screen with key "DirectorIndividual"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    And I navigate to "Navigate to Legal Entity" screen of the added "NonIndividual" AssociatedParty
    When I complete "LEDetailsNonIndividual" screen with key "SCN_CRA_084"
    When I search for the "CaseId"
    When I navigate to "CompleteAMLGrid" task
    And I navigate to "Navigate to Legal Entity" screen of the added "Individual" AssociatedParty
    When I complete "LEDetailsIndividual" screen with key "SCN_CRA_084"
    When I search for the "CaseId"
    When I navigate to "CompleteAMLGrid" task
		When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "SCN_CRA_084" 
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    And I verify the populated risk rating is "Medium-Low"
    When I complete "RiskAssessment" task 
    
   	Then I login to Fenergo Application with "RM:IBG-GOVT ENTITY" 
		When I search for the "CaseId"
		Then I store the "CaseId" from LE360 
		When I navigate to "ReviewSignOffGrid" task 
		When I complete "ReviewSignOff" task 
	
	   Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "BUH:IBG-GOVT ENTITY" 
     When I search for the "CaseId" 
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
		 Then I login to Fenergo Application with "KYCMaker: Corporate" 
     When I search for the "CaseId" 
     Then I store the "CaseId" from LE360 
     And I complete "Waiting for UID from GLCMS" task from Actions button 
     When I navigate to "CaptureFabReferencesGrid" task 
     When I complete "CaptureFABReferences" task 
 		
 		#Comment : Initiate Regular Review     
		And I initiate "Regular Review" from action button 
	
		When I complete "CloseAssociatedCase" task 
		When I navigate to "ValidateKYCandRegulatoryGrid" task 
		Then I store the "CaseId" from LE360 
		When I complete "ValidateKYC" screen with key "SCN_CRA_077" 
		And I click on "SaveandCompleteforValidateKYC" button 
		When I navigate to "ReviewRequestGrid" task 
		When I complete "RRReviewRequest" task 
		
		When I navigate to "Review/EditClientDataTask" task 
		When I add a "AnticipatedTransactionActivity" from "Review/EditClientData" 
		When I complete "EnrichKYC" screen with key "RegularReviewClientaData" 
		And I click on "SaveandCompleteforEnrichKYC" button 
		When I navigate to "KYCDocumentRequirementsGrid" task 
		When I add a "DocumentUpload" in KYCDocument 
		Then I complete "KYCDocumentRequirements" task 
		
		When I navigate to "CompleteAMLGrid" task 
		When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
		Then I complete "CompleteAML" task 
		
		When I navigate to "CompleteID&VGrid" task  
		When I complete "CompleteID&V" task 
		
		When I navigate to "CompleteRiskAssessmentGrid" task 
		Then I verify the populated risk rating is "Very High"
		When I complete "RiskAssessment" task 
		
     Then I login to Fenergo Application with "BUH:IBG-GOVT ENTITY" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360 
     When I navigate to "ReviewSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - VP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "SVP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverSVPReviewGrid" task 
     When I complete "ReviewSignOff" task
  
     Then I login to Fenergo Application with "Group Compliance (CDD)" 
     When I search for the "CaseId" 
     When I navigate to "GroupComplianceReviewGrid" task 
     When I complete "ReviewSignOff" task
  
     Then I login to Fenergo Application with "BUH:IBG-GOVT ENTITY" 
     When I search for the "CaseId" 
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "BH:Corporate" 
     When I search for the "CaseId" 
     When I navigate to "BHReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task
  
