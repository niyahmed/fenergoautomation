#Test Case: SCN_CRA_122 
#User Story ID: 
#Designed by: Vibhav Kumar	
#Last Edited by: Vibhav Kumar
Feature: SCN_CRA_122

 @Automation
  Scenario: Verify the overall risk rating is auto overridden to Very High from WAC Risk rating (LOW) when Country of Incorporation/Establishment score is 5 and override is true  
    #Given I login to Fenergo Application with "RM:IBG-DNE" 
#	When I complete "NewRequest" screen with key "SCN_CRA_122" 
#	When I complete "Product" screen with key "LoanAgainstShares" 
#	When I complete "Product" screen with key "Fixed/Time/SmartDeposits" 
#	When I complete "Product" screen with key "CorporateLoan"  
  #And I complete "CaptureNewRequest" with Key "SCN_CRA_122" and below data 
#		| Product | Relationship |
#		| C1      | C1           |
#	And I click on "Continue" button 
#	When I complete "ReviewRequest" task 
#	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "8964"
	Then I store the "CaseId" from LE360  
#	When I navigate to "ValidateKYCandRegulatoryGrid" task 
#	When I complete "ValidateKYC" screen with key "SCN_CRA_122" 
#	And I click on "SaveandCompleteforValidateKYC" button 
#	
#	When I navigate to "EnrichKYCProfileGrid" task 
   #When I complete "AddAddressFAB" task 
#	When I complete "EnrichKYC" screen with key "SCN_CRA_122" 
#	And I click on "SaveandCompleteforEnrichKYC" button 
#	
#	When I navigate to "CaptureHierarchyDetailsGrid" task 
#	When I add AssociatedParty by right clicking 
#	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
#	When I complete "AssociationDetails" screen with key "UBO_NoSignificantControl" 
#	When I add AssociatedParty by right clicking 
#	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
#	When I complete "AssociationDetails" screen with key "CEOIndividual" 
#	When I complete "CaptureHierarchyDetails" task 
 #
#	
#	When I navigate to "KYCDocumentRequirementsGrid" task  
#	When I add a "DocumentUpload" in KYCDocument 
#	Then I complete "KYCDocumentRequirements" task 
#	
#	When I navigate to "CompleteAMLGrid" task 
#	And I navigate to "Navigate to Legal Entity" screen of the added "NonIndividual" AssociatedParty 
#	When I complete "LEDetailsNonIndividual" screen with key "SCN_CRA_122" 
#	When I search for the "CaseId" 
#	
#	When I navigate to "CompleteAMLGrid" task 
#	And I navigate to "Navigate to Legal Entity" screen of the added "Individual" AssociatedParty 
#	When I complete "LEDetailsIndividual" screen with key "SCN_CRA_122" 
#	When I search for the "CaseId" 
#	
#	When I navigate to "CompleteAMLGrid" task 
#	When I Initiate "Fircosoft" by rightclicking 
#	When I Initiate "Fircosoft" by rightclicking for "2" associated party 
#	And I complete "Fircosoft" from assessment grid with Key "SCN_CRA_122"
#	Then I complete "CompleteAML" task 
#	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task
	And I verify the overridden risk rating is "Very High"
	 And I validate individual risk rating of labels as given below
      | LabelName                                           | RiskRating | 
      | Country of Incorporation / Establishment:           | Low        | 
      | Country of Domicile / Physical Presence:            | Medium     | 
      | Countries of Business Operations/Economic Activity: | Medium-Low | 
      | Association Country Risk                            | Medium     | 
      | Legal Entity Type:                                  | Low 				| 
      | Types of Shares (Bearer/Registered):                | -          | 
      | Length of Relationship:                             | Medium     | 
      | Industry (Primary/Secondary):                       | Very High  | 
      | Main Entity / Association Screening Risk            | Medium     | 
      | Product Type:                                       | Medium     | 
      | Anticipated Transactions Turnover (Annual in AED):  | Medium     | 
      | Channel & Interface:                                | Low        | 


	

	