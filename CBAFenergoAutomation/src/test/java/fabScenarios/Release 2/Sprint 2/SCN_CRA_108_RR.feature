#Test Case: SCN_CRA_108_RR
#PBI: R1S2EPIC003PBI100 
#User Story ID: 
#Designed by: Sanjeet Singh
#Last Edited by: Sanjeet Singh
Feature: SCN_CRA_108_RR

@Automation 
Scenario: 
	Verify the overall risk rating is auto overridden to Very High from WAC Risk rating (LOW) when Country of Incorporation/Establishment score is 5 and override is true 
	Given I login to Fenergo Application with "SuperUser" 
	When I complete "NewRequest" screen with key "SCN_CRA_108" 
	When I complete "Product" screen with key "LoanAgainstShares" 
	When I complete "Product" screen with key "TrustAccount" 
	When I complete "Product" screen with key "CorporateLoan" 
	When I complete "Product" screen with key "LiquidityManagementServices" 
	And I store the "CaseId" from LE360 
	And I complete "CaptureNewRequest" with Key "SCN_CRA_108" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "SCN_CRA_108" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "SCN_CRA_108" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "CIO" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	And I store the "CaseId" from LE360 
	And I navigate to "Navigate to Legal Entity" screen of the added "NonIndividual" AssociatedParty 
	When I complete "LEDetailsNonIndividual" screen with key "SCN_CRA_108" 
	When I search for the "CaseId" 
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "Individual" AssociatedParty 
	When I complete "LEDetailsIndividual" screen with key "SCN_CRA_108" 
	When I search for the "CaseId" 
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	When I Initiate "Fircosoft" by rightclicking for "2" associated party 
	And I complete "Fircosoft" from assessment grid with Key "SCN_CRA_108" 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	And I validate that the RiskCategory is "Very High" 
	And I validate individual risk rating of labels as given below 
		| LabelName                                           | RiskRating | 
		| Country of Incorporation / Establishment:           | Very High      | 
		| Country of Domicile / Physical Presence:            | Medium-Low      | 
		| Countries of Business Operations/Economic Activity: | Medium       | 
		| Legal Entity Type:                                  | Medium       | 
		| Types of Shares (Bearer/Registered):                | -          | 
		| Length of Relationship:                             | Medium-Low    | 	
		| Industry (Primary/Secondary):                       |  Medium-Low    | 
		| Main Entity / Association Screening Risk            |Medium-Low     | 
		| Product Type:                                       | Very High | 
		| Anticipated Transactions Turnover (Annual in AED):  | Medium| 
		| Channel & Interface:                                |High      | 
   When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	And I complete "Waiting for UID from GLCMS" task from Actions button 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferencesCIFId" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed"   
	And I initiate "Regular Review" from action button 
	When I complete "CloseAssociatedCase" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "SCN_CRA_108" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	When I complete "RRReviewRequest" task 
	
	When I navigate to "Review/EditClientDataTask" task 
	When I complete "EnrichKYC" screen with key "SCN_CRA_108" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
		When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	When I Initiate "Fircosoft" by rightclicking for "2" associated party 
	And I complete "Fircosoft" from assessment grid with Key "SCN_CRA_108" 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CompleteRiskAssessmentGrid" task 
	And I validate that the RiskCategory is "Very High" 
	And I validate individual risk rating of labels as given below 
		| LabelName                                           | RiskRating | 
		| Country of Incorporation / Establishment:           | Very High      | 
		| Country of Domicile / Physical Presence:            | Medium-Low      | 
		| Countries of Business Operations/Economic Activity: | Medium       | 
		| Legal Entity Type:                                  | Medium       | 
		| Types of Shares (Bearer/Registered):                | -          | 
		| Length of Relationship:                             | Medium-Low    | 	
		| Industry (Primary/Secondary):                       |  Medium-Low    | 
		| Main Entity / Association Screening Risk            |Medium-Low     | 
		| Product Type:                                       | Very High | 
		| Anticipated Transactions Turnover (Annual in AED):  | Medium| 
		| Channel & Interface:                                |High      | 
	When I complete "RiskAssessment" task 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 

	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	And I complete "Publish to GLCMS" task from Actions button
		
		
		
		