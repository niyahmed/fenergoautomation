#Test Case: SCN_CRA_125 
#User Story ID: 
#Designed by: Vibhav Kumar	
#Last Edited by: Vibhav Kumar
Feature: SCN_CRA_125

 @Automation
  Scenario: Verify the overall risk rating is auto overridden to Very High from WAC Risk rating (LOW) when Country of Incorporation/Establishment score is 5 and override is true  
    Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "SCN_CRA_125" 
	When I complete "Product" screen with key "FundsFABFundsMutualFunds"  
  And I complete "CaptureNewRequest" with Key "SCN_CRA_125" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId"
	Then I store the "CaseId" from LE360  
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "SCN_CRA_125" 
	And I click on "SaveandCompleteforValidateKYC" button 
	
	When I navigate to "EnrichKYCProfileGrid" task 
   When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "SCN_CRA_125" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Settlor" 
	
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "IndividualTrustee" 
	When I complete "CaptureHierarchyDetails" task 
	
	When I navigate to "KYCDocumentRequirementsGrid" task  
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "NonIndividual" AssociatedParty 
	When I complete "LEDetailsNonIndividual" screen with key "SCN_CRA_125" 
	When I search for the "CaseId" 
	
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "Individual" AssociatedParty 
	When I complete "LEDetailsIndividual" screen with key "SCN_CRA_125" 
	When I search for the "CaseId" 
 
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "SCN_CRA_125" 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task
	And I verify the populated risk rating is "Low"
	
    Then I login to Fenergo Application with "SuperUser"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "CompleteRiskAssessmentGrid" task
	 And I validate individual risk rating of labels as given below
      | LabelName                                           | RiskRating | 
      | Country of Incorporation / Establishment:           | Medium     | 
      | Country of Domicile / Physical Presence:            | Medium-Low | 
      | Countries of Business Operations/Economic Activity: | Medium-Low | 
      | Association Country Risk                            | High       | 
      | Legal Entity Type:                                  | High       | 
      | Types of Shares (Bearer/Registered):                | -          | 
      | Length of Relationship:                             | Medium     | 
      | Industry (Primary/Secondary):                       | Medium-Low | 
      | Main Entity / Association Screening Risk            | Medium     | 
      | Product Type:                                       | Low        | 
      | Anticipated Transactions Turnover (Annual in AED):  | Low        | 
      | Channel & Interface:                                | Low        | 
      
   	When I complete "RiskAssessment" task 
	
#	Then I login to Fenergo Application with "RM:IBG-DNE" 
#	When I search for the "CaseId" 
#	Then I store the "CaseId" from LE360 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
#	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
#	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
#	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
#	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
#	Then I login to Fenergo Application with "BUH:IBG-DNE" 
#	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
#	Then I login to Fenergo Application with "KYCMaker: Corporate" 
#	When I search for the "CaseId" 
#	Then I store the "CaseId" from LE360 
	And I complete "Waiting for UID from GLCMS" task from Actions button 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferencesCIFId" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed"   
	
	And I initiate "Regular Review" from action button 
	When I complete "CloseAssociatedCase" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	Then I store the "CaseId" from LE360 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	When I complete "RRReviewRequest" task 
	
	When I navigate to "Review/EditClientDataTask" task 
	When I complete "EnrichKYC" screen with key "RegularReviewClientaDataSanity" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 

	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "SCN_CRA_125" 
	Then I complete "CompleteAML" task 
		
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CompleteRiskAssessmentGrid" task 
		 And I validate individual risk rating of labels as given below
      | LabelName                                           | RiskRating | 
      | Country of Incorporation / Establishment:           | Medium     | 
      | Country of Domicile / Physical Presence:            | Medium-Low | 
      | Countries of Business Operations/Economic Activity: | Medium-Low | 
      | Association Country Risk                            | High       | 
      | Legal Entity Type:                                  | High       | 
      | Types of Shares (Bearer/Registered):                | -          | 
      | Length of Relationship:                             | Medium     | 
      | Industry (Primary/Secondary):                       | Medium-Low | 
      | Main Entity / Association Screening Risk            | Medium     | 
      | Product Type:                                       | Low        | 
      | Anticipated Transactions Turnover (Annual in AED):  | Low        | 
      | Channel & Interface:                                | Low        | 
	And I verify the populated risk rating is "Low" 
	When I complete "RiskAssessment" task 
	
#	Then I login to Fenergo Application with "RM:IBG-DNE" 
#	When I search for the "CaseId" 
#	Then I store the "CaseId" from LE360 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
#	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
#	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	
#	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
#	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
#	Then I login to Fenergo Application with "BUH:IBG-DNE" 
#	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	And I complete "Publish to GLCMS" task from Actions button


	

	