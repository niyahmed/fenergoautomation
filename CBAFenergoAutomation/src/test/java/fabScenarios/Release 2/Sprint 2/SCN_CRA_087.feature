#Test Case: TC1_CLM_CIB_TC_087
#PBI: R1S2EPIC003PBI100 
#User Story ID: 
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC1_CLM_CIB_TC_087

 @Automation
  Scenario: Verify the overall risk rating is auto overridden to Very High from WAC Risk rating (LOW) when Country of Incorporation/Establishment score is 5 and override is true  
    Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "SCN_CRA_087" 
	When I complete "Product" screen with key "Credit Card" 
	When I complete "Product" screen with key "CapitalMarketAdvisory" 
	When I complete "Product" screen with key "ElectronicBanking"
     And I complete "CaptureNewRequest" with Key "SCN_CRA_087" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "5791"
	Then I store the "CaseId" from LE360  
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "SCN_CRA_087" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
   When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "SCN_CRA_087" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "DirectorIndividual" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
###	##	Then I compare the list of documents should be same as "COBDocument" for ClientType "Corporate" 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "NonIndividual" AssociatedParty 
	When I complete "LEDetailsNonIndividual" screen with key "SCN_CRA_087" 
	When I search for the "CaseId" 
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "Individual" AssociatedParty 
	When I complete "LEDetailsIndividual" screen with key "SCN_CRA_087" 
	When I search for the "CaseId" 
 When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Google" by rightclicking 
	When I Initiate "Google" by rightclicking for "2" associated party 
	And I complete "Google" from assessment grid with Key "SCN_CRA_087" 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task
	And I validate that the RiskCategory is "High"
	 And I validate individual risk rating of labels as given below
      | LabelName                                           | RiskRating | 
      | Country of Incorporation / Establishment:           | Low        | 
      | Country of Domicile / Physical Presence:            | Low        | 
      | Countries of Business Operations/Economic Activity: | Low        | 
      | Association Country Risk                            | Low        | 
      | Legal Entity Type:                                  | High       | 
      | Types of Shares (Bearer/Registered):                | -          | 
      | Length of Relationship:                             | Medium     | 
      | Industry (Primary/Secondary):                       | Medium     | 
      | Main Entity / Association Screening Risk            | Low        | 
      | Product Type:                                       | Medium     | 
      | Anticipated Transactions Turnover (Annual in AED):  | Medium-Low | 
      | Channel & Interface:                                | Low        | 

#	
#	
#	
#	
#	
	
	
	
	
	
	
	
	
	
	
	
	
	