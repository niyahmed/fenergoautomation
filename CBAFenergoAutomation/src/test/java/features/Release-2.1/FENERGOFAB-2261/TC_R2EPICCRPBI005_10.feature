#Test Case: TC_R2EPICCRPBI005_10
#PBI: R2EPICCRPBI005
#AC ID: AC-1,AC-3,AC-4,AC-6,AC-7,AC-8,AC-9,AC-10,AC-15,AC-21
#User Story ID: GLCMS)_INT_001,GLCMS)_INT_005,GLCMS)_INT_006,GLCMS)_INT_007,GLCMS)_INT_008,GLCMS)_INT_014
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R2EPICCRPBI005_10

  Scenario: RR:Verify the field behaviour in "ReviewRequestDetails" and "Review Edit Client" screen for Client type 'PCG'
  #Verify the LOVs for "Sector Description" field on "ReviewRequestDetails" screen
  #Verify "Contacts" subflow should be mandatory and "Contacts" panel/ GRID should remain expanded, on ReviewRequestDetails screen 
  #Verify field "Primary Phone number" should be mandatory  on screen " Contact Details" in "Contacts" subflow and value "Mobile" should be removed from  "Primary Phone Number" drop down
  #Verify the error message"At 	least 1 Primary Contact details is mandatory" should appear on "ReviewRequestDetails" screen when user has not added/ entered a primary contact 
  #Verify "Continue" button should be disabled on "ReviewRequestDetails" screen if a primary contact is not added/ entered by the user
  #Verify the error message "Atleast 1 Primary Relationship details is mandatory" should appear on "Review Request Details" screen when  a "primary relationship" is not added/ entered by the user
  #Verify "Continue" button should be disabled on "Review Request Details" screen if a "primary Relationship" is not added/ entered by the user
  #Verify field "Is this the primary relationship for this legal entity?" is visible on screen "Add Relationship" in "Relationship" subflow if user selects "Relationship Type" as "Relationship Manager"
  #Verify field "Is this the primary relationship for this legal entity?" is not visible on screen "Add Relationship" in "Relationship" subflow if  user selects "Relationship Type" other than"Relationship Manager"
  #Verify the error message "Group Relationship Manager already exists" should appear on screen if user  attempts to add multiple Group Relationship Manager details   
  #Verify the 'Trading/Operation Name' field behavior in Review Edit Client screen
  #Verify the 'Legal Counterparty Type' field behavior in Review Edit Client screen
  # Verify the "Address Line 2" field behavior in Address subflow under "Review Edit Client" screen
  ##########################################################################################################################
    
    Given I login to Fenergo Application with "RM:PCG"
    When I complete "NewRequest" screen with key "PCG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: PCG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "Preliminary Tax Assessment" task
    When I complete the "Preliminary Tax Assessment" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    When I login to Fenergo Application with "RM:PCG"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:PCG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: PCG"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
    # Initiate RR workflow
     And I initiate "Regular Review" from action button
    When I navigate to "ClosedAssociatedCases" task
    When I complete the "ClosedAssociatedCases" task   
    When I navigate to "ValidateKYCandRegulatoryData" task
    When I complete "ValidateKYCandRegulatoryData" task  
    When I navigate to "ReviewRequestDetails" task
    #Review Request Details screen
     #verify field behavior for the field  "Sector Description"
     And I validate the following fields in "Customer Details" section
      | Label              | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Sector Description | Dropdown     | Yes     | Yes      | Yes       |  Select...        |
     #Verify the LOVs of  "Sector Description" field 
     And I validate the Lovs for "Sector Description" field 
     #Please refer "LOV" tab in PBI 
     When I navigate to "Relationship" subflow
     #Add Relationship screen
     #Verify the error message "Atleast 1 Primary Relationship details is mandatory" should appear on "Review Request Details" screen when  a "primary relationship" is not added/ entered by the user
     And I select a user from "Expand Filters" section and click on "Cancel" buton
     #Review Request Details screen
     And I validate the error message "Atleast 1 Primary Relationship details is mandatory" is appearing on screen
     #Verify "Continue" button should be disabled on "Review Request Details" screen if a "primary Relationship" is not added/ entered by the user
     And I validate "Continue" button is disabled 
     When I navigate to "Relationship" subflow
     #Verify field "Is this the primary relationship for this legal entity?" is visible on screen "Add Relationship"  if user selects "Relationship Type" as "Relationship Manager"
     And I select "Relationship Manager"" for "Relationship Type" field
     And I validate "Is this the primary relationship for this legal entity?" is visible on screen
     #Verify field "Is this the primary relationship for this legal entity?" is not visible on screen "Add Relationship" if  user selects "Relationship Type" other than"Relationship Manager"
     And I select "Account Manager" for "Relationship Type" field
     And I validate "Is this the primary relationship for this legal entity?" is not visible on screen
     #Verify the error message "Group Relationship Manager already exists"is appearing on screen if user  attempts to add multiple Group Relationship Manager details   
     And I select "Group Relationship Manager" for "Relationship Type" field and click on "SAVE AND ADD ANOTHER" button
     And I select "Group Relationship Manager" for "Relationship Type" field and click on "SAVE" button
     And I validate the error message "Group Relationship Manager already exists"is appearing on screen  
     When I add a "Primary Relationship" in Relationship  
     #Verify "Contacts" subflow should be mandatory and "Contacts" panel/ GRID should remain expanded
     And I validate "Contacts" subflow is appearing mandatory and remain expanded
     When I navigate to "Contacts" subflow
     #Add Contact screen
     #Verify field "Primary Phone number" should be mandatory  on screen "Contact Details" and value "Mobile" should be removed from  "Primary Phone Number" drop down
     And I Validate field "Primary Phone number" is appering as mandatory and value "Mobile" is not available in "Primary Phone Number" drop down
     And click on "Cancel" button
     #Review Request Details screen
     #Verify the error message"At least 1 Primary Contact details is mandatory" should appear on "Review Request Details" screen when user has not added/ entered a primary contact 
     And I validate the error message"At least 1 Primary Contact details is mandatory" is appearing  on screen
     #Verify "Continue" button should be disabled on "Review Request Details" screen if a primary contact is not added/ entered by the user
     And I validate "Continue" button is disabled 
     When I add a "Contact" in Contact     
    And I click on "Continue" button
    When I complete ReviewRequestDetails"" task
    When I navigate to "Review Edit Client" task
    #verify field behavior for the field 'Trading/Operation Name'
    #Same existing behaviour. New change is 'Trading/Operation Name' field becomes mandatory
    And I validate the following fields in "KYC Conditions" section
      | Label                  | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Trading/Operation Name | Alphanumeric | Yes     | Yes      | Yes       |                   |
    #Verify Trading/Operation Name field NOT accepts special characters. Test Data:Special characters
    And I fill the data for "enrichkycprofile" with key "Data1"
    #Verify Trading/Operation Name field NOT accepts lower case characters. Test Data: Lower case char
    And I fill the data for "enrichkycprofile" with key "Data2"
    #Verify Trading/Operation field NOT accepts Value having "_WC" at the end. Test Data: TEST_WC
    And I fill the data for "enrichkycprofile" with key "Data2"
    #Verify Trading/Operation Name field accepts only Upper case. Test Data: Upper case char
    And I fill the data for "enrichkycprofile" with key "Data3"
    #Verify the 'Legal Counterparty Type' field behavior in Enrich KYC profile screen
    And I validate the following fields in "Customer Details" section
      | Label                   | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Legal Counterparty Type | DropDown     | Yes     | Yes      | Yes       |  Select...        |
      And I validate the existing LOVs for field "Legal Counterparty Type "
          
    When I navigate to "Address" subflow
     #Address screen
     #verify field behavior for the field "Address Line 2"
     And I validate the following fields in "Addrees Information" section
      | Label          | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Address Line 2 | DropDown     | Yes     | Yes      | Yes       |                   |
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    
