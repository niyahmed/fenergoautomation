#Test Case: TC_R2EPICCRPBI005_08
#PBI: R2EPICCRPBI005
#AC ID: AC-14
#User Story ID: (GLCMS)_INT_008
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R2EPICCRPBI005_08

  Scenario: COB:Verify when multiple RM and GRM is added are different then Fenergo receives UID from GLCMS system for Client type 'BBG'
  # This test can be executed only in FAB envrionement (Dev2)
  ##########################################################################################################################
    
    Given I login to Fenergo Application with "RM:BBG"
    When I complete "NewRequest" screen with key "BBG"
    When I navigate to "Capture Request Details" screen
    #Capture Request Details screen
    When I navigate to "Relationship Subflow" 
    And I select a user from "Expand Filters" section and click on "Select" buton
    And I select "Relationship Manager" for "Relationship Type" field  #Test Data: Vidhya Damodharan
    And I fill the data for "Coverage Geography" field
    And I Select "Active" for "Relationship Status" field
    And I Check the checkbox against "Is this the primary relationship for this legal entity?" field
    And I click on "Save and Add another" button
    And I select a user from "Expand Filters" section and click on "Select" buton
    And I select "Relationship Manager" for "Relationship Type" field #Test Data: Danil Bekrar
    And I fill the data for "Coverage Geography" field
    And I Select "Active" for "Relationship Status" field
    And I click on "Save and Add another" button
    And I select a user from "Expand Filters" section and click on "Select" buton
    And I select "Group Relationship Manager" for "Relationship Type" field #Test Data:Firose Khan
    And I fill the data for "Coverage Geography" field
    And I Select "Active" for "Relationship Status" field
    And I click on "Save" button
    And I complete "CaptureNewRequest" task
    And I click on "Continue" button
    And I complete "CaptureNewRequest" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "Preliminary Tax Assessment" task
    When I complete the "Preliminary Tax Assessment" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    When I login to Fenergo Application with "RM:BBG"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:BBG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    When I navigate to "Waiting for UID from GLCMS" task
    #Verify when multiple RM and GRM is added are different then Fenergo receives UID from GLCMS system
    And I validate Fenergo receives UID from GLCMS system
    When I navigate to 'CaptureFABreferennces' task  
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
    