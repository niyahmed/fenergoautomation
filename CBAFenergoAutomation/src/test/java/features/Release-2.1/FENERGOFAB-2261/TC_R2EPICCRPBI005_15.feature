#Test Case: TC_R2EPICCRPBI005_15
#PBI: R2EPICCRPBI005
#AC ID: AC-1,AC-3,AC-4,AC-6,AC-7,AC-8,AC-9,AC-10,AC-15,AC-21
#User Story ID: GLCMS)_INT_001,GLCMS)_INT_005,GLCMS)_INT_006,GLCMS)_INT_007,GLCMS)_INT_008,GLCMS)_INT_014
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R2EPICCRPBI005_15

  Scenario: COB:Verify the field behaviour in "Capture Request Details" and "Enrich KYC Profile" screen for Client type 'NBFI'
  #Verify the LOVs for "Sector Description" field on "Capture Request details" screen
  #Verify "Contacts" subflow should be mandatory and "Contacts" panel/ GRID should remain expanded, on Capture Request details screen 
  #Verify field "Primary Phone number" should be mandatory  on screen " Contact Details" in "Contacts" subflow and value "Mobile" should be removed from  "Primary Phone Number" drop down
  #Verify the error message"At 	least 1 Primary Contact details is mandatory" should appear on "Capture Request Details" screen when user has not added/ entered a primary contact 
  #Verify "Continue" button should be disabled on "Capture Request Details" screen if a primary contact is not added/ entered by the user
  #Verify the error message "Atleast 1 Primary Relationship details is mandatory" should appear on "Capture Request Details" screen when  a "primary relationship" is not added/ entered by the user
  #Verify "Continue" button should be disabled on "Capture Request Details" screen if a "primary Relationship" is not added/ entered by the user
  #Verify field "Is this the primary relationship for this legal entity?" is visible on screen "Add Relationship" in "Relationship" subflow if user selects "Relationship Type" as "Relationship Manager"
  #Verify field "Is this the primary relationship for this legal entity?" is not visible on screen "Add Relationship" in "Relationship" subflow if  user selects "Relationship Type" other than"Relationship Manager"
  #Verify the error message "Group Relationship Manager already exists" should appear on screen if user  attempts to add multiple Group Relationship Manager details   
  #Verify the 'Trading/Operation Name' field behavior in Enrich KYC profile screen
  #Verify the 'Legal Counterparty Type' field behavior in Enrich KYC profile screen
  # Verify the "Address Line 2" field behavior in Address subflow under "Enrich KYC Profile" screen
  ##########################################################################################################################
    Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "NBFI"
    When I navigate to "Capture Request" task
    #Capture Request Details screen
     #verify field behavior for the field  "Sector Description"
     And I validate the following fields in "Customer Details" section
      | Label              | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Sector Description | Dropdown     | Yes     | Yes      | Yes       |  Select...        |
     #Verify the LOVs of  "Sector Description" field 
     And I validate the Lovs for "Sector Description" field 
     #Please refer "LOV" tab in PBI 
     When I navigate to "Relationship" subflow
     #Add Relationship screen
     #Verify the error message "Atleast 1 Primary Relationship details is mandatory" should appear on "Capture Request Details" screen when  a "primary relationship" is not added/ entered by the user
     And I select a user from "Expand Filters" section and click on "Cancel" buton
     #Capture Request Details screen
     And I validate the error message "Atleast 1 Primary Relationship details is mandatory" is appearing on screen
     #Verify "Continue" button should be disabled on "Capture Request Details" screen if a "primary Relationship" is not added/ entered by the user
     And I validate "Continue" button is disabled
     When I navigate to "Relationship" subflow
     #Verify field "Is this the primary relationship for this legal entity?" is visible on screen "Add Relationship"  if user selects "Relationship Type" as "Relationship Manager"
     And I select "Relationship Manager"" for "Relationship Type" field
     And I validate "Is this the primary relationship for this legal entity?" is visible on screen
     #Verify field "Is this the primary relationship for this legal entity?" is not visible on screen "Add Relationship" if  user selects "Relationship Type" other than"Relationship Manager"
     And I select "Account Manager" for "Relationship Type" field
     And I validate "Is this the primary relationship for this legal entity?" is not visible on screen
     #Verify the error message "Group Relationship Manager already exists"is appearing on screen if user  attempts to add multiple Group Relationship Manager details   
     And I select "Group Relationship Manager" for "Relationship Type" field and click on "SAVE AND ADD ANOTHER" button
     And I select "Group Relationship Manager" for "Relationship Type" field and click on "SAVE" button
     And I validate the error message "Group Relationship Manager already exists"is appearing on screen    
     When I add a "Primary Relationship" in Relationship
     #Verify "Contacts" subflow should be mandatory and "Contacts" panel/ GRID should remain expanded
     And I validate "Contacts" subflow is appearing mandatory and remain expanded
     When I navigate to "Contacts" subflow
     #Add Contact screen
     #Verify field "Primary Phone number" should be mandatory  on screen "Contact Details" and value "Mobile" should be removed from  "Primary Phone Number" drop down
     And I Validate field "Primary Phone number" is appering as mandatory and value "Mobile" is not available in "Primary Phone Number" drop down
     And click on "Cancel" button
     #Capture Request Details screen
     #Verify the error message"At least 1 Primary Contact details is mandatory" should appear on "Capture Request Details" screen when user has not added/ entered a primary contact 
     And I validate the error message"At least 1 Primary Contact details is mandatory" is appearing  on screen
     #Verify "Continue" button should be disabled on "Capture Request Details" screen if a primary contact is not added/ entered by the user
     And I validate "Continue" button is disabled 
     When I add a "Contact" in Contact     
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "SuperUser"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    #verify field behavior for the field 'Trading/Operation Name'
    #Same existing behaviour. New change is 'Trading/Operation Name' field becomes mandatory
    And I validate the following fields in "KYC Conditions" section
      | Label                  | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Trading/Operation Name | Alphanumeric | Yes     | Yes      | Yes       |                   |
    #Verify Trading/Operation Name field NOT accepts special characters. Test Data:Special characters
    And I fill the data for "enrichkycprofile" with key "Data1"
    #Verify Trading/Operation Name field NOT accepts lower case characters. Test Data: Lower case char
    And I fill the data for "enrichkycprofile" with key "Data2"
    #Verify Trading/Operation field NOT accepts Value having "_WC" at the end. Test Data: TEST_WC
    And I fill the data for "enrichkycprofile" with key "Data2"
    #Verify Trading/Operation Name field accepts only Upper case. Test Data: Upper case char
    And I fill the data for "enrichkycprofile" with key "Data3"
    #Verify the 'Legal Counterparty Type' field behavior in Enrich KYC profile screen
    And I validate the following fields in "Customer Details" section
      | Label                   | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Legal Counterparty Type | DropDown     | Yes     | Yes      | Yes       |  Select...        |
      And I validate the existing LOVs for field "Legal Counterparty Type "
          
    When I navigate to "Address" subflow
     #Address screen
     #verify field behavior for the field "Address Line 2"
     And I validate the following fields in "Addrees Information" section
      | Label          | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Address Line 2 | DropDown     | Yes     | Yes      | Yes       |                   |
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    
