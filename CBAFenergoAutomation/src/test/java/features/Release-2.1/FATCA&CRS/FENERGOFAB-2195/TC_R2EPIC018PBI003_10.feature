#Test Case: TC_R2EPIC018PBI003_10
#PBI:R2EPIC018PBI003
#User Story ID:USTAX-011a,USTAX-023
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R2EPIC018PBI003_10

  Scenario: BBG: Validate the LOVs of "Tax Form Type" field  under section "Tax Form Details" on screen "US Tax Classification" in COB workflow
    #Validate the LOVs of 'Chapter 4 FATCA Status' field for all 8  "Tax Form Details" type under  section "Chapter 4 / FATCA Details"  on screen "US Tax Classification"
    #Field behavior in Tax Form Details section (3 field should be hidden,  1 field should be renamed	) 
   #Field behavior in Chapter 4 / FATCA Details section ( 1 new field should be added)  
	##########################################################################################################################
   #PreCondition: Create entity with client type as PCG and confidential as PCG.
   #############################################################################################################
    Given I login to Fenergo Application with "RM:BBG"
    When I complete "NewRequest" screen with key "BBG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "Preliminary Tax Assessment" task
    And I select "Yes" for the field "Do you have a US Tax Form from the client?"
    When I complete the "Preliminary Tax Assessment" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
   When I navigate to "US Tax Classification " task
   #"US Tax Classification"Screen
   #Validate below LOVs for field "Tax Form Type" under section "Tax Form Details"
  And I Validate  below values are available 
  |Tax Form Type                            |
  |CRS & FATCA Self-Certification Form      |
  |No Document Provided                     |
  |W-8BEN                                   |
  |W-8BEN-E                                 |
  |W-8ECI                                   |
  |W-8EXP                                   |
  |W-8IMY                                   |
  |W-9                                      |
  #Validate tax Form Type 'Self Certification' should be renamed as 'CRS & FATCA Self-Certification Form' in field "Tax Form Type"
  And I validate 'Self Certification' is renamed as 'CRS & FATCA Self-Certification Form' 
  #Validate Below tax form types  should be hidden in field "Tax Form Type"
  And I Validate below tax form types are not visible in field "Tax Form Type"
  |Tax Form Type                  |
  |Account Opening Form           |
  |Alternative Documentation      |
  |Entity Tax Declaration Form    |
  
  #Validate the LOVs of 'Chapter 4 FATCA Status' field under section "Chapter 4 / FATCA Details" when 'CRS & FATCA Self-Certification Form' is selected as the 'Tax Form Type'
  And I select "CRS & FATCA Self-Certification Form" for "Dropdown" field "Tax Form Type"
  Then I verify "Tax Form Type" drop-down value  as "CRS & FATCA Self-Certification Form"
  And I validate LOVs of "Chapter 4 FATCA Status" field
   #Refer "LOVs-2" tab in PBI for LOV list (5 new values,Total LOVs(new + existing) should be 37
   
   #Validate the new value 'Trading entity (entity engaged in a trade or business where less than 50% of the NFFE's gross income in the preceding calendar year is derived from passive income AND less than 50% of the assets held by the NFFE in the preceding calendar year are assets that produce or are held for the production of passive income)'  added in the field 'Chapter 4 FATCA Status'
  And I validate above mentioned new value is present in the field 'Chapter 4 FATCA Status'
    #Validate the LOVs of 'Chapter 4 FATCA Status' field under section "Chapter 4 / FATCA Details" when 'No Document Provided' is selected as the 'Tax Form Type'
  And I select "No Document Provided" for "Dropdown" field "Tax Form Type"
  Then I verify "Tax Form Type" drop-down value  as "No Document Provided"
  And I validate LOVs of 'Chapter 4 FATCA Status' field
   #Refer "LOVs-2" tab in PBI for LOV list (1 lov should be available in the dropdown)
   
   #Validate the LOVs of 'Chapter 4 FATCA Status' field under section "Chapter 4 / FATCA Details" when 'W-8BEN' is selected as the 'Tax Form Type'
  And I select "W-8BEN" for "Dropdown" field "Tax Form Type"
  Then I verify "Tax Form Type" drop-down value  as "W-8BEN"
  And I validate LOVs of 'Chapter 4 FATCA Status' field
   #Refer "LOVs-2" tab in PBI for LOV list (1 lov should be available in the dropdown)
    
    #Validate the LOVs of 'Chapter 4 FATCA Status' field under section "Chapter 4 / FATCA Details" when 'W-8BEN-E' is selected as the 'Tax Form Type'
  And I select "W-8BEN-E" for "Dropdown" field "Tax Form Type"
  Then I verify "Tax Form Type" drop-down value  as "W-8BEN-E"
  And I validate LOVs of "Chapter 4 FATCA Status" field
   #Refer "LOVs-2" tab in PBI for LOV list (38 lov should be available in the dropdown)
   
    #Validate the LOVs of 'Chapter 4 FATCA Status' field under section "Chapter 4 / FATCA Details" when 'W-8ECI' is selected as the 'Tax Form Type'
  And I select "W-8ECI" for "Dropdown" field "Tax Form Type"
  Then I verify "Tax Form Type" drop-down value  as "W-8ECI"
  And I validate LOVs of "Chapter 4 FATCA Status" field
   #Refer "LOVs-2" tab in PBI for LOV list (34 lov should be available in the dropdown)
   
    #Validate the LOVs of 'Chapter 4 FATCA Status' field under section "Chapter 4 / FATCA Details" when 'W-8EXP' is selected as the 'Tax Form Type'
  And I select "W-8EXP" for "Dropdown" field "Tax Form Type"
  Then I verify "Tax Form Type" drop-down value  as "W-8EXP"
  And I validate LOVs of "Chapter 4 FATCA Status" field
   #Refer "LOVs-2" tab in PBI for LOV list (13 lov should be available in the dropdown)
    
    
    #Validate the LOVs of 'Chapter 4 FATCA Status' field under section "Chapter 4 / FATCA Details" when 'W-8IMY' is selected as the 'Tax Form Type'
  And I select "W-8IMY" for "Dropdown" field "Tax Form Type"
  Then I verify "Tax Form Type" drop-down value  as "W-8IMY"
  And I validate LOVs of "Chapter 4 FATCA Status" field
   #Refer "LOVs-2" tab in PBI for LOV list (27 lov should be available in the dropdown)
   
   #Validate the LOVs of 'Chapter 4 FATCA Status' field under section "Chapter 4 / FATCA Details" when 'W-9' is selected as the 'Tax Form Type'
  And I select "W-9" for "Dropdown" field "Tax Form Type"
  Then I verify "Tax Form Type" drop-down value  as "W-9"
  And I validate LOVs of "Chapter 4 FATCA Status" field
   #Refer "LOVs-2" tab in PBI for LOV list (2 lov should be available in the dropdown)
    
    
    
    
    
    
    
    
    
    
    
    