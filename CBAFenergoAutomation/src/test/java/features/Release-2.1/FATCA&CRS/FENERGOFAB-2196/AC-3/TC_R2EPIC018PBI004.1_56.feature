#Test Case: TC_FENERGOFAB-2196_USTAX-AC03_14
#PBI:FENERGOFAB-2196
#User Story ID: USTAX-015
#Designed by: Jagdev Singh
#Last Edited by: Jagdev Singh
Feature: TC_FENERGOFAB-2196_USTAX-AC03_14

@COB, @RR @LEM
@Client - Corporate,BBG,FI,NBFI,PCG-Entity

Scenario: Verify FATCA Status is not set to Default Status (Undocumented Individual, IGA Passive NFFE, U.S. Regs NPFFI), Recalcitrant Flag <> Yes
 and Recalcitrant Date <> Set to Today, when Tax Form Type = No Document Provided, Recalcitrant Date = null, Form Received Date = Greater than 60 Days ago, Tax Identifier = United States - TIN Not available. 

    Given I login to Fenergo Application with "RM:IBG-DNE" 
     When I complete "NewRequest" screen with key "Corporate" 
      And I complete "CaptureNewRequest" with Key "C1" and below data 
      | Product | Relationship | 
      | C1      | C1           | 
      And I click on "Continue" button 
     When I complete "ReviewRequest" task 
     Then I store the "CaseId" from LE360 
  
    Given I login to Fenergo Application with "KYCMaker: Corporate" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360  
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     When I complete "ValidateKYC" screen with key "C1" 
      And I click on "SaveandCompleteforValidateKYC" button 
  
     When I navigate to "EnrichKYCProfileGrid" task 
     When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
     When I complete "AddAddressFAB" task 
     Then I store the "CaseId" from LE360 
     When I complete "EnrichKYC" screen with key "C1" 
      And I click on "SaveandCompleteforEnrichKYC" button 
  
     When I navigate to "CaptureHierarchyDetailsGrid" task 
     When I add AssociatedParty by right clicking 
     When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
     When I complete "AssociationDetails" screen with key "Director" 
     When I complete "CaptureHierarchyDetails" task 
     
     When I navigate to "PreliminaryTaxAssessmentGrid" task 
     Then I select field "Do you have a US Tax Form from the client?" as Yes
     Then I complete "PreliminaryTaxAssessmentGrid" task 
     
     #Verify US TAX classification is triggered in stage-6 (Classification)
     When I navigate to case details page
     Then I Verify US Tax classification is triggered in stage-6
     Then I navigate to US Tax classification task.
     #FATCA Status is not set to Default Status (Undocumented Individual, IGA Passive NFFE, U.S. Regs NPFFI), Recalcitrant Flag <> Yes, Recalcitrant Date <> Set to Today, when Tax Form Type = No Document Provided, Recalcitrant Date = null, Form Received Date = NA, Tax Identifier = NA. 
     When Tax Form Type = No Document Provided, Recalcitrant Date = null, Form Received Date = Greater than 60 Days ago, Tax Identifier = United States - TIN Not available.
     Then I verify FATCA Status is not set to Default Status (Undocumented Individual, IGA Passive NFFE, U.S. Regs NPFFI),Recalcitrant Flag <> Yes, Recalcitrant Date <> Set to Today
     Then I complete US Tax classification task
  
     When I navigate to "KYCDocumentRequirementsGrid" task 
     When I add a "DocumentUpload" in KYCDocument 
     Then I complete "KYCDocumentRequirements" task 
    
     When I navigate to "CompleteAMLGrid" task 
     When I Initiate "Fircosoft" by rightclicking 
      And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
     Then I complete "CompleteAML" task 
  
     When I navigate to "CompleteID&VGrid" task 
      And I click on "ID&VLinkInRR" button 
     When I click on "SaveandCompleteforEditVerification" button 
     When I complete "CompleteID&V" task 
  
     When I navigate to "CompleteRiskAssessmentGrid" task 
     And I verify the populated risk rating is "Low"
     When I complete "RiskAssessment" task 
  
     Then I login to Fenergo Application with "RM:IBG-DNE" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360 
     When I navigate to "ReviewSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "BUH:IBG-DNE" 
     When I search for the "CaseId" 
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 
     
       #Legal Entity Maintenance
    
   I initiate "Maintenance Request" from action button  
	And I complete "MaintenanceRequest" screen with Area "LE Details" and LE Details Changes "Regulatory Updates" 
	Then I store the "CaseId" from LE360 
	Then I see "SelectClassifications" task is generated 
	And I navigate to "SelectClassifications" task
	Then I store the "CaseId" from LE360 
	When I select "SelectClassificationstoTrigger" as "US Tax"
	When I complete "SelectClassifications" screen
	And I click on "SaveandCompleteUpdateCustomerDetails" button 
        
     #Verify US TAX classification is triggered in stage-6 (Classification)
     When I navigate to case details page
     Then I Verify US Tax classification is triggered in stage-6
     Then I navigate to US Tax classification task.
     #FATCA Status is not set to Default Status (Undocumented Individual, IGA Passive NFFE, U.S. Regs NPFFI), Recalcitrant Flag <> Yes, Recalcitrant Date <> Set to Today, when Tax Form Type = No Document Provided, Recalcitrant Date = null, Form Received Date = NA, Tax Identifier = NA. 
     When Tax Form Type = No Document Provided, Recalcitrant Date = null, Form Received Date = Greater than 60 Days ago, Tax Identifier = United States - TIN Not available.
     Then I verify FATCA Status is not set to Default Status (Undocumented Individual, IGA Passive NFFE, U.S. Regs NPFFI),Recalcitrant Flag <> Yes, Recalcitrant Date <> Set to Today
     Then I complete US Tax classification task
  
   
 #Comment:=RegularReview
      And I initiate "Regular Review" from action button 
  
     When I complete "CloseAssociatedCase" task 
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     Then I store the "CaseId" from LE360 
     When I complete "ValidateKYC" screen with key "C1"  
      When I navigate to add Product screen
      #verify Booking Entity is Bank <> United States Branch
      Then I verify a product with Booking Entity is NOT Bank - USA Branch 
      And I click on "Continue" button 
     
      And I click on "SaveandCompleteforValidateKYC" button 
     When I navigate to "ReviewRequestGrid" task 
     When I complete "RRReviewRequest" task 
  
     When I navigate to "Review/EditClientDataTask" task 
     When I add a "AnticipatedTransactionActivity" from "Review/EditClientData" 
     When I complete "EnrichKYC" screen with key "RegularReviewClientaDataSanity" 
     And I click on "SaveandCompleteforEnrichKYC" button 
     
     When I navigate to "PreliminaryTaxAssessmentGrid" task 
     Then I select Do you have a US Tax Form from the client? as Yes
     Then I complete "PreliminaryTaxAssessmentGrid" task 
 
     When I navigate to "KYCDocumentRequirementsGrid" task 
     When I add a "DocumentUpload" in KYCDocument 
     Then I complete "KYCDocumentRequirements" task 
		
     When I navigate to "CompleteAMLGrid" task 
     When I Initiate "Fircosoft" by rightclicking 
      And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
     Then I complete "CompleteAML" task 
  
     When I navigate to "CompleteID&VGrid" task 
      And I click on "ID&VLinkInRR" button 
     When I click on "SaveandCompleteforEditVerification" button 
     When I complete "CompleteID&V" task 
  
     When I navigate to "CompleteRiskAssessmentGrid" task 
     And I verify the populated risk rating is "Low"
     When I complete "RiskAssessment" task 
     
     
     #Verify US TAX classification is triggered in stage-6 (Classification)
     When I navigate to case details page
     Then I Verify US Tax classification is triggered in stage-6
     Then I navigate to US Tax classification task.
     #FATCA Status is not set to Default Status (Undocumented Individual, IGA Passive NFFE, U.S. Regs NPFFI), Recalcitrant Flag <> Yes, Recalcitrant Date <> Set to Today, when Tax Form Type = No Document Provided, Recalcitrant Date = null, Form Received Date = NA, Tax Identifier = NA. 
     When Tax Form Type = No Document Provided, Recalcitrant Date = null, Form Received Date = Greater than 60 Days ago, Tax Identifier = United States - TIN Not available.
     Then I verify FATCA Status is not set to Default Status (Undocumented Individual, IGA Passive NFFE, U.S. Regs NPFFI),Recalcitrant Flag <> Yes, Recalcitrant Date <> Set to Today
     Then I complete US Tax classification task
  
  
     Then I login to Fenergo Application with "RM:IBG-DNE" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360 
     When I navigate to "ReviewSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "BUH:IBG-DNE" 
     When I search for the "CaseId" 
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 