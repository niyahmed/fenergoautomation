#Test Case: TC_FENERGOFAB-2196_USTAX-AC02_10
#PBI:FENERGOFAB-2196
#User Story ID: USTAX-008
#Designed by: Jagdev Singh
#Last Edited by: Jagdev Singh
Feature: TC_FENERGOFAB-2196_USTAX-AC02_10

@COB, @RR @LEM
@Client - Corporate,BBG,FI,NBFI,PCG-Entity

Scenario: Verify Treaty Claim Expiry Date field is set to Signed Date Year + 3Years, when Tax Form Type = W-8BENE, Chapter 4 FATCA Status = Excepted nonfinancial group entity, 501(c) organization, Nonprofit organization, Nonreporting IGA FFI, Sponsored FFI, Participating FFI, Active NFFE, Registered deemed-compliant FFI, Territory financial institution, Exempt retirement plans, Foreign government (including a political subdivision), government of a U.S. possession, or foreign central bank of issue, Publicly traded NFFE or NFFE affiliate of a publicly traded corporation, Reporting Model 1 FFI, Reporting Model 2 FFI, Direct reporting NFFE, Excepted territory NFFE, Restricted distributor, International organization, Sponsored direct reporting NFFE OR Excepted nonfinancial entity in liquidation or bankruptcy and US Onshore Obligations <>null

Given I login to Fenergo Application with "RM:IBG-DNE" 
     When I complete "NewRequest" screen with key "Corporate" 
      And I complete "CaptureNewRequest" with Key "C1" and below data 
      | Product | Relationship | 
      | C1      | C1           | 
      And I click on "Continue" button 
      When I complete "ReviewRequest" task 
      Then I store the "CaseId" from LE360 
  
     Given I login to Fenergo Application with "KYCMaker: Corporate" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360  
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     When I complete "ValidateKYC" screen with key "C1" 
      And I click on "SaveandCompleteforValidateKYC" button 
  
     When I navigate to "EnrichKYCProfileGrid" task 
     When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
     When I complete "AddAddressFAB" task 
     Then I store the "CaseId" from LE360 
     When I complete "EnrichKYC" screen with key "C1" 
     And I click on "SaveandCompleteforEnrichKYC" button 
  
     When I navigate to "CaptureHierarchyDetailsGrid" task 
     When I add AssociatedParty by right clicking 
     When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
     When I complete "AssociationDetails" screen with key "Director" 
     When I complete "CaptureHierarchyDetails" task 
     
     When I navigate to "PreliminaryTaxAssessmentGrid" task 
     Then I select Do you have a US Tax Form from the client? as Yes
     Then I complete "PreliminaryTaxAssessmentGrid" task 
    
     When I navigate to "KYCDocumentRequirementsGrid" task 
     When I add a "DocumentUpload" in KYCDocument 
     Then I complete "KYCDocumentRequirements" task 
  
     When I navigate to "CompleteID&VGrid" task 
     When I complete "ID&V" task 
     When I complete "EditID&V" task 
     When I complete "AddressAddition" in "Edit Verification" screen 
     When I complete "Documents" in "Edit Verification" screen 
     When I complete "TaxIdentifier" in "Edit Verification" screen 
     When I complete "LE Details" in "Edit Verification" screen 
     When I click on "SaveandCompleteforEditVerification" button 
     When I complete "CompleteID&V" task 
  
     When I navigate to "CompleteRiskAssessmentGrid" task
     And I verify the populated risk rating is "Low"
     When I complete "RiskAssessment" task 
      
  	 #Verify US TAX classification is triggered
	 When I navigate to case details page 
     When I navigate to "USTaxClassification" task 
     #Verify Treaty Claim Expiry Date field is set to Signed Date Year + 3Years
	 Then I Verify Treaty Claim Expiry Date field is set to Signed Date Year + 3Years, when Tax Form Type = W-8BENE, Chapter 4 FATCA Status = Excepted nonfinancial group entity, 501(c) organization, Nonprofit organization, Nonreporting IGA FFI, Sponsored FFI, Participating FFI, Active NFFE, Registered deemed-compliant FFI, Territory financial institution, Exempt retirement plans, Foreign government (including a political subdivision), government of a U.S. possession, or foreign central bank of issue, Publicly traded NFFE or NFFE affiliate of a publicly traded corporation, Reporting Model 1 FFI, Reporting Model 2 FFI, Direct reporting NFFE, Excepted territory NFFE, Restricted distributor, International organization, Sponsored direct reporting NFFE OR Excepted nonfinancial entity in liquidation or bankruptcy and US Onshore Obligations <>null
     Then I complete "USTaxClassification" task 
  
     Then I login to Fenergo Application with "RM:IBG-DNE" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360 
     When I navigate to "ReviewSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "BUH:IBG-DNE" 
     When I search for the "CaseId" 
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "KYCMaker: Corporate" 
     When I search for the "CaseId" 
     Then I store the "CaseId" from LE360 
      And I complete "Waiting for UID from GLCMS" task from Actions button 
     When I navigate to "CaptureFabReferencesGrid" task 
     When I complete "CaptureFABReferences" task 
      And I assert that the CaseStatus is "Closed"


   #Legal Entity Maintenance
    
   I initiate "Maintenance Request" from action button  
	And I complete "MaintenanceRequest" screen with Area "LE Details" and LE Details Changes "Regulatory Updates" 
	Then I store the "CaseId" from LE360 
	Then I see "SelectClassifications" task is generated 
	And I navigate to "SelectClassifications" task
	Then I store the "CaseId" from LE360 
	When I select "SelectClassificationstoTrigger" as "US Tax"
	When I complete "SelectClassifications" screen
	And I click on "SaveandCompleteUpdateCustomerDetails" button 
	  
    #Verify US TAX classification is triggered
	 When I navigate to case details page 
     When I navigate to "USTaxClassification" task 
     #Verify Treaty Claim Expiry Date field is set to Signed Date Year + 3Years
	 Then I Verify Treaty Claim Expiry Date field is set to Signed Date Year + 3Years, when Tax Form Type = W-8BENE, Chapter 4 FATCA Status = Excepted nonfinancial group entity, 501(c) organization, Nonprofit organization, Nonreporting IGA FFI, Sponsored FFI, Participating FFI, Active NFFE, Registered deemed-compliant FFI, Territory financial institution, Exempt retirement plans, Foreign government (including a political subdivision), government of a U.S. possession, or foreign central bank of issue, Publicly traded NFFE or NFFE affiliate of a publicly traded corporation, Reporting Model 1 FFI, Reporting Model 2 FFI, Direct reporting NFFE, Excepted territory NFFE, Restricted distributor, International organization, Sponsored direct reporting NFFE OR Excepted nonfinancial entity in liquidation or bankruptcy and US Onshore Obligations <>null
     Then I complete "USTaxClassification" task
   
 #Comment:=RegularReview
      And I initiate "Regular Review" from action button 
  
     When I complete "CloseAssociatedCase" task 
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     Then I store the "CaseId" from LE360 
     When I complete "ValidateKYC" screen with key "C1"  
      When I navigate to add Product screen
      #verify Booking Entity is Bank <> United States Branch
      Then I verify a product with Booking Entity is NOT Bank - USA Branch 
      And I click on "Continue" button 
     
      And I click on "SaveandCompleteforValidateKYC" button 
     When I navigate to "ReviewRequestGrid" task 
     When I complete "RRReviewRequest" task 
  
     When I navigate to "Review/EditClientDataTask" task 
     When I add a "AnticipatedTransactionActivity" from "Review/EditClientData" 
     When I complete "EnrichKYC" screen with key "RegularReviewClientaDataSanity" 
     And I click on "SaveandCompleteforEnrichKYC" button 
     
     When I navigate to "PreliminaryTaxAssessmentGrid" task 
     Then I select Do you have a US Tax Form from the client? as Yes
     Then I complete "PreliminaryTaxAssessmentGrid" task 
 
     When I navigate to "KYCDocumentRequirementsGrid" task 
     When I add a "DocumentUpload" in KYCDocument 
     Then I complete "KYCDocumentRequirements" task 
		
     When I navigate to "CompleteAMLGrid" task 
     When I Initiate "Fircosoft" by rightclicking 
      And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
     Then I complete "CompleteAML" task 
  
     When I navigate to "CompleteID&VGrid" task 
      And I click on "ID&VLinkInRR" button 
     When I click on "SaveandCompleteforEditVerification" button 
     When I complete "CompleteID&V" task 
  
     When I navigate to "CompleteRiskAssessmentGrid" task 
     And I verify the populated risk rating is "Low"
     When I complete "RiskAssessment" task 
     
 	 #Verify US TAX classification is triggered
	 When I navigate to case details page 
     When I navigate to "USTaxClassification" task 
     #Verify Treaty Claim Expiry Date field is set to Signed Date Year + 3Years
	 Then I Verify Treaty Claim Expiry Date field is set to Signed Date Year + 3Years, when Tax Form Type = W-8BENE, Chapter 4 FATCA Status = Excepted nonfinancial group entity, 501(c) organization, Nonprofit organization, Nonreporting IGA FFI, Sponsored FFI, Participating FFI, Active NFFE, Registered deemed-compliant FFI, Territory financial institution, Exempt retirement plans, Foreign government (including a political subdivision), government of a U.S. possession, or foreign central bank of issue, Publicly traded NFFE or NFFE affiliate of a publicly traded corporation, Reporting Model 1 FFI, Reporting Model 2 FFI, Direct reporting NFFE, Excepted territory NFFE, Restricted distributor, International organization, Sponsored direct reporting NFFE OR Excepted nonfinancial entity in liquidation or bankruptcy and US Onshore Obligations <>null
     Then I complete "USTaxClassification" task
  
     Then I login to Fenergo Application with "RM:IBG-DNE" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360 
     When I navigate to "ReviewSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "BUH:IBG-DNE" 
     When I search for the "CaseId" 
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 
       