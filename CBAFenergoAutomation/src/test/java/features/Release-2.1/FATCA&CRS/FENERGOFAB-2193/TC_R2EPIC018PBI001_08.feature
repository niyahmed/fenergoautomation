#Test Case: TC_R2EPIC018PBI001_08
#PBI:R2EPIC018PBI001
#User Story ID:PRELIM-001,PRELIM-002,PRELIM-003
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R2EPIC018PBI001_08

  Scenario: Corporate- Validate the task "Preliminary Tax Assessment " is triggered after the task "Review/Edit Client Data" in Stage 1 -Review Client Data and assign to KYC Maker role group in the RR workflow
   #Validate the field behavior in "Other US Indicia" and "US Tax Form" section on "Preliminary Tax Assessment" screen
   #############################################################################################################
    #PreCondition: Create entity with client type as PCG and confidential as PCG.
    #PreCondition: The task "Complete US Tax Classification" should not be triggered in COB workflow
  #####################################################################################################################
    Given I login to Fenergo Application with "RM:PCG"
    When I complete "NewRequest" screen with key "PCG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: PCG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "Preliminary Tax Assessment" task
    When I complete the "Preliminary Tax Assessment" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:PCG"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:PCG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: PCG"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
     # Initiate RR workflow
     And I initiate "Regular Review" from action button
 
    When I navigate to "ClosedAssociatedCases" task
    When I complete the "ClosedAssociatedCases" task
    
    When I navigate to "ValidateKYCandRegulatoryData" task
    When I complete "ValidateKYCandRegulatoryData" task
    
    When I navigate to "ReviewRequestDetails" task
    When I complete "ReviewRequestDetails" task
    
    When I navigate to "Review Client Data" task
    When I complete "Review Client Data" task
    
    # Validate the task "Preliminary Tax Assessment" is triggered after the task "Review/Edit Client Data"  and assign to KYC Maker role group in the RR workflow
    And I Validate the task "Preliminary Tax Assessment " is triggered aftre the task "Review/Edit Client Data" and assign to KYC Maker role group
    
    When I navigate to "Preliminary Tax Assessment" task
    #"Preliminary Tax Assessment" screen
    #verify field behavior for the fields "Does the client have any standing instructions to the US?,Does the client have an 'in-care of' / 'Hold mail' address that is the sole address of the entity?
    And I validate the following fields in "Other US Indicia" section
      | Label                                                                                             | Field Type     | Visible | Editable | Mandatory | Field Defaults To |
      | Does the client have any standing instructions to the US?                                         |  drop-down     | Yes     | Yes      | No        |  Select...        |
      | Does the client have an 'in-care of' / 'Hold mail' address that is the sole address of the entity?|  drop-down     | Yes     | Yes      | No        |  Select...        |
    #Verify field behavior for the fields ""Do you have a US Tax Form from the client?""
    And I validate the following fields in "US Tax Form" section
    | Label                                         | Field Type    | Visible | Editable | Mandatory | Field Defaults To |
    | "Do you have a US Tax Form from the client?"  | drop-down     | Yes     | Yes      | No        |  Select...        |
   
      
    
    
    