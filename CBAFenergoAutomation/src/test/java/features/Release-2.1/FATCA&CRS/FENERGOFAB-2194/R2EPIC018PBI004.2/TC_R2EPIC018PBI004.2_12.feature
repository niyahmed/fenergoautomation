#Test Case: TC_R2EPIC018PBI004.2_12
#PBI:R2EPIC018PBI004.2
#User Story ID:USTAX-011a
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R2EPIC018PBI004.2_12

  Scenario: NBFI-Validate the "Document requirements" are updated  correctly against the relevant "Tax Form Type " under section "Document Requirements" on screen  " Us Tax Classification" in LEM workflow
     
	##########################################################################################################################
  #PreCondition: Create entity with client type as NBFI and confidential as NBFI.
   #############################################################################################################
    Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "Preliminary Tax Assessment" task
    And I select "Yes" for the field "Do you have a US Tax Form from the client?"
    When I complete the "Preliminary Tax Assessment" task
    When I navigate to "US Tax Classification " task
    When I complete the "US Tax Classification" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    When I navigate to "Classification" task
    When I complete "Classification" task    
    When I login to Fenergo Application with "RM:NBFI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:NBFI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
     # Initiate LEM 
     And I initiate "Legal Entity Maintenance" from action button
    #Select Area as 'LEdetails' and Area of change as 'Regulatory Updates'
    When I navigate to "SelectClassifications" task
    When I select "US TAX" for the field "Select Classification to trigger"
   When I navigate to "US Tax Classification " task
   #"US Tax Classification"Screen
   #Validate the below "Document requirements" are updated  correctly against the relevant "Tax Form Type" under section "Document Requirements"
  |Document Requirements                    |Tax Form Type                            |
  |CRS & FATCA Self-Certification Form      |CRS & FATCA Self-Certification Form      |
  |No Document Provided                     |Proof of Attempts to source documentation|
  |W-8BEN                                   |W-8BEN                                   |
  |W-8BENE                                  |W-8BENE                                  |
  |W-8ECI                                   |W-8ECI                                   |
  |W-8EXP                                   |W-8EXP                                   |
  |W-8IMY                                   |W-8IMY                                   |
  |W-9                                      |W-9                                      |
    And I select "CRS & FATCA Self-Certification Form" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "CRS & FATCA Self-Certification Form" document requirement is generated under section "Document Requirements"
    
     #Repeat the same validation for above mentioned "Tax Form Type"
     And I select "No Document Provided" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "Proof of Attempts to source documentation" document requirement is generated under section "Document Requirements" 
    And I select "W-8BEN" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "W-8BEN" document requirement is generated under section "Document Requirements"
    And I select "W-8BENE" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "W-8BENE" document requirement is generated under section "Document Requirements"
    And I select "W-8ECI" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "W-8ECI" document requirement is generated under section "Document Requirements"
    And I select "W-8EXP" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "W-8EXP" document requirement is generated under section "Document Requirements"
    And I select "W-8IMY" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "W-8IMY" document requirement is generated under section "Document Requirements"
    And I select "W-9" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "W-9" document requirement is generated under section "Document Requirements"
    
    
    
    
    
    
    
    
    
    
    
    
    
    