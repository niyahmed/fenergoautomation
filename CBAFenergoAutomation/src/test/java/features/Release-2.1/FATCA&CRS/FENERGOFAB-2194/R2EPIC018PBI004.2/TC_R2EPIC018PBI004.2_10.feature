#Test Case: TC_R2EPIC018PBI004.2_10
#PBI:R2EPIC018PBI004.2
#User Story ID:USTAX-011a
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R2EPIC018PBI004.2_10

  Scenario: NBFI: Validate the "Document requirements" are updated  correctly against the relevant "Tax Form Type " under section "Document Requirements" on screen  " Us Tax Classification" in COB workflow
     
	##########################################################################################################################
  #PreCondition: Create entity with client type as NBFI and confidential as NBFI.
   #############################################################################################################
    Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "Preliminary Tax Assessment" task
    And I select "Yes" for the field "Do you have a US Tax Form from the client?"
    When I complete the "Preliminary Tax Assessment" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
   When I navigate to "US Tax Classification " task
   #"US Tax Classification"Screen
   #Validate the below "Document requirements" are updated  correctly against the relevant "Tax Form Type" under section "Document Requirements"
  |Document Requirements                    |Tax Form Type                            |
  |CRS & FATCA Self-Certification Form      |CRS & FATCA Self-Certification Form      |
  |No Document Provided                     |Proof of Attempts to source documentation|
  |W-8BEN                                   |W-8BEN                                   |
  |W-8BENE                                  |W-8BENE                                  |
  |W-8ECI                                   |W-8ECI                                   |
  |W-8EXP                                   |W-8EXP                                   |
  |W-8IMY                                   |W-8IMY                                   |
  |W-9                                      |W-9                                      |
    And I select "CRS & FATCA Self-Certification Form" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "CRS & FATCA Self-Certification Form" document requirement is generated under section "Document Requirements"
    
     #Repeat the same validation for above mentioned "Tax Form Type"
     And I select "No Document Provided" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "Proof of Attempts to source documentation" document requirement is generated under section "Document Requirements" 
    And I select "W-8BEN" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "W-8BEN" document requirement is generated under section "Document Requirements"
    And I select "W-8BENE" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "W-8BENE" document requirement is generated under section "Document Requirements"
    And I select "W-8ECI" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "W-8ECI" document requirement is generated under section "Document Requirements"
    And I select "W-8EXP" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "W-8EXP" document requirement is generated under section "Document Requirements"
    And I select "W-8IMY" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "W-8IMY" document requirement is generated under section "Document Requirements"
    And I select "W-9" for "Dropdown" field "Tax Form Type"
    When I navigate to "Document Requirement" section
    And I verify "W-9" document requirement is generated under section "Document Requirements"
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    