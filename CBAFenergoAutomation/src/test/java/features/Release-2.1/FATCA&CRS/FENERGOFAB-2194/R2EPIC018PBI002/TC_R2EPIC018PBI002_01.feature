#Test Case: TC_R2EPIC018PBI002_01
#PBI:R2EPIC018PBI002
#User Story ID:USTAX-011a
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R2EPIC018PBI002_01

  Scenario: Corporate- Validate the LOVs of "Document Type" field for  client type "Corporate" on screen " Document Details" (In all 7 screens ) in COB work flow
  	# Validate these new doc types should be added to the existing LOV for doc type in alphabetical order
  ########################################################################################################################
   #Create a LE with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty"
  #####################################################################################################################  
   
   Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "Corporate" 
    When I navigate to "Document" screen by clicking on "Plus" button from "CaptureRequestDetails"
    And I select "AOF" for "Dropdown" field "Document Category"
    Then I verify "Document Type" drop-down values with "DocumentCategory" as "AOF"
    #Validate the LOVs of "Document Type" field 
    And I validate LOVs of "Document Type" field
     #Refer "LOV & Screen Mockup" tab in PBI for LOV list (6 new values)
     
    #Repeat the same validation in the below screens
    #Capture Request Details > Document Details
    #LE > Documents > Document Details
    #AML > Complete AML > Document Details
    #AML > Complete ID&V > Edit Verification > Document Details
    #Capture Request Details > Product > Document Details
    #Enrich KYC Profile > Tax Identifier > Document Details
    #AML > Complete AML > Hierarchy > Add Fircosoft Screening > Assessment > Document Details
   
    
    
    
    
    
    
