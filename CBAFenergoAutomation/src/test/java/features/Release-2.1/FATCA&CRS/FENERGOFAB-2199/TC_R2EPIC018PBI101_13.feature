#Test Case: TC_R2EPIC018PBI101_13
#PBI:R2EPIC018PBI101
#User Story ID: CRS 106 - 1, CRS 106 - 2, CRS 106-3
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC018PBI101_13

Scenario: Validate LOV values for Document type drop-down "CRS Self Certification Form- Entity" is renamed as "CRS & FATCA Self-Certification Form" under CRS details sub-flow and the same is reflecting under Document requirement sub-flow in Type field on Complete CRS classification screen for COB workflow for Client Type Corporate.

	#Validate below LOVs for 'Document Type' drop-down under CRS details sub-flow on Complete CRS classification screen for COB workflow for Client Type Corporate:

	#Validate user is able to upload one doc for each doc requirement under Document requirement sub-flow on on Complete CRS classification screen for COB workflow for Client Type Corporate
	#PreCondition: Create entity with client type as Corporate and confidential as IBG-DNE.
	Given I login to Fenergo Application with "RM:Corporate" 
	When I complete "NewRequest" screen with key "Corporate" 
	#Do not add product to LE
	And I complete "CaptureNewRequest"  
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker: Corporate 
	When I search for the "CaseId" 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I select "Counter Party Type" as "Corporate" and "Legal Constitution Type" as "Public Limited Company"
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "Preliminary Tax Assessment" task
	When I complete the "Preliminary Tax Assessment" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "ID&V" task 
	When I complete "EditID&V" task 
	When I complete "AddressAddition" in "Edit Verification" screen 
	When I complete "Documents" in "Edit Verification" screen 
	When I complete "TaxIdentifier" in "Edit Verification" screen 
	When I complete "LE Details" in "Edit Verification" screen 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	When I navigate to 'Classifications' stage
	#Validate Complete CRS classification is triggered in Classification stage
	Then I validate 'CRSClaasification' task is triggered
	When I navigate to 'CRSClassification' task screen
	When I navigate to 'CRSDetails' sub-flow
	#Validate LOV values for Document type drop-down "CRS Self Certification Form- Entity" is renamed as "CRS & FATCA Self-Certification Form" 
	Then I Validate LOV values for Document type drop-down "CRS Self Certification Form- Entity" is renamed as "CRS & FATCA Self-Certification Form" 
	#Validate below LOVs for 'Document Type' drop-down
	Then I validate below LOVs for 'Document Type' drop-down	
	|CRS & FATCA Self-Certification Form|
	|CRS Waiver|
	
	#Validate user is able to upload one doc for each doc requirement under Document requirement sub-flow 
	When I add Document corresponding to document requirement 'CRS & FATCA Self-Certification Form' and save the document
	Then I add another Document corresponding to document requirement 'CRS & FATCA Self-Certification Form' and save the document
	#Validate only second uploaded document is displaying as added document for 'CRS & FATCA Self-Certification Form' requirement
	And I Validate only second uploaded document is displaying as added document for 'CRS & FATCA Self-Certification Form' requirement
	#add document for  'CRS Waiver' document requirement
	When I add Document corresponding to document requirement 'CRS Waiver' and save the document
	Then I add another Document corresponding to document requirement 'CRS Waiver' and save the document
	#Validate only second uploaded document is displaying as added document for 'CRS Waiver' requirement
	And I Validate only second uploaded document is displaying as added document for 'CRS Waiver' requirement
	And I complete "CRSClassification" task    
	

  	