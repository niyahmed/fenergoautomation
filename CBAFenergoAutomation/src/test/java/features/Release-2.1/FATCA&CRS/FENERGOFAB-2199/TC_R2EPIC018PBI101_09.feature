#Test Case: TC_R2EPIC018PBI101_09
#PBI:R2EPIC018PBI101
#User Story ID: CRS 104, CRS 105
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC018PBI101_09

Scenario: Validate the field 'Financial Institution &  Alt Doc Applicable’ display as Hidden on Complete CRS classification screen under 'Legal Entity/Counterparty details' sub-flow on CRS clssification task screen of COB workflow for client type Corporate
	#Validate the Below mentioned fields as per DD(visible, editable,mandatory, defaults to..) under CRS details sub-flow on Complete CRS classification screen for COB workflow for client type Corporate
	
	#PreCondition: Create entity with client type as Corporate and confidential as IBG-DNE.
	Given I login to Fenergo Application with "RM:Corporate" 
	When I complete "NewRequest" screen with key "Corporate" 
	#Do not add product to LE
	And I complete "CaptureNewRequest"  
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker: Corporate 
	When I search for the "CaseId" 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I select "Counter Party Type" as "Corporate" and "Legal Constitution Type" as "Special Purpose Entity"
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "Preliminary Tax Assessment" task
	When I complete the "Preliminary Tax Assessment" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "ID&V" task 
	When I complete "EditID&V" task 
	When I complete "AddressAddition" in "Edit Verification" screen 
	When I complete "Documents" in "Edit Verification" screen 
	When I complete "TaxIdentifier" in "Edit Verification" screen 
	When I complete "LE Details" in "Edit Verification" screen 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	When I navigate to 'Classifications' stage
	#Validate Complete CRS classification is triggered in Classification stage
	Then I validate 'CRSClaasification' task is triggered
	When I navigate to 'CRSClaasification' task screen
	# validate 'Financial Institution &  Alt Doc Applicable’ display as Hidden under 'Legal Entity/ Counterparty details' details sub-flow
	Then I validate 'Financial Institution & Alt Doc Applicable’ display as Hidden under 'Legal Entity/ Counterparty details' details sub-flow
	#Validate the Below mentioned fields as per DD(visible, editable,mandatory, defaults to..) under CRS details sub-flow
	| Label                        		| Field Type    | Visible | Editable | Mandatory 	| Field Defaults To |
	| Document Type   					| drop-down     | Yes     | Yes      | Yes 			|  Select...        |
	| Document Signed date  			| date   		| Yes     | Yes      | Yes			|  blank	        |
	| Document received date 			| date		    | Yes     | Yes      | Yes			|  blank  		    |
	| CRS entity Type  					| drop-down     | Yes     | Yes      | Yes			|  Select...        |
	
	And I complete "Classification" task    
	

  	