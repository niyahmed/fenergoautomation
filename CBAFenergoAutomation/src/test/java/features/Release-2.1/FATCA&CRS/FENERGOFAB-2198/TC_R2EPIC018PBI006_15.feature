#Test Case: TC_R2EPIC018PBI006_15
#PBI:R2EPIC018PBI006
#User Story ID:USTAX-022
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC018PBI006_15 

Scenario: 
	Validate for KYC Maker "Substantial US Owners/Controlling Persons " question becomes visible when  "Tax Form type = W8-BENE"  (Visible, editable, mandatory and defaults to as per DD) for Classification stage for Client type 'PCG-Entity' for COB workflow 
	#(Validate the same while adding 'Related Party Company' on capture Hierarchy task screen)
	
	#Validate for KYC Maker "Substantial US Owners/Controlling Persons " question becomes visible when  "Tax Form type = W8-BENE"  (Visible, editable, mandatory and defaults to as per DD) for Classification stage for Client type 'PCG-Entity'
	# for RR workflow. (Validate the same while adding 'Related Party Company' on capture Hierarchy task screen)
	
	#Validate for KYC Maker "Substantial US Owners/Controlling Persons " question becomes visible when  "Tax Form type = W8-BENE"  (Visible, editable, mandatory and defaults to as per DD) for Classification stage for Client type 'PCG-Entity' 
	#for LEM workflow. (Validate the same while adding 'Related Party individual' on capture Hierarchy task screen)

	#PreCondition: Create entity with client type as Corporate and confidential as PCG-Entity.
	Given I login to Fenergo Application with "RM:PCG-Entity" 
	When I complete "NewRequest" screen with key "PCG-Entity" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker:PCG-Entity" 
	When I search for the "CaseId" 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty 'Tax Related Party Company' by right clicking  and source as 'US Tax'
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "Preliminary Tax Assessment" task 
	And I select "Yes" for the field "Do you have a US Tax Form from the client?" 
	When I complete the "Preliminary Tax Assessment" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "ID&V" task 
	When I complete "EditID&V" task 
	When I complete "AddressAddition" in "Edit Verification" screen 
	When I complete "Documents" in "Edit Verification" screen 
	When I complete "TaxIdentifier" in "Edit Verification" screen 
	When I complete "LE Details" in "Edit Verification" screen 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	When I navigate to "US Tax Classification " task 
	Then I select "CRS & FATCA Self-Certification Form" in "Tax Form Type" drop-down 
	And I validate field "Substantial US Owners/Controlling Persons " question becomes visible
	#Validate firld Substantial US Owners/Controlling Persons" is displaying as below:
		| Label                        					   | Field Type    | Visible | Editable | Mandatory 			| Field Defaults To |
		| Substantial US Owners/Controlling Persons 	   | drop-down     | Yes     | Yes      | No                    |  Select...        |
	
	When I expand 'Taxrelatedparties' sub-flow
	When I navigate to the added record by clicking on 'Edit' button
	When I navigate to 'TaxRelatedPartyDetailsgrid' task		
	When I Expand 'USTaxClassifications' sub-flow
	When I Click on 'Edit' button from the options button 
	Then I select "CRS & FATCA Self-Certification Form" in "Tax Form Type" drop-down 
	Then I select "CRS & FATCA Self-Certification Form" in "Tax Form Type" drop-down 
	And I validate field "Substantial US Owners/Controlling Persons " question becomes visible
	#Validate firld Substantial US Owners/Controlling Persons" is displaying as below:
		| Label                        					   | Field Type    | Visible | Editable | Mandatory 			| Field Defaults To |
		| Substantial US Owners/Controlling Persons 	   | drop-down     | Yes     | Yes      | No                    |  Select...        |
	When I select "U.S. Person Exempt from Reporting" in "Chapter 4 FATCA Status field" drop-down 
	Then I validate field "Form W-9 - Exemption from FATCA Reporting Code" becomes mandatory
	When I complete "Classification" task    
    When I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker:PCG-Entity"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
		
	 # Initiate RR workflow
     And I initiate "Regular Review" from action button
     And I initiate "Regular Review" from action button 
	When I complete "CloseAssociatedCase" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	Then I store the "CaseId" from LE360 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	When I complete "ReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task 
	When I add AssociatedParty 'Tax Related Party Individual' by right clicking  and source as 'US Tax'
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 	
	When I complete "Review/EditClientData" Task
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "Preliminary Tax Assessment" task
    And I select "Yes" for the field "Do you have a US Tax Form from the client?"
    When I complete the "Preliminary Tax Assessment" task	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	And I click on "ID&VLinkInRR" button 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task
   When I navigate to "US Tax Classification " task
   Then I select "W8-BENE" in "Tax Form Type" drop-down 
	And I validate field "Substantial US Owners/Controlling Persons " question becomes visible
	#Validate firld Substantial US Owners/Controlling Persons" is displaying as below:
		| Label                        					   | Field Type    | Visible | Editable | Mandatory 			| Field Defaults To |
		| Substantial US Owners/Controlling Persons 	   | drop-down     | Yes     | Yes      | No                    |  Select...        |		
	
	When I expand 'Taxrelatedparties' sub-flow
	When I navigate to the added record by clicking on 'Edit' button
	When I navigate to 'TaxRelatedPartyDetailsgrid' task		
	When I Expand 'USTaxClassifications' sub-flow
	When I Click on 'Edit' button from the options button 
	Then I select "W8-BENE" in "Tax Form Type" drop-down 
	And I validate field "Substantial US Owners/Controlling Persons " question becomes visible
	#Validate firld Substantial US Owners/Controlling Persons" is displaying as below:
		| Label                        					   | Field Type    | Visible | Editable | Mandatory 			| Field Defaults To |
		| Substantial US Owners/Controlling Persons 	   | drop-down     | Yes     | Yes      | No                    |  Select...        |
  	When I complete "Classification" task    
    When I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    And I validate case status is updated as 'closed'
       
    #Initiate LEM case
    When I initiate "Legal Entity Maintenance" from action button
    #Select Area as 'LEdetails' and Area of change as 'Regulatory Updates'
    When I navigate to "CaptureProposedChangesGrid" task is generated 
    When I navigate to "SelectClassifications" task
    When I select "US TAX" from drop-down field "Select Classification to trigger"    
    When I click on "SaveandComplete" button        
    When I navigate to "USTaxClassification" task
    Then I select "CRS & FATCA Self-Certification Form" in "Tax Form Type" drop-down 
	And I validate field "Substantial US Owners/Controlling Persons " question becomes visible
	#Validate firld Substantial US Owners/Controlling Persons" is displaying as below:
		| Label                        					   | Field Type    | Visible | Editable | Mandatory 			| Field Defaults To |
		| Substantial US Owners/Controlling Persons 	   | drop-down     | Yes     | Yes      | No                    |  Select...        |
	When I expand 'Taxrelatedparties' sub-flow
	When I navigate to the added record by clicking on 'Edit' button
	When I navigate to 'TaxRelatedPartyDetailsgrid' task		
	When I Expand 'USTaxClassifications' sub-flow	
	When I Click on plus button from the options button to add "Tax related party-Company"
	Then I select "W8-BENE" in "Tax Form Type" drop-down 
	And I validate field "Substantial US Owners/Controlling Persons " question becomes visible
	#Validate firld Substantial US Owners/Controlling Persons" is displaying as below:
		| Label                        					   | Field Type    | Visible | Editable | Mandatory 			| Field Defaults To |
		| Substantial US Owners/Controlling Persons 	   | drop-down     | Yes     | Yes      | No                    |  Select...        |conditional mandatory |  Select...        |				
	
  	And I complete "Classification" task
    
    
    
    
    
    
    
    
    
    
    
    
    