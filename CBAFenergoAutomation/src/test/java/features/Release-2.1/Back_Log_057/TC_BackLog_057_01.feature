#Test Case: TC_Backlog 057_01
#PBI: Backlog 057
#User Story ID:
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_Backlog 057_01

Scenario: Validate when a case is referred to previous stage,  post referred task completion same stage(same task) is assigned back to the same user 
#who has referred the case for 'Corporate' Client type
#Validate the task is assigned to the same user(user who has  referred the case to previous stage) under 'My Tasks' basket on My dashboard screen.
#Validate the task is assigned to the same user(user who has  referred the case to previous stage) under 'Team Tasks' basket on My dashboard screen.
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    #Given I login to Fenergo Application with "SuperUser"
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
     #refer the case to "ValidateKYCandRegulatoryGrid" task
    When I click on Actions button and click on refer option
    When I select refer to stage as "ValidateKYCandRegulatoryGrid" screen and submit the task
    Then I verify the case is referred back to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    #Validate "CompleteAMLGrid" task is assigned to the same user who has referred the case(KYCMaker: Corporate)
    When I navigate to "CompleteAMLGrid" task
    Then I see "KYCMaker: Corporate" is displaying as "Assigned user" for "CompleteAMLGrid" task 
    When I navigate to "CompleteAMLGrid" task
    #Validate the task is assigned to the same user(user who has  referred the case to previous stage(KYCMaker: Corporate)) under 'My Tasks' basket on My dashboard screen.
    When I navigate to "Mydashboard" task and click on "MyTasks" grid
    Then I validate task is displaying under 'MyTasks' grid
    #*Validate the task is assigned to the same user(user who has  referred the case to previous stage(KYCMaker: Corporate)) under 'Team Tasks' basket on My dashboard screen.
    When I navigate to "Mydashboard" task and click on "TeamTasks" grid
    Then I validate task is displaying under 'TeamTasks' grid
    
    
    
    
    
    
    
    
