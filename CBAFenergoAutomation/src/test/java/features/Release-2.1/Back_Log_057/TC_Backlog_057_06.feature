#Test Case: TC_Backlog 057_06
#PBI: Backlog 057
#User Story ID:
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_Backlog 057_06

Scenario: Validate the user who is referring the case back to previous stage doesn't have access to the task(assign the task to a user who doesn't have access to the task) 
#from where the case is being referred, then the task should be assigned to the users to whom it was originally assigned to(core behavior) 
#Validate the task is assigned to the same user (users to whom it was originally assigned to(core behavior)) under 'My Tasks' basket on My dashboard screen.
#Validate the task is assigned to the same user(user who has  referred the case to previous stage) under 'Team Tasks' basket on My dashboard screen.
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    #Given I login to Fenergo Application with "SuperUser"
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    #Login with 'KYCManager' and refer the task to 'EnrichKYC' stage
    When I login to Fenergo Application with "KYCManager"
    When I click on Actions button and click on refer option
    When I select refer to stage as "EnrichKYCProfileGrid" screen and submit the task
    Then I verify the case is referred back to "EnrichKYCProfileGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen 
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    #validate KYCManager does not have access to the task
    #Validate "CompleteAMLGrid" task is assigned to the original user to whom it was originally assigned to(core behavior) since KYC manager does not have access to the task
    When I navigate to "CompleteAMLGrid" task
    Then I validate KYCManager does not have access to the task
    And I see "KYCManager" is not displaying as "Assigned user" for "CompleteAMLGrid" task    	
   	#The task is assigned to 'KYCMaker_Corporate'(original user)
   	Then I validate Task is assigned to "KYCMaker_Corporate"
   	When I login to Fenergo Application with "KYCMaker: Corporate"   	
    When I navigate to "CompleteAMLGrid" task    
    #Validate the task is assigned to the same user (KYCMaker_Corporate) under 'My Tasks' basket on My dashboard screen.
    When I navigate to "Mydashboard" task and click on "MyTasks" grid
    Then I validate task is displaying under 'MyTasks' grid
    #Validate the task is assigned to the same user(KYCMaker_Corporate) under 'Team Tasks' basket on My dashboard screen.
    When I navigate to "Mydashboard" task and click on "TeamTasks" grid
    Then I validate task is displaying under 'TeamTasks' grid
    When I navigate to "CompleteAMLGrid" task
    And I Complete the "CompleteAMLGrid" task as "KYCMaker: Corporate"
    
    
    
    
    
    
    
    
