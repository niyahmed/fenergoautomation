#Test Case: TC_R3EPIC026PBI002_08
#PBI:R2EPIC018PBI006
#User Story ID: Sub Funds_005
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R3EPIC026PBI002_08

  Scenario: Verify the field behaviour and validations in 'Review and Approve' screen
    Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "NBFI" and below data
      | Product | Relationship         |
      | C1      | Relationship Manager |
    When I complete "Contacts" in "CaptureRequestDetails" screen
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "PreliminaryTaxAssessmentGrid" task
    And I click on "SaveandCompletePreliminaryTaxAssessment" button
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I click on "ID&VLink" button
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessment" task
    Then I see "CompleteUS TaxClassificationGrid" task is generated
    When I navigate to "CompleteUS TaxClassificationGrid" task
    When I complete "USTax" screen with key "CRS"
    When I complete "DocumentRequirementUSTax" in "USTax" screen
    When I navigate to "CRSClassificationGrid" task
    When I complete "CRSTax" screen with key "CRSFatca"
    When I click on Plus button of "Tax Identifier" subflow
    When I complete "TaxIdentifier" screen with key "C1"
    And I click on "SaveTaxIdentifier" button
    #=When I complete "DocumentRequirementCRSClassification" in "CRS Classifications" screen
    And I click on "SaveandCompleteforCRSClassification" button
    #Then I login to Fenergo Application with "RM:NBFI"
    #When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    #When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    #When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGridLEM" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BUH:NBFI"
    #When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGridLEM" task
    When I complete "ReviewSignOff" task
    #Given I login to Fenergo Application with "KYCMaker: FIG"
    #When I search for the "CaseId"
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferencesCIFId" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #Initiate Agency Request by clicking on Action (...)
    #Login with user 'GMO BO Maker / RM'
    Given I login to Fenergo Application with "GMO BO Maker / RM"
    #Verify 'Agency Request' option is available under Action button
    And I initiate "Agency Request" from action button
    #Verify the task name "Request Details: Select Trading Agreement" is generated
    When I navigate to "Request Details: Select Trading Agreement" task
    When I complete "Request Details: Select Trading Agreement " screen with key "NBFI"
    And I click on "SaveandComplete" button
    When I navigate to "Request Details: Fulfil DTCC Letter Doc Requirements" task
    When I complete "Request Details: Fulfil DTCC Letter Doc Requirement " screen with key "NBFI"
    And I click on "SaveandComplete" button
    #Verify the task name "Request Details: Underlying Principals and Product Data File Upload" is generated
    When I navigate to "Request Details: Underlying Principals and Product Data File Upload" task
    #Upload the valid bulk upload file
    When I complete "Request Details: Underlying Principals and Product Data File Upload" screen with key "NBFI"
    And I click on "SaveandComplete" button
    #Verify the task name "Complete Associated Cases" is generated
    When I navigate to "Complete Associated Cases" task
    When I complete "Complete Associated Cases" screen with key "NBFI"
    And I click on "SaveandComplete" button
    #Login with user 'GMO BO checker'
    Given I login to Fenergo Application with "GMO BO Checker"
    #Verify the task name "Review and Approve" is generated
    When I navigate to "Review and Approve" task
    And I validate the following fields in "Task Summary"
      | Label         | Field Type     | Field Defaults To   |
      | Assigned Team | Auto-populated | GMO Back office     |
      | Assigned User | Auto-populated | GMO BO Checker      |
      | Due Date      | Auto-populated |                     |
      | Status        | Auto-populated | In-Progress         |
      | Stage         | Auto-populated | Review and Approval |
    When I complete "Review and Approve" screen with key "NBFI"
    And I click on "SaveandComplete" button
    And I assert that the CaseStatus is "Closed"
