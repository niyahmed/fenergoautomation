#Test Case: TC_R3EPIC026PBI002_02
#PBI:R2EPIC018PBI006
#User Story ID: Sub Funds_001
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R3EPIC026PBI002_02 

Scenario: Verify the Inability to trigger Agency Request for NBFI client type on COB in-flight cases

    Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "NBFI" and below data
      | Product | Relationship				 |
      | C1      | Relationship Manager |
    When I complete "Contacts" in "CaptureRequestDetails" screen  
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360

     
   	#Verify the user should not able to trigger Agency Request by clicking on Action (...)
  	#Login with user 'GMO BO Maker / RM'
  	Given I login to Fenergo Application with "GMO BO Maker / RM" 
  	#Verify 'Agency Request' option is Not available or disabled under Action button
  	
    