#Test Case: TC_R3EPIC026PBIGLCMS_01
#PBI:R3EPIC026PBIGLCMS
#User Story ID: Sub Funds_GLCMS_INT
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R3EPIC026PBIGLCMS_01

  Scenario: Dev2-GLCMS-Validate the 'GLCMS Field' are defaulted with the values mentioned in th PBI 'GLCMS values (Defaulted)' column.
   #This test cases needs to be executed in Dev2 GLCMS system - after Counterparty is fetched
    Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "NBFI" and below data
      | Product | Relationship         |
      | C1      | Relationship Manager |
    When I complete "Contacts" in "CaptureRequestDetails" screen
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "PreliminaryTaxAssessmentGrid" task
    And I click on "SaveandCompletePreliminaryTaxAssessment" button
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I click on "ID&VLink" button
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessment" task
    Then I see "CompleteUS TaxClassificationGrid" task is generated
    When I navigate to "CompleteUS TaxClassificationGrid" task
    When I complete "USTax" screen with key "CRS"
    When I complete "DocumentRequirementUSTax" in "USTax" screen
    When I navigate to "CRSClassificationGrid" task
    When I complete "CRSTax" screen with key "CRSFatca"
    When I click on Plus button of "Tax Identifier" subflow
    When I complete "TaxIdentifier" screen with key "C1"
    And I click on "SaveTaxIdentifier" button
    #=When I complete "DocumentRequirementCRSClassification" in "CRS Classifications" screen
    And I click on "SaveandCompleteforCRSClassification" button
    #Then I login to Fenergo Application with "RM:NBFI"
    #When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    #When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    #When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGridLEM" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BUH:NBFI"
    #When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGridLEM" task
    When I complete "ReviewSignOff" task
    #Given I login to Fenergo Application with "KYCMaker: FIG"
    #When I search for the "CaseId"
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferencesCIFId" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #Initiate Agency Request by clicking on Action (...)
    #Login with user 'GMO BO Maker / RM'
    Given I login to Fenergo Application with "GMO BO Maker / RM"
    #Verify 'Agency Request' option is available under Action button
    And I initiate "Agency Request" from action button
    #Verify the task name "Request Details: Select Trading Agreement" is generated
    When I navigate to "Request Details: Select Trading Agreement" task
    When I complete "Request Details: Select Trading Agreement " screen with key "NBFI"
    And I click on "SaveandComplete" button
    When I navigate to "Request Details: Fulfil DTCC Letter Doc Requirements" task
    When I complete "Request Details: Fulfil DTCC Letter Doc Requirement " screen with key "NBFI"
    And I click on "SaveandComplete" button
    #Verify the task name "Request Details: Underlying Principals and Product Data File Upload" is generated
    When I navigate to "Request Details: Underlying Principals and Product Data File Upload" task
    #Upload the valid bulk upload file
    When I complete "Request Details: Underlying Principals and Product Data File Upload" screen with key "NBFI"
    And I click on "SaveandComplete" button
    #Verify the task name "Complete Associated Cases" is generated
    When I navigate to "Complete Associated Cases" task
    When I complete "Complete Associated Cases" screen with key "NBFI"
    And I click on "SaveandComplete" button
    #Login with user 'GMO BO checker'
    Given I login to Fenergo Application with "GMO BO Checker"
    #Verify the task name "Review and Approve" is generated
    When I navigate to "Review and Approve" task
    When I complete "Review and Approve" screen with key "NBFI"
    And I click on "SaveandComplete" button
    And I assert that the CaseStatus is "Closed"
    #Verify the AOB workflow
    #Verify the below stages are skipped or autocompleted in AOB workflow
    #-Validate Data
    #- Enrich Client Information
    #- Complete ID & V task (AML Stage)
    #- Risk Assessment
    #- Classification
    #- Review and Approval
    #Verify AOB cases are created under 'Associaed cases' grid in the agency request case (AOB cases = No of entities uploaded via bulk upload excel sheet)
    #Verify Complete AML task is generated
    When I navigate to "Complete AML" task
    #Add associated parties
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    #Add screening
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    When I complete "Complete AML" task
    #Waiting for UID From GLCMS task should be autocompleted when UID is fetched
    When I navigate to "Waiting for UID from GLCMS" task
  	#Check UID is fetched
    #Waiting for T24 CIF task shoud be autocompleted when CIF is fetched 
    When I complete "Waiting for T24 CIF" task
    When I navigate to "Capture Fab References" task
    #Add Counterparty UID (if not autofetched) and Add CIF ID (If not autofeched) (For lux QA env, both the Ids should be added manually)
    When I complete "Capture Fab References" task
    #Verify the Risk Rating and Overall Risk Rating is defaulted to 'Non-Rated' and Valdiate 'Compliance Review Date', 'Review Start Date' are blank.
    #Verify the KYC Approval date is generated after AOB case is closed
    And I assert that the CaseStatus is "Closed"
    #Login to GLCMS system
    #Check the GLCMS fields are mentioned in the PBI have default values which is mentioned in the PBI 'GLCMS values (Defaulted)' column
    And I validate the following fields in "GLCMS"
      | GLCMS Label                       | Fenergo Label               | Field Defaults To                     |
      | Salutation                        | Title                       | M/S                                   |
      | Counterparty Category             | Client Type                 | WBG                                   |
      | COUNTERPARTY TYPE                 | LEGAL COUNTERPARTY TYPE     | FINANCIAL_INSTITUTION_OTHER_THAN_BANK |
      | LEGAL CONSTITUTION TYPE           | LEGAL CONSTITUTION TYPE     | PRIVATE_LIMITED_COMPANY               |
      | T24 TARGET CODE                   | PRIVATE_LIMITED_COMPANY     |                                    28 |
      | UID ORIGINATING BRANCH            | UID ORIGINATING BRANCH      | Head Office _ BNK-100                 |
      | LEGAL FLAG                        | CIF Creation Required       | TRUE                                  |
      | T24 MAIL LOCATION                 | Default Value               | LOCAL                                 |
      | COUNTERPARTY ONBOARDING LOCATION: | Booking Country             | UAE (Multi value field)               |
      | Identifier Name                   | Document Name               | Others                                |
      | Identifier Valid Until            | Expiration Date             | 13/11/2169                            |
      | IS PRIMARY CONTACT                | IS PRIMARY CONTACT          | TRUE                                  |
      | T24 AML SEGMENT                   | KYC Risk Rating             | Yet to be Determined                  |
      | INDIVIDUAL/NON-INDIVIDUAL         | Entity Type                 | Non_Individual                        |
      | PROPAGATE TO TARGET SYSTEMS       | PROPAGATE TO TARGET SYSTEMS | BNK                                   |
      | IS HNI                            | IS HNI                      | FALSE                                 |
