#Test Case: TC_R3EPIC026PBI001_05
#PBI:R3EPIC026PBI001
#AC ID:AC-1,AC-
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R3EPIC026PBI001_05

  Scenario: Corporate -Validate "GMO BO Maker / RM"  user should not able to edit any cases for assigned screens and workflow tasks, e.g upload documents and bulk upload file 
  # Validate "GMO BO Maker / RM" user should not able to search Agency Request + view all Agency record
  # Validate "GMO BO Maker / RM" user should not able to view and review all Agency Request cases and refer back for re-work
  # Validate "GMO BO Maker / RM" user should not able to view all the assigned task under "Team Task"
  ########################################################################################################################
   #PreCondition: Create entity with client type as Corporate and confidential as CMB
  #####################################################################################################################  

   Given I login to Fenergo Application with "RM: Corporate"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "AssociationDetails" screen
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "Preliminary Tax Assessment" task
    When I complete the "Preliminary Tax Assessment" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    When I login to Fenergo Application with "RM:Corporate"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:corporate"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: corporate"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
 	
    # Initiate Agency Request
    Given I login to Fenergo Application with "GMO BO Maker / RM: corporate"
    When I search for the "CaseId"
    When I navigate to LE 360 screen
    And I initiate  "Agency Request"  from action button
  	When I navigate to "Request Details: Select Trading Agreement " task
  	And I click on "Save Later" button
  	When I navigate to "Case Details" screen
  	#Case Details screen
   	# Validate "GMO BO Maker / RM"  user should not able to edit any cases for assigned screens 
  	When I naviagte to "Request Details: Select Trading Agreement" task
  	And I validate user is not able to  edit the data on screen "Request Details: Select Trading Agreement" 
  	
  	# Validate "GMO BO Maker / RM"  user should not able to edit any cases for assigned screens and workflow tasks, e.g upload documents and bulk upload file
  	When I navigate to "Request Details: Fulfil  DTCC Letter Doc Requirements" task
   	And I verify user is not able to edit the assigned screens and workflows task
  	When I complete "Request Details: Fulfil  DTCC Letter Doc Requirements" task
  	
  	When I navigate to "Request Details: Underlying Principals and Product Data File Upload" task
  	When I complete "Request Details: Underlying Principals and Product Data File Upload" task
  	#Case Details Screen
  	And I Validate "Complete Associated Cases" task is assigned to "GMO BO Maker / RM" role group
  	When I navigate to "Complete Associated Cases" task
  	When I complete "Complete Associated Cases" task
  
	# Validate "GMO BO Maker / RM" user should not able to view and review all Agency Request cases and refer back for re-work
  	Then naviagated to "Case Details" page
	When I click on "Actions" button on LHS GPI
	And I verify user is not able to refer the case
    When I navigate to "Case Details" screen
  	#Case Details screen
  	Given I login to Fenergo Application with "GMO BO Checker"
  	When I search for the "CaseId"
  	When I navigate to "Review and Approve" task
  	When I complete "Review and Approve" task
  	#End of the AOB
  	# Validate "GMO BO Maker / RM" user should not able able to search Agency Request + view all Agency record
  	When I navigate to "Landing Page"
  	When I search the "Case ID " in Global Search
  	And I verify user is not able to search the Agency Request and view all the agency record
   # Validate "GMO BO Maker / RM" user should not view all the assigned task under "Team Task"
   When I navigate to "Team task" 
   And I verify "GMO BO Maker / RM" user is not able to all the assigned task under "Team Task"
   
  	
  	
  	
    
    
    
    
    
    
    