#Test Case: TC_R2EPIC018_25
#PBI:R2EPIC018PBIXXX
#AC ID:FIRCO-002
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R2EPIC018_25

  Scenario: Corporate -Validate the status(Error,Timeout,NoMatch) updated as per the request details receive from Fircosoft under "Active screening" on screen "Assessments" in COB workflow
  # Validate SAVE AND COMPLETE button should NOT be activated and "Retry"option should be available in Action button , If status is updated as "Error" / "Time Out" and user should be able to resend the request to Fircosoft for status 'Error' & 'Time Out' by clicking "Retry" option
  
 
  ########################################################################################################################
   #PreCondition: Create entity with client type as PCG and confidential as PCG
  #####################################################################################################################  

   Given I login to Fenergo Application with "RM:PCG"
    When I complete "NewRequest" screen with key "PCG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: PCG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking 
    When I complete "AssociationDetails" screen  
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking on onboarding LE 
   
    #Validate If there is error message returned by Fircosoft, the status should change to 'error' under section "Active Screenings" on screen "Assessment"
    # Validate SAVE AND COMPLETE button should NOT be enabled and "Retry"option should be available in Action button , If status is updated as "Error" / "Time Out" and user should be able to resend the request to Fircosoft for status 'Error' & 'Time Out' by clicking "Retry" option
    When I selct "Edit" from Context menu "..." for the LE 
    And I validate the status is changed to "Error" under section "Active Screenings" on screen "Assessment"
    And I validated "SAVE AND COMPLETE button" is not enabled
    When I click on "..." context menu from Action button
    And I validate "Retry" option is available in Action button
    When I click on "Retry" option
    And I validate user is able to resend the request to Fircosoft
    
    When I selct "Edit" from Context menu "..." for the LE 
    And I validate the status is changed to "No Match" under section "Active Screenings" on screen "Assessment"
    And I validated "SAVE AND COMPLETE button" is not enabled
    When I click on "..." context menu from Action button
    And I validate "Retry" option is available in Action button
    When I click on "Retry" option
    And I validate user is able to resend the request to Fircosoft
       
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    