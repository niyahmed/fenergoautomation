Feature: LEM - CIB Screening : Integration of Fircosoft result to Fenergo

  #Test Case: TC_R2EPIC018PBI_08
  #PBI: R2EPIC018PBI
  ##AC ID: FIRCO-008-010, FIRCO-011, FIRCO-011 - 016, FIRCO-013
  #Designed by: Jagdev Singh
  #Last Edited by: Jagdev Singh
  #LEM
    Scenario: PCG -Validate "KYC maker" user should be able to add Fircosoft Screening  for an LE and the associated parties present in the hierarchy details In LEM workflow
  # Validate "Assesment status" should be "In progress" once Fircosoft Screening is added for all relevant entities and related associated parties and for each particular party screened  under section "Assesments" on screen "Complete AML"
  # Validate If Fircosoft returns hits then the status should change to "Suspect" under section "Active Screenings" on screen "Assessment"
  # Validate System should Automatically send all the customer fields to Fircosoft for the party on which screening assessment is added under section "Fircosoft Screening Summary" on screen "Assessment=>Fircosoft Screening"
  # Validate  user should be able to navigate "Match Resolution and Hit Summary Page " by clicking  the status hyperlink: Suspect  or Edit from the Action button
  # Validate the status(Error,Timeout,NoMatch and Suspect) updated as per the request details receive from Fircosoft under "Active screening" on screen "Assessments"
  # Validate SAVE AND COMPLETE button should NOT be activated and "Retry"option should be available in Action button , If status is updated as "Error" / "Time Out" and user should be able to resend the request to Fircosoft for status 'Error' & 'Time Out' by clicking "Retry" option
  # Validate KYC maker user should be able to add bulk screening to an entity and select all/multi-select the parties for Fircosoft Screening and separate Fircosoft screening assessments should be created  for all the selected parties on which screening needs to be performed.
  # Validate KYC maker user should be able to view detailed hit response and resolve each hits-whether Positive Match or False match for Sanction, Adverse media, PEP and Internal watch list the screening results that were returned back from Fircosoft and expand each hit to view the hit details
  # Validate Save and Complete button is only enabled when all screening (Google/fircosoft etc) status is completed under "Active Screenings section"
  # validate user should able to navigate "Assessment GRID in Hierarchy  Page" once user click "Save and complete" button(under Active Screenings section)and "assessment  status is auto-populated as 'Complete' on screen "Assesments"
  # Validate Save button is only enabled when "Review" is selected in "Action" drop-down and Checkbox clicked as "Hit Analysis and Review Completed" 
  # Validate "Match Status" column in "Screening Matches" section is updated "Review" once user has updated the Hit
  # Validate "Save and Complete" button is only enabled when all the hits have been resolved and once clicked on "Save and Complete" button user will be taken back to "AML KYC Approval task"
  # Validate Submit button is only enabled once user selects any value in "Review Outcome" dropdown
  # Validate if user selects "Review outcome" as "Approve" then next workflow approval task is triggred
  # Validate if user selects "Review outcome" as "Refer", user can refer back the case to any previous stage
 
 ########################################################################################################################
   #PreCondition: Create entity with client type as PCG and confidential as PCG
   #PreCondition: No Screening added in COB workflow
  #####################################################################################################################  

   Given I login to Fenergo Application with "RM:PCG"
    When I complete "NewRequest" screen with key "PCG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    When I search for the "CaseId"
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: PCG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    #Adding multiple Associated Party
    When I add AssociatedParty by right clicking 
    When I add "AssociatedPartiesExpressAddition" screen with key "Non-Individual"  #TestData1:RP Director 
    When I add "AssociatedPartiesExpressAddition" screen with key "Non-Individual"  #TestData2:RP Shareholder
    When I add "AssociatedPartiesExpressAddition" screen with key "Non-Individual"  #TestData3:RP UBO
    When I add "AssociatedPartiesExpressAddition" screen with key "Non-Individual"  #TestData4:RP IBO
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "Preliminary Tax Assessment" task
    When I complete the "Preliminary Tax Assessment" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    When I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: PCG"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
	
	#End of COB Case
	
	#Initiate LEM Workflow.
	
	And I initiate "Legal Entity Maintenance" from action button
    #Select Area as 'LE Details' and LE Details change as 'KYC Data and Customer Details'
    When I navigate to "Capture Proposed Changes" task
    When I Complete "Capture Proposed Changes" task
    When I naviagte to "Update Customer Details" task
    When I complete "Update Customer Details" task
    When I navigate to "KYC Document Requirements" task
    When I complete "KYC Document Requirements" task
    When I navigate to "Onboarding Review" task
    When I complete "Onboarding Review" task
    When I navigate to "Complete AMLGrid" task
    #Validate "KYC maker" user should be able to add Fircosoft Screening  for onboarding LE and the associated parties present in the hierarchy details of the LE
    When I Initiate "Fircosoft" by rightclicking on onboarding LE 
    When I Initiate "Fircosoft" by rightclicking on associated party "RP Director "
    When I Initiate "Fircosoft" by rightclicking on associated party "RP Shareholder "
    And I validate "KYC maker" user is able to add Fircosoft Screening  for onboarding LE and the associated parties present in the hierarchy details of the LE
    
    # Validate KYC maker user should be able to add bulk screening to an entity and select all/multi-select the parties for Fircosoft Screening and separate Fircosoft screening assessments should be created  for all the selected parties on which screening needs to be performed.
    When I click "+" from "Assesments" grid
    When I click on First checkbox from "Screening Candidates" grid and select "Select All" 
    When I select "Firscoft Screening"  from field "Screening Type"
    And I click on "Create Assesments" button
    And I validate KYC maker user is able to add bulk screening to an entity and select all/multi-select the parties for Fircosoft Screening and separate Fircosoft screening assessments should be created  for all the selected parties on which screening needs to be performed.
    
    # Validate "Assesment status" should be "In progress" once Fircosoft Screening is added for all relevant entities and related associated parties under section "Assesments" on screen "Complete AML"
    And I Validate "Assesment status" is showing as "In progress" after Fircosoft Screening is added for all relevant entities and related associated parties 
    
    
    #Validate If Fircosoft returns hits then the status should change to "Suspect" under section "Active Screenings" on screen "Assessment"
    When I selct "Edit" from Context menu "..." for the associated partry "RP Director"
    And I validate the status is changed to "Suspect" under section "Active Screenings" on screen "Assessment"
   
    #Validate  user should be able to navigate "Match Resolution and Hit Summary Page " by clicking  the status hyperlink: Suspect  or Edit from the Action Context menu
     When I selct "Edit" from Context menu "..." for the associated partry "RP Director" 
     And I validate user is able to navigate "Match Resolution and Hit Summary Page " by clicking Edit from the Action Context menu
    When I click on "Back" button 
    When I naviagate to "Assesment" screen
    When I click status hyperlink: Suspect from the Action Context menu
    And I validate user is able to navigate "Match Resolution and Hit Summary Page " by clicking the status hyperlink: Suspect
  
    # Match Resolution and Hit Summary Page screen
    # Validate KYC maker user should be able to view detailed hit response and resolve each hits-whether Positive Match or False match for Sanction, Adverse media, PEP and Internal watch list the screening results that were returned back from Fircosoft and expand each hit to view the hit details
    When I click on expand button for each hit 
    And I validate user is able to view detailed hit response and resolve each hits-whether Positive Match or False match for Sanction, Adverse media, PEP and Internal watch list the screening results that were returned back from Fircosoft and expand each hit to view the hit details
    #Validate KYC Maker should be able to upload documents at each hit level
    When I upload a document in each hit level
    And I validate user is able to upload dicuments at each hit level
    # Validate Comments section should be mandatory for KYC Maker and Comments entered by KYC Maker at each screening hit should be auto-populated in the "screener comments" column of the Screening Matches" grid
    And I validate Comments section is mandatory and Comments entered by KYC Maker at each screening hit is auto-populated in the "screener comments" column of the Screening Matches" grid
    #Validate SAVE  button should not be enabled if fields "Resolution Status & Action & Comment"  are not updated
    And I validate SAVE  button is not enabled
    #Validate SAVE AND COMPLETE should not be enabled until all the hits have been resolved
    And I validate SAVE AND COMPLETE should not be enabled 
    When I Select "Resolve" for field "Actions" 
    When I select a value for field Resolution "Status"
    And I Validate "SAVE" button is enabled after updating the fields "Resolution Status & Action & Comment" 
    #Validate  Match Status column in the Screening Matches grid should change to "Resolve� once Once user has selected"Resolve " from the Action
    When I click on "SAVE" button
    And I validate Match Status column in the Screening Matches grid is changed to "Resolve" once Once user has selected "Resolve " from the Action
    #Validate The Resolution Status column in the screening matches grid  is autopopulated accordingly, once user has selected the relevant Resolution Status for the hit
    And I Validate The Resolution Status column in the screening matches grid  is autopopulated accordingly, once user has selected the relevant Resolution Status for the hit
    And I validate SAVE AND COMPLETE is enabled 
    When I click on "SAVE AND COMPLETE " button
    
    # Fircosoft screening screen
    # Validate System should Automatically send all the customer fields to Fircosoft for the party on which screening assessment is added under section "Fircosoft Screening Summary" 
    #Validate Fircosoft Screening summary grid should be auto-populated accordingly if Fircosoft returns no match/false match/positive match for any screening type and SAVE AND COMPLETE button should not be enabled until final screening summary grid has been populated for the entity/associated party under screening  
    And I validate  Fircosoft Screening summary grid is  auto-populated accordingly
    #Validate SAVE AND COMPLETE button should  be enabled once final screening summary grid is populated for the entity/associated party under screening
    And I validate SAVE AND COMPLETE button is enabled 
    #Validate user should be able to navigate the Assessment /Active Screening page after click  the SAVE AND COMPLETE button 
    And I click on "SAVE AND COMPLETE" button
    And Validate user is able to navigate the Assessment /Active Screening page
    #Validate the status should change to "Completed" under section "Active Screening" on screen Assessment
     And status is changed to "Completed" under section "Active Screening" on screen Assessment
	
	#Navigate to "Active Screenings"
	Then I navigate to "Assemnent/Active Screenings" section under under "Assessment" screen
	Then I Validate Save and Complete button is only enabled when all screening (Google/fircosoft etc) status is completed under "Active Screenings section"
	When I click on "Save and Complete button"
	Then I navigate to Assessment GRID in Hierarchy Page
	Then I validate Assessment  status is auto-populated as 'Complete' on screen "Assesments"
	Then I complete "CompleteAML" task
	
	When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
	
	Then I login to Fenergo Application with "RM:CBG"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
	
	#New task for AML KYC Review and Signoff
	Then I login to Fenergo Application with "KYC Approver (L2)"
    When I search for the "CaseId"
	When I navigate to "AMLKYCReviewandSignoff" task
	#KYC Approver can see and review all the  results that was updated by KYC Maker
	The I Validate KYC Approver can see and review all the results that was updated by KYC Maker.
	When I navigate to any assesment on "AMLKYCReviewandSignoff" page
	#Click on "Hit Details" context menu will take you to screening resolution page
	Then I click on "Hit Details" option on context menu "screening resolution page"
	#User should able to "expand" each hit to view the hit details
	When I Expand a hit on "screening resolution page"
	Then I validate details on "Screening Matches"
	#Select "Review" option in "Action" dropdown
	When I navigate to "Update Matches" section and drop down "Action"
	Then I select "Review" option in "Action" dropdown
	#Select "Hit Analysis and Review Completed" Check box
	Then I select "Hit Analysis and Review Completed" Check box
	#Validate Save button is only enabled when "Review" is selected in "Action" drop-down and Checkbox clicked as "Hit Analysis and Review Completed"
	Then I validate Save button is only enabled 
	#Validate "Match Status" column in "Screening Matches" section is updated "Review" once user has updated the Hit
	When I navigate to "Match Status" column in "Screening Matches" section
	Then I validate "Match Status" column in "Screening Matches" section is updated "Review"
	#Validate "Save and Complete" button is only enabled when all the hits have been resolved and once clicked on "Save and Complete" button user will be taken back to "AML KYC Approval task"
	Then I validate "Save and Complete" button is only enabled when all the hits have been resolved
	When I click on "Save and Complete" button
	Then I validate user will be taken back to "AML KYC Approval task"
	When User is naviagated to "AML KYC Approval task"
	#Validate Submit button is only enabled once user selects any value in "Review Outcome" dropdown
	Then I validate Submit button is only enabled once user selects any value in "Review Outcome" dropdown
	# Validate if user selects "Review outcome" as "Approve" then next workflow approval task is triggred
	Then I Validate if user selects "Review outcome" as "Approve" then next workflow approval task is triggred
	# Validate if user selects "Review outcome" as "Refer", user can refer back the case to any previous stage
	Then I validate case is referred back to any previous when "Review outcome" is selected as "Refer"
    When I complete "AMLKYCReviewandSignoff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "BUH:CBG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "KYCMaker: PCG"
    When I search for the "CaseId"
    And I complete "Waiting for UID from GLCMS" task from Actions button
    And I assert that the CaseStatus is "Closed"
	
	#End of LEM case