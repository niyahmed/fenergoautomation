#Test Case: TC_R2EPIC018_05
#PBI:R2EPIC018PBIXXX
#AC ID:FIRCO-002
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R2EPIC018PBI_05

  Scenario: Corporate -Validate the status(Error,Timeout,NoMatch) updated as per the request details receive from Fircosoft under "Active screening" on screen "Assessments" in LEM workflow
  # Validate SAVE AND COMPLETE button should NOT be activated and "Retry"option should be available in Action button , If status is updated as "Error" / "Time Out" and user should be able to resend the request to Fircosoft for status 'Error' & 'Time Out' by clicking "Retry" option
  
 
  ########################################################################################################################
   #PreCondition: Create entity with client type as Corporate and confidential as IBG-DNE
   #PreCondition: Trigger full KYC workflow in LEM
   #PreCondition: No Screening in COB workflow
  #####################################################################################################################  

   Given I login to Fenergo Application with "RM:Corporate"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    #Adding multiple Associated Party
    When I add AssociatedParty by right clicking 
    When I add "AssociatedPartiesExpressAddition" screen with key "Non-Individual"  #TestData1:RP Director 
    When I add "AssociatedPartiesExpressAddition" screen with key "Non-Individual"  #TestData2:RP Shareholder
    When I add "AssociatedPartiesExpressAddition" screen with key "Non-Individual"  #TestData3:RP UBO
    When I add "AssociatedPartiesExpressAddition" screen with key "Non-Individual"  #TestData4:RP IBO
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "Preliminary Tax Assessment" task
    When I complete the "Preliminary Tax Assessment" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    When I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
    # Initiate LEM workflow
    And I initiate "Legal Entity Maintenance" from action button
    #Select Area as 'LE Details' and LE Details change as 'KYC Data and Customer Details'
    When I navigate to "Capture Proposed Changes" task
    When I Complete "Capture Proposed Changes" task
    When I naviagte to "Update Customer Details" task
    When I complete "Update Customer Details" task
    When I navigate to "KYC Document Requirements" task
    When I complete "KYC Document Requirements" task
    When I navigate to "Onboarding Review" task
    When I complete "Onboarding Review" task   
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking on onboarding LE 
   
    #Validate If there is error message returned by Fircosoft, the status should change to 'error' under section "Active Screenings" on screen "Assessment"
    # Validate SAVE AND COMPLETE button should NOT be enabled and "Retry"option should be available in Action button , If status is updated as "Error" / "Time Out" and user should be able to resend the request to Fircosoft for status 'Error' & 'Time Out' by clicking "Retry" option
    When I selct "Edit" from Context menu "..." for the LE 
    And I validate the status is changed to "Error" under section "Active Screenings" on screen "Assessment"
    And I validated "SAVE AND COMPLETE button" is not enabled
    When I click on "..." context menu from Action button
    And I validate "Retry" option is available in Action button
    When I click on "Retry" option
    And I validate user is able to resend the request to Fircosoft
    
    When I selct "Edit" from Context menu "..." for the LE 
    And I validate the status is changed to "No Match" under section "Active Screenings" on screen "Assessment"
    And I validated "SAVE AND COMPLETE button" is not enabled
    When I click on "..." context menu from Action button
    And I validate "Retry" option is available in Action button
    When I click on "Retry" option
    And I validate user is able to resend the request to Fircosoft
       
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    