#Test Case: TC_R2EPIC018_18
#PBI:R2EPIC018PBIXXX
#AC ID:FIRCO-001,FIRCO-002,FIRCO-004,FIRCO-006
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R2EPIC018_18

  Scenario: FI -Validate "KYC maker" user should be able to add Fircosoft Screening  for onboarding LE and the associated parties present in the hierarchy details of the LE In COB workflow
  # Validate "Assesment status" should be "In progress" once Fircosoft Screening is added for all relevant entities and related associated parties 
  # Validate If Fircosoft returns hits then the status should change to "Suspect" under section "Active Screenings" on screen "Assessment"
  # Validate KYC maker user should be able to add bulk screening to an entity and select all/multi-select the parties for Fircosoft Screening and separate Fircosoft screening assessments should be created  for all the selected parties on which screening needs to be performed.
  # Validate  user should be able to navigate "Match Resolution and Hit Summary Page " by clicking  the status hyperlink: Suspect  or Edit from the Action button
  # Validate KYC maker user should be able to view detailed hit response and resolve each hits-whether Positive Match or False match for Sanction, Adverse media, PEP and Internal watch list the screening results that were returned back from Fircosoft and expand each hit to view the hit detail
  # Validate KYC Maker should be able to upload documents at each hit level
  # Validate Comments section should be mandatory for KYC Maker and Comments entered by KYC Maker at each screening hit should be auto-populated in the "screener comments" column of the Screening Matches" grid
  # Validate SAVE  button should not be enabled if fields "Resolution Status & Action & Comment"  are not updated
  # Validate Match Status column in the Screening Matches grid should change to "Resolve� once Once user has selected"Resolve " from the Action 
  # The Resolution Status column in the screening matches grid  is autopopulated accordingly, once user has selected the relevant Resolution Status for the hit
  # Validate SAVE AND COMPLETE should not be enabled until all the hits have been resolved
  # Validate System should Automatically send all the customer fields to Fircosoft for the party on which screening assessment is added under section "Fircosoft Screening Summary" on screen "Assessment=>Fircosoft Screening"
  # Validate Fircosoft Screening summary grid should be auto-populated accordingly if Fircosoft returns no match/false match/positive match for any screening type and SAVE AND COMPLETE button should not be enabled until final screening summary grid has been populated for the entity/associated party under screening
  # Validate user should be able to navigate the Assessment /Active Screening page after click  the SAVE AND COMPLETE button and the status should change to "Completed� on screen
  
 
  ########################################################################################################################
   #PreCondition: Create entity with client type as FI and confidential as FIG
  #####################################################################################################################  

   Given I login to Fenergo Application with "RM:FI"
    When I complete "NewRequest" screen with key "FI"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    #Adding multiple Associated Party
    When I add AssociatedParty by right clicking 
    When I add "AssociatedPartiesExpressAddition" screen with key "Non-Individual"  #TestData1:RP Director 
    When I add "AssociatedPartiesExpressAddition" screen with key "Non-Individual"  #TestData2:RP Shareholder
    When I add "AssociatedPartiesExpressAddition" screen with key "Non-Individual"  #TestData3:RP UBO
    When I add "AssociatedPartiesExpressAddition" screen with key "Non-Individual"  #TestData4:RP IBO
    When I complete "AssociationDetails" screen  
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    #Validate "KYC maker" user should be able to add Fircosoft Screening  for onboarding LE and the associated parties present in the hierarchy details of the LE
    When I Initiate "Fircosoft" by rightclicking on onboarding LE 
    When I Initiate "Fircosoft" by rightclicking on associated party "RP Director "
    When I Initiate "Fircosoft" by rightclicking on associated party "RP Shareholder "
    And I validate "KYC maker" user is able to add Fircosoft Screening  for onboarding LE and the associated parties present in the hierarchy details of the LE
    
    # Validate KYC maker user should be able to add bulk screening to an entity and select all/multi-select the parties for Fircosoft Screening and separate Fircosoft screening assessments should be created  for all the selected parties on which screening needs to be performed.
    When I click "+" from "Assesments" grid
    When I click on First checkbox from "Screening Candidates" grid and select "Select All" 
    When I select "Firscoft Screening"  from field "Screening Type"
    And I click on "Create Assesments" button
    And I validate KYC maker user is able to add bulk screening to an entity and select all/multi-select the parties for Fircosoft Screening and separate Fircosoft screening assessments should be created  for all the selected parties on which screening needs to be performed.
    
    # Validate "Assesment status" should be "In progress" once Fircosoft Screening is added for all relevant entities and related associated parties under section "Assesments" on screen "Complete AML"
    And I Validate "Assesment status" is showing as "In progress" after Fircosoft Screening is added for all relevant entities and related associated parties 
    
    
    #Validate If Fircosoft returns hits then the status should change to "Suspect" under section "Active Screenings" on screen "Assessment"
    When I selct "Edit" from Context menu "..." for the associated partry "RP Director"
    And I validate the status is changed to "Suspect" under section "Active Screenings" on screen "Assessment"
   
    #Validate  user should be able to navigate "Match Resolution and Hit Summary Page " by clicking  the status hyperlink: Suspect  or Edit from the Action Context menu
     When I selct "Edit" from Context menu "..." for the associated partry "RP Director" 
     And I validate user is able to navigate "Match Resolution and Hit Summary Page " by clicking Edit from the Action Context menu
    When I click on "Back" button 
    When I naviagate to "Assesment" screen
    When I click status hyperlink: Suspect from the Action Context menu
    And I validate user is able to navigate "Match Resolution and Hit Summary Page " by clicking the status hyperlink: Suspect
  
    # Match Resolution and Hit Summary Page screen
    # Validate KYC maker user should be able to view detailed hit response and resolve each hits-whether Positive Match or False match for Sanction, Adverse media, PEP and Internal watch list the screening results that were returned back from Fircosoft and expand each hit to view the hit details
    When I click on expand button for each hit 
    And I validate user is able to view detailed hit response and resolve each hits-whether Positive Match or False match for Sanction, Adverse media, PEP and Internal watch list the screening results that were returned back from Fircosoft and expand each hit to view the hit details
    #Validate KYC Maker should be able to upload documents at each hit level
    When I upload a document in each hit level
    And I validate user is able to upload dicuments at each hit level
    # Validate Comments section should be mandatory for KYC Maker and Comments entered by KYC Maker at each screening hit should be auto-populated in the "screener comments" column of the Screening Matches" grid
    And I validate Comments section is mandatory and Comments entered by KYC Maker at each screening hit is auto-populated in the "screener comments" column of the Screening Matches" grid
    #Validate SAVE  button should not be enabled if fields "Resolution Status & Action & Comment"  are not updated
    And I validate SAVE  button is not enabled
    #Validate SAVE AND COMPLETE should not be enabled until all the hits have been resolved
    And I validate SAVE AND COMPLETE should not be enabled 
    When I Select "Resolve" for field "Actions" 
    When I select a value for field Resolution "Status"
    And I Validate "SAVE" button is enabled after updating the fields "Resolution Status & Action & Comment" 
    #Validate  Match Status column in the Screening Matches grid should change to "Resolve� once Once user has selected"Resolve " from the Action
    When I click on "SAVE" button
    And I validate Match Status column in the Screening Matches grid is changed to "Resolve" once Once user has selected "Resolve " from the Action
    #Validate The Resolution Status column in the screening matches grid  is autopopulated accordingly, once user has selected the relevant Resolution Status for the hit
    And I Validate The Resolution Status column in the screening matches grid  is autopopulated accordingly, once user has selected the relevant Resolution Status for the hit
    And I validate SAVE AND COMPLETE is enabled 
    When I click on "SAVE AND COMPLETE " button
    
    #Fircosoft screening screen
    # Validate System should Automatically send all the customer fields to Fircosoft for the party on which screening assessment is added under section "Fircosoft Screening Summary" 
    #Validate Fircosoft Screening summary grid should be auto-populated accordingly if Fircosoft returns no match/false match/positive match for any screening type and SAVE AND COMPLETE button should not be enabled until final screening summary grid has been populated for the entity/associated party under screening  
    And I validate  Fircosoft Screening summary grid is  auto-populated accordingly
    #Validate SAVE AND COMPLETE button should  be enabled once final screening summary grid is populated for the entity/associated party under screening
    And I validate SAVE AND COMPLETE button is enabled 
    #Validate user should be able to navigate the Assessment /Active Screening page after click  the SAVE AND COMPLETE button 
    And I click on "SAVE AND COMPLETE" button
    And Validate user is able to navigate the Assessment /Active Screening page
    #Validate the status should change to "Completed" under section "Active Screening" on screen Assessment
     And status is changed to "Completed" under section "Active Screening" on screen Assessment
    
  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    