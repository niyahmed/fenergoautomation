#Test Case: TC_R2.1S2EPIC024PBI041.4_04
#PBI: TC_R2.1S2EPIC024PBI041.4_04
#User Story ID:AC-2
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2.1S2EPIC024PBI041.4_04 

	Scenario:Validate below fields as per DD(Editable, Visible, Mandatory, Defaults to..) on  "Publish to GLCMS" task "Publish FAB references" stage for Regular Review WF over Lite KYC workflow
	Login and Creating an Onboarding flow for an FAB application
	Given  I login to Fenergo Application with "RM:NBFI" 
	When  I click on plus button to create new request 
	When  I select Client Type "NBFI" 
	When  I complete "NewRequest" screen with key "LiteKYC-NBFI" 
	And  I complete "CaptureNewRequest" with Key "LiteKYC-NBFI" and below data 
	| Product | Relationship |
	| C1      | C1           |
	And  I click on "Continue" button 
	When  I complete "ReviewRequest" task 
	Then  I store the "CaseId" from LE360 
	
	Given  I login to Fenergo Application with "KYCMaker: FIG" 
	When  I search for the "CaseId" 
	
	When  I navigate to "ValidateKYCandRegulatoryGrid" task 
	When  I complete "ValidateKYC" screen with key "LiteKYC-NBFI" 
	And  I click on "SaveandCompleteforValidateKYC" button 
	When  I navigate to "EnrichKYCProfileGrid" task 
	When  I complete "AddAddressFAB" task 
	
	When  I complete "EnrichKYC" screen with key "LiteKYC-NBFI" 
	And  I click on "SaveandCompleteforEnrichKYC" button 
	When  I navigate to "CaptureHierarchyDetailsGrid" task 
	When  I add AssociatedParty by right clicking 
	When  I complete "AssociatedPartiesExpressAddition" screen with key
	"Non-Individual" 
	When  I complete "AssociationDetails" screen with key "Director" 
	When  I complete "CaptureHierarchyDetails" task 
	When  I navigate to "KYCDocumentRequirementsGrid" task 
	Then  I complete "KYCDocumentRequirements" task 
	
	When  I navigate to "CompleteAMLGrid" task 
	When  I Initiate "Fircosoft" by rightclicking 
	And  I complete "Fircosoft" from assessment grid with Key
	"FicrosoftScreeningData" 
	Then  I complete "CompleteAML" task 
	
	When  I navigate to "CompleteID&VGrid" task 
	When  I complete "ID&V" task 
	When  I complete "EditID&V" task 
	When  I complete "AddressAddition" in "Edit Verification" screen 
	When  I complete "Documents" in "Edit Verification" screen 
	When  I complete "TaxIdentifier" in "Edit Verification" screen 
	When  I complete "LE Details" in "Edit Verification" screen 
	When  I click on "SaveandCompleteforEditVerification" button 
	When  I complete "CompleteID&V" task 
	
	When  I navigate to "CaptureRiskCategoryGrid" task 
	When  I complete "RiskAssessment" screen with key "LiteKYC" 
	
	Then  I login to Fenergo Application with "RM:NBFI" 
	When  I search for the "CaseId" 
	When  I navigate to "ReviewSignOffGrid" task 
	When  I complete "ReviewSignOff" task 
	
	Then  I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When  I search for the "CaseId" 
	When  I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When  I complete "ReviewSignOff" task 
	
	Then  I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When  I search for the "CaseId" 
	When  I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When  I complete "ReviewSignOff" task 
	
	Then  I login to Fenergo Application with "BUH:NBFI" 
	When  I search for the "CaseId" 
	When  I navigate to "BHUReviewandSignOffGrid" task 
	When  I complete "ReviewSignOff" task 
	
	Then  I login to Fenergo Application with "KYCMaker: FIG" 
	When  I search for the "CaseId" 
	When  I navigate to "CaptureFabReferencesGrid" task 
	When  I complete "CaptureFABReferences" task 
	And  I assert that the CaseStatus is "Closed" 
	
	#Verify the auto-Trigger of 'Regular review' case over KYC Lite workflow  when the case status is updated as closed
	#Run the query to change 'Review start date' as current date automatically since the Review start date is same as current date
	#Verify 'Review start date' is same as the 'current date'
	Then  I validate 'Review start date' is same as the 'current date' 
	#Verify Regular review case is triggered 	
	When  I navigate to LE360-LEdetails screen 
	Then  I validate 'Regular-review' case is already triggered 
	
	#verify 'Close Associated Cases' task is displaying 'In Progress'
	And  I validate 'Close Associated screen grid' task is displaying 'In Progress' 
	
	When  I login to Fenergo Application with "KYCMaker:NBFI" 
	When  I navigate to 'Close Associated screen grid' task 
	When  I click on 'Submit' button 
	Then  I complete 'Close Associated screen grid' task 
	
	#verify 'Validate KYC and Regulatory Data' task is triggered
	When  I navigate to 'Validate KYC and Regulatory Data grid' task 
	When  I click on 'Save and complete' button 
	Then  I complete 'Validate KYC and Regulatory Data grid' task 
	
	#verify 'Review Request Details' task is triggered
	When  I navigate to 'Review Request Details' task 
	When  I click on 'Save and complete' button 
	Then  I complete 'Review Request Details' task 
	
	#verify 'Review/Edit Client Data' task is triggered
	When  I navigate to 'Review/Edit Client Data' task 
	When  I click on 'Save and complete' button 
	Then  I complete 'Review/Edit Client Data' task 
	
	#verify 'KYC Document Requirement' task is triggered
	When  I navigate to 'KYC Document Requirement' task 
	Then  I Add the documents 
	When  I click on 'Save and complete' button 
	Then  I complete 'KYC Document Requirement' task 
	
	#verify 'KYC Document Requirement' task is triggered
	When  I navigate to 'KYC Document Requirement' task 
	Then  I complete 'KYC Document Requirement' task by adding all documents 
	When  I navigate to "CompleteAMLGrid" task 
	Then  I complete "CompleteAML" task 
	
	When  I navigate to "CompleteID&VGrid" task 
	When  I complete "CompleteID&V" task 
	
	When  I navigate to "CaptureRiskCategoryGrid" task 
	When  I complete "RiskAssessment" screen with key "LiteKYC" 
	
	Then  I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When  I search for the "CaseId" 
	When  I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When  I complete "ReviewSignOff" task 
	
	Then  I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When  I search for the "CaseId" 
	When  I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When  I complete "ReviewSignOff" task 
	
	Then  I login to Fenergo Application with "BUH:FI" 
	When  I search for the "CaseId" 
	When  I navigate to "BHUReviewandSignOffGrid" task 
	When  I complete "ReviewSignOff" task 
	
	#validate "Publish to GLCMS" task in "Publish FAB references" stage is generated 
	When  I navigate to 'Casedetails' task grid 
	Then  I validate "Publish to GLCMS" task in "Publish FAB references" stage is generated 
	
	When  I click on "navigate" button 
	Then  I validate user is navigated to 'Publish Fab references' task screen 
	Then  I validate counterparty UID is generated 
	#Validate the Below mentioned fields as per DD(visible, editable,mandatory, defaults to..)
	| Label                        		| Field Type    | Visible | Editable| Mandatory 	| 
	| Legal Entity Name  				| Alphanumeric  | Yes     | No      | NA 			| 
	| Client Type  						| drop-down		| Yes     | No      | NA			| 
	| Counterparty UID 					| Alphanumeric  | Yes     | No      | NA			|  
	| Status  							| drop-down     | Yes     | No      | NA			|  
	| GLCMS Status Description 			| drop-down  	| Yes     | No      | NA			|  
	| Request Sent Date and Time		| date		    | Yes     | No      | NA			| 
	| Response Received Date and Time	| date          | Yes     | No      | NA			|  	
	
	
	
	
	
	
	
	
	
	
	
	