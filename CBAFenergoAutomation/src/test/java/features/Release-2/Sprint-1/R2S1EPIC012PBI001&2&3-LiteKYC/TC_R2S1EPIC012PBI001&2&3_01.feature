#Test Case: TC_R2S1EPIC012PBI001&2&3_01
#PBI: R2S1EPIC012PBI001, R2S1EPIC012PBI002, R2S1EPIC012PBI003
#User Story ID: Light_KYC_001, Light_KYC_006, Light_KYC_015
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: TC_R2S1EPIC012PBI001&2&3_01-Lite KYC

  @Automation
  Scenario: Validate if "Does the CDD profile qualify for Lite KYC?" field is present for Client Type "FI"  and LE Category "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship"
    ##Validate if Lite KYC is triggered and validate the name for Lite KYC workfow
    ##Validate Doc Matrix requirement for Lite KYC flow with Client Type "FI"  and LE Category "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship"
    ##Validate if risk is defaulted to "Medium" and is non-editable
    Given I login to Fenergo Application with "RM:FI"
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Financial Institution (FI)"
    When I select "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship" for "Dropdown" field "Legal Entity Category"
    And I check that below data is visible
      | FieldLabel                                 |
      | Does the CDD profile qualify for Lite KYC? |
    And I validate the following fields in "Complete" Screen
      | Label                                      | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Does the CDD profile qualify for Lite KYC? | Dropdown  | true    | false    | true      | Select...  |
    
    When I select "Client/Counterparty" for "Dropdown" field "Legal Entity Role"
    When I select "Bank - France Branch" for "Dropdown" field "Entity of Onboarding"
    When I select "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship" for "Dropdown" field "Legal Entity Category"
    When I select "Yes" for "Dropdown" field "Does the CDD profile qualify for Lite KYC?"
    And I click on "CreateEntity" button
    When I navigate to "LE360overview" screen
    When I navigate to "Cases" from LHN section
    Then I Validate the CaseName Contains "Lite KYC Onboarding" in it
    And I navigate to "CaptureRequestDetailsGrid" task
    And I complete "CaptureNewRequest" with Key "LiteKYC-FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "FI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "FI"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    And I assert only the following document requirements are listed for LiteKYC WF
      | KYC Document Requirement                                                                     | Default Document Type                                                                        | Default Document Category | Mandatory |
      | Certificate of Incorporation or Trade License or Registration Certificate or Banking License | Certificate of Incorporation or Trade License or Registration Certificate or Banking License | Constitutive              | True      |
      | Wolfsberg Questionnaire                                                                      | Wolfsberg Questionnaire                                                                      | AOF                       | False     |
      | Others                                                                                       | Others                                                                                       | MISC                      | False     |
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CaptureRiskCategoryGrid" task
    Then I check that the "RiskAssessmentLiteKYC" button is disabled
    And I assert "RiskAssessmentLiteKYC" is populated as "Medium"
    When I complete "RiskAssessment" screen with key "LiteKYC"
    
    Then I login to Fenergo Application with "RM:FI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:FI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    
    
    
    
    ##==>Below steps are written as Manaul testing steps
    #Given I login to Fenergo Application with "RM:FI"
    #When I click on "+" sign to create new request
    #When I navigate to "Enter Entity details" screen
    #Test data: Client Type - FI
    #When I complete "Enter Entity details" screen task with ClientEntityType as "FI"
    #When I complete "Search For Duplicates" screen task
    #Validate the behavior of "Does the CDD profile qualify for Lite KYC?" field in "Complete" screen
    #Then I assert "Does the CDD profile qualify for Lite KYC?" field is not visible
    #Then I select "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship" for Legal Entity Category field
    #Then I assert that "Does the CDD profile qualify for Lite KYC?" field is visible
    #And I validate the field in "Complete" screen
      #| Fenergo Label Name                         | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      #| Does the CDD profile qualify for Lite KYC? | Drop-down  | Yes     | Yes      | Yes       | Select...         |
    #And I validate LOVs for "Does the CDD profile qualify for Lite KYC?" field
      #| Yes |
      #| No  |
    #Validate the behaviour of "CREATE ENTITY" button
    #And I select "Client/Counterparty" for "Legal Entity Role" field
    #And I select any value for "Entity of Onboarding" field
    #And I assert "CREATE ENTITY" button is not enabled
    #And I select "Yes" for "Does the CDD profile qualify for Lite KYC?" field
    #And I assert "CREATE ENTITY" button is enabled
    #And I click on "CREATE ENTITY" button
    ##Validate the workflow name for Lite KYC
    #And I assert "Lite KYC Onboarding" workflow is triggered
    #Validate if the user is directly taken to Capture Request Details screen
    #And I assert user is navigated to "Capture Request Details" screen
    #And I assert name of the workflow/case is "Lite KYC Onboarding" in the LHN panel
    #And I navigate to LE360 screen
    #And I navigate to cases section
    #And I assert name of the workflow is "Lite KYC Onboarding" in cases section
    #And I navigate to "Capture Request Details" screen #by clicking "Capture Request Details" task from the task grid
    #Test data - Confidential value - FI
    #And I complete "CaptureNewRequest" with Key "C1" and below data
      #| Product | Relationship |
      #| C1      | C1           |
    #And I click on "Continue" button
    #When I complete "ReviewRequest" task
    #Then I store the "CaseId" from LE360
    #Given I login to Fenergo Application with "KYCMaker: FIG"
    #When I search for the "CaseId"
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #When I complete "EnrichKYC" screen with key "C1"
    #And I click on "SaveandCompleteforEnrichKYC" button
    #Add an associated party
    #When I navigate to "CaptureHierarchyDetailsGrid" task
    #Then I add an associated party
    #When I complete "CaptureHierarchyDetails" task
    #When I navigate to "KYCDocumentRequirementsGrid" task
    ##Validate Doc Matrix requirement for Lite KYC flow with Client Type "FI"  and LE Category "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship"
    #And I assert only the following document requirements are listed
      #| KYC Document Requirement                                                                     | Default Document type                                                                        | Default Document Category | Mandatory |
      #| Certificate of Incorporation or Trade License or Registration Certificate or Banking License | Certificate of Incorporation or Trade License or Registration Certificate or Banking License | Constitutive              | True      |
      #| Wolfsberg Questionnaire                                                                      | Wolfsberg Questionnaire                                                                      | MISC                      | False     |
      #| Others                                                                                       | Others                                                                                       | MISC                      | False     |
    #Then I click on "Save & Complete
    #Then I assert Error is thrown to add mandatory documents
    #When I add a "DocumentUpload" in KYCDocument # add only mandaotry documents
    #Then I complete "KYCDocumentRequirements" task
    #When I navigate to "CompleteAMLGrid" task
    #When I Initiate "Fircosoft" by rightclicking
    #And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    #And I click on "SaveandCompleteforAssessmentScreen1" button
    #Then I complete "CompleteAML" task
    #When I navigate to "CompleteID&VGrid" task
    #When I complete "CompleteID&V" task
    #When I navigate to "CaptureRiskCategoryGrid" task
    ##Validate if risk is defaulted to "Medium" and is non-editable
    #And I assert "Risk Category" is populated as "Medium"
    #And I assert "Risk Category" is not editable
    #And I assert "Continue" button is enabled
    #When I complete "RiskAssessmentFAB" task
    #Then I login to Fenergo Application with "RM"
    #When I search for the "CaseId"
    #When I complete "ReviewSignOff" task #Relationship Manager Review and Sign-Off
    #Then I login to Fenergo Application with "KYCManager"
    #When I search for the "CaseId"
    #When I complete "ReviewSignOff" task #CIB R&C KYC Approver - KYC Manager Review and Sign-Off
    #Then I login to Fenergo Application with "AVP"
    #When I search for the "CaseId"
    #When I complete "ReviewSignOff" task #CIB R&C KYC Approver - AVP Review and Sign-Off
    #Then I login to Fenergo Application with "BUH:FI"
    #When I search for the "CaseId"
    #When I complete "ReviewSignOff" task #Business Unit Head Review and Sign-Off
    