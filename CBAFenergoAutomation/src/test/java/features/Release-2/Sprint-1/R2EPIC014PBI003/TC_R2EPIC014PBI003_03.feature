#Test Case: TC_R2EPIC014PBI003_03
#PBI: R2EPIC014PBI003
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2EPIC014PBI003_03

  @Automation
  Scenario: Corporate:Validate the field behaviours in 'Source Of Funds And Wealth Details' and 'Industry Codes Details' section of Review/Edit Client Data Screen in RR workflow
    #Precondition: COB case with Client type as Corporte and COI as UAE to be created by filling all the mandatory and non-mandatory fields and Case status should be Closed
    Given I login to Fenergo Application with "RM:IBG-DNE" 
		When I complete "NewRequest" screen with key "Corporate" 
		And I complete "CaptureNewRequest" with Key "C1" and below data 
			| Product | Relationship |
			| C1      | C1           |
		And I click on "Continue" button 
		When I complete "ReviewRequest" task 
		Then I store the "CaseId" from LE360 
		
		Given I login to Fenergo Application with "KYCMaker: Corporate" 
		When I search for the "CaseId" 
		When I navigate to "ValidateKYCandRegulatoryGrid" task 
		When I complete "ValidateKYC" screen with key "C1" 
		And I click on "SaveandCompleteforValidateKYC" button 
		When I navigate to "EnrichKYCProfileGrid" task 
		When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
		When I complete "AddAddressFAB" task 
		Then I store the "CaseId" from LE360 
		When I complete "EnrichKYC" screen with key "C1" 
		And I click on "SaveandCompleteforEnrichKYC" button 
		When I navigate to "CaptureHierarchyDetailsGrid" task 
		When I add AssociatedParty by right clicking 
		When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
		When I complete "AssociationDetails" screen with key "Director" 
		When I complete "CaptureHierarchyDetails" task 
		When I navigate to "KYCDocumentRequirementsGrid" task 
		Then I store the "CaseId" from LE360 
	    When I add a "DocumentUpload" in KYCDocument 
		Then I complete "KYCDocumentRequirements" task 	
		When I navigate to "CompleteAMLGrid" task 
		When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"  
		Then I complete "CompleteAML" task 
		When I navigate to "CompleteID&VGrid" task 
		When I complete "CompleteID&V" task 
	    When I navigate to "CompleteRiskAssessmentGrid" task
       When I complete "RiskAssessment" task 
		Then I login to Fenergo Application with "RM:IBG-DNE" 
		When I search for the "CaseId" 
		When I navigate to "ReviewSignOffGrid" task 
		When I complete "ReviewSignOff" task 
		Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
		When I search for the "CaseId" 
		When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
		When I complete "ReviewSignOff" task 
		Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
		When I search for the "CaseId" 
		When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
		When I complete "ReviewSignOff" task 		
		Then I login to Fenergo Application with "BUH:IBG-DNE" 
		When I search for the "CaseId" 
		When I navigate to "BHUReviewandSignOffGrid" task 
		When I complete "ReviewSignOff" task 
        Then I login to Fenergo Application with "KYCMaker: Corporate" 
		When I search for the "CaseId"
		Then I store the "CaseId" from LE360
		And I complete "Waiting for UID from GLCMS" task from Actions button 
		When I navigate to "CaptureFabReferencesGrid" task 
		When I complete "CaptureFABReferences" task 
		And I assert that the CaseStatus is "Closed" 
    
    # =Initiate Regular Review
		And I initiate "Regular Review" from action button 
#		#	=Then I navigate to "CloseAssociatedCasesGrid" task 
		When I complete "CloseAssociatedCase" task 
		Then I store the "CaseId" from LE360 
        When I navigate to "ValidateKYCandRegulatoryGrid" task
        When I complete "ValidateKYC" screen with key "C1"
        And I click on "SaveandCompleteforValidateKYC" button
        When I navigate to "ReviewRequestGrid" task
        When I complete "RRReviewRequest" task
        When I navigate to "Review/EditClientDataTask" task
        And I validate the following fields in "Source Of Funds And Wealth Details" Sub Flow
      | Label                                  | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Legal Entity Source of Income & Wealth | TextBox   | true    | false    | NA        | NA         |
      | Legal Entity Source of Funds           | TextBox   | true    | false    | NA        | NA         |
    And I fill "Alphanumeric" data of length "1001" in "Legal Entity Source of Income & Wealth" field
    And I validate the error messgage for "Legal Entity Source of Income & Wealth" as "You've reached the maximum length. Legal Entity Source of Income & Wealth accepts 1000 characters."
    And I fill "Alphanumeric" data of length "995" in "Legal Entity Source of Income & Wealth" field
    And I fill "Alphanumeric" data of length "1000" in "Legal Entity Source of Income & Wealth" field
    And I fill "Alphanumeric" data of length "1001" in "Legal Entity Source of Funds" field
    And I validate the error messgage for "Legal Entity Source of Funds" as "You've reached the maximum length. Legal Entity Source of Funds accepts 1000 characters."
    And I fill "Alphanumeric" data of length "995" in "Legal Entity Source of Funds" field
    And I fill "Alphanumeric" data of length "1000" in "Legal Entity Source of Funds" field
    And I validate the following fields in "Industry Codes Details" Sub Flow
      | Label                                 | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Primary Industry of Operation Islamic | Dropdown            | true    | true     | false     | NA         |
      | Primary Industry of Operation         | Dropdown            | true    | false    | true      | NA         |
      | Primary Industry of Operation UAE     | Dropdown            | true    | false    | true      | NA         |
      | Secondary Industry of Operation       | MultiSelectDropdown | true    | false    | false     | NA         |
    And I check that below data is not visible
      | FieldLabel          |
      | NAIC                |
      | ISIN                |
      | Secondary ISIC      |
      | NACE 2 Code         |
      | Stock Exchange Code |
      | Central Index Key   |
      | SWIFT BIC           |
    And I verify "Primary Industry of Operation" drop-down values
    And I verify "Primary Industry of Operation UAE" drop-down values
    And I verify "Secondary Industry of Operation" drop-down values
    
    
    
    
    
    
    
    
    
    #=> Steps written manually.
    #Given I login to Fenergo Application with "KYCMaker: Corporate"
    #When I search for the "CaseId"
    #When I navigate to "CloseAssociatedCasesGrid" task
    #And I click on "SaveandComplete" button
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #When I navigate to "ReviewEditClientData" task
    #Source Of Funds And Wealth Details section
    #Validate in Source Of Funds And Wealth Details section the below fields are available (Label change) and values are autopopulated from the COB case
    #Validate the field type, visibility, editable and mandatory and field values are defautled from COB
    #And I validate the following fields in "Source Of Funds And Wealth Details" Sub Flow
      #| Label                                  | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo              |
      #| Legal Entity Source of Income & Wealth | String    | true    | false    | false     | Auto populated from COB |
      #| Legal Entity Source of Funds           | String    | true    | false    | false     | Auto populated from COB |
    ##
    #Field Validation for 'Legal Entity Source of Income & Wealth' field
    #Verify 'Legal Entity Source of Income & Wealth' field NOT accepts more than 1000 Alphanumeric characters. Test data : 1001 Alphanumeric characters
    #And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Legal Entity Source of Income & Wealth' field accepts less than 1000 Alphanumeric characters. Test data : 999 Alphanumeric characters
    #And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Legal Entity Source of Income & Wealth' field accepts 1000 Alphanumeric characters. Test data : 1000 Alphanumeric characters
    #And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field Validation for 'Legal Entity Source of Funds' field
    #Verify 'Legal Entity Source of Funds' field NOT accepts more than 1000 Alphanumeric characters. Test data : 1001 Alphanumeric characters
    #And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Legal Entity Source of Funds' field accepts less than 1000 Alphanumeric characters. Test data : 999 Alphanumeric characters
    #And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Legal Entity Source of Funds' field accepts 1000 Alphanumeric characters. Test data : 1000 Alphanumeric characters
    #And I fill the data for "ReviewEditClientData" with key "Data10"
    #Industry Code details section
    #Validate in Industry Codes Details section the below fields are available (Label change) and values are autopopulated from the COB case
    #Validate the field type, visibility, editable and mandatory and field values are defautled from COB
    #And I validate the following fields in "Source Of Funds And Wealth Details" Sub Flow
      #| FieldLabel                            | Field Type | Visible | Editable | Mandatory | Field Defaults To       |
      #| Primary Industry of Operation Islamic | Drop-down  | True    | False    | True      | Auto populated from COB |
      #| Primary Industry of Operation         | Drop-down  | True    | True     | True      | Auto populated from COB |
      #| Primary Industry of Operation UAE     | Drop-down  | True    | True     | False     | Auto populated from COB |
    ##
    #Validate the below new fields are available (new fields) in Industry Code Details section and values are autopopulated from COB case
    #Validate the field type, visibility, editable and mandatory and field values are defautled from COB
    #And I validate the following fields in "Industry Code Details" Sub Flow
      #| FieldLabel                      | Field Type | Visible | Editable | Mandatory | Field Defaults To       |
      #| Secondary Industry of Operation | Drop-down  | True    | True     | False     | Auto populated from COB |
    #Validate the below fields are hidden in Industry Code Details section
    #And I validate the following fields in "Industry Code Details" Sub Flow
      #| Label               | Visible |
      #| NAIC                | false   |
      #| ISIN                | false   |
      #| Secondary ISIC      | false   |
      #| NACE 2 Code         | false   |
      #| Stock Exchange Code | false   |
      #| Central Index Key   | false   |
      #| SWIFT BIC           | false   |
    ##
    #LOV validation - Primary Industry of Operation. (Refer PBI-LOV tab)
    #And I validate the LOV of "PrimaryIndustryofOperation" with key "PrimaryIndustryofOperation"
    ##
    #LOV validation - Primary Industry of Operation UAE. (Refer PBI-LOV tab)
    #And I validate the LOV of "PrimaryIndustryofOperationUAE" with key "PrimaryIndustryofOperationUAE"
    ##
    #LOV validation - Secondary Industry of Operation. (Refer PBI-LOV tab)
    #And I validate the LOV of "SecondaryIndustryofOperation" with key "SecondaryIndustryofOperation"
