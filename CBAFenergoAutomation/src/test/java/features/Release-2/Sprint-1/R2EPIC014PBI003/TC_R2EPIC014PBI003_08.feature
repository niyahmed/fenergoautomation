#Test Case: TC_R2EPIC014PBI003_08
#PBI: R2EPIC014PBI003
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2EPIC014PBI003_08 

@Automation 
Scenario:
FI:Validate the field behaviours in 'Internal Booking Details', 'Anticipated Transactional Activity (Per Month)' section and other sections of Review/Edit Client Data Screen in RR workflow 
#Addtional objective: In LE360 screen validate all th data are retained
#Additional objective: Refer back and check the data are retained in 'Validate KYC and Regulatory Date' and 'Review/Edit Client data screens
#Precondition: COB case with Client type as FI and COI as UAE to be created by filling all the mandatory and non-mandatory fields and Case status should be Closed
	Given I login to Fenergo Application with "RM:FI" 
	When I complete "NewRequest" screen with key "FI" 
	And I complete "CaptureNewRequest" with Key "FI" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "FI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "FI" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	
	Then I login to Fenergo Application with "RM:FI" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "BUH:FI" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	And I complete "Waiting for UID from GLCMS" task from Actions button 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
	#Initiate Regular Review
	And I initiate "Regular Review" from action button 
	#Then I navigate to "CloseAssociatedCasesGrid" task 
	When I complete "CloseAssociatedCase" task 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "FI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	Then I validate the following fields in "Internal Booking Details" Sub Flow 
		| Label           | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
		| Booking Country | Dropdown  | true    | false    | true      | NA         |
		| Confidential    | Dropdown  | true    | false    | true      | NA         |
	And I validate the following fields in "Internal Booking Details" Sub Flow 
		| Label                      | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
		| Target Code                | Dropdown  | true    | false    | true      | NA         |
		| Sector Description         | Dropdown  | true    | false    | NA        | NA         |
		| UID Originating Branch     | Dropdown  | true    | false    | true      | NA         |
		| Propagate To Target System | TextBox   | true    | false    | true      | NA         |
	And I check that below data is not visible 
		| FieldLabel                              |
		| Priority                                |
		| From Office Area                        |
		| On Behalf Of                            |
		| Internal DeskNACE 2 Code                |
		| Request Type                            |
		| Request Origin                          |
		| Client Reference                        |
		| Consented to Data Sharing Jurisdictions |
		| Jurisdiction                            |
	Then I verify "Booking Country" drop-down values 
	And I verify "Confidential" drop-down values 
	And I verify "Target Code" drop-down values 
	And I verify "Sector Description" drop-down values 
	And I verify "UID Originating Branch" drop-down values 
	#=>No data is added in Review Request stage. so data of COB must be present otherwise stage will not be completed.
	When I complete "RRReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "LE360overview" screen 
	And I take a screenshot 
	Given I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	And I refer back the case to "Review Client Data" stage 
	When I navigate to "CloseAssociatedCasesGrid" task 
	When I complete "CloseAssociatedCase" task 
	#=> Since no data is being entered and Save and Complete button is being pressed to show that all the data are retained.
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	When I complete "RRReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	
	
	
	#=> Below steps are written for manual testing
	#Given I login to Fenergo Application with "KYCMaker_FIG"
	#When I search for the "2386"
	#Initiate Regular Review
	#When I select "RegularReview" from Actions menu
	#When I navigate to "CloseAssociatedCases" task
	#And I click on "SaveandComplete" button
	#When I navigate to "ValidateKYCandRegulatoryGrid" task
	#When I complete "ValidateKYC" screen with key "C1"
	#And I click on "SaveandCompleteforValidateKYC" button
	#When I navigate to "ReviewEditClientData" task
	#Internal Booking Details section
	#Validate in Internal Booking Details section the below fields are available (Label change) and values are autopopulated from the COB case
	#Validate the field type, visibility, editable and mandatory and field values are defautled from COB
	#And I validate the following fields in "Source Of Funds And Wealth Details" Sub Flow
	#| FieldLabel      | Field Type | Visible | Editable | Mandatory | Field Defaults To       |
	#| Booking Country | Drop-down  | true    | true     | true      | Auto populated from COB |
	#| Confidential    | Drop-down  | true    | true     | true      | Auto populated from COB |
	##
	#Validate the below new fields are available (new fields) in Internal Booking Details section and values are autopopulated from COB case
	#Validate the field type, visibility, editable and mandatory and field values are defautled from COB
	#And I validate the following fields in "Internal Booking Details" Sub Flow
	#| FieldLabel                  | Field Type   | Visible | Editable | Mandatory | Field Defaults To              |
	#| Target Code                 | Drop-down    | true    | true     | true      | 28-FINANCIAL INSTITUTION GROUP |
	#| Sector Description          | Drop-down    | true    | true     | false     | Auto populated from COB        |
	#| UID Originating Branch      | Drop-down    | true    | true     | true      | Head Office _ BNK-100          |
	#| Propagate To Target Systems | Alphanumeric | true    | false    | true      | BNK                            |
	#Validate the below fields are hidden in Internal Booking Details section
	#And I validate the following fields in "Internal Booking Details" Sub Flow
	#| Label                                   | Visible |
	#| Priority                                | false   |
	#| From Office Area                        | false   |
	#| On Behalf Of                            | false   |
	#| Internal DeskNACE 2 Code                | false   |
	#| Request Type                            | false   |
	#| Request Origin                          | false   |
	#| Client Reference                        | false   |
	#| Consented to Data Sharing Jurisdictions | false   |
	#| Jurisdiction                            | false   |
	##
	#LOV validation - Booking Country. (Refer PBI-LOV tab)
	#And I validate the LOV of "BookingCountry" with key "BookingCountry"
	##
	#LOV validation - Confidential. (Refer PBI-LOV tab)
	#And I validate the LOV of "Confidential" with key "Confidential"
	##
	#LOV validation - Target Code. (Refer PBI-LOV tab)
	#And I validate the LOV of "TargetCode" with key "TargetCode"
	##
	#LOV validation - Sector Description. (Refer PBI-LOV tab)
	#And I validate the LOV of "SectorDescription" with key "SectorDescription"
	##
	#LOV validation - UID Originating Branch. (Refer PBI-LOV tab)
	#And I validate the LOV of "UIDOriginatingBranch" with key "UIDOriginatingBranch"
	#Anticipated Transactional Activity (Per Month) section
	#Verify 'Anticipated Transactional Activity (Per Month)' section and fields are hidden for FI client type
	##
	#Verify 'Business Markets' grid/section is hidden from the screen
	#Verify 'External Data' grid/section is hidden from the screen
	#Verify 'Trading Agreements' grid/section is hidden from the screen
	#Verify 'Relationships' section/grid is Mandatory
	#Verify 'Tax Identifier' section is NON-mandatory
	##
	#Verify the below existing grids are available
	#Addresses
	#Trading Entities
	#Products
	#External References
	#Comments
	#Associated Parties
	#Contacts
	#Roles
	#Managed Accounts
	#History
	#When I complete "ReviewEditClientData" screen with key "C1"
	#When I navigate to "KYCDocumentRequirementsGrid" task
	#When I add a "DocumentUpload" in KYCDocument
	#Then I complete "KYCDocumentRequirements" task
	#Validate the data are retained in LE360 screen
	#Then I navigate to LE360 screen
	#Refer back to Review Client Data Stage
	#When I navigate to "ValidateKYCandRegulatoryGrid" task
	#Verify all the data are retained in this screen
	#And I click on "SaveandCompleteforValidateKYC" button
	#When I navigate to "ReviewEditClientData" task
