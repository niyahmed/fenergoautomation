#Test Case: TC_R2EPIC014PBI003_09
#PBI: R2EPIC014PBI003
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2EPIC014PBI003_09

  @To_be_automated
  Scenario: NBFI:Validate the field behaviours in Customer Details section of Review/Edit Client Data Screen in RR workflow
    #Precondition: COB case with Client type as NBFI and COI as NON UAE (ex: Andora) to be created by filling all the mandatory and non-mandatory fields and Case status should be Closed
    Given I login to Fenergo Application with "KYCMaker_FIG"
    When I search for the "CaseId"
    #Initiate Regular Review
    When I select "RegularReview" from Actions menu
    When I navigate to "CloseAssociatedCases" task
    And I click on "SaveandComplete" button
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "ReviewEditClientData" task
    #Validate in customer details section the below fields are available (Label change) and values are autopopulated from the COB case
    #Validate the field type, visibility, editable and mandatory and field values are defautled from COB
    And I validate the following fields in "Customer Details" Sub Flow
      | FieldLabel                             | Field Type   | Visible | Editable | Mandatory | Field Defaults To       |
      | Client Type                            | Drop-down    | True    | False    | False     | Auto populated from COB |
      | Name of Registration Body              | Alphanumeric | True    | True     | True      | Auto populated from COB |
      | Country of Domicile/ Physical Presence | Drop-down    | True    | True     | True      | Auto populated from COB |
      | Channel & Interface                    | Drop-down    | True    | True     | True      | Auto populated from COB |
      | Trading/Operation Name                 | Alphanumeric | True    | True     | False     | Auto populated from COB |
      | Group Name                             | Alphanumeric | True    | True     | False     | Auto populated from COB |
      | FAB Segment                            | Drop-down    | True    | True     | True      | Auto populated from COB |
    #Validate the below new fields & Existing fields are available in customer details section and values are autopopulated from COB case
    #Validate the field type, visibility, editable and mandatory and field values are defautled from COB
    And I validate the following fields in "Customer Details" Sub Flow
      | FieldLabel                               | Field Type   | Visible | Editable | Mandatory | Field Defaults To       |
      | ID                                       | Alphanumeric | True    | False    | False     | Auto populated from COB |
      | Legal Entity Name                        | Alphanumeric | True    | False    | False     | Auto populated from COB |
      | Legal Entity Type                        | Drop-down    | True    | False    | False     | Auto populated from COB |
      | Entity Type                              | Drop-down    | True    | False    | False     | Auto populated from COB |
      | Legal Entity Category                    | Drop-down    | True    | False    | False     | Auto populated from COB |
      | LEI                                      | Alphanumeric | True    | True     | False     | Auto populated from COB |
      | GLEIF Legal Name                         | Alphanumeric | True    | True     | False     | Auto populated from COB |
      | GLEIF Legal Jurisdiction                 | Alphanumeric | True    | True     | False     | Auto populated from COB |
      | GLEIF Legal Form                         | Alphanumeric | True    | True     | False     | Auto populated from COB |
      | GLEIF Entity Status                      | Alphanumeric | True    | True     | False     | Auto populated from COB |
      | Country of Incorporation / Establishment | Drop-down    | True    | True     | True      | Auto populated from COB |
      | Date of Incorporation / Establishment    | Date         | True    | True     | True      | Auto populated from COB |
      | Registration Number                      | Alphanumeric | True    | True     | True      | Auto populated from COB |
      | Customer Relationship Status             | Drop-down    | True    | False    | False     | Auto populated from COB |
      | Length of Relationship                   | Drop-down    | True    | True     | True      | Auto populated from COB |
      | Does the entity have a previous name(s)? | Drop-down    | True    | True     | True      | Auto populated from COB |
      | Previous Name(s)                         | Alphanumeric | True    | True     | True      | Auto populated from COB |
      | Legal Entity Name (Parent)               | Alphanumeric | True    | True     | False     | Auto populated from COB |
      | Website Address                          | Alphanumeric | True    | True     | False     | Auto populated from COB |
      | Real Name                                |              | False   |          |           |                         |
      | Original Name                            |              | False   |          |           |                         |
      | Legal Counter party type                 | Drop-down    | True    | True     | True      | Auto populated from COB |
      | Legal Constitution Type                  | Drop-down    | True    | True     | True      | Auto populated from COB |
      | Emirate                                  |              | False   |          |           |                         |
      | Entity Level                             | Drop-down    | False   | True     | True      | Auto populated from COB |
      | SWIFT Address                            | Alphanumeric | False   | True     | True      | Auto populated from COB |
    #Validate the below fields are hidden in customer details section
    And I validate the following fields in "Customer Details" Sub Flow
      | Label                           | Visible |
      | Legal Status                    | false   |
      | Anticipated Activity of Account | false   |
      | Primary Business Location       | false   |
      | Residential Status              | false   |
    #Field Validation for 'Name of Registration Body' field
    #Verify 'Name of Registration Body' field NOT accepts more than 255 Alphanumeric characters. Test data : 256 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify Legal Entity Name field accepts less than 255 Alphanumeric characters. Test data : 254 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify Legal Entity Name field accepts 255 Alphanumeric characters. Test data : 255 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field validation for 'Trading/Operation Name' field
    #Verify 'Trading/Operation Name' field NOT accepts special characters. Test Data:Special characters
    And I fill the data for "ReviewEditClientData" with key "Data1"
    #Verify 'Trading/Operation Name' field NOT accepts lower case characters. Test Data: Lower case char
    And I fill the data for "ReviewEditClientData" with key "Data2"
    #Verify 'Trading/Operation Name' field accepts valid characters other than special and lowercase.
    And I fill the data for "ReviewEditClientData" with key "Data3"
    #Verify 'Trading/Operation Name' field NOT accepts more than 150 Alphanumeric characters. Test data : 151 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Trading/Operation Name' field accepts less than 150 Alphanumeric characters. Test data : 149 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Trading/Operation Name' field accepts 150 Alphanumeric characters. Test data : 150 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field Validation for 'Group Name' field
    #Verify 'Group Name' field NOT accepts more than 255 Alphanumeric characters. Test data : 256 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Group Name' field accepts less than 255 Alphanumeric characters. Test data : 254 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Group Name' field accepts 255 Alphanumeric characters. Test data : 255 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field validation for 'Legal Entity Name (parent)' field
    #Verify Legal Entity Name (parent) field NOT accepts special characters. Test Data:Special characters
    And I fill the data for "ReviewEditClientData" with key "Data1"
    #Verify Legal Entity Name (parent) field NOT accepts lower case characters. Test Data: Lower case char
    And I fill the data for "ReviewEditClientData" with key "Data2"
    #Verify Legal Entity Name (parent) field accepts valid characters other than special and lowercase.
    And I fill the data for "ReviewEditClientData" with key "Data3"
    #Verify 'Legal Entity Name (parent)' field NOT accepts more than 255 Alphanumeric characters. Test data : 256 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Legal Entity Name (parent)' field accepts less than 255 Alphanumeric characters. Test data : 254 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Legal Entity Name (parent)' field accepts 255 Alphanumeric characters. Test data : 255 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field validation for 'Website Address' field
    #Verify Website field does not accept duplicate value. Test Data: Enter dupliate website address
    #Verify 'Website Address' field NOT accepts more than 255 Alphanumeric characters. Test data : 256 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Website Address' field accepts less than 255 Alphanumeric characters. Test data : 254 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Website Address' field accepts 255 Alphanumeric characters. Test data : 255 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field validation for 'SWIFT Address' field
    #Verify 'Original Name' field NOT accepts more than 35 Alphanumeric characters. Test data : 66 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Original Name' field accepts less than 35 Alphanumeric characters. Test data : 34 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Original Name' field accepts 35 Alphanumeric characters. Test data : 35 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field validation for 'Date of Incorporation / Establishment' field
    #Verify 'Date of Incorporation / Establishment' field does not accepts future date.
    ##
    #LOV validation - Country of Domicile/ Physical Presence. (Refer to Countries in the PBI-LOV tab)
    And I validate the LOV of "CountryofDomicile/PhysicalPresence" with key "countrieslov"
    ##
    #LOV validation - Channel & Interface. (Refer to Channellist in the PBI-LOV tab)
    And I validate the LOV of "Channel&Interface" with key "Channellist"
    ##
    #LOV validation - Country of Incorporation / Establishment. (Refer to Countries in the PBI-LOV tab)
    And I validate the LOV of "CountryofIncorporation/Establishment" with key "countrieslov"
    ##
    #LOV validation - Length of Relationship. (Refer to LengthofRelationship in the PBI-LOV tab)
    And I validate the LOV of "LengthofRelationship" with key "LengthofRelationship"
    ##
    #LOV validation - Legal Counter party type. (Refer to Legal Counterparty Type in the PBI-LOV tab)
    And I validate the LOV of "LegalCounterpartytype" with key "LegalCounterpartyType"
    ##
    #LOV validation - Legal Constitution Type. (Refer to Legal Constitution Type in the PBI-LOV tab)
    And I validate the LOV of "LegalConstitutionType" with key "LegalConstitutionType"
    ##
    #LOV validation - Entity Level. (Refer PBI-LOV tab)
    And I validate the LOV of "EntityLevel" with key "EntityLevel"
    ##
    #LOV validation - FAB Segment. (Refer FABsegmentcode&desc in the PBI-LOV tab)
    And I validate the LOV of "FABSegment" with key "FABSegment"
