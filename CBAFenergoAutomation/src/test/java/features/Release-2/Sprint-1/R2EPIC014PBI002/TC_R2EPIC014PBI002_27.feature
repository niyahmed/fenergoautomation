#Test Case: TC_R2EPIC014PBI002_27
#PBI: R2EPIC014PBI002
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC014PBI002_27


  @To_be_automated
  Scenario: Validate End to End regular review workflow(deletion of data in sub-flow) for risk rating "Medium-low" and for client type "Corporate"
    #Test-data: Login and Creating an Onboarding flow
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddress" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I add "AssociatedParty" via express addition
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydKYC"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydAVP"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BusinessUnitHead"
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    # Initiate regular Review workflow
    When I navigate to 'LE360- LE details' screen
    When I Click on 'Actions' button and select 'RegularReview' workflow
    # Verify Regular review case has been triggered
    Then I see 'RegularReview' Workflow has been triggered
    # Verify 'Close Associated Cases' task has been triggered
    And I navigated to 'CloseAssociatedCases' task
    Then I complete 'CloseAssociatedCases' task
    # Verify 'Validate KYC and Regulatory Grid' task has been triggered
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    Then I complete "ValidateKYCandRegulatoryGrid" task
     # Verify 'Review request Details' task has been triggered
    When I navigate to "ReviewrequestDetails" task
    Then I complete "ReviewrequestDetails" task
    # Verify 'Review/edit client data' task has been triggered
    When I navigate to "Review/edit client data" task
    # Delete and add new Contact to 'Review/edit client data' task screen
    #verify deleted contact is not getting displayed under grid
    When I Click on �Remove� button displaying at the top of 'Contacts' grid
    Then I see added contact is removed from contact grid
    When I Click on + button displaying at the top of 'Contacts' grid
    When I navigate to 'AddContacts' task screen and save the details
    Then I see Address is added under 'Contacts' grid
    # Delete and add new Associated Party to 'Review/edit client data' task screen
    #verify deleted Associated Party is not getting displayed under grid
    When I Click on �Remove� button displaying at the top of 'AssociatedParties' grid
    When I Click on + button displaying at the top of "AssociatedParties grid"
    When I navigate to 'Addassociatedparties' task screen and save the details
    Then I see AssociatedParty is added under 'AssociatedParties' grid
    When I complete "Review/editClientData" task
    # Verify 'KYC Document requirement' task has been triggered
    When I navigate to "KYCDocumentrequirement" task
    Then I complete "KYCDocumentrequirement" task
    # Verify 'Complete AML' task has been triggered
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key       "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
When I navigate to "CompleteAMLGrid" task
When I Initiate "Googlescreening" by rightclicking
    And I complete "Googlescreening" from assessment grid with Key       "Googlescreening"
    Then I complete "CompleteAML" task
    # Verify 'Complete ID&V' task has been triggered
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Select the Risk category as "Medium-low" and complete "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task with 'medium-low' risk rating
    #Verify "Relationship Manager Review SignOff' task is generated
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "RelationshipManagerReviewSignOffGrid" task
    When I complete "RelationshipManagerReviewSignOffGrid" task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task with key "RefertoRiskAssessment"
    And I click on "Submit" button
    #Validate the case is referred to "RiskAssessmentFAB" stage
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "CaptureRiskCategoryGrid" task with 'Medium' risk rating
    #Verify "Relationship Manager Review SignOff' task is generated
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "RelationshipManagerReviewSignOffGrid" task
    When I complete "RelationshipManagerReviewSignOffGrid" task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task with key "RefertoRiskAssessment"
    And I click on "Submit" button
    #Verify 'Business Unit Head Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "Business Unit Head (N3)"
    When I search for the "CaseId"
    When I navigate to "BusinessUnitHeadReviewandSign-Off" task
    When I complete "BusinessUnitHeadReviewandSign-Off" task
    #Verify 'Capture FAB References' task is generated
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "CaptureFABReferences" task
    When I complete "CaptureFABReferences" task
    And I Assert case status as 'Closed'



