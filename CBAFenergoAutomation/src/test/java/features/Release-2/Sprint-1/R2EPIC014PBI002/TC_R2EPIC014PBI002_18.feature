#Test Case: TC_R2EPIC014PBI002_18
#PBI: R2EPIC014PBI002
#User Story ID: Corp/PCG-9, FIG-9
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Regular Review

  Scenario: Validate if "Quality Control task" is removed for RR workflow for all client types
  #Dummy test case validate if the for RR workflow for all client types. This can be passed once RR end-end test cases are complete
