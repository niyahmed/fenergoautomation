#Test Case: TC_R2EPIC014PBI002_15
#PBI: R2EPIC014PBI002
#User Story ID: Corp/PCG-7, FIG-7
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Regular Review

  Scenario: Non Banking Financial Institution (NBFI):Validate if the data entered in "Review Request" task is getting saved on completing the task in the task screen while referring back and  in the LE360 screen
    #Precondition: Closed COB case with Client type as Corporte with Country of Incorpoation as "UAE" and data for non mandatory subflows Tax Identifier, Trading Entities and Contacts
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    #Initiate Regular Review
    When I select "RegularReview" from Actions menu
    When I navigate to "CloseAssociatedCases" task
    And I click on "SaveandComplete" button
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    When I navigate to "Review Request Details" task
    #Make sure data is filled for all fields including Non-Mandatory fields
    When I complete "ReviewRequest" screen with key "C3"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "ReviewEditClientData" task
    When I complete "ReviewEditClientData" screen with key "C3"
    When I navigate to "KYCDocumentRequirements" task
    When I complete "KYCDocumentRequirements" screen with key "C3"
    #Refer back
    Then I refer back to "Review Client Data" stage
    And I click on "SaveandComplete" button # "CloseAssociatedCases" task
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    When I navigate to "Review Request Details" task
    #Validate data is not wiped of for any fields
    And I validate data in the following fields in "Customer Details" panel
      | FieldLabel                                                 |
      | ID                                                         |
      | Client Type                                                |
      | Legal Entity Name                                          |
      | Legal Entity Type                                          |
      | Entity Type                                                |
      | Legal Entity Category                                      |
      | Country of Incorporation / Establishment                   |
      | Country of Domicile/ Physical Presence                     |
      | Date of Incorporation                                      |
      | Channel & Interface                                        |
      | Purpose of Account/Relationship                            |
      | Residential Status                                         |
      | Relationship with bank                                     |
      | CIF Creation Required?                                     |
      | Emirate                                                    |
    And I validate the data in following fields in "Internal Booking Details" panel
      | FieldLabel                              |
      | Request Type                            |
      | Request Origin                          |
      | Client Reference                        |
      | Entity of Onboarding Booking Country    |
      | Consented to Data Sharing Jurisdictions |
      | Confidential                            |
      | Jurisdiction                            |
      | Target Code                             |
      | Sector Description                      |
      | UID Originating Branch                  |
      | Propagate To Target Systems             |
    And I validate data in the  following subflows
      | Tax Identifier   |
      | Trading Entities |
      | Products         |
      | Relationships    |
      | Addresses        |
      | Contacts         |
      | Documents        |
    #Validate in LE360 screen
    When I naviage to LE360 screen
    And validate the data for all the above fields
