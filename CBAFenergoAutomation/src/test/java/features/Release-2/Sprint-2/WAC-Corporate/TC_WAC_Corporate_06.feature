#Test Case: TC_WAC_Corporate_06
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Sasmita
#Last Edited by: Sasmita
Feature: TC_WAC_Corporate_06

  Scenario: COB-Derive Overall Risk Rating as Very High and all individual attributes risk rating as Very High
    #Refer to TC06 in the WAC Corp Data Sheet