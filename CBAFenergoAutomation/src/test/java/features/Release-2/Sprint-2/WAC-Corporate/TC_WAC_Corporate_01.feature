#Test Case: TC_WAC_Corporate_01
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corporate_01

  Scenario: COB-Derive Overall Risk Rating as Low and all individual attributes risk rating as LOW
    #Refer to TC01 in the WAC Corp Data Sheet