#Test Case: TC_WAC_Corporate_08
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Sasmita
#Last Edited by: Sasmita
Feature: TC_WAC_Corporate_08

  Scenario: COB-Derive Overall Risk Rating as Medium-Low and all individual attributes with different risk rating 
    #Refer to TC08 in the WAC Corp Data Sheet