#Test Case: TC_WAC_Corporate_13
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corporate_13

  Scenario:Validate adding multiple layer (5 layers) associated parites (Individual / Non-Individual). Adding Non-Individual AP with UBO relationship (Medium score) in the fifth layer.
    #Refer to TC13 in the WAC Corp Data Sheet