#Test Case: TC_WAC_Corporate_11
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corporate_11

  Scenario: Validate adding multiple associated parites (Individual / Non-Individual) in first layer, Non-Individual AP with any relationship (high score) and Individual AP with UBO (lowscore) and check system is considering the high risk AP for risk calculation.
    #Refer to TC11 in the WAC Corp Data Sheet