#Test Case: TC_WAC_Corporate_15
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corporate_15

  Scenario: COB-Refer back and derive the overrall risk rating as Very High (previous risk rating Low)
    #Refer to TC15 in the WAC Corp Data Sheet