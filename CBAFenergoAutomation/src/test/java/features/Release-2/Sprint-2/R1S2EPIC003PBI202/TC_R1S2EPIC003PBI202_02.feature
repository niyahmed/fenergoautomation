#Test Case: TC_R1S2EPIC003PBI202_02
#PBI: R1S2EPIC003PBI202
#User Story ID: NA
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S2EPIC003PBI202_02

  @Automation
  Scenario: BBG-Validate the field behaviour in Complete Risk Assessment screen for KYC maker User, CDD User and KYC Manager User
    #Additional Scenario: Verify stage name = Risk assessment, task name = Risk Assessment NonIndividual, Screen name = Complete Risk Assessment
    #Additional Scenario: KYC Maker user assigns the Complete Risk Assessment task to KYC Manager and CDD users
    #Precondition: Input the appropriate data to get Low Risk rating in Complete Risk Assessment screen
    Given I login to Fenergo Application with "RM:BBG"
    When I complete "NewRequest" screen with key "BBG"
    And I complete "CaptureNewRequest" with Key "BBG" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "BBG"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "BBG"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I store the "CaseId" from LE360
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task 
		When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
		Then I complete "CompleteAML" task 
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
       
    When I navigate to "CompleteRiskAssessmentGrid" task
    Then I store the "CaseId" from LE360
    And I click on "LegalEntityDetailsSubflowText" button
    And I validate the following fields in "Legal Entity Details" Sub Flow
      | Label         | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Legal Entity  | NA        | true    | NA       | NA        | NA         | 
      | LE_Name       | NA        | false   | NA       | NA        | NA         | 
      | LegalEntityId | NA        | false   | NA       | NA        | NA         | 
    And I validate the following fields in "Country Risk" Sub Flow with dataKey
      | Label                                               | DataKey                           | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Country of Incorporation / Establishment:           | CountryOfIncorporation            | Dropdown            | true    | true     | NA        | NA         | 
      | Country of Domicile / Physical Presence:            | CountryOfDomicile                 | Dropdown            | true    | true     | NA        | NA         | 
      | Countries of Business Operations/Economic Activity: | FAB_PrincipalPlaceBusinessId      | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Association Country Risk                            | NA                                | NA                  | true    | NA       | NA        | NA         | 
      | Non Individual                                      | NA                                | NA                  | true    | NA       | NA        | NA         | 
      | Country of Incorporation / Establishment            | CountryOfIncorporationAssociation | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Country of Domicile / Physical Presence             | CountryOfDomicileAssociation      | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Individual                                          | NA                                | NA                  | true    | NA       | NA        | NA         | 
      | Nationality / Other Nationality                     | NA                                | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Country Of Residence                                | NA                                | MultiSelectDropdown | true    | true     | NA        | NA         |  
    And I validate the following fields in "Customer Type" Sub Flow with dataKey
      | Label                                    | DataKey  | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Legal Entity Type:                       | NA       | Dropdown            | true    | true     | NA        | NA         | 
      | Types of Shares (Bearer/Registered):     | NA       | Dropdown            | true    | true     | NA        | NA         | 
      | Length of Relationship:                  | NA       | Dropdown            | true    | true     | NA        | NA         | 
      | Industry (Primary/Secondary):            | NA       | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Main Entity / Association Screening Risk | NA       | NA                  | true    | NA       | NA        | NA         | 
      | Adverse Media Category                   | NA       | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Sanctions Category                       | NA       | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | PEP Category                             | NA       | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | FAB Internal Watch List Category         | NA       | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Notes                                    | L2A_Note | TextArea            | true    | false    | NA        | NA         |
    And I validate the following fields in "Product Risk" Sub Flow with dataKey
      | Label         | DataKey  | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Product Type: | NA       | TextBox   | true    | true     | NA        | NA         | 
      | Notes:        | L2C_Note | TextArea  | true    | false    | NA        | NA         | 
    And I validate the following fields in "Transaction Profile" Sub Flow with dataKey
      | Label                                              | DataKey  | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Anticipated Transactions Turnover (Annual in AED): | NA       | Dropdown  | true    | true     | NA        | NA         | 
      | Notes:                                             | L2Z_Note | TextArea  | true    | false    | NA        | NA         | 
    And I validate the following fields in "Channel and Interface" Sub Flow with dataKey
      | Label                | DataKey  | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Channel & Interface: | NA       | Dropdown  | true    | true     | NA        | NA         | 
      | Notes:               | L2Y_Note | TextArea  | true    | false    | NA        | NA         | 
    
    And I validate the following fields in "Risk Assessment" Sub Flow with dataKey
      | Label        | DataKey                        			    | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Risk Rating: | RiskRatingDropDown_RiskCategory_Override | Dropdown  | true    | true     | NA        | NA         | 
      | Risk Model:  | NA                          					    | TextBox   | true    | true     | NA        | NA         | 
      | Notes        | DerivedRiskCategoryItemNotes 				    | TextArea  | true    | false    | NA        | NA         |  
    And I click on "Case Details" button
    When I assign the task "Complete Risk Assessment" to role group "CIB R&C KYC APPROVER - KYC Manager" and user name "LastName, KYCManager"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CompleteRiskAssessmentGrid" task
    
    And I click on "LegalEntityDetailsSubflowText" button
    And I validate the following fields in "Legal Entity Details" Sub Flow
      | Label         | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Legal Entity  | NA        | true    | NA       | NA        | NA         | 
      | LE_Name       | NA        | false   | NA       | NA        | NA         | 
      | LegalEntityId | NA        | false   | NA       | NA        | NA         | 
    And I validate the following fields in "Country Risk" Sub Flow with dataKey
      | Label                                               | DataKey                                   | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Country of Incorporation / Establishment:           | CountryOfIncorporation                    | Dropdown            | true    | true     | NA        | NA         | 
      | Risk Rating:                                        | CountryOfIncorporation_RiskCategory       | Dropdown            | true    | true     | NA        | NA         | 
      | Country of Domicile / Physical Presence:            | CountryOfDomicile                         | Dropdown            | true    | true     | NA        | NA         | 
      | Risk Rating:                                        | CountryOfDomicile_RiskCategory            | Dropdown            | true    | true     | NA        | NA         | 
      | Countries of Business Operations/Economic Activity: | FAB_PrincipalPlaceBusinessId              | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Risk Rating:                                        | FAB_PrincipalPlaceBusinessId_RiskCategory | Dropdown            | true    | true     | NA        | NA         | 
      | Association Country Risk                            | NA                                        | NA                  | true    | NA       | NA        | NA         | 
      | Non Individual                                      | NA                                        | NA                  | true    | NA       | NA        | NA         | 
      | Country of Incorporation / Establishment            | CountryOfIncorporationAssociation         | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Country of Domicile / Physical Presence             | CountryOfDomicileAssociation              | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Individual                                          | NA                                        | NA                  | true    | NA       | NA        | NA         | 
      | Nationality / Other Nationality                     | NA                                        | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Country Of Residence                                | NA                                        | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Risk Rating:                                        | AssociationCountriesRisk_RiskCategory     | Dropdown            | true    | true     | NA        | NA         | 
  
    And I validate the following fields in "Customer Type" Sub Flow with dataKey
      | Label                                    | DataKey                                     | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Legal Entity Type:                       | NA                                          | Dropdown            | true    | true     | NA        | NA         | 
      | Risk Rating:                             | SubTypeId_RiskCategory                      | Dropdown            | true    | true     | NA        | NA         | 
      | Types of Shares (Bearer/Registered):     | NA                                          | Dropdown            | true    | true     | NA        | NA         | 
      | Risk Rating:                             | LECompany_CanIssueBearerShares_RiskCategory | Dropdown            | true    | true     | NA        | NA         | 
      | Length of Relationship:                  | NA                                          | Dropdown            | true    | true     | NA        | NA         | 
      | Risk Rating:                             | FABLE_LengthofRelationship_RiskCategory     | Dropdown            | true    | true     | NA        | NA         | 
      | Industry (Primary/Secondary):            | NA                                          | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Risk Rating:                             | FAB_PrimaryIndustryOfOperation_RiskCategory | Dropdown            | true    | true     | NA        | NA         | 
      | Main Entity / Association Screening Risk | NA                                          | NA                  | true    | NA       | NA        | NA         | 
      | Adverse Media Category                   | NA                                          | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Sanctions Category                       | NA                                          | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | PEP Category                             | NA                                          | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | FAB Internal Watch List Category         | NA                                          | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Risk Rating:                             | ScreeningRiskCategories_RiskCategory        | Dropdown            | true    | true     | NA        | NA         | 
      | Notes                                    | L2A_Note                                    | TextArea            | true    | false    | NA        | NA         | 
    And I validate the following fields in "Product Risk" Sub Flow with dataKey
      | Label         | DataKey                          | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Product Type: | NA                               | TextBox   | true    | true     | NA        | NA         | 
      | Risk Rating:  | ProductTypesDisplay_RiskCategory | Dropdown  | true    | true     | NA        | NA         | 
      | Notes:        | L2C_Note                         | TextArea  | true    | false    | NA        | NA         |    
    And I validate the following fields in "Transaction Profile" Sub Flow with dataKey
      | Label                                              | DataKey                                          | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Anticipated Transactions Turnover (Annual in AED): | NA                                               | Dropdown  | true    | true     | NA        | NA         | 
      | Risk Rating:                                       | FAB_AnticipatedTransactionsTurnover_RiskCategory | Dropdown  | true    | true     | NA        | NA         | 
      | Notes                                              | L2Z_Note                                         | TextArea  | true    | false    | NA        | NA         | 
    And I validate the following fields in "Channel and Interface" Sub Flow with dataKey      
      | Label                | DataKey                  | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Channel & Interface: | NA                       | Dropdown  | true    | true     | NA        | NA         | 
      | Risk Rating:         | FAB_Channel_RiskCategory | Dropdown  | true    | true     | NA        | NA         | 
      | Notes                | L2Y_Note                 | TextArea  | true    | false    | NA        | NA         | 
    And I validate the following fields in "Risk Assessment" Sub Flow with dataKey
      | Label        | DataKey                      			      | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Risk Rating: | RiskRatingDropDown_RiskCategory_Override | Dropdown  | true    | true     | NA        | NA         | 
      | Risk Model:  | NA                         			 	      | TextBox   | true    | true     | NA        | NA         | 
      | Notes        | DerivedRiskCategoryItemNotes 				    | TextArea  | true    | false    | NA        | NA         | 

    And I click on "Case Details" button
    When I assign the task "Complete Risk Assessment" to role group "Group Compliance (CDD)" and user name "LastName, CDD"
    
    Then I login to Fenergo Application with "Group Compliance (CDD)"
    When I search for the "CaseId"
    When I navigate to "CompleteRiskAssessmentGrid" task
    And I click on "LegalEntityDetailsSubflowText" button
    And I validate the following fields in "Legal Entity Details" Sub Flow
      | Label         | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Legal Entity  | NA        | true    | NA       | NA        | NA         | 
      | LE_Name       | NA        | false   | NA       | NA        | NA         | 
      | LegalEntityId | NA        | false   | NA       | NA        | NA         | 
    And I validate the following fields in "Country Risk" Sub Flow with dataKey
      | Label                                               | DataKey                                   | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Country of Incorporation / Establishment:           | CountryOfIncorporation                    | Dropdown            | true    | true     | NA        | NA         | 
      | Risk Rating:                                        | CountryOfIncorporation_RiskCategory       | Dropdown            | true    | true     | NA        | NA         | 
      | Country of Domicile / Physical Presence:            | CountryOfDomicile                         | Dropdown            | true    | true     | NA        | NA         |  
      | Countries of Business Operations/Economic Activity: | FAB_PrincipalPlaceBusinessId              | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Association Country Risk                            | NA                                        | NA                  | true    | NA       | NA        | NA         | 
      | Non Individual                                      | NA                                        | NA                  | true    | NA       | NA        | NA         | 
      | Country of Incorporation / Establishment            | CountryOfIncorporationAssociation         | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Country of Domicile / Physical Presence             | CountryOfDomicileAssociation              | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Individual                                          | NA                                        | NA                  | true    | NA       | NA        | NA         | 
      | Nationality / Other Nationality                     | NA                                        | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Country Of Residence                                | NA                                        | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Risk Rating:                                        | AssociationCountriesRisk_RiskCategory     | Dropdown            | true    | true     | NA        | NA         | 
  
    And I validate the following fields in "Customer Type" Sub Flow with dataKey
      | Label                                    | DataKey                                     | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Legal Entity Type:                       | NA                                          | Dropdown            | true    | true     | NA        | NA         | 
      | Risk Rating:                             | SubTypeId_RiskCategory                      | Dropdown            | true    | true     | NA        | NA         | 
      | Types of Shares (Bearer/Registered):     | NA                                          | Dropdown            | true    | true     | NA        | NA         | 
      | Risk Rating:                             | LECompany_CanIssueBearerShares_RiskCategory | Dropdown            | true    | true     | NA        | NA         | 
      | Length of Relationship:                  | NA                                          | Dropdown            | true    | true     | NA        | NA         | 
      | Risk Rating:                             | FABLE_LengthofRelationship_RiskCategory     | Dropdown            | true    | true     | NA        | NA         | 
      | Industry (Primary/Secondary):            | NA                                          | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Risk Rating:                             | FAB_PrimaryIndustryOfOperation_RiskCategory | Dropdown            | true    | true     | NA        | NA         | 
      | Main Entity / Association Screening Risk | NA                                          | NA                  | true    | NA       | NA        | NA         | 
      | Adverse Media Category                   | NA                                          | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Sanctions Category                       | NA                                          | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | PEP Category                             | NA                                          | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | FAB Internal Watch List Category         | NA                                          | MultiSelectDropdown | true    | true     | NA        | NA         | 
      | Risk Rating:                             | ScreeningRiskCategories_RiskCategory        | Dropdown            | true    | true     | NA        | NA         | 
      | Notes                                    | L2A_Note                                    | TextArea            | true    | false    | NA        | NA         | 
    And I validate the following fields in "Product Risk" Sub Flow with dataKey
      | Label         | DataKey                          | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Product Type: | NA                               | TextBox   | true    | true     | NA        | NA         | 
      | Risk Rating:  | ProductTypesDisplay_RiskCategory | Dropdown  | true    | true     | NA        | NA         | 
      | Notes:        | L2C_Note                         | TextArea  | true    | false    | NA        | NA         |  
    And I validate the following fields in "Transaction Profile" Sub Flow with dataKey
      | Label                                              | DataKey                                          | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Anticipated Transactions Turnover (Annual in AED): | NA                                               | Dropdown  | true    | true     | NA        | NA         | 
      | Risk Rating:                                       | FAB_AnticipatedTransactionsTurnover_RiskCategory | Dropdown  | true    | true     | NA        | NA         | 
      | Notes                                              | L2Z_Note                                         | TextArea  | true    | false    | NA        | NA         | 
    And I validate the following fields in "Channel and Interface" Sub Flow with dataKey      
      | Label                | DataKey                  | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Channel & Interface: | NA                       | Dropdown  | true    | true     | NA        | NA         | 
      | Risk Rating:         | FAB_Channel_RiskCategory | Dropdown  | true    | true     | NA        | NA         | 
      | Notes                | L2Y_Note                 | TextArea  | true    | false    | NA        | NA         | 
    And I validate the following fields in "Risk Assessment" Sub Flow with dataKey
      | Label        | DataKey                        			    | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Risk Rating: | RiskRatingDropDown_RiskCategory_Override | Dropdown  | true    | true     | NA        | NA         | 
      | Risk Model:  | NA                          					    | TextBox   | true    | true     | NA        | NA         | 
      | Notes        | DerivedRiskCategoryItemNotes 				    | TextArea  | true    | false    | NA        | NA         | 
      
    
 #	==========================================================
 #	Below Steps are written for manual testing
    #Given I login to Fenergo Application with "RM:BBG"
    #When I complete "NewRequest" screen with key "BBG"
    #And I complete "CaptureNewRequest" with Key "C1" and below data
      #| Product | Relationship |
      #| C1      | C1           |
    #And I click on "Continue" button
    #When I complete "ReviewRequest" task
    #Then I store the "CaseId" from LE360
    #Given I login to Fenergo Application with "KYCMaker: BBG"
    #When I search for the "CaseId"
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #When I navigate to "EnrichKYCProfileGrid" task
    #When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    #When I complete "AddAddressFAB" task
    #Then I store the "CaseId" from LE360
    #When I complete "EnrichKYC" screen with key "C1"
    #And I click on "SaveandCompleteforEnrichKYC" button
    #When I navigate to "CaptureHierarchyDetailsGrid" task
    #When I add AssociatedParty by right clicking
    #When I add "AssociatedParty" via express addition
    #When I complete "AssociationDetails" screen with key "Director"
    #When I complete "CaptureHierarchyDetails" task
    #When I navigate to "KYCDocumentRequirementsGrid" task
    #Then I store the "CaseId" from LE360
    #When I add a "DocumentUpload" in KYCDocument
    #Then I complete "KYCDocumentRequirements" task
    #When I navigate to "CompleteAMLGrid" task
    #When I Initiate "Fircosoft" by rightclicking
    #And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    #And I click on "SaveandCompleteforAssessmentScreen1" button
    #Then I complete "CompleteAML" task
    #When I navigate to "CompleteID&VGrid" task
    #When I complete "CompleteID&V" task
    #When I navigate to "CaptureRiskCategoryGrid" task
    #Verify KYC Maker NOT able to view the scores for each risk attributes. Able to view overrall score
    #Verify stage name = Risk assessment, task name = Risk Assessment NonIndividual, Screen name = Complete Risk Assessment
    #And I validate the following fields in "Legal Entity Details" Sub Flow
      #| Label         | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Legal Entity  | Hyperlink    | true    | true     | false     | NA         |
      #| LE_Name       | Alphanumeric | true    | true     | false     | NA         |
      #| LegalEntityId | Alphanumeric | true    | true     | false     | NA         |
    #And I validate the following fields in "Country Risk" Sub Flow
      #| Label                                             | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Country of Incorporation / Establishment          | Dropdown  | true    | true     | false     | NA         |
      #| Country of Domicile/ Physical Presence            | Dropdown  | true    | true     | false     | NA         |
      #| Country of Business Operations/ Economic Activity | Dropdown  | true    | true     | false     | NA         |
      #| Associaton Country Risk                           | NA        | true    | true     | false     | NA         |
      #| Non Individual                                    | NA        | true    | true     | false     | NA         |
      #| Country of Incorporation / Establishment          | Dropdown  | true    | true     | false     | NA         |
      #| Country of Domicile/ Physical Presence            | Dropdown  | true    | true     | false     | NA         |
      #| Country of Business Operations/ Economic Activity | Dropdown  | true    | true     | false     | NA         |
      #| Individual                                        | NA        | true    | true     | false     | NA         |
      #| Nationality/Other Nationality                     | Dropdown  | true    | true     | false     | NA         |
      #| Residential Status                                | Dropdown  | true    | true     | false     | NA         |
    #And I validate the following fields in "Customer Type" Sub Flow
      #| Label                                  | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Legal Entity Type                      | Dropdown     | true    | true     | false     | NA         |
      #| Types of Shares (Bearer/Registered)    | Dropdown     | true    | true     | false     | NA         |
      #| Length of Relationship                 | Alphanumeric | true    | true     | false     | NA         |
      #| Industry (Primary/Secondary)           | Dropdown     | true    | true     | false     | NA         |
      #| Main Entity/Association Screening Risk | NA           | true    | true     | false     | NA         |
      #| Adverse Media Category                 | Dropdown     | true    | true     | false     | NA         |
      #| Sanctions Category                     | Dropdown     | true    | true     | false     | NA         |
      #| PEP Category                           | Dropdown     | true    | true     | false     | NA         |
      #| FAB Internal Watch List Category       | Dropdown     | true    | true     | false     | NA         |
      #| Notes                                  | Alphanumeric | true    | false    | false     | NA         |
    #And I validate the following fields in "Product Risk" Sub Flow
      #| Label        | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Product Type | Dropdown     | true    | true     | false     | NA         |
      #| Notes        | Alphanumeric | true    | false    | false     | NA         |
    #And I validate the following fields in "Transaction Profile" Sub Flow
      #| Label                                             | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Anticipated Transactions Turnover (Annual in AED) | Dropdown     | true    | true     | false     | NA         |
      #| Notes                                             | Alphanumeric | true    | false    | false     | NA         |
    #And I validate the following fields in "Channel and Interface" Sub Flow
      #| Label                 | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Channel and Interface | Dropdown     | true    | true     | false     | NA         |
      #| Notes                 | Alphanumeric | true    | false    | false     | NA         |
    #Verify pencil icon (override) is not available for KYC Maker user
    #Verify the Final risk is autopopulated
    #And I validate the following fields in "Risk Assessment" Sub Flow
      #| Label              | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Risk Rating        | Dropdown     | true    | true     | false     | NA         |
      #| Risk Model         | Alphanumeric | true    | true     | false     | NA         |
      #| Risk Model Version | Alphanumeric | true    | true     | false     | NA         |
      #| Notes              | Alphanumeric | true    | false    | false     | NA         |
    #Verify 'Save for Later' and 'Continue' buttons are available and enabled by default
    #KYC Maker user assigns the Complete Risk Assessment task to KYC Manager
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    #When I navigate to "CaptureRiskCategoryGrid" task
    #Verify KYC Manager able to view the scores for each risk attributes
    #And I validate the following fields in "Legal Entity Details" Sub Flow
      #| Label         | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Legal Entity  | Hyperlink    | true    | true     | false     | NA         |
      #| LE_Name       | Alphanumeric | true    | true     | false     | NA         |
      #| LegalEntityId | Alphanumeric | true    | true     | false     | NA         |
    #And I validate the following fields in "Country Risk" Sub Flow
      #| Label                                             | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Country of Incorporation / Establishment          | Dropdown  | true    | true     | false     | NA         |
      #| Risk Rating                                       | Dropdown  | true    | true     | false     | NA         |
      #| Country of Domicile/ Physical Presence            | Dropdown  | true    | true     | false     | NA         |
      #| Risk Rating                                       | Dropdown  | true    | true     | false     | NA         |
      #| Country of Business Operations/ Economic Activity | Dropdown  | true    | true     | false     | NA         |
      #| Risk Rating                                       | Dropdown  | true    | true     | false     | NA         |
      #| Associaton Country Risk                           | NA        | true    | true     | false     | NA         |
      #| Non Individual                                    | NA        | true    | true     | false     | NA         |
      #| Country of Incorporation / Establishment          | Dropdown  | true    | true     | false     | NA         |
      #| Country of Domicile/ Physical Presence            | Dropdown  | true    | true     | false     | NA         |
      #| Country of Business Operations/ Economic Activity | Dropdown  | true    | true     | false     | NA         |
      #| Individual                                        | NA        | true    | true     | false     | NA         |
      #| Nationality/Other Nationality                     | Dropdown  | true    | true     | false     | NA         |
      #| Residential Status                                | Dropdown  | true    | true     | false     | NA         |
      #| Risk Rating                                       | Dropdown  | true    | true     | false     | NA         |
    #And I validate the following fields in "Customer Type" Sub Flow
      #| Label                                  | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Legal Entity Type                      | Dropdown     | true    | true     | false     | NA         |
      #| Risk Rating                            | Dropdown     | true    | true     | false     | NA         |
      #| Types of Shares (Bearer/Registered)    | Dropdown     | true    | true     | false     | NA         |
      #| Risk Rating                            | Dropdown     | true    | true     | false     | NA         |
      #| Length of Relationship                 | Alphanumeric | true    | true     | false     | NA         |
      #| Risk Rating                            | Dropdown     | true    | true     | false     | NA         |
      #| Industry (Primary/Secondary)           | Dropdown     | true    | true     | false     | NA         |
      #| Risk Rating                            | Dropdown     | true    | true     | false     | NA         |
      #| Main Entity/Association Screening Risk | NA           | true    | true     | false     | NA         |
      #| Adverse Media Category                 | Dropdown     | true    | true     | false     | NA         |
      #| Sanctions Category                     | Dropdown     | true    | true     | false     | NA         |
      #| PEP Category                           | Dropdown     | true    | true     | false     | NA         |
      #| FAB Internal Watch List Category       | Dropdown     | true    | true     | false     | NA         |
      #| Risk Rating                            | Dropdown     | true    | true     | false     | NA         |
      #| Notes                                  | Alphanumeric | true    | false    | false     | NA         |
    #And I validate the following fields in "Product Risk" Sub Flow
      #| Label        | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Product Type | Dropdown     | true    | true     | false     | NA         |
      #| Risk Rating  | Dropdown     | true    | true     | false     | NA         |
      #| Notes        | Alphanumeric | true    | false    | false     | NA         |
    #And I validate the following fields in "Transaction Profile" Sub Flow
      #| Label                                             | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Anticipated Transactions Turnover (Annual in AED) | Dropdown     | true    | true     | false     | NA         |
      #| Notes                                             | Alphanumeric | true    | false    | false     | NA         |
    #And I validate the following fields in "Channel and Interface" Sub Flow
      #| Label                 | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Channel and Interface | Dropdown     | true    | true     | false     | NA         |
      #| Risk Rating           | Dropdown     | true    | true     | false     | NA         |
      #| Notes                 | Alphanumeric | true    | false    | false     | NA         |
    #Verify the Final risk is autopopulated
    #verify the pencil icon is available to override the risk
    #And I validate the following fields in "Risk Assessment" Sub Flow
      #| Label                | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Risk Rating          | Dropdown     | true    | true     | false     | NA         |
      #| Override Risk Rating | Dropdown     | true    | false    | false     | NA         |
      #| Risk Model           | Alphanumeric | true    | true     | false     | NA         |
      #| Risk Model Version   | Alphanumeric | true    | true     | false     | NA         |
      #| Notes                | Alphanumeric | true    | false    | false     | NA         |
    #KYC Maker user assigns the Complete Risk Assessment task to CDD user
    #Verify CDD user able to view the scores for each risk attributes
    #Then I login to Fenergo Application with "CDD"
    #When I navigate to "CaptureRiskCategoryGrid" task
    #And I validate the following fields in "Legal Entity Details" Sub Flow
      #| Label         | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Legal Entity  | Hyperlink    | true    | true     | false     | NA         |
      #| LE_Name       | Alphanumeric | true    | true     | false     | NA         |
      #| LegalEntityId | Alphanumeric | true    | true     | false     | NA         |
    #And I validate the following fields in "Country Risk" Sub Flow
      #| Label                                             | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Country of Incorporation / Establishment          | Dropdown  | true    | true     | false     | NA         |
      #| Risk Rating                                       | Dropdown  | true    | true     | false     | NA         |
      #| Country of Domicile/ Physical Presence            | Dropdown  | true    | true     | false     | NA         |
      #| Risk Rating                                       | Dropdown  | true    | true     | false     | NA         |
      #| Country of Business Operations/ Economic Activity | Dropdown  | true    | true     | false     | NA         |
      #| Risk Rating                                       | Dropdown  | true    | true     | false     | NA         |
      #| Associaton Country Risk                           | NA        | true    | true     | false     | NA         |
      #| Non Individual                                    | NA        | true    | true     | false     | NA         |
      #| Country of Incorporation / Establishment          | Dropdown  | true    | true     | false     | NA         |
      #| Country of Domicile/ Physical Presence            | Dropdown  | true    | true     | false     | NA         |
      #| Country of Business Operations/ Economic Activity | Dropdown  | true    | true     | false     | NA         |
      #| Individual                                        | NA        | true    | true     | false     | NA         |
      #| Nationality/Other Nationality                     | Dropdown  | true    | true     | false     | NA         |
      #| Residential Status                                | Dropdown  | true    | true     | false     | NA         |
      #| Risk Rating                                       | Dropdown  | true    | true     | false     | NA         |
    #And I validate the following fields in "Customer Type" Sub Flow
      #| Label                                  | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Legal Entity Type                      | Dropdown     | true    | true     | false     | NA         |
      #| Risk Rating                            | Dropdown     | true    | true     | false     | NA         |
      #| Types of Shares (Bearer/Registered)    | Dropdown     | true    | true     | false     | NA         |
      #| Risk Rating                            | Dropdown     | true    | true     | false     | NA         |
      #| Length of Relationship                 | Alphanumeric | true    | true     | false     | NA         |
      #| Risk Rating                            | Dropdown     | true    | true     | false     | NA         |
      #| Industry (Primary/Secondary)           | Dropdown     | true    | true     | false     | NA         |
      #| Risk Rating                            | Dropdown     | true    | true     | false     | NA         |
      #| Main Entity/Association Screening Risk | NA           | true    | true     | false     | NA         |
      #| Adverse Media Category                 | Dropdown     | true    | true     | false     | NA         |
      #| Sanctions Category                     | Dropdown     | true    | true     | false     | NA         |
      #| PEP Category                           | Dropdown     | true    | true     | false     | NA         |
      #| FAB Internal Watch List Category       | Dropdown     | true    | true     | false     | NA         |
      #| Risk Rating                            | Dropdown     | true    | true     | false     | NA         |
      #| Notes                                  | Alphanumeric | true    | false    | false     | NA         |
    #And I validate the following fields in "Product Risk" Sub Flow
      #| Label        | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Product Type | Dropdown     | true    | true     | false     | NA         |
      #| Risk Rating  | Dropdown     | true    | true     | false     | NA         |
      #| Notes        | Alphanumeric | true    | false    | false     | NA         |
    #And I validate the following fields in "Transaction Profile" Sub Flow
      #| Label                                             | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Anticipated Transactions Turnover (Annual in AED) | Dropdown     | true    | true     | false     | NA         |
      #| Notes                                             | Alphanumeric | true    | false    | false     | NA         |
    #And I validate the following fields in "Channel and Interface" Sub Flow
      #| Label                 | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Channel and Interface | Dropdown     | true    | true     | false     | NA         |
      #| Risk Rating           | Dropdown     | true    | true     | false     | NA         |
      #| Notes                 | Alphanumeric | true    | false    | false     | NA         |
    #Verify the Final risk is autopopulated
    #verify the pencil icon is available to override the risk
    #And I validate the following fields in "Risk Assessment" Sub Flow
      #| Label                | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      #| Risk Rating          | Dropdown     | true    | true     | false     | NA         |
      #| Override Risk Rating | Dropdown     | true    | false    | false     | NA         |
      #| Risk Model           | Alphanumeric | true    | true     | false     | NA         |
      #| Risk Model Version   | Alphanumeric | true    | true     | false     | NA         |
      #| Notes                | Alphanumeric | true    | false    | false     | NA         |
