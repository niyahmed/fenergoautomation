#Test Case: TC_R1S2EPIC003PBI202_21
#PBI: R1S2EPIC003PBI202
#User Story ID: NA
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S2EPIC003PBI202_21

 @tobeautomated
  Scenario: Corporate-Verify the KYC Approver (KYC Manager) user is able to override the risk from 'High' to 'Very High' and verify the appropriate tasks are trigerred in Review and approval stage for 'Very High' risk.
  #Additional Scenario: KYC Maker user assigns the Complete Risk Assessment task to KYC Manager
  #Precondition: Input the appropriate data to get High Risk rating in Complete Risk Assessment screen
	Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "Corporate" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
	When I complete "AddAddressFAB" task 
	Then I store the "CaseId" from LE360 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I add "AssociatedParty" via express addition 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I store the "CaseId" from LE360 
	
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	And I click on "SaveandCompleteforAssessmentScreen1" button 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I click on 'Calculate' button
	#Final risk is autopopulated by the system
	#Ensure the risk rating is calculated as High 
	#Assign the Complete Risk Assessment to 'KYC Manager'
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
	When I navigate to "CaptureRiskCategoryGrid" task  
	#Verify the KYC Manager able to see the Final risk by default
	#Override the risk from High to 'Very High' by clicking on pencil icon
	#Verify risk is upgraded successfully to 'Very High' and complete the task
	When I complete "RiskAssessmentFAB" task 
  #Verify the appropriate tasks are trigerred in review and approval stage for 'Very High' risk
	Then I login to Fenergo Application with "RM:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - VP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - SVP"
  When I search for the "CaseId"
  When I navigate to "CIBR&CKYCApproverSVPReviewGrid" task
  When I complete "ReviewSignOff" task
    
  Then I login to Fenergo Application with "CIB R&C KYC APPROVER - CDD"
  When I search for the "CaseId"
  When I navigate to "CIBR&CKYCApproverCDDReviewGrid" task
  When I complete "ReviewSignOff" task
	
	Then I login to Fenergo Application with "BUH:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BH:Corporate" 
	When I search for the "CaseId" 
	When I navigate to "BHReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "KYCMaker:Corporate" 
	When I search for the "CaseId" 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
	