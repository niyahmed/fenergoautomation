#Test Case: TC_R2S2EPIC003PBI201_04
#PBI: R2S2EPIC003PBI201
#User Story ID: UBO_003
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: UBO

  Scenario: Validate if RM/KYC maker adds any Association Type and mark it as UBO by checking the check-box then screening will become mandatory and user
  # will not be able to proceed further without completing screening(error message will appear on AML screen to complete screening) for both COB and RR Workflow.
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "PCG-Entity"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I right click on Hologram and select 'Add Association' option
    When I navigate to Associated parties screen
    When I select legal entity from existing legal entities
    When I navigate to 'Association details' task screen
    #Test-data: Validate 'Ultimate Benifiial owner(UBO)' check-box is available on 'Association details' task screen
    Then I validate 'Ultimate Benifiial owner(UBO)' check-box is available
    When I select Association Type as 'Custodian', Type of Control as 'Significant Control'
    When I check the check-box 'Ultimate Benifiial owner(UBO)' check-box
    When I save the details
    #Test-data: Validate if UBO check-box isin't checked then UBO tag doesn't appear on added association on "CaptureHierarchyDetailsGrid"
    Then I validate 'Ultimate Benifiial owner(UBO)' check-box is available
    When I select Association Type as 'Settlor'
    #do not check 'Ultimate Benifiial owner(UBO)' check-box
    When I DO NOT check the check-box 'Ultimate Benifiial owner(UBO)'
    When I save the details
    Then I Validate UBO tag does not appear on added association on "CaptureHierarchyDetailsGrid"
    When I complete "CaptureHierarchyDetails" task
    #Then I login to Fenergo Application with "Onboarding Maker"
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I click on 'saveandcomplete' button 
    #Test-data: Error message should be displayed to complete screening
    And I see Error message appeared on 'CompleteAML' task screen to complete screening
    When I Initiate "Google" by rightclicking
    And I complete "Google" from assessment grid with Key "googleScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    #Test-data:Validate task is displayed as completed after completing screening
    When I complete "CompleteID&V" task
    And I validate the task is displayed as completed
    When I navigate to "CaptureRiskCategoryGrid" task
    #Validate Risk category as 'Low'
    Then I Select Risk category as 'Low'
    And I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydKYC"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydAVP"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BusinessUnitHead"
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I navigate to 'CapturefabReferences' task screen
    When I Complete 'CapturefabReferences' task
    And I assert case has been completed and case status is updated as closed
    # Initiate regular Review workflow
    When I navigate to 'LE360- LE details' screen
    When I Click on 'Actions' button and select 'RegularReview' workflow
    # Verify Regular review case has been triggered
    Then I see 'RegularReview' Workflow has been triggered
    # Verify 'Close Associated Cases' task has been triggered
    And I navigated to 'CloseAssociatedCases' task
    Then I complete 'CloseAssociatedCases' task
    # Verify 'Validate KYC and Regulatory Grid' task has been triggered
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    Then I complete "ValidateKYCandRegulatoryGrid" task
    # Verify 'Review request Details' task has been triggered
    When I navigate to "ReviewrequestDetails" task
    Then I complete "ReviewrequestDetails" task
    # Verify 'Review/edit client data' task has been triggered
    When I navigate to "Review/edit client data" task
    When I complete "Review/editClientData" task
    # Verify 'KYC Document requirement' task has been triggered
    When I navigate to "KYCDocumentrequirement" task
    Then I complete "KYCDocumentrequirement" task
    #Test-data: Validate added Association in COB workflow is displaying on Complete AML screen
    When I navigate to "CompleteAML" task workflow 
    Then I verify Association added in COB is displaying on Complete AML screen
    #Add another association on Complete AML screen
    When I right click on Hologram and select 'Add Association' option to add another association
    When I navigate to Associated parties screen
    When I select legal entity from existing legal entities
    When I navigate to 'Association details' task screen
    When I select Association Type as 'Advisor', Type of Control as 'Significant Control'
    When I check the check-box 'Ultimate Benifiial owner(UBO)' check-box
    When I save the detail
    And I Validate added association can be seen in relationship with a UBO Badge
    When I complete "ComplteAML" task
    When I click on 'saveandcomplete' button 
    #Test-data: Error message should be displayed to complete screening
    And I see Error message appeared on 'CompleteAML' task screen to complete screening
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FircosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    #Test-data:Validate task is displayed as completed after completing screening
    When I complete "CompleteID&V" task
    And I validate the task is displayed as completed    
    #Verify 'Complete ID&V' task has been triggered
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Select the Risk category as "Medium" and complete "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task with 'medium' risk rating
    #Verify "Relationship Manager Review SignOff' task is generated
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "RelationshipManagerReviewSignOffGrid" task
    When I complete "RelationshipManagerReviewSignOffGrid" task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task with key "RefertoRiskAssessment"
    And I click on "Submit" button
    #Validate the case is referred to "RiskAssessmentFAB" stage
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "CaptureRiskCategoryGrid" task with 'low' risk rating
    #Verify "Relationship Manager Review SignOff' task is generated
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "RelationshipManagerReviewSignOffGrid" task
    When I complete "RelationshipManagerReviewSignOffGrid" task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task with key "RefertoRiskAssessment"
    And I click on "Submit" button
    #Verify 'Business Unit Head Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "Business Unit Head (N3)"
    When I search for the "CaseId"
    When I navigate to "BusinessUnitHeadReviewandSign-Off" task
    When I complete "BusinessUnitHeadReviewandSign-Off" task
    #Verify 'Capture FAB References' task is generated
  
