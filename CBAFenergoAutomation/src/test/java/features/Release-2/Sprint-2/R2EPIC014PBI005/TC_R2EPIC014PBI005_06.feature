#Test Case: TC_R2EPIC014PBI005_06
#PBI: R2EPIC014PBI005
#User Story ID: NA
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC014PBI005_06

  Scenario: Validate for 'BBG' client type 'Capture Fab References' task is generating for COB workflow with 'Very-high' Risk rating
    Given I login to Fenergo Application with "RM:BBG"
    When I complete "NewRequest" screen with key "BBG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to 'Capture Hierarchy Details' screen
    When I complete "CaptureHierarchyDetails" task
    #Then I login to Fenergo Application with "Onboarding Maker"
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Validate Risk category as 'Very-High'
    Then I Select Risk category as 'Very-high'
    When I navigate to "RiskAssessmentFAB" task
    And I complete "RiskAssessmentFAB" task
    #Verify the appropriate tasks are trigerred in review and approval stage for 'Very-High' risk
    Then I login to Fenergo Application with "RM:BBG"
    #Verify "Relationship Manager Review SignOff' task is generated
    Then I login to Fenergo Application with "RM:BBG"
    When I search for the "CaseId"
    When I navigate to "RelationshipManagerReviewSignOffGrid" task
    When I complete "RelationshipManagerReviewSignOffGrid" task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Verify 'Business Unit Head Review and Sign-off' task is generated
    Then I login to Fenergo Application with "BUH:BBG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    #refer the case to "RiskAssessmentFAB" stage
    #Validate the case is referred to "RiskAssessmentFAB" stage
    Then I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "CaptureRiskCategoryGrid" task with 'Very-high' risk rating 
    #Select the Risk category as "Very-high" and complete "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task with 'very-high' risk rating
    #Verify "Relationship Manager Review SignOff' task is generated
    Then I login to Fenergo Application with "RM:BBG"
    When I search for the "CaseId"
    When I navigate to "RelationshipManagerReviewSignOffGrid" task
    When I complete "RelationshipManagerReviewSignOffGrid" task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-AVPReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-AVPReviewandSign-Off" task
    And I click on "Submit" button
    #Verify 'CIB R&C KYC Approver - VP' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - VP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Verify 'CIB R&C KYC Approver - SVP' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - SVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverSVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Verify 'CIB R&C KYC Approver - CDD' task is generated
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - CDD"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverCDDReviewGrid" task
    When I complete "ReviewSignOff" task
    #Verify 'Business Unit Head Review and Sign-off(N3)' task is generated
    Then I login to Fenergo Application with "BUH:BBG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Verify 'Business Unit Head Review and Sign-off' task is generated
    Then I login to Fenergo Application with "BUH:BBG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task    
    #Verify 'Capture FAB references' task is generated
    Then I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    #Validate all the tasks display as completed and case status is updated as 'Closed'
    And I assert that the Case Status is "Closed"