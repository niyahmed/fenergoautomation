#Test Case: TC_R2S3EPIC018PBI001.3.1_26
#PBI: R2S3EPIC018PBI001.3.1 
#User Story ID: 
#Designed by: Niyaz Ahmed
#Last Edited by: Vibhav Kumar.
Feature: TC_R2S3EPIC018PBI001.3.1_26

#Placeholder for LEM Trigger Event Rules
#Refer to TC No.26 from the sheet 'R2S3EPIC018PBI001.3- V 1-0 LEM- Trigger Event QA Test only'
@Automation
  Scenario: MATERIAL  TRIGGERS	LE Details	KYC Data and Customer Details	Capture Proposed Changes	Customer Details	Update	Residential Status	From Non-Resident to Resident
    Given I login to Fenergo Application with "RM:IBG-DNE" 
     When I complete "NewRequest" screen with key "Corporate" 
      And I complete "CaptureNewRequest" with Key "C1" and below data 
      | Product | Relationship | 
      | C1      | C1           | 
      And I click on "Continue" button 
     When I complete "ReviewRequest" task 
     Then I store the "CaseId" from LE360 
  
    Given I login to Fenergo Application with "SuperUser" 
     When I search for the "CaseId" 
     Then I store the "CaseId" from LE360 
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     When I complete "ValidateKYC" screen with key "C1" 
      And I click on "SaveandCompleteforValidateKYC" button 
     When I navigate to "EnrichKYCProfileGrid" task 
     When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
     When I complete "AddAddressFAB" task 
     Then I store the "CaseId" from LE360 
     When I complete "EnrichKYC" screen with key "C1" 
      And I click on "SaveandCompleteforEnrichKYC" button 
     When I navigate to "CaptureHierarchyDetailsGrid" task 
     When I add AssociatedParty by right clicking 
     When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
     When I complete "AssociationDetails" screen with key "Director" 
     When I complete "CaptureHierarchyDetails" task 
     When I navigate to "KYCDocumentRequirementsGrid" task 
     When I add a "DocumentUpload" in KYCDocument 
     Then I complete "KYCDocumentRequirements" task 
     When I navigate to "CompleteAMLGrid" task 
     When I Initiate "Fircosoft" by rightclicking 
      And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
     Then I complete "CompleteAML" task 
     When I navigate to "CompleteID&VGrid" task 
     When I complete "ID&V" task 
     When I complete "EditID&V" task 
     When I complete "CompleteID&V" task 
     When I navigate to "CompleteRiskAssessmentGrid" task 
      And I verify the populated risk rating is "Low" 
     When I complete "RiskAssessment" task 
  
     When I navigate to "ReviewSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
      And I complete "Waiting for UID from GLCMS" task from Actions button 
     When I navigate to "CaptureFabReferencesGrid" task 
     When I complete "CaptureFABReferencesCIFId" task 
     When I complete "CaptureFABReferences" task 
      And I assert that the CaseStatus is "Closed" 
  
      And I initiate "Maintenance Request" from action button  
      And I complete "MaintenanceRequest" screen with key "KYCData" 
  
     Then I see "CaptureProposedChangesGrid" task is generated 
      And I navigate to "CaptureProposedChangesGrid" task
     Then I store the "CaseId" from LE360 
     When I complete "CaptureProposedChanges" screen with key "Registered Shares" 
      And I click on "SaveandCompleteCaptureProposedChanges" button 
  
      And I navigate to "UpdateCusotmerDetailsGrid" task 
     #When I complete "UpdateCustomerDetails" screen with key "LowRisk" 
      And I click on "SaveandCompleteUpdateCustomerDetails" button 
  
     When I navigate to "KYCDocumentRequirementsGrid" task 
      And I do "DocumentUpload" for all pending documents 
     Then I complete "KYCDocumentRequirements" task 
  
     When I navigate to "OnboardingReviewGrid" task 
     When I select "Approve" for "Dropdown" field "Review Outcome" 
     Then I complete "OnboardingReview" task 
  
     When I navigate to "CompleteAMLGridinLEM" task 
     When I Initiate "Fircosoft" by rightclicking 
      And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
     Then I complete "CompleteAML" task 
  
     When I navigate to "CompleteID&VGridinLEM" task 
     When I complete "CompleteID&V" task 
  
     When I navigate to "CompleteRiskAssessmentGrid" task 
      And I verify the populated risk rating is "High" 
     When I complete "RiskAssessment" task 
  
     Then I store the "CaseId" from LE360 
     
     When I navigate to "ReviewSignOffGrid" task
     When I complete "ReviewSignOff" task
  
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
     When I complete "ReviewSignOff" task
  
     When I navigate to "CIBR&CKYCApproverAVPReviewGridLEM" task
     When I complete "ReviewSignOff" task
  
     When I navigate to "CIBR&CKYCApproverVPReviewGridLEM" task 
     When I complete "ReviewSignOff" task 
     
     When I navigate to "BHUReviewandSignOffGridLEM" task
     When I complete "ReviewSignOff" task
  
     When I navigate to "BHReviewandSignOffGridLEM" task 
     When I complete "ReviewSignOff" task
  
      And I complete "Publish to GLCMS" task from Actions button 
  
  
