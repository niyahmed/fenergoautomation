Feature: LEM - KYC Data and Customer Details - Data Attributes Changes
#Test Case: TC_R2EPIC018PBI002.3_02
#PBI: R2EPIC018PBI002.3v0.2
#User Story ID: NA
#Designed by: Sasmita Pradhan
#Last Edited by: Sasmita Pradhan
@Automation @LEM
Scenario: NBFI - Validate the behavior of "Capture Proposed Changes" task when area is 'LE Details' and Le Details Changes is 'KYC Data and Customer details' during LEM :
#Area for Change section - No change (OOTB feature)
#'Business Markets' subflow should be hidden
#Comments section - No change (OOTB feature) 
#Field behavior in Customer Details section (11 fields should be hidden, 8 new fields should be added, 9 fields should be modified) 
#Field behavior in KYC Conditions section (7 fields should be hidden, 5 new fields should be added, 12 fields should be modified)
#Field behavior in  Regulatory Data section (1field should be modified)

#Precondition: Create COB with Client Type = NBFI and country of incorporation = AE-UNITED ARAB EMIRATES and following data in "Validate KYC and Regulatory Data" task and case status should be closed
 ##Is this entity publicly listed? - No
  ##Is this Entity Regulated? = Yes
##Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)?-Yes
	
		Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "NBFI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "TC_R2EPIC018PBI002.3_02"
    When I select "India - Reserve Bank of India (RBI)" for "Dropdown" field "Name of Regulatory Body"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforEnrichKYC" button
    
		When I navigate to "CaptureHierarchyDetailsGrid" task 
		When I add AssociatedParty by right clicking 
		When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
		When I complete "AssociationDetails" screen with key "Director" 
		When I complete "CaptureHierarchyDetails" task 
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
    Then I complete "CompleteAML" task
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
   
		When I navigate to "CompleteRiskAssessmentGrid" task 
		When I complete "RiskAssessment" task 
    
    Then I login to Fenergo Application with "RM:NBFI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "BUH:NBFI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
	
	Given I login to Fenergo Application with "SuperUser"
	When I search for the "CaseId"

	And I initiate "Maintenance Request" from action button
	When I select "LE Details" for field "Area"
	When I select "KYC Data and Customer Details" for field "LE Details Changes"  
	And I click on "CreateLEMSubmit" button
	Then I see "CaptureProposedChangesGrid" task is generated
	And I navigate to "CaptureProposedChangesGrid" task
  Then I store the "CaseId" from LE360
	And I validate the following fields in "Areas For Change" Sub Flow 
	      | Label  | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo                    | 
	      | Area   | MultiSelectDropdown | true    | true     | NA        | KYC Data and Customer details | 
	      | Reason | TextBox						 | true    | true     | NA        | NA                            |
	Then I check that below subflow is not visible
		|Subflow				 |
		|Business Markets|

	And I validate the following fields in "Customer Details" Sub Flow
		      | Label                                    | FieldType  | Visible | ReadOnly | Mandatory | DefaultsTo | 
		      | ID                                       | TextBox    | true    | true     | NA        | NA         | 
		      | FAB Segment                              | Dropdown   | true    | false    | true      | NA         | 
		      | Client Type                              | Dropdown   | true    | true     | NA        | NA         | 
		      | Legal Entity Name                        | TextBox    | true    | false    | true      | NA         | 
		      | Legal Entity Type                        | Dropdown   | true    | true     | NA        | NA         | 
		      | Entity Type                              | Dropdown   | true    | true     | NA        | NA         | 
		      | Country of Incorporation / Establishment | Dropdown   | true    | false    | NA        | NA         | 
		      | Country of Domicile/ Physical Presence   | Dropdown   | true    | false    | NA        | NA         | 
		      | Date of Incorporation                    | DatePicker | true    | false    | NA        | NA         | 
		      | Channel & Interface                      | Dropdown   | true    | false    | true      | NA         | 
		      | Purpose of Account/Relationship          | TextBox    | true    | false    | NA        | NA         | 
		      | Residential Status                       | Dropdown   | true    | true     | true      | NA         | 
		      | Customer Tier                            | NA  				| false   | NA  	   | NA 	     | NA         | 
		      | Relationship with bank                   | Dropdown   | true    | false    | true      | NA         | 
		      | CIF Creation Required?                   | CheckBox   | true    | false    | NA        | NA         | 
		      | Emirate                                  | Dropdown   | true    | false    | true      | NA         | 

	And I fill "SpecialCharacters" data of length "10" in "Legal Entity Name" field of "Capture Proposed Changes" screen in "LEM" workflow
	When I select "01-01-2022" for "TextBox" field "Date of Incorporation"
	And I verify "FAB Segment" drop-down values


	And I check that below data is not visible
		      | FieldLabel                                                      | 
		      | Registration Number                                             | 
		      | Principal Place of Business                                     | 
		      | Group Name                                                      | 
		      | Is this entity publicly listed?                                 | 
		      | Name of Stock Exchange                                          | 
		      | Stock Exchange Domiciled in FAB's Recognised List of Countries? | 
		      | Country of Stock Exchange                                       | 
		      | Is this entity regulated?                                       | 
		      | Regulated By                                                    | 
		      | Regulatory ID                                                   | 
		      | SWIFT BIC                                                       | 

	When I select "Yes" for "Dropdown" field "Is this entity regulated?"
	And I validate the following fields in "KYC Conditions" Sub Flow 
      | Label                                                                      | FieldType    | Visible     | ReadOnly    | Mandatory   | DefaultsTo | 
      | Legal Entity Type                                                          | Dropdown     | true        | true        | NA          | NA         | 
      | Country of Incorporation / Establishment                                   | Dropdown     | true        | false       | NA          | NA         | 
      | Is this entity publicly listed?                                            | Dropdown     | true        | false       | true        | NA         | 
      | Is this parent entity publicly listed?                                     | Dropdown     | true        | NA          | NA          | NA         | 
      | Name of Stock Exchange                                                     | NA           | Conditional | NA          | NA          | NA         | 
      | Stock Exchange Domicile Country                                            | NA           | Conditional | NA          | NA          | NA         | 
      | Is this FAB's Recognized Stock Exchange?                                   | NA           | Conditional | NA          | NA          | NA         | 
      | Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)?       | Dropdown     | true        | false       | true        | NA         | 
      | Specify the prohibited Category                                            | TextBox      | true        | NA          | NA          | NA         | 
      | Has the exceptional approvals been obtained to on-board/retain the client? | Dropdown     | true        | false       | true        | NA         | 
      | Is this entity regulated?                                                  | Dropdown     | true        | false       | NA          | true       | 
      | Name of Regulatory Body                                                    | Dropdown     | Conditional | NA          | NA          | NA         | 
      | Regulatory ID                                                              | TextBox      | Conditional | NA          | NA          | NA         | 
      | Is there an AML program in place?                                          | Dropdown     | Conditional | NA          | NA          | NA         | 
      | Is the entity a wholly-owned subsidiary of a parent?                       | Dropdown     | true        | false       | NA          | NA         | 
      | Types of Shares (Bearer/Registered)                                        | Dropdown     | true        | false       | true        | NA         | 
      | Is the Entity operating with Flexi Desk?                                   | Dropdown     | true        | false       | true        | NA         | 
      | Is UAE Licensed?                                                           | Dropdown     | true        | false       | NA          | NA         | 
	And I take a screenshot
	When I select "No" for "Dropdown" field "Is this entity regulated?"

	And I check that below data is not visible
		      | FieldLabel                                         | 
					| Legal Entity Category                              |
					| Country of Domicile                                |
					| Parent Company: Country of Incorporation           |
					| Is the parent entity regulated?                    |
					| Parent Regulated By                                |                                                
					| Parent's AML Guidelines Reviewed and Approved      |
					| Name of Exchange(s) the Parent Entity is Listed On |



	And I validate the following fields in "Regulatory Data" Sub Flow
		      | Label                                              | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo | 
		      | Countries of Business Operations/Economic Activity | MultiSelectDropdown | true    | false    | false     | NA         | 
		      | Is the entity an Intragroup entity?                | Dropdown            | true    | false    | NA        | NA         | 
		      | Dodd-Frank US Person Type                          | Dropdown            | true    | false    | NA        | NA         | 
		      | Direct Electronic Access Client?                   | Dropdown            | true    | false    | NA        | NA         | 
 
#=======================================================================================
#=>Comment: Below steps are written for manual testing.

#
#	Given I login to Fenergo Application with "SuperUser"
#	When I search for the "CaseId"
#	#LEM flow starts	
#	#Initiate "Maintenance Request" 
#	And I initiate "Maintenance Request" from action button
#	When I select "LE Details" for field "Area"
#	When I select "KYC Data and Customer Details" for field "LE Details Changes"  
#	And I click on "CreateLEMSubmit" button
#	Then I see "CaptureProposedChangesGrid" task is generated
#	And I navigate to "CaptureProposedChangesGrid" task
#	#Validate the behavior of "Capture Proposed Changes" task when area for change during LEM is 'Products/Trading Entities'
#	#Area for Change section - No change (OOTB feature) 
#	And I validate the following fields in "Areas For Change" Sub Flow 
#	      | Label  | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo                    | 
#	      | Area   | MultiSelectDropdown | true    | true     | NA        | KYC Data and Customer details | 
#	      | Reason | TextBox						 | true    | true     | NA        | NA                            |
#	Then I check that below subflow is not visible
#		|Subflow				 |
#		|Business Markets|
##Field behavior in Customer Details section (11 fields should be hidden, 8 new fields should be added, 9 fields should be modified)
#And I validate the following fields in "Customer Details " section #Only the following fields should be present in the mentioned order
#And I validate the following fields in "Customer Details" Sub Flow
      #| Label                                    | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo | 
      #| ID                                       | Alphanumeric | true    | false    | false     | NA         | 
      #| FAB Segment                              | Dropdown     | true    | true     | true      | NA         | 
      #| Client Type                              | Dropdown     | true    | false    | false     | NA         | 
      #| Legal Entity Name                        | Alphanumeric | true    | true     | true      | NA         | 
      #| Legal Entity Type                        | Dropdown     | true    | false    | false     | NA         | 
      #| Entity Type                              | Dropdown     | true    | false    | false     | NA         | 
      #| Country of Incorporation / Establishment | Dropdown     | NA      | true     | false     | NA         | 
      #| Country of Domicile/ Physical Presence   | Dropdown     | true    | true     | false     | NA         | 
      #| Date of Incorporation                    | Date         | true    | true     | false     | NA         | 
      #| Channel & Interface                      | Dropdown     | true    | NA       | NA        | NA         | 
      #| Purpose of Account/Relationship          | Alphanumeric | true    | true     | false     | NA         | 
      #| Residential Status                       | Dropdown     | true    | false    | false     | NA         |  
      #| Relationship with bank                   | Dropdown     | true    | true     | true      | NA         | 
      #| CIF Creation Required?                   | Check box    | true    | true     | true      | NA         | 
      #| Emirate                                  | Dropdown     | true    | NA       | NA        | NA         | 
                        #
#	#And I validate No special characters allowed for field Legal Entity Type
#	#Only A-Z, a-z and 0-9  and(period/full stop/space) allowed for field Legal Entity Type
#	#And I validate date cannot be in the future for field Date of Incorporation
#	#LOV validation - FAB Segment. (Refer PBI-LOV tab)
#	#And I validate the LOV for the field "FAB Segment"
#
#	And I fill "SpecialCharacters" data of length "10" in "Legal Entity Name" field of "Capture Proposed Changes" screen in "LEM" workflow
#	When I select "01-01-2022" for "TextBox" field "Date of Incorporation"
#	And I verify "FAB Segment" drop-down values
#
#	#Validate the below fields are not visible under "Customer Details" section on "Capture Proposed Changes" screen
#	#And I validate below fields are not visible under "Customer Details" section on "Capture Proposed Changes" screen
#	And I check that below data is not visible
#	| FieldLabel                                                      |
#	| Registration Number                                             |
#	| Principal Place of Business                                     | 
#	| Group Name                                                      |
#	| Is this entity publicly listed?                                 |
#	| Name of Stock Exchange                                          |
#	| Stock Exchange Domiciled in FAB's Recognised List of Countries? |
#	| Country of Stock Exchange                                       |
#	| Is this entity regulated?                                       |
#	| Regulated By                                                    |
#	| Regulatory ID                                                   |
#	| SWIFT BIC                                                       |
#
#
##Field behavior in KYC Conditions section (7 fields should be hidden, 5 new fields should be added, 12 fields should be modified) 
#Only the following fields should be present in the mentioned order
#And I validate the following fields in "KYC Conditions" Sub Flow 
      #| Label                                                                      | FieldType           | Visible | ReadOnly    | Mandatory   | DefaultsTo |  
      #| Legal Entity Type                                                          | Dropdown            | true    | true        | false       | NA         | 
      #| Country of Incorporation / Establishment                                   | Dropdown            | true    | false       | false       | NA         | 
      #| Is this entity publicly listed?                                            | Dropdown            | true    | false       | true        | false      | 
      #| Is this parent entity publicly listed?                                     | Dropdown            | true    | NA          | NA          | true       | 
      #| Name of Stock Exchange                                                     | MultiSelectDropdown | true    | NA          | NA          | true       | 
      #| Stock Exchange Domicile Country                                            | MultiSelectDropdown | true    | NA          | NA          | NA         | 
      #| Is this FAB's Recognized Stock Exchange?                                   | Dropdown            | true    | NA          | NA          | NA         | 
      #| Is this a Prohibited client (as per FAB’s AML/CTF/Sanctions Policy)?     | Dropdown            | true    | false       | true        | true       | 
      #| Specify the prohibited Category                                            | Alphanumeric        | true    | NA          | NA          | NA         | 
      #| Has the exceptional approvals been obtained to on-board/retain the client? | Dropdown            | true    | NA          | NA          | NA         | 
      #| Is this entity regulated?                                                  | Dropdown            | true    | Conditional | false       | true       | 
      #| Name of Regulatory Body                                                    | Dropdown            | true    | true        | Conditional | NA         | 
      #| Regulatory ID                                                              | Alphanumeric        | true    | Conditional | false       | NA         | 
      #| Is there an AML program in place?                                          | Dropdown            | true    | Conditional | false       | NA         | 
      #| Is the entity a wholly-owned subsidiary of a parent?                       | Dropdown            | true    | NA          | NA          | NA         | 
      #| Types of Shares (Bearer/Registered)                                        | Dropdown            | true    | false       | true        | NA         | 
      #| Is the Entity operating with Flexi Desk?                                   | Dropdown            | true    | NA          | NA          | NA         | 
      #| Is UAE Licensed                                                            | Dropdown            | true    | false       | false       | true       | 
#
#Validate the below fields are not visible under "KYC Conditions" section on "Capture Proposed Changes" screen
#And I Validate the below fields are not visible under "KYC Conditions" section on "Capture Proposed Changes" screen
#	And I check that below data is not visible
#	| FieldLabel                                         | 
#	| Legal Entity Category                              |
#	| Country of Domicile                                |
#	| Parent Company: Country of Incorporation           |
#	| Is the parent entity regulated?                    |
#	| Parent Regulated By                                |                                                
#	| Parent's AML Guidelines Reviewed and Approved      |
#	| Name of Exchange(s) the Parent Entity is Listed On |
#
##Field behavior in Regulatory Data section 
#And I validate the following fields in " Regulatory Data " section #Only the following fields should be present in the mentioned order
#| FieldLabel                                           | Field Type            | Visible | Editable    | Mandatory   | DefaultsTo   |
#| Countries of Business Operations/Economic Activity   | MultiSelect Drop-down | Yes     | Yes         | No          |              |
#| Is the entity an Intragroup entity?                  | Drop-down             | Yes     | No          | No          |              |
#| Dodd-Frank US Person Type                            | Drop-down             | Yes     | No          | No          |              |
#| Direct Electronic Access Client?                     | Drop-down             | Yes     | No          | No|         |              |
#
#And I validate the following fields in "Regulatory Data" Sub Flow
      #| Label                                              | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo | 
      #| Countries of Business Operations/Economic Activity | MultiSelectDropdown | true    | false    | true      | NA         | 
      #| Is the entity an Intragroup entity?                | Dropdown            | true    | true     | false     | NA         | 
      #| Dodd-Frank US Person Type                          | Dropdown            | true    | true     | false     | NA         | 
      #| Direct Electronic Access Client?                   | Dropdown            | true    | true     | false     | NA         | 
 #
                    
