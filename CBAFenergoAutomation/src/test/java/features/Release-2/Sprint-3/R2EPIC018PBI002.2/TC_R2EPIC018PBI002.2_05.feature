Feature: LEM - Product Area WF & Data Attribute Changes

  #Test Case: TC_R2EPIC018PBI002.2_05
  #PBI: R2EPIC018PBI002.2
  #User Story ID: NA
  #Designed by: Anusha PS
  #Last Edited by: Anusha PS
  @TobeAutomated @LEM
  Scenario: NBFI - Validate the behavior of "Capture Proposed Changes" task when area for change during LEM is 'Products/Trading Entities':
    #Area for Change section - No change (OOTB feature)
    #Customer Details section should be hidden
    #Field behavior in KYC Conditions section (7 fields should be hidden, 5 new fields should be added, 7 fields should be modified)
    #Trading Entities section should be greyed out
    #Products section - No change (OOTB feature)
    #Comments section - No change (OOTB feature)
    ##Additional Scenario: Validate if "Update Customer Details" task is removed
    #######################################################################################
    #######Precondition: Create COB with Client Type = NBFI, Confidential = NBFI and country of incorporation = AE-UNITED ARAB EMIRATES and following data in "Validate KYC and Regulatory Data" task.
    ##Is this entity publicly listed? - Yes
    ##Is this entity regulated? - No
    ##Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)?-Yes
    ##Select multiple values from "Name of Stock Exchange" and "Stock Exchange Domicile Country" fields
    ##Add comments from "Enrich KYC Profile" screen
    #######################################################################################
    Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:NBFI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:NBFI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #LEM flow starts
    And I initiate "Maintenance Request" from action button
    When I select "Products / Trading Entities" for field "Area"
    And I fill "Lem test" for field "Reason"
    And I submit the "Maintenance Request"
    And I assert that "Capture Proposed Changes" task is triggered
    And I navigate to "Capture Proposed Changes" task
    #Validate the behavior of "Capture Proposed Changes" task when area for change during LEM is 'Products/Trading Entities'
    ##Area for Change section - No change (OOTB feature)
    And I validate the following fields in "Area for Change" section
      | Label  | FieldType    | Visible | ReadOnly | DefaultsTo                |
      | Area   | Dropdown     | true    | true     | Products/Trading Entities |
      | Reason | Alphanumeric | true    | true     | Lem test                  |
    ##Customer Details section should be hidden
    And I assert "Customer Details" section is not visible
    ##Field behavior in KYC Conditions section (7 fields should be hidden, 5 new fields should be hidden, 7 fields should be modified)
    And I validate the following fields in "KYC Conditions" section #Only the following fields should be present in the mentioned order
      | Label                                                                | FieldType             | Visible | ReadOnly | Mandatory | DefaultsTo              |
      | Legal Entity Type                                                    | Drop-down             | Yes     | true     | No        | Value from COB          |
      | Country of Incorporation / Establishment                             | Drop-down             | Yes     | true     | No        | AE-United Arab Emirates |
      | Is this entity publicly listed?                                      | Drop-down             | Yes     | true     | Yes       | Yes                     |
      | Name of Stock Exchange                                               | Multiselect drop-down | Yes     | true     | Yes       | Value from COB          |
      | Stock Exchange Domicile Country                                      | Multiselect drop-down | Yes     | true     | Yes       | Value from COB          |
      | Is this FAB''s Recognized Stock Exchange?                            | Drop-down             | Yes     | true     | Yes       | Value from COB          |
      | Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)? | Drop-down             | Yes     | true     | Yes       | No                      |
      | Is this entity regulated?                                            | Drop-down             | Yes     | No       | No        | No                      |
      | Is the entity a wholly-owned subsidiary of a parent?                 | Drop-down             | Yes     | true     | No        |                         |
      | Types of Shares (Bearer/Registered)                                  | Drop-down             | Yes     | true     | Yes       | Value from COB          |
      | Is the Entity operating with Flexi Desk?                             | Drop-down             | Yes     | true     | Yes       | Value from COB          |
      | Is UAE Licensed                                                      | Auto                  | Yes     | true     | No        | False                   |
    And I validate the following fields are not visible in "KYC Conditions" section
      | Legal Entity Category                              |
      | Country of Domicile                                |
      | Parent Company: Country of Incorporation           |
      | Is the parent entity regulated?                    |
      | Parent Regulated By                                |
      | Parent''s AML Guidelines Reviewed and Approved     |
      | Name of Exchange(s) the Parent Entity is Listed On |
    ##Validate if Trading Entities section should be greyed out
    And I assert "Trading Entities" section is greyed out
    ##Validate Products section - No change (OOTB feature)
    And I assert "Product section" is popluated with values from COB
    ##Validate Comments section - No change (OOTB feature)
    And I assert "Comments" section is popluated with values from COB
    And I complete "Capture Proposed Changes" task
		##Validate if "Update Customer Details" task is not triggered
		And I assert "Update Customer Details" task is not triggered
		And I assert "KYC Document Requirements" task is trigerred
