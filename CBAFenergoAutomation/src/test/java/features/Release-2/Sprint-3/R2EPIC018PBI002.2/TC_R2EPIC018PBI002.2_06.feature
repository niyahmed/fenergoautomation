Feature: LEM - Product Area WF & Data Attribute Changes

  #Test Case: TC_R2EPIC018PBI002.2_06
  #PBI: R2EPIC018PBI002.2
  #User Story ID: NA
  #Designed by: Anusha PS
  #Last Edited by: Anusha PS
  @LEM
  Scenario: DM - Validate the behavior of "Capture Proposed Changes" task when area for change during LEM is 'Products/Trading Entities':
    #Area for Change section - No change (OOTB feature)
    #Customer Details section should be hidden
    #Field behavior in KYC Conditions section (7 fields should be hidden, 5 new fields should be added, 9 fields should be modified)
    #Trading Entities section should be greyed out
    #Products section - No change (OOTB feature)
    #Comments section - No change (OOTB feature)
    ##Additional Scenario: Validate if "Update Customer Details" task is removed
    #######################################################################################
    #######Precondition: Create DM with following data
    ##Client Type = Corporate, Confidential = IBG-AUH and country of incorporation = AE-UNITED ARAB EMIRATES
    ##Is this entity publicly listed? - No
    ##Is this parent entity publicly listed?-Yes
    ##Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)?-Yes
    ##Select multiple values from "Name of Stock Exchange" and "Stock Exchange Domicile Country" fields
    ##Add comments from "Enrich KYC Profile" screen
    #######################################################################################
    #Create a DM entity and close it with the data mentioned in the precondition
    #LEM flow starts
    And I initiate "Maintenance Request" from action button
    When I select "Products / Trading Entities" for field "Area"
    And I fill "Lem test" for field "Reason"
    And I submit the "Maintenance Request"
    And I assert that "Capture Proposed Changes" task is triggered
    And I navigate to "Capture Proposed Changes" task
    #Validate the behavior of "Capture Proposed Changes" task when area for change during LEM is 'Products/Trading Entities'
    ##Area for Change section - No change (OOTB feature)
    And I validate the following fields in "Area for Change" section
      | Label  | FieldType    | Visible | ReadOnly | DefaultsTo                |
      | Area   | Dropdown     | true    | true     | Products/Trading Entities |
      | Reason | Alphanumeric | true    | true     | Lem test                  |
    ##Customer Details section should be hidden
    And I assert "Customer Details" section is not visible
    ##Field behavior in KYC Conditions section (7 fields should be hidden, 5 new fields should be hidden, 9 fields should be modified)
    And I validate the following fields in "KYC Conditions" section #Only the following fields should be present in the mentioned order
      | Label                                                                      | FieldType             | Visible | ReadOnly | Mandatory | DefaultsTo              |
      | Legal Entity Type                                                          | Drop-down             | Yes     | true     | No        | Value from DM          |
      | Country of Incorporation / Establishment                                   | Drop-down             | Yes     | true     | No        | AE-United Arab Emirates |
      | Is this entity publicly listed?                                            | Drop-down             | Yes     | true     | Yes       | No                      |
      | Is this parent entity publicly listed?                                     | Drop-down             | Yes     | true     | Yes       | Yes                     |
      | Name of Stock Exchange                                                     | Multiselect drop-down | Yes     | true     | Yes       | Value from DM          |
      | Stock Exchange Domicile Country                                            | Multiselect drop-down | Yes     | true     | Yes       | Value from DM          |
      | Is this FAB''s Recognized Stock Exchange?                                  | Drop-down             | Yes     | true     | Yes       | Value from DM          |
      | Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?       | Drop-down             | Yes     | true     | Yes       | Yes                     |
      | Specify the prohibited Category                                            | Alphanumeric          | Yes     | true     | Yes       | Value from DM          |
      | Has the exceptional approvals been obtained to on-board/retain the client? | Drop-down             | Yes     | true     | Yes       | Value from DM          |
      | Is the entity a wholly-owned subsidiary of a parent?                       | Drop-down             | Yes     | true     | No        |                         |
      | Types of Shares (Bearer/Registered)                                        | Drop-down             | Yes     | true     | Yes       | Value from DM          |
      | Is the Entity operating with Flexi Desk?                                   | Drop-down             | Yes     | true     | Yes       | Value from DM          |
      | Is UAE Licensed                                                            | Auto                  | Yes     | true     | No        | True                    |
    And I validate the following fields are not visible in "KYC Conditions" section
      | Legal Entity Category                              |
      | Country of Domicile                                |
      | Parent Company: Country of Incorporation           |
      | Is the parent entity regulated?                    |
      | Parent Regulated By                                |
      | Parent''s AML Guidelines Reviewed and Approved     |
      | Name of Exchange(s) the Parent Entity is Listed On |
    ##Validate if Trading Entities section should be greyed out
    And I assert "Trading Entities" section is greyed out
    ##Validate Products section - No change (OOTB feature)
    And I assert "Product section" is popluated with values from DM
    ##Validate Comments section - No change (OOTB feature)
    And I assert "Comments" section is popluated with values from DM
    And I complete "Capture Proposed Changes" task
		##Validate if "Update Customer Details" task is not triggered
		And I assert "Update Customer Details" task is not triggered
		And I assert "KYC Document Requirements" task is trigerred