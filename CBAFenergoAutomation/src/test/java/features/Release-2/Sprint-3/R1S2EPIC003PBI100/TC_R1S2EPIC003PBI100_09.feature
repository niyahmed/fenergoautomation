#Test Case: TC_R1S2EPIC003PBI100_09
#PBI: R1S2EPIC003PBI100 
#User Story ID: 
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1S2EPIC003PBI100_09

  #Placeholder for Corp CRAM Override 
  #Refer to S.No 9 from the R1S2EPIC003PBI100 Corp CRAM_Test_Scenarios sheet
  @Automation
  Scenario: Verify the overall risk rating is auto overridden to High from WAC Risk rating (High) when Countries of Business Operations/Economic Activity score is 4 and override is true and Residential Status is Non Resident
    Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "RO09" 
	When I complete "Product" screen with key "Insurance" 
	And I complete "CaptureNewRequest" with Key "RO09" and below data 
 	| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId"
	Then I store the "CaseId" from LE360  
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "RO09" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
#	##	#	When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
	When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "RO09" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "DirectorIndividual" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
##	##	Then I compare the list of documents should be same as "COBDocument" for ClientType "Corporate" 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "NonIndividual" AssociatedParty 
	When I complete "LEDetailsNonIndividual" screen with key "RO09" 
	When I search for the "CaseId" 
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "Individual" AssociatedParty 
	When I complete "LEDetailsIndividual" screen with key "RO09" 
	When I search for the "CaseId" 
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Google" by rightclicking 
	When I Initiate "RO08" by rightclicking for "2" associated party 
	And I complete "RO08" from assessment grid with Key "RO09" 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task
	And I validate that the RiskCategory is "High"
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	