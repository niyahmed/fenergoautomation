#Test Case: TC_R1S2EPIC004PBI202_07
#PBI: R1S2EPIC004PBI202
#User Story ID: RM_Entity_041
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S2EPIC004PBI202_07
@Automation
  Scenario: FI-Validate for KYC maker the same risk rating is retained if case has been referred back to Enrich KYC Profile stage once Complete risk assessment task is completed
    #Precondition: Input the appropriate data to get High Risk rating in Complete Risk Assessment screen
   
   Given I login to Fenergo Application with "RM:FI"
    When I complete "NewRequest" screen with key "FI"
    And I complete "CaptureNewRequest" with Key "TC_R1S2EPIC004PBI202_07" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "FI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "HighRiskFI"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking 
		When I complete "AssociatedPartiesExpressAddition" screen with key "TC_R1S2EPIC004PBI202_07" 
		When I complete "AssociationDetails" screen with key "Custodian" 
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task 
		When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
		Then I complete "CompleteAML" task 
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    #=>Comment : since risk calculation is not implemented for FIG till R2/S3, the risk coming is Low. After sprint 4 test data needs to be updated
    #=>Comment : to get high risk
		When I navigate to "CompleteRiskAssessmentGrid" task 
		Then I verify the populated risk rating is "Low" 
		Then I check that "Save For Later" button is enabled in "Complete Risk Assessment" screen
		Then I check that "Continue" button is enabled in "Complete Risk Assessment" screen
		
		When I click on "Case Details" button
		And I refer back the case to "Enrich Client Information" stage
		When I navigate to "EnrichKYCProfileGrid" task
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task 
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    And I do "DocumentUpload" for all pending documents
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task  
		Then I complete "CompleteAML" task 
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task 
		Then I verify the populated risk rating is "Low" 
		
		#======================================
   
   #
    #Given I login to Fenergo Application with "RM:IBG-DNE"
    #When I complete "NewRequest" screen with key "FI"
    #And I complete "CaptureNewRequest" with Key "C1" and below data
      #| Product | Relationship |
      #| C1      | C1           |
    #And I click on "Continue" button
    #When I complete "ReviewRequest" task
    #Then I store the "CaseId" from LE360
    #Given I login to Fenergo Application with "KYCMaker: FIG"
    #When I search for the "CaseId"
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #When I navigate to "EnrichKYCProfileGrid" task
    #When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    #When I complete "AddAddressFAB" task
    #Then I store the "CaseId" from LE360
    #When I complete "EnrichKYC" screen with key "C1"
    #And I click on "SaveandCompleteforEnrichKYC" button
    #When I navigate to "CaptureHierarchyDetailsGrid" task
    #When I add AssociatedParty by right clicking
    #When I add "AssociatedParty" via express addition
    #When I complete "AssociationDetails" screen with key "COO"
    #When I complete "CaptureHierarchyDetails" task
    #When I navigate to "KYCDocumentRequirementsGrid" task
    #Then I store the "CaseId" from LE360
    #When I add a "DocumentUpload" in KYCDocument
    #Then I complete "KYCDocumentRequirements" task
    #When I navigate to "CompleteAMLGrid" task
    #When I Initiate "Fircosoft" by rightclicking
    #And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    #And I click on "SaveandCompleteforAssessmentScreen1" button
    #Then I complete "CompleteAML" task
    #When I navigate to "CompleteID&VGrid" task
    #When I complete "CompleteID&V" task
    #When I navigate to "CaptureRiskCategoryGrid" task
    #Verify KYC Maker NOT able to view the scores for each risk attributes. Able to view overrall score    
    #Verify the Final risk is autopopulated as high
   #Then I validate High Risk rating is displaying as Final Risk rating
    #Verify 'Save for Later' and 'Continue' buttons are available and enabled by default
    #Then I validate 'Save for Later' and 'Continue' buttons are available and enabled by default
    #When I complete "CaptureRiskCategoryGrid" task
    #When I navigate to "Casedetails" screen
    #refer the case to Enrich KYC Profile stage"
    #When I click on 'Actions' button and select 'refer' option
    #When I refer the case to "EnrichKYCProfileGrid" task  
    #When I complete "EnrichKYC" screen with key "C1"
    #When I navigate to "CaptureHierarchyDetailsGrid" task
    #When I complete "AssociationDetails" screen with key "COO"
    #When I complete "CaptureHierarchyDetails" task
    #When I navigate to "KYCDocumentRequirementsGrid" task
    #Then I store the "CaseId" from LE360
    #When I add a "DocumentUpload" in KYCDocument
    #Then I complete "KYCDocumentRequirements" task
    #When I navigate to "CompleteAMLGrid" task
    #And I click on "SaveandCompleteforAssessmentScreen1" button
    #Then I complete "CompleteAML" task
    #When I navigate to "CompleteID&VGrid" task
    #When I complete "CompleteID&V" task
    #When I navigate to "CaptureRiskCategoryGrid" task
    #Validate the same risk rating(high) is retained
    #Then I validate High Risk rating is appearing as final Risk rating
    #And I Complete "CaptureRiskCategoryGrid" task
    
    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    