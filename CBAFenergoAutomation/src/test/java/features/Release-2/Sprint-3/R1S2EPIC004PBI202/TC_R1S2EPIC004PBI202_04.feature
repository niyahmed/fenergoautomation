#Test Case: TC_R1S2EPIC004PBI202_04
#PBI: R1S2EPIC004PBI202
#User Story ID: RM_Entity_04
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S2EPIC004PBI202_04

  Scenario: FI-Validate Compliance user is able to View all Risk Attributes and their scores as per DD on Complete risk assessment screen
    #Precondition: Input the appropriate data to get High Risk rating in Complete Risk Assessment screen
    #Additional Scenario: Verify stage name = Risk assessment, Screen name = Complete Risk Assessment
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "FI"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I add "AssociatedParty" via express addition
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I store the "CaseId" from LE360
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I login to Fenergo Application with "CDD"
    #Verify CDD user is able to view the scores for each risk attributes. Able to view overrall score
    #Verify stage name = Risk assessment, Screen name = Complete Risk Assessment
    And I validate the following fields in "Legal Entity Details" Sub Flow
      | Label         | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Legal Entity  | Hyperlink    | true    | true     | false     | NA         |
      | LE_Name       | Alphanumeric | true    | true     | false     | NA         |
      | LegalEntityId | Alphanumeric | true    | true     | false     | NA         |
    And I validate the following fields in "Country Risk" Sub Flow
      | Label                                    | FieldType        | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Country of Incorporation / Establishment | Dropdown         | true    | true     | false     | NA         |
      | Risk Rating                              | Dropdown    true | true    | false    | NA        |            |
      | Country of Domicile/ Physical Presence   | Dropdown         | true    | true     | false     | NA         |
      | Risk Rating                              | Dropdown         | true    | true     | false     | NA         |
      | Associaton Country Risk                  | NA               | true    | true     | false     | NA         |
      | Non Individual                           | NA               | true    | true     | false     | NA         |
      | Country of Incorporation / Establishment | Dropdown         | true    | true     | false     | NA         |
      | Individual                               | NA               | true    | true     | false     | NA         |
      | Nationality/Other Nationality            | Dropdown         | true    | true     | false     | NA         |
      | Risk Rating                              | Dropdown         | true    | true     | false     | NA         |
    And I validate the following fields in "Customer Type" Sub Flow
      | Label                                  | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Legal Entity Type                      | Dropdown     | true    | true     | false     | NA         |
      | Risk Rating                            | Dropdown     | true    | true     | false     | NA         |
      | Length of Relationship                 | Alphanumeric | true    | true     | false     | NA         |
      | Risk Rating                            | Dropdown     | true    | true     | false     | NA         |
      | Main Entity/Association Screening Risk | NA           | true    | true     | false     | NA         |
      | Adverse Media Category                 | Dropdown     | true    | true     | false     | NA         |
      | Sanctions Category                     | Dropdown     | true    | true     | false     | NA         |
      | PEP Category                           | Dropdown     | true    | true     | false     | NA         |
      | FAB Internal Watch List Category       | Dropdown     | true    | true     | false     | NA         |
      | Risk Rating                            | Dropdown     | true    | true     | false     | NA         |
      | Notes                                  | Alphanumeric | true    | false    | false     | NA         |
    And I validate the following fields in "Product Risk" Sub Flow
      | Label        | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Product Type | Dropdown     | true    | true     | false     | NA         |
      | Risk Rating  | Dropdown     | true    | true     | false     | NA         |
      | Notes        | Alphanumeric | true    | false    | false     | NA         |
    And I validate the following fields in "Transaction Profile" Sub Flow
      | Label                                             | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Anticipated Transactions Turnover (Annual in AED) | Dropdown     | true    | true     | false     | NA         |
      | Risk Rating                                       | Dropdown     | true    | true     | false     | NA         |
      | Notes                                             | Alphanumeric | true    | false    | false     | NA         |
    And I validate the following fields in "Channel and Interface" Sub Flow
      | Label                 | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Channel and Interface | Dropdown     | true    | true     | false     | NA         |
      | Risk Rating           | Dropdown     | true    | true     | false     | NA         |
      | Notes                 | Alphanumeric | true    | false    | false     | NA         |
    #Verify the Final risk is autopopulated
    And I validate the following fields in "Risk Assessment" Sub Flow
      | Label              | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Risk Rating        | Dropdown     | true    | true     | false     | NA         |
      | Risk Model         | Alphanumeric | true    | true     | false     | NA         |
      | Risk Model Version | Alphanumeric | true    | true     | false     | NA         |
      | Notes              | Alphanumeric | true    | false    | false     | NA         |
    #Verify 'Save for Later' and 'Continue' buttons are available and enabled by default
    Then I validate 'Save for Later' and 'Continue' buttons are available and enabled by default
    #Verify pencil icon (override) is available for KYC Approver Role group and user is able to Override risk rating
    Then I Override risk rating to 'Medium-low'
