#Test Case: TC_R1S2EPIC004PBI202_12
#PBI: R1S2EPIC004PBI202
#User Story ID: RM_Entity_041
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S2EPIC004PBI202_12
@Automation
  Scenario: FI- Validate for 'Medium' Risk rating all the review and Approval tasks are generated correctly when KYC Manager overrides
    #the Risk rating from 'Low to Medium' on Complete risk assessment task screen
    #Precondition: Input the appropriate data to get 'Low' Risk rating in Complete Risk Assessment screen
    
    Given I login to Fenergo Application with "RM:FI"
    When I complete "NewRequest" screen with key "FI"
    And I complete "CaptureNewRequest" with Key "LowRiskFI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "LowRiskFI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "LowRiskFI"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking 
		When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
		When I complete "AssociationDetails" screen with key "Director" 
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task 
		When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "LowRisk" 
		Then I complete "CompleteAML" task 
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task 
		Then I verify the populated risk rating is "Low" 
		And I click on "Case Details" button
	  When I assign the task "Complete Risk Assessment" to role group "CIB R&C KYC APPROVER - KYC Manager" and user name "KYCManager"
		Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
		When I search for the "CaseId"
		Then I store the "CaseId" from LE360 
		
		When I navigate to "CompleteRiskAssessmentGrid" task
		Then I verify the populated risk rating is "Low"
		Then I override risk to "Medium" 
		Then I verify the populated risk rating is "Medium"
		When I complete "RiskAssessment" task
    
    Then I login to Fenergo Application with "RM:FI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "BUH:FI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    
    
    
    #=========================
    #Given I login to Fenergo Application with "RM:IBG-DNE"
    #When I complete "NewRequest" screen with key "FI"
    #And I complete "CaptureNewRequest" with Key "C1" and below data
      #| Product | Relationship |
      #| C1      | C1           |
    #And I click on "Continue" button
    #When I complete "ReviewRequest" task
    #Then I store the "CaseId" from LE360
    #Given I login to Fenergo Application with "KYCMaker: FIG"
    #When I search for the "CaseId"
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #When I navigate to "EnrichKYCProfileGrid" task
    #When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    #When I complete "AddAddressFAB" task
    #Then I store the "CaseId" from LE360
    #When I complete "EnrichKYC" screen with key "C1"
    #And I click on "SaveandCompleteforEnrichKYC" button
    #When I navigate to "CaptureHierarchyDetailsGrid" task
    #When I add AssociatedParty by right clicking
    #When I add "AssociatedParty" via express addition
    #When I complete "AssociationDetails" screen with key "COO"
    #When I complete "CaptureHierarchyDetails" task
    #When I navigate to "KYCDocumentRequirementsGrid" task
    #Then I store the "CaseId" from LE360
    #When I add a "DocumentUpload" in KYCDocument
    #Then I complete "KYCDocumentRequirements" task
    #When I navigate to "CompleteAMLGrid" task
    #When I Initiate "Fircosoft" by rightclicking
    #And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    #And I click on "SaveandCompleteforAssessmentScreen1" button
    #Then I complete "CompleteAML" task
    #When I navigate to "CompleteID&VGrid" task
    #When I complete "CompleteID&V" task
    #When I navigate to "CaptureRiskCategoryGrid" task
    #When I login to Fenergo Application with "CDD"
    #Verify Compliance user(CDD) is able to view the scores for each risk attributes and overrall score
    #Verify pencil icon (override) is available for KYC Manager
    #Verify the Final risk is autopopulated as 'Low'
    #Then I validate Final risk is autopopulated as 'Low'
    #Override Risk rating from 'very-high' to 'Medium'
    #Then I click on pencil to override risk as 'Medium'
    #Then I validate overall Risk rating is updated as 'Medium'
    #Verify "Relationship Manager Review SignOff' task is generated
    #Then I login to Fenergo Application with "RM:IBG-DNE"
    #When I search for the "CaseId"
    #When I navigate to "RelationshipManagerReviewSignOffGrid" task
    #When I complete "RelationshipManagerReviewSignOffGrid" task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    #When I search for the "CaseId"
    #When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    #When I search for the "CaseId"
    #When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task with key "RefertoRiskAssessment"
    #And I click on "Submit" button
    #Verify 'Business Unit Head Review and Sign-Off' task is generated
    #Then I login to Fenergo Application with "Business Unit Head (N3)"
    #When I search for the "CaseId"
    #When I navigate to "BusinessUnitHeadReviewandSign-Off" task
    #When I complete "BusinessUnitHeadReviewandSign-Off" task
    #Verify 'Capture FAB References' task is generated
    #Then I login to Fenergo Application with "KYCMaker: Corporate"
    #When I search for the "CaseId"
    #When I navigate to "CaptureFABReferences" task
    #When I complete "CaptureFABReferences" task
    #And I Assert case status as 'Closed'
