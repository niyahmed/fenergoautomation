Feature: LEM Launch Screen Changes
#Test Case: TC_R2EPIC018PBI002.1_01
#PBI: R2EPIC018PBI002.1
#User Story ID: NA
#Designed by: Sasmita Pradhan
#Last Edited by: Sasmita Pradhan
@Automation @LEM
Scenario: Validate fields for Mantainenance request screen 
#Additional Scenario: Verify the fields "LE Details" and "Product / Trading Entities"  are selectable in Area type field
#Additional Scenario: Verify field "LE Details Changes" should appear on screen "Maintenance Request" when "Area=LE Details".
#Additional Scenario: Verify the LOVs for the fields 'LE Details Changes' on Maintenance Request screen of LEM flow
#Additional scenario: Verify the LOVs should not be available for the fields 'LE Details Changes' on Maintenance Request screen of LEM flow
Given I login to Fenergo Application with "SuperUser" 
When I complete "NewRequest" screen with key "Corporate" 
And I complete "CaptureNewRequest" with Key "C1" and below data 
| Product | Relationship |
| C1      | C1           |
And I click on "Continue" button 
When I complete "ReviewRequest" task 
Then I store the "CaseId" from LE360 
Given I login to Fenergo Application with "KYCMaker: Corporate" 
When I search for the "CaseId" 
Then I store the "CaseId" from LE360 
When I navigate to "ValidateKYCandRegulatoryGrid" task 
When I complete "ValidateKYC" screen with key "C1" 
And I click on "SaveandCompleteforValidateKYC" button 
When I navigate to "EnrichKYCProfileGrid" task 
When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
When I complete "AddAddressFAB" task 
Then I store the "CaseId" from LE360 
When I complete "EnrichKYC" screen with key "C1" 
And I click on "SaveandCompleteforEnrichKYC" button 
When I navigate to "CaptureHierarchyDetailsGrid" task 
When I add AssociatedParty by right clicking 
When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
When I complete "AssociationDetails" screen with key "Director" 
When I complete "CaptureHierarchyDetails" task 
When I navigate to "KYCDocumentRequirementsGrid" task 
When I add a "DocumentUpload" in KYCDocument 
Then I complete "KYCDocumentRequirements" task 
When I navigate to "CompleteAMLGrid" task 
When I Initiate "Fircosoft" by rightclicking 
And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
Then I complete "CompleteAML" task 
When I navigate to "CompleteID&VGrid" task 
When I complete "ID&V" task 
When I complete "EditID&V" task 
When I complete "AddressAddition" in "Edit Verification" screen 
When I complete "Documents" in "Edit Verification" screen 
When I complete "TaxIdentifier" in "Edit Verification" screen 
When I complete "LE Details" in "Edit Verification" screen 
When I click on "SaveandCompleteforEditVerification" button 
When I complete "CompleteID&V" task 
When I navigate to "CompleteRiskAssessmentGrid" task 
When I complete "RiskAssessment" task 
Then I login to Fenergo Application with "SuperUser" 
When I search for the "CaseId" 
When I navigate to "ReviewSignOffGrid" task 
When I complete "ReviewSignOff" task 
#Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
#When I search for the "CaseId" 
When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
When I complete "ReviewSignOff" task 
#Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
#When I search for the "CaseId" 
When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
When I complete "ReviewSignOff" task 
#Then I login to Fenergo Application with "BUH:IBG-DNE" 
#When I search for the "CaseId" 
When I navigate to "BHUReviewandSignOffGrid" task 
When I complete "ReviewSignOff" task 
#Then I login to Fenergo Application with "KYCMaker: Corporate" 
#When I search for the "CaseId" 
And I complete "Waiting for UID from GLCMS" task from Actions button
When I navigate to "CaptureFabReferencesGrid" task 
When I complete "CaptureFABReferences" task 
And I assert that the CaseStatus is "Closed" 
#LEM flow starts
And I initiate "Maintenance Request" from action button 
#Verify the fields "Area" and "Reason" should be available on "Maintenance Request" screen under secion Select Area of Changes
Then I check that below data is visible
|FieldLabel|
|Area|
|Reason|
#Verify the fields "LE Details" and "Product / Trading Entities"  are selectable in Area type field
And I verify "Area" drop-down values
#|FieldLabel|
#|LE Details|
#|Product / Trading Entities|	
When I select "LE Details" for "Dropdown" field "Area"
Then I check that below data is visible
|FieldLabel|
|Area|
|LE Details Changes|
#Validate the LOV against PBI for the field 'LE Details Changes'
And I verify "LE Details Changes" drop-down values
#Validate the LOVs should not be available for the fields 'LE Details Changes'
Then I check that below data is not visible
|FieldLabel|
|Accounts|
|GLCMS|
|SSLs|
|Trading Agreement|

  	 
	