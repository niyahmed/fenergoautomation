#Test Case: TC_R2EPIC014PBI005.1_03
#PBI:R2EPIC014PBI005.1
#User Story ID: AC-1, AC-2, AC-3, AC-4, AC-5, AC-6,
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2EPIC014PBI005.1_03

 Scenario: Verify success code / error code received in "Publish to GLCMS" task is generated during Regular Review
     #Validate IF Fenergo receives "success code/ message" from integrated system THEN, "Publish to GLCMS" task status should update the status as completed
    #Verify IF Fenergo does not receive success code/ message from integrated system THEN, KYC Maker will be able to view the GLCMS 'Status' and 'description' other relevant details along with 'Resend' button
		Given I login to Fenergo Application with "RM:IBG-DNE" 
     When I complete "NewRequest" screen with key "Corporate" 
      And I complete "CaptureNewRequest" with Key "C1" and below data 
      | Product | Relationship | 
      | C1      | C1           | 
      And I click on "Continue" button 
     When I complete "ReviewRequest" task 
     Then I store the "CaseId" from LE360 
  
    Given I login to Fenergo Application with "KYCMaker: Corporate" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360  
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     When I complete "ValidateKYC" screen with key "C1" 
      And I click on "SaveandCompleteforValidateKYC" button 
  
     When I navigate to "EnrichKYCProfileGrid" task 
     When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
     When I complete "AddAddressFAB" task 
     Then I store the "CaseId" from LE360 
     When I complete "EnrichKYC" screen with key "C1" 
      And I click on "SaveandCompleteforEnrichKYC" button 
  
     When I navigate to "CaptureHierarchyDetailsGrid" task 
     When I add AssociatedParty by right clicking 
     When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
     When I complete "AssociationDetails" screen with key "Director" 
     When I complete "CaptureHierarchyDetails" task 
  
     When I navigate to "KYCDocumentRequirementsGrid" task 
     When I add a "DocumentUpload" in KYCDocument 
     Then I complete "KYCDocumentRequirements" task 
  
     When I navigate to "CompleteAMLGrid" task 
     When I Initiate "Fircosoft" by rightclicking 
      And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
     Then I complete "CompleteAML" task 
  
     When I navigate to "CompleteID&VGrid" task 
     When I complete "ID&V" task 
     When I complete "EditID&V" task 
     When I complete "AddressAddition" in "Edit Verification" screen 
     When I complete "Documents" in "Edit Verification" screen 
     When I complete "TaxIdentifier" in "Edit Verification" screen 
     When I complete "LE Details" in "Edit Verification" screen 
     When I click on "SaveandCompleteforEditVerification" button 
     When I complete "CompleteID&V" task 
  
     When I navigate to "CompleteRiskAssessmentGrid" task
      And I verify the populated risk rating is "Low"
     When I complete "RiskAssessment" task 
  
     Then I login to Fenergo Application with "RM:IBG-DNE" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360 
     When I navigate to "ReviewSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "BUH:IBG-DNE" 
     When I search for the "CaseId" 
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "KYCMaker: Corporate" 
     When I search for the "CaseId" 
     Then I store the "CaseId" from LE360 
      And I complete "Waiting for UID from GLCMS" task from Actions button 
     When I navigate to "CaptureFabReferencesGrid" task 
     When I complete "CaptureFABReferences" task 
      And I assert that the CaseStatus is "Closed"
  
  #Comment:=RegularReview
      And I initiate "Regular Review" from action button 
  
     When I complete "CloseAssociatedCase" task 
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     Then I store the "CaseId" from LE360 
     When I complete "ValidateKYC" screen with key "C1" 
      And I click on "SaveandCompleteforValidateKYC" button 
     When I navigate to "ReviewRequestGrid" task 
     When I complete "RRReviewRequest" task 
  
     When I navigate to "Review/EditClientDataTask" task 
     When I add a "AnticipatedTransactionActivity" from "Review/EditClientData" 
     When I complete "EnrichKYC" screen with key "RegularReviewClientaDataSanity" 
      And I click on "SaveandCompleteforEnrichKYC" button 
     
     When I navigate to "KYCDocumentRequirementsGrid" task 
     When I add a "DocumentUpload" in KYCDocument 
     Then I complete "KYCDocumentRequirements" task 
  
     When I navigate to "CompleteAMLGrid" task 
     When I Initiate "Fircosoft" by rightclicking 
      And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
     Then I complete "CompleteAML" task 
  
     When I navigate to "CompleteID&VGrid" task 
      And I click on "ID&VLinkInRR" button 
     When I click on "SaveandCompleteforEditVerification" button 
     When I complete "CompleteID&V" task 
  
     When I navigate to "CompleteRiskAssessmentGrid" task 
     And I verify the populated risk rating is "Low"
     When I complete "RiskAssessment" task 
  
     Then I login to Fenergo Application with "RM:IBG-DNE" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360 
     When I navigate to "ReviewSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "BUH:IBG-DNE" 
     When I search for the "CaseId" 
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 

    #Check Publish to GLCMS" task in "Publish FAB reference" stage is generated
    Then I validate "Publish to GLCMS" task in "Publish FAB reference" stage is generated
    #Verify Publish to GLCMS taks is assigned to KYC Maker 
    #Verify 'Navigate' and 'View task' button are present under options button and user is able to navigate through the task using these buttons
    When I click on option button on "Publish to GLCMS" task
    Then I validate 'Navigate' and 'View task' button are displaying 
    When I Click on 'Navigate' button, I navigated to "Publish FAB reference" screen
    When I click on 'view' button, I navigated to "Publish FAB reference" screen
    #Verify IF Fenergo does not receive success code/ message from integrated system THEN, KYC Maker will be able to view the GLCMS 'Status' and 'description' other relevant details along with 'Resend' button
    When I validated Status field in grid is updated as 'Error' and values in 'Request sent date and Time' and 'Response received date and Time' are updated(in miliseconds)
    And 'resend' button also displayed as Enabled
    Then user clicks on 'resend' button(until Counter party UID field is updated with value) and status gets updated to completed
    When I navigated back to 'Casedetails' task screen
    Then I validate task status of "Publish to GLCMS" task is updated as 'Completed'
    And I Assert case status as 'Closed'
    
    #Validate IF Fenergo receives "success code/ message" from integrated system THEN, "Publish to GLCMS" task status should update the status as completed
    When I validated Status field in grid is updated as 'Processed' and values in 'Request sent date and Time' and 'Response received date and Time' are updated(in miliseconds)
    When I navigated back to 'Casedetails' task screen
    Then I validate task status of "Publish to GLCMS" task is updated as 'Completed'
    And 'resend' button also displayed as disabled
      
