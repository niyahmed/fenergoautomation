#Test Case: TC_R2EPIC024PBI059.2b_04
#PBI:R2EPIC024PBI059.2c
#User Story ID: BL059 - LE_CAT_01
#Designed by: Jagdev Singh
#Last Edited by: Jagdev Singh
Feature: TC_R2EPIC024PBI059.2b_04

Scenario: Validate the refreshed (updated) field value should then be visible in the LE Category field on the LE360 LE Details screen

#Pre-Requisite - An already COB case, Complete first task either a Regular Review or LE Maintenance case. .
 
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "Case Id" 
	Then I store the "CaseId" from LE360 
	Then I navigate to the LE360 page
	and Navigate to LEDetails screen
	#Validate 'LE Category' on LEDetails screen
    Then I validate 'LE Category' field showed the updated values(after refresh)
    Then I validate 'LE Category' field is un-editable