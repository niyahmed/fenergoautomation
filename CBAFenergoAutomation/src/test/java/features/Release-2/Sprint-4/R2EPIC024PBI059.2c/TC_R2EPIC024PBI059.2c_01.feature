#Test Case: TC_R2EPIC024PBI059.2c_01
#PBI:R2EPIC024PBI059.2c
#User Story ID: BL059 - LE_CAT_01
#Designed by: Jagdev Singh
#Last Edited by: Jagdev Singh
Feature: TC_R2EPIC024PBI059.2c_01

Scenario: Validate the logic for the new 'LE Category' field applied and the field values refreshed upon completion of the Capture FAB References
 		  task for in-flight cases in COB but field is not visible.

#Pre-Requisite : An in-flight COB case, Complete the case after implementation of PBI R2EPIC024PBI059.2c.
	
	 Given I login to Fenergo Application with "RM:IBG-DNE"	
	 When I search for the In-flight "CaseId"
     Then I store the "CaseId" from LE360       
     When I Navigate "Capture Request Details" screen
     Validate 'LE Category' field is not visible on Capture Request Details screen
     Then I validate 'LE Category' field not visible.
     Then I validate 'LE Category' field is un-editable
     
     When I Navigate "Review Request" screen 
     Validate 'LE Category' field is not visible on Complete screen
     Then I validate 'LE Category' field not visible.
     Then I validate 'LE Category' field is un-editable
     
     When I navigate to "ValidateKYCandRegulatoryGrid" task
     Validate 'LE Category' field is not visible on Data screen
     Then I validate 'LE Category' field not visible.
     Then I validate 'LE Category' field is un-editable
     
     When I navigate to "EnrichKYCProfileGrid" task   
     Validate 'LE Category' field is not visible on Enrich KYC Profile screen 
     Then I validate 'LE Category' field not visible.
     Then I validate 'LE Category' field is un-editable
      