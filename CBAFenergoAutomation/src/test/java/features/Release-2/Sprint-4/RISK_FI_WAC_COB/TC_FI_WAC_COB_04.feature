#Test Case: TC_FI_WAC_COB_04
#PBI: FAB_Fenergo-WAC FI RiskAssessmentModel v1.4 
#User Story ID: 
#Designed by: Priyanka/Sasmita
#Last Edited by: Vibhav Kumar
Feature: TC_FI_WAC_COB_04

  #Placeholder for FI WAC COB
  #Refer to S.No 04 from the FAB_Fenergo-WAC FI_datasheet sheet
  @Automation
  Scenario: TC_FI_WAC_COB_04
	
	Given I login to Fenergo Application with "SuperUser" 
	When I complete "NewRequest" screen with key "TC_FI_WAC_COB_04" 
	When I complete "Product" screen with key "RMAThirdParty (Non Customer)" 
	When I complete "Product" screen with key "EscrowServices_Commercial&Real Estate" 

	And I complete "CaptureNewRequest" with Key "TC_FI_WAC_COB_04" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button
	And I store the "CaseId" from LE360  
	When I complete "ReviewRequest" task 
	
	Given I login to Fenergo Application with "SuperUser"
	And I search for the "CaseId"
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "TC_FI_WAC_COB_04" 
	And I click on "SaveandCompleteforValidateKYC" button 
	
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "TC_FI_WAC_COB_04" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "DirectorIndividual" 
	When I complete "CaptureHierarchyDetails" task 
	
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	And I store the "CaseId" from LE360 
	And I navigate to "Navigate to Legal Entity" screen of the added "NonIndividual" AssociatedParty 
	When I complete "LEDetailsNonIndividual" screen with key "TC_FI_WAC_COB_04" 
	
	When I search for the "CaseId" 
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "Individual" AssociatedParty 
	When I complete "LEDetailsIndividual" screen with key "TC_FI_WAC_COB_04" 
	When I search for the "CaseId" 
	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "TC_FI_WAC_COB_04" 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CompleteRiskAssessmentGrid" task 
	And I verify the populated risk rating is "Medium-Low"
	
	And I validate individual risk rating of labels as given below 
      | LabelName                                            | RiskRating | 
      | Country of Incorporation / Establishment:            | Medium-Low | 
      | Country of Domicile / Physical Presence:             | Medium     | 
      | Active Presence in Sanctioned Countries/Territories: |            | 
      | Association Country Risk                             | Medium     | 
      | Legal Entity Type:                                   | Medium-Low | 
      | Types of Shares (Bearer/Registered):                 | -          | 
      | Length of Relationship:                              | Low  			| 
      | Offshore Banking License:                            | -          | 
      | Main Entity / Association Screening Risk             | Medium     | 
      | Product Type:                                        | High       | 
	
	
	
	
	