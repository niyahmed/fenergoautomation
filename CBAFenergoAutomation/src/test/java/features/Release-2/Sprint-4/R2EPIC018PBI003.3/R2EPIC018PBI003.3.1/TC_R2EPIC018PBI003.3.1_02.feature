#Test Case: TC_R2EPIC018PBI003.3.1_02
#PBI:R2EPIC018PBI003.3.1
#User Story ID:AC-2, AC-3, AC-4, AC-5
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC018PBI003.3.1_02

  Scenario: Validate "Status" field is auto-populated as "Error" if Fenergo does not  receive "Counterparty UID" and "Counterparty UID" is displayed as Empty for "Publish to GLCMS" task in "Capture FAB reference" stage for KYC Maker
    #Verify Resend button display as enabled for KYC Maker on "Publish to GLCMS" task when "Status" field is auto-populated as "Error" when Fenergo does not receive "Counterparty UID" through Mule Middleware system.Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
    # Initiate LEM workflow
    When I navigate to 'LE360- LE details' screen
    When I Click on 'Actions' button and select 'Maintenance request' workflow
    When I select 'LE details' from Area drop-down, 'KYC data and details' from LE details changes drop-down and submit the details
    # Verify LEM case has been triggered
    #Verify Non-material workflow is triggered 
    Then I see 'Maintenance request' Workflow has been triggered
    And I navigated to 'CaptureProposedchanges' task
    Then I complete 'CaptureProposedchanges' task
    When I navigate to "UpdateCustomerdetails" task
    When I update 'Type' from address sub-flow and save the details
    Then I complete "UpdateCustomerdetails" task
    When I navigate to "KYCDocumentrequirement" task
    Then I complete "KYCDocumentrequirement" task
    When I navigate to "OnboardingReview" task
    When I complete "OnboardingReview" task
    #"Publish to GLCMS" task in "Publish FAB reference" stage is generated
    When I navigate to 'casedetails' task screen
    Then I validate "Publish to GLCMS" task in "Publish FAB reference" stage is generated
    # Validate "Status" field is auto-populated as "Error" if Fenergo does not  receive "Counterparty UID" and "Counterparty UID" is displayed as Empty for "Publish to GLCMS" task in "Capture FAB reference" stage for KYC Maker
    #Verify Resend button display as enabled for KYC Maker on "Publish to GLCMS" task when "Status" field is auto-populated as "Error" when Fenergo does not receive "Counterparty UID" through Mule Middleware system.Given I login to Fenergo Application with "RM:IBG-DNE"When I validated Status field in grid is updated as 'Error' and values in 'Request sent date and Time' and 'Response received date and Time' are updated(in miliseconds)
    #Verify 'Navigate' button is present under options button and user is able to navigate through the task using navigate button
    When I click on option button on "Publish to GLCMS" task
    Then I validate 'Navigate' button is displaying 
    When I click on 'Navigate' button, I navigated to "Publish FAB reference" screenWhen I validated Status field in grid is updated as 'Error' and values in 'Request sent date and Time' and 'Response received date and Time' are updated(in miliseconds)
    And 'resend' button also displayed as Enabled
    Then user clicks on 'resend' button(until Counter party UID field is updated with value) and status gets updated to completed
    When I navigated back to 'Casedetails' task screen
    Then I validate task status of "Publish to GLCMS" task is updated as 'Completed'    
    And I Assert case status as 'Closed'
