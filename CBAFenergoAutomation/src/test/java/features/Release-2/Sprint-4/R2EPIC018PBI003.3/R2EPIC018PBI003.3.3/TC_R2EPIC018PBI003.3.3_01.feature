#Test Case: TC_R2EPIC018PBI003.3.3_01
#PBI:R2EPIC018PBI003.3.3
#User Story ID:AC-1
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC018PBI003.3.3_01

  Scenario: #Validate "Publish to GLCMS" task in "Publish FAB references" stage is generated after 'review and approval' task for 'Full KYC workflow' and assigned to 'KYC Maker' in LEM
    #Verify 'Navigate' and 'View task' button are present under options button and user is able to navigate through the task using these buttons
    #Validate IF Fenergo receives "success code/ message" from integrated system THEN, "Publish to GLCMS" task status should update the status as completed
    #Verify IF Fenergo does not receive success code/ message from integrated system THEN, KYC Maker will be able to view the GLCMS 'Status' and 'description' other relevant details along with 'Resend' button
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task 
    And I validate case status is updated as 'closed'
    # Initiate LEM workflow
    When I navigate to 'LE360- LE details' screen
    When I Click on 'Actions' button and select 'Maintenance request' workflow
    When I select 'LE details' from Area drop-down, 'KYC data and details' from LE details changes drop-down and submit the details
    # Verify LEM case has been triggered    
    #Verify Full KYC workflow is triggered 
    Then I see 'Maintenance request' Workflow has been triggered
    And I navigated to 'CaptureProposedchanges' task
    Then I complete 'CaptureProposedchanges' task
    When I navigate to "UpdateCustomerdetails" task
    When I update 'Country of incorporation' from "UAE to Non-UAE" and risk elevates
    When I save the details
    Then I complete "UpdateCustomerdetails" task
    When I navigate to "KYCDocumentrequirement" task
    Then I complete "KYCDocumentrequirement" task
    When I navigate to "OnboardingReview" task
    When I complete "OnboardingReview" task
    When I navigate to 'CompleteRiskAssessment' task grid
    When I complete to 'CompleteRiskAssessment' task grid
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #"Publish to GLCMS" task in "Publish FAB reference" stage is generated and the task is assigned to KYC maker
    When I navigate to 'casedetails' task screen
    Then I validate "Publish to GLCMS" task in "Publish FAB reference" stage is generated
    And the task "Publish to GLCMS" is assigned to KYC maker
    #Verify 'View task' button is present under options button and user is able to navigate through the task using view button
    When I click on option button on "Publish to GLCMS" task
    Then I validate 'View task' button are displaying
    When I click on 'view' button, I navigated to "Publish FAB reference" screen
    #Validate IF Fenergo receives "success code/ message" from integrated system THEN, "Publish to GLCMS" task status should update the status as completed
    When I navigate to 'casedetails' task screen
    When I validated Status field in grid is updated as 'Processed' and values in 'Request sent date and Time' and 'Response received date and Time' are updated(in miliseconds)
    And 'resend' button also displayed as disabled
    When I navigated back to 'Casedetails' task screen
    Then I validate task status of "Publish to GLCMS" task is updated as 'Completed'
    And I assert case is 'closed'
    
    
    
    
    
    
    
