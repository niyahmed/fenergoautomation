#Test Case: TC_R2EPIC024PBI067.2_09
  #PBI: R2EPIC024PBI067.2
  #User Story ID: BL_067_010
  #Designed by: Sasmita Pradhan
  #Last Edited by:
  @RR
  Scenario: BBG - Validate the field behaviours in "Validate KYC and Regulatory Data" Screens in Regular Review stage:
  #Field behavior in KYC Conditions section (1 new field should be hidden,I field should be removed)
  #Validate field "Specify the prohibited Category" should be hidden from the UI
  #Validate the field "Specify the prohibited Category"should be visible ,when "Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)? "type LOV is selected as Yes
  #Validate Specify the Prohibited Category" data field with "Text" should be removed from the UI
  #Validate field"Specify the prohibited Category " should  be mandatory
  #Validate Field "Specify the prohibited Category"should be come after field "Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)?"
  #Validate Specify the prohibited Category LoVs are displayed in the order mentioned in the PBI-LOV tab
  ##################################################################################################
  #PreCondition: Create entity with client type as BBG and confidential as BBG.
  ###############################################################################################
  Given I login to Fenergo Application with "RM:BBG"
    When I complete "NewRequest" screen with key "BBG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:BBG"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:BBG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #Initiate RR
    And I initiate "Regular Review" from action button
 
    When I navigate to "ClosedAssociatedCases" task
    When I complete the "ClosedAssociatedCases" task
    
    When I navigate to "ValidateKYCandRegulatoryData" task
    #ValidateKYCandRegulatoryData screen
    # Review Request Details screen
    
    #Verify the field "Specify the prohibited Category" is not visible
    And I validate field "Specify the prohibited Category" is not visible
    #Verify the field validation of '"Specify the prohibited Category ' when Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)? type lov is selected as"Yes"
    And I select "yes " for "Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)?" field
    And I validate "Specify the prohibited Category" field is visible
    #Verify field "Specify the prohibited Category" should be come after field "Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)?"
    And I validate that field "Specify the prohibited Category" is coming after field "Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)?"
     #verify field behavior for the fields "Specify the prohibited Category"
    And I validate the following fields in "KYC Conditions" section
      | Label                                       | Field Type                | Visible | Editable | Mandatory | Field Defaults To |
      | Specify the prohibited Category             | Multiselect drop-down     | Yes     | Yes      | Yes       |                   |
      
    #Validate the Specify the prohibited Category LoVs are displayed in the order mentioned in the PBI-LOV tab
    And I validate LOVs of "Specify the prohibited Category" field
    #Refer PBI for LOV list 
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    