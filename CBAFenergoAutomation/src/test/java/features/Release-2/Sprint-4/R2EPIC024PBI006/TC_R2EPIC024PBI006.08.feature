#Test Case: TC_R2EPIC024PBI006.08
  #PBI: R2EPIC024PBI006
  #User Story ID: Finance_001
  #User Story ID: Finance_002  
  #User Story ID: Finance_003
  #User Story ID: Finance_004
  #User Story ID: Finance_005			
  #Designed by: Sasmita Pradhan
  #Last Edited by:
  @LEM
  Scenario: BBG - Validate the field behaviours in "Maintenance Request" Stage:
   #Field behavior in Customer Details section (1 new field should be added,1 field should be hidden)  
   #Validate field FAB segment Should be visible on capture request details screen in New Request Stage and  LoVs are displayed in the order mentioned in the PBI  "LoV Mapping updated for Dev "tab
   #Validate the "CRD" value types should be available during LEM flow for the field "FAB Segment"
   
   #Validate the field "Target Code" should be hidden from UI (internal booking details of COB, LEM and RR work flow )and the value should be derived based on selection of FAB segment and should be "visible,Readonly" in LE360
   #Validate the field "FAB segment and Target code" value should be displayed as concatenation of Code and description for FAB Segment and T24 target code
   #Validate the value "CIB-Default" should not be visible among the LoV for the field "FAB Segment" 
   #Validate the field "FAB segment" should be  editable  at Enrich Client Information   
   #Validate When referring back to New Request stage, updated values should be retained
    ##################################################################################################
    #PreCondition: Create entity with client type as BBG and confidential as BBG.
    #######################################################################################
    Given I login to Fenergo Application with "RM:BBG"
    When I complete "NewRequest" screen with key "BBG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:BBG"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:BBG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    
     #Initiate LEM
    And I initiate "Legal Entity Maintenance" from action button
    #Select Area as 'LEdetails' and Area of change as 'KYC Data and Customer Details'
    When I navigate to "CaptureProposedChanges" task
    #CaptureProposedChanges
    #Verify field FAB segment Should be visible  on capture request details screen
    And I Validate field "FAB segment" is visible on capture request details screen
    #Verify Field Target Code should not be visible on UI
    And I validate field "Target Code" is not visible on screen
    #verify field behavior for the field "FAB Segment"
    And I validate the following fields in "Customer Details" section
      | Label        | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | FAB Segment  | drop-down    | Yes     | Yes      | Yes       |                   |
    #Verify the FAB Segment LoVs are displayed in the order mentioned in the PBI-LoV Mapping updated for Dev tab
    And I validate LOVs of "FAB Segment" field
    #Verify the "CRD" value types should be available for the field "FAB Segment"
    And I Validate "CRD" value types is available for the field "FAB Segment"
    #Refer PBI-LoV Mapping updated for Dev tab   
    When I complete  "CaptureProposedChanges" task
    
     When I navigate to "Update Customer Details" task   
    #Internal Booking Details Section
   #Verify Field Target Code should not be visible on UI
    And I validate field "Target Code" is not visible on screen 
    When I complete "UpdateCustomerDetails" task
    When I naviagate to "KYC Document Requirements" task
    When I Complete "KYC Document Requirements" task
    When I naviagate to "KYC Document Requirements" task
    When I Complete "KYC Document Requirements" task
    When I naviagate to "OnboardingReview" task
    When I Complete "OnboardingReview" task
    #Verify When referring back to Maintenance Request stage then data entered earlier  should be retained for the fields "Address line 1", "Address line 2", and "Town/ city"
    When I select "Refer" from action button
    When I select "Maintenance Request stage" for field "RefertoStage"
    when I write "Test" for field "Referral Reason"
    And I click on "Refer" button
    Then I see "CaptureProposedChanges" task is generated
    When I navigate to "CaptureProposedChanges" task
    And I Validate all the updated values are retained in "CaptureRequestDetails" screen
    When I complete "CaptureProposedChanges" task
    When I navigate to "Update Customer Details" task
    And I Validate all the updated values are retained in "CaptureRequestDetails" screen
    
    
  