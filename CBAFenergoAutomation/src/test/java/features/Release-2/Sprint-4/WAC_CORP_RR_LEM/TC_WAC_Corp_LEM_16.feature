#Test Case: TC_WAC_Corp_LEM_16
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corp_LEM_16

  Scenario: LEM Refer back -Derive the overrall risk rating as Medium-Low (COB overall risk rating is Very High)
    #Refer to TC28 in the WAC Corp Data Sheet