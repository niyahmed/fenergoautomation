#Test Case: TC_WAC_Corp_LEM_14
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corp_LEM_14

  Scenario: LEM Refer back -Derive the overrall risk rating as Very High (COB overall risk rating is Medium-Low)
    #Refer to TC26 in the WAC Corp Data Sheet
    
    Given I login to Fenergo Application with "RM:IBG-DNE" 
		When I complete "NewRequest" screen with key "Corporate" 
		When I complete "Product" screen with key "C1"
		And I complete "CaptureNewRequest" with Key "C1" and below data 
			| Product | Relationship |
			| C1      | C1           |
		And I click on "Continue" button 
		When I complete "ReviewRequest" task 
		Then I store the "CaseId" from LE360 
		
		Given I login to Fenergo Application with "KYCMaker: Corporate" 
		When I search for the "CaseId"
		Then I store the "CaseId" from LE360  
		When I navigate to "ValidateKYCandRegulatoryGrid" task 
		When I complete "ValidateKYC" screen with key "C1" 
		And I click on "SaveandCompleteforValidateKYC" button 
		
		When I navigate to "EnrichKYCProfileGrid" task 
		When I complete "AddAddressFAB" task 
		Then I store the "CaseId" from LE360 
		When I complete "EnrichKYC" screen with key "C1" 
		And I click on "SaveandCompleteforEnrichKYC" button 
		
		When I navigate to "CaptureHierarchyDetailsGrid" task 
		When I add AssociatedParty by right clicking 
		When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
		When I complete "AssociationDetails" screen with key "Director" 
		When I complete "CaptureHierarchyDetails" task 
		
		When I navigate to "KYCDocumentRequirementsGrid" task 
		When I add a "DocumentUpload" in KYCDocument 
		Then I complete "KYCDocumentRequirements" task 
		
		When I navigate to "CompleteAMLGrid" task 
		When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
		Then I complete "CompleteAML" task 
		
		When I navigate to "CompleteID&VGrid" task 
		When I complete "CompleteID&V" task 
		
		When I navigate to "CompleteRiskAssessmentGrid" task
		And I verify the populated risk rating is "Very High"
		When I complete "RiskAssessment" task 
		
		Then I login to Fenergo Application with "RM:IBG-DNE" 
		When I search for the "CaseId"
		Then I store the "CaseId" from LE360 
		When I navigate to "ReviewSignOffGrid" task 
		When I complete "ReviewSignOff" task 
	
	   Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
  
     Then I login to Fenergo Application with "CIB R&C KYC APPROVER - VP" 
     When I search for the "CaseId" 
     When I navigate to "CIBR&CKYCApproverVPReviewGrid" task 
     When I complete "ReviewSignOff" task 
     
     Then I login to Fenergo Application with "BUH:IBG-DNE" 
     When I search for the "CaseId" 
     When I navigate to "BHUReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task 
  	
  	 Then I login to Fenergo Application with "BH:Corporate" 
     When I search for the "CaseId" 
     When I navigate to "BHReviewandSignOffGrid" task 
     When I complete "ReviewSignOff" task
  	 

     Then I login to Fenergo Application with "KYCMaker: Corporate" 
     When I search for the "CaseId" 
     Then I store the "CaseId" from LE360 
      And I complete "Waiting for UID from GLCMS" task from Actions button 
     When I navigate to "CaptureFabReferencesGrid" task 
     When I complete "CaptureFABReferences" task 
      And I assert that the CaseStatus is "Closed"
 
		Given I login to Fenergo Application with "SuperUser"
		When I search for the "CaseId"
		 
		And I initiate "Maintenance Request" from action button
		Then I store the "CaseId" from LE360
		When I select "LE Details" for field "Area"
		When I select "KYC Data and Customer Details" for field "LE Details Changes"  
		And I click on "CreateLEMSubmit" button
		
		Then I see "CaptureProposedChangesGrid" task is generated
		And I navigate to "CaptureProposedChangesGrid" task
		And I click on "SaveandCompleteCaptureProposedChanges" button
		
		And I navigate to "UpdateCusotmerDetailsGrid" task
		And I click on "SaveandCompleteUpdateCustomerDetails" button
		
		When I navigate to "KYCDocumentRequirementsGrid" task 
		And I do "DocumentUpload" for all pending documents
		Then I complete "KYCDocumentRequirements" task
		
		When I navigate to "OnboardingReviewGrid" task 
		When I select "Approve" for "Dropdown" field "Review Outcome"
		Then I complete "OnboardingReview" task 
		
		
		When I navigate to "CompleteAMLGridinLEM" task 
		When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
		Then I complete "CompleteAML" task 
		
		When I navigate to "CompleteID&VGridinLEM" task 
		When I complete "CompleteID&V" task  
		
		When I navigate to "CompleteRiskAssessmentGrid" task
		And I verify the populated risk rating is "Low"
		When I complete "RiskAssessment" task
		
		And I click on "LEMCaseDetails" button
    And I refer back the case to "Maintenance Request" stage
    
    And I navigate to "CaptureProposedChangesGrid" task
		When I complete "CaptureProposedChanges" screen with key "TC_WAC_Corp_LEM_14"
		And I click on "SaveandCompleteCaptureProposedChanges" button
		
		And I navigate to "UpdateCusotmerDetailsGrid" task
		When I complete "UpdateCustomerDetails" screen with key "TC_WAC_Corp_LEM_14"
		And I click on "SaveandCompleteUpdateCustomerDetails" button
		
		When I navigate to "KYCDocumentRequirementsGrid" task 
		And I do "DocumentUpload" for all pending documents
		Then I complete "KYCDocumentRequirements" task
		
		When I navigate to "OnboardingReviewGrid" task 
		When I select "Approve" for "Dropdown" field "Review Outcome"
		Then I complete "OnboardingReview" task 
		
		
		When I navigate to "CompleteAMLGridinLEM" task 
		Then I complete "CompleteAML" task 
		
		When I navigate to "CompleteID&VGridinLEM" task 
		When I complete "CompleteID&V" task  
		
		When I navigate to "CompleteRiskAssessmentGrid" task
		And I verify the populated risk rating is "Very High"
		When I complete "RiskAssessment" task