#Test Case: TC_Backlog 71_05
#PBI: Backlog 71
#User Story ID:
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_Backlog 71_05

  Scenario: Validate 'fenergo' logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen across all the tasks of DM workflow
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a new DM request with FABEntityType as "PCG-Entity" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    #Test-data: Verify Address sub-flow is a non-mandatory sub-flow
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    And I assert that "AddressesGrid" is non-mondatory
    When I click on plus button displaying at the top of "Address" sub-flow
    Then I navigate to "AddAddress" screen
    Then I add Address Type as "Correspondence" with country as "AE-United Arab Emirates"
    When I click on "Save" button
    #Test-data: Verify Address is added under Address sub-flow on "CaptureLEdetails" task
    Then I Check that the newly added Address is visible
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    #Test-data:Verify user is able to Add Contact
    When I click on plus button displaying at the top of "Contacts" sub-flow
    Then I navigate to "Addcontact" screen
    Then I enter all the required details and click on save
    And I see contact is added to Contact grid
    #Test-data:Verify user is able to Add Product
    When I click on plus button displaying at the top of "Products" sub-flow
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "ProductInformation" screen
    Then I enter all the required details and click on save
    And I see  product is added to Product grid
    #Test-data:Verify user is able to Add Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I enter all the required details and click on save
    Then I see relaionship is added to relationship grid
    And I click on 'save' button to save the details
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    And I navigate to DM Screen-2 #using url
    And I provide value for "T24 CIF ID" field
    And I click on "SEARCH" button
    And I assert the migrated legal entity is fetched and shown in the result grid
    And I click on button "Add Selected"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    And I am redirected to "Capture Hierarchy Details" screen
    And I assert the migrated entity is visible in the hierarchy
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    #Add Fircosoft Screening by clikcing the actions(...) button from tree view and click "Add Fircosoft Screening"
    And I add "FircosoftScreening" for the entity
    #To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
    Then I navigate to "Assessment" screen
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete 'Assessment' task
    And I assert case status is updated as 'closed'
