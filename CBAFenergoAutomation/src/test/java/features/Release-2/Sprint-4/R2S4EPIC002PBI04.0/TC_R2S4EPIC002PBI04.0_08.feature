#Test Case: TC_R2S4EPIC002PBI04.0_08
#PBI: R2S4EPIC002PBI04.0
#User Story ID: UBO_002
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2S4EPIC002PBI04.0_08

  Scenario: Validate system is mandating for screening for the UBO AP's added in multilayer in AML stage in LEM flow
    #Create entity with client type as FI and confidential as FI. Country of Incorporation as UAE
    Given I login to Fenergo Application with "RM:FI"
    When I complete "NewRequest" screen with key "FI"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #Initiate LEM
    #Change country of incorporation from UAE to Non-UAE to trigger Full kyc workflow
    And I initiate "Legal Entity Maintenance" from action button
    #Select Area as 'LEdetails' and Area of change as 'Associated Parties' and 'KYC Data and Customer Details'
    When I navigate to "CaptureProposedChanges" task
    When I complete "CaptureProposedChanges" screen with key "C1"
    When I navigate to "UpdateCustomerDetails" task
    When I complete "UpdateCustomerDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "OnboardingReview" task
    When I complete "OnboardingReview" task
    When I navigate to "CompleteAMLGrid" task
    #Add associated parties in three layers with UBO ticked
    When I add AssociatedParty by clicking on + icon
    #First layer (AP1 under main entity): Add Individual associated party via Express addition with UBO ticked
    When I complete "AssociatedPartiesExpressAddition" screen with key "Individual"
    When I select Association Type as 'Authorised Signatory', Type of Control as 'Significant Control'
    And I able to tick the check-box 'Ultimate Benifiial owner(UBO)' check-box
    #First layer (AP2 under main entity): Add Existing Individual associated party with UBO ticked
    When I add AssociatedParty by clicking on + icon
    When I select Association Type as 'Director', Type of Control as 'NA'
    And I able to tick the check-box 'Ultimate Benifiial owner(UBO)' check-box
    #First layer (AP3 under main entity): Add Non-Individual associated party with UBO ticked
    When I add AssociatedParty by clicking on + icon
    When I select Association Type as 'Ultimate Benifiial owner(UBO)', Type of Control as 'Significant Control'
    And I validate 'Ultimate Beneficial Owner' check-box is auto ticked and not editable
    #Second layer (AP4 under AP1 ): Add Existing Non-Individual associated party with UBO ticked
    When I add AssociatedParty by clicking on + icon
    When I select Association Type as 'Shareholder', Type of Control as 'No Significant Control' and 'Shareholding %' as '27'
    And I validate 'Ultimate Beneficial Owner' check-box is auto ticked and not editable
    #Third layer (AP5 under AP4 ): Add Non-Individual associated party with UBO ticked
    When I select Association Type as 'Shareholder', Type of Control as 'Significant Control' and 'Shareholding %' as '13'
    And I able to tick the check-box 'Ultimate Benifiial owner(UBO)' check-box
    #Verify system is mandating screening for all layers UBO associated parties
    When I Save and complete AML Screening task without screening
    And I validate system is giving appropriate validation message for all layer UBO's
    #Complete fircosoft screeing for only few UBO's and check screening is mandating for remaining UBO's
    When I Initiate "Fircosoft" by rightclicking
    When I Initiate "Fircosoft" by rightclicking for "2" associated party
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    And I validate system is giving appropriate validation message for all layer UBO's
