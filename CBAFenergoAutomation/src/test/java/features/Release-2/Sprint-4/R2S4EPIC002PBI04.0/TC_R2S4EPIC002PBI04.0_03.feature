#Test Case: TC_R2S4EPIC002PBI04.0_03
#PBI: R2S4EPIC002PBI04.0
#User Story ID: US009, US010, US068, US109, US111, US113
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2S4EPIC002PBI04.0_03

  Scenario: Validate the field behaviours in Association Details Screens in AML stage while adding Individual AP as Express addition in LEM flow
    #Create entity with client type as PCG-Entity and confidential as PCG. Country of Incorporation as UAE
    Given I login to Fenergo Application with "RM:PCG"
    When I complete "NewRequest" screen with key "PCG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #Initiate LEM
    #Change country of incorporation from UAE to Non-UAE to trigger Full kyc workflow
    And I initiate "Legal Entity Maintenance" from action button
    #Select Area as 'LEdetails' and Area of change as 'Associated Parties' and 'KYC Data and Customer Details'
    When I navigate to "CaptureProposedChanges" task
    When I complete "CaptureProposedChanges" screen with key "C1"
    When I navigate to "UpdateCustomerDetails" task
    When I complete "UpdateCustomerDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "OnboardingReview" task
    When I complete "OnboardingReview" task
    When I navigate to "CompleteAMLGrid" task
    When I add AssociatedParty by clicking on + icon
    #Add Individual associated party via Express addition
    When I complete "AssociatedPartiesExpressAddition" screen with key "Individual"
    #Association Details screen
    #Verify label is renamed from 'Relationship' to 'Association Type'
    And I validate the following fields in "Association" section
      | Label                           | Field Type            | Visible | Editable | Mandatory | Field Defaults To |
      | Assoication Type                | Drop-down             | Yes     | Yes      | Yes       | Select...         |
      | Type of Control                 | Drop-down             | Yes     | Yes      | Yes       | Select...         |
      | Ultimate Beneficial Owner (UBO) | Check box             | Yes     | Yes      | No        | Unchecked         |
      | Legal Basis for Data Processing | Multiselect drop-down | No      |          |           |                   |
      | GDPR Additional Comment         | Alphanumeric          | No      |          |           |                   |
      | Is BOD?                         | Drop-down             | No      |          |           |                   |
      | Is Greater than 5% Shareholder? | Alphanumeric          | No      |          |           |                   |
      | Shareholding %                  | Numeric               | No      |          |           |                   |
      | Notes On Shareholding           | Alphanumeric          | No      |          |           |                   |
    #Validate the Association Type & Type of control LoVs are dispalyed in the order mentioned in the PBI-LOV tab
    And I validate LOVs of "Assoication Type" field
    #Refer PBI for LOV list
    And I validate LOVs of "Type of Control" field
    #Refer PBI for LOV list
    #Verify the field validation of 'Is BOD' when Association type lov is selected as Director
    And I select "Director" for "Assoication Type" field
    And I validate "Is BOD?" field is visible
    And I validate the conditional field in "Association" section
      | Label   | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      | Is BOD? | Drop-down  | Yes     | Yes      | Yes       | Select...         |
    And I select "Yes" for "Is BOD?" field
    And I validate "Is Greater than 5% Shareholder?" field is visible
    And I validate the conditional field in "Association" section
      | Label                           | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Is Greater than 5% Shareholder? | Alphanumeric | Yes     | Yes      | Yes       |                   |
    And I select "No" for "Is BOD?" field
    And I validate "Is Greater than 5% Shareholder?" field is not visible
    And I select "Custodian" for "Assoication Type" field
    And I validate "Is BOD?" field is not visible
    And I validate "Is Greater than 5% Shareholder?" field is not visible
    #Verify the field validation of 'Is BOD' when Association type lov is selected as Non Executive Director
    And I select "Non Executive Director" for "Assoication Type" field
    And I validate "Is BOD?" field is visible
    And I validate the conditional field in "Association" section
      | Label   | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      | Is BOD? | Drop-down  | Yes     | Yes      | Yes       | Select...         |
    And I select "Yes" for "Is BOD?" field
    And I validate "Is Greater than 5% Shareholder?" field is visible
    And I validate the conditional field in "Association" section
      | Label                           | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Is Greater than 5% Shareholder? | Alphanumeric | Yes     | Yes      | Yes       |                   |
    And I select "No" for "Is BOD?" field
    And I validate "Is Greater than 5% Shareholder?" field is not visible
    And I select "CEO" for "Assoication Type" field
    And I validate "Is BOD?" field is not visible
    And I validate "Is Greater than 5% Shareholder?" field is not visible
    #Verify the field validation of 'Is BOD' when Association type lov is selected as Trustee
    And I select "Trustee" for "Assoication Type" field
    And I validate "Is BOD?" field is visible
    And I validate the conditional field in "Association" section
      | Label   | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      | Is BOD? | Drop-down  | Yes     | Yes      | Yes       | Select...         |
    And I select "Yes" for "Is BOD?" field
    And I validate "Is Greater than 5% Shareholder?" field is visible
    And I validate the conditional field in "Association" section
      | Label                           | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Is Greater than 5% Shareholder? | Alphanumeric | Yes     | Yes      | Yes       |                   |
    And I select "No" for "Is BOD?" field
    And I validate "Is Greater than 5% Shareholder?" field is not visible
    And I select "Fund" for "Assoication Type" field
    And I validate "Is BOD?" field is not visible
    And I validate "Is Greater than 5% Shareholder?" field is not visible
    #Verify the field validation of 'Shareholding %' when Association type lov is selected as Shareholder
    And I select "Shareholder" for "Assoication Type" field
    And I validate "Shareholding %" field is visible
    And I validate the conditional field in "Association" section
      | Label                 | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Shareholding %        | Numeric      | Yes     | Yes      | No        | Select...         |
      | Notes On Shareholding | AlphaNumeric | Yes     | Yes      | No        | Select...         |
    And I select "Buyer/Customer" for "Assoication Type" field
    And I validate "Shareholding %" field is not visible
    And I validate "Notes On Shareholding" field is not visible
