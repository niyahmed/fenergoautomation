#Test Case: TC_R2S4EPIC002PBI04.0_06
#PBI: R2S4EPIC002PBI04.0
#User Story ID: UBO_001, UBO_003b, UBO_004
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2S4EPIC002PBI04.0_06

  Scenario: Validaiton of UBO in Maintenance Request stage while adding Existing Individual AP in LEM flow
    #Create entity with client type as Corporate and confidential as IBG-GovtEntity. Country of Incorporation as UAE
    Given I login to Fenergo Application with "RM:IBG-GovtEntity"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #Initiate LEM
    #Change country of incorporation from UAE to Non-UAE to trigger Full kyc workflow
    And I initiate "Legal Entity Maintenance" from action button
    #Select Area as 'LEdetails' and Area of change as 'Associated Parties' and 'KYC Data and Customer Details'
    When I navigate to "CaptureProposedChanges" task
    #Add Existing Individual associated party
    When I add AssociatedParty by clicking on + icon
    When I complete "AssociatedPartiesExpressAddition" screen with key "Individual"
    #Association Details screen
    #Verify ability to tick UBO checkbox manually for association types other than UBO
    When I select Association Type as 'Guarantor', Type of Control as 'No Significant Control'
    And I Validate 'Ultimate Beneficial Owner' check-box is not auto ticked and editable
    And I able to tick the check-box 'Ultimate Benifiial owner(UBO)' check-box
    When I select Association Type as 'Prime Broker', Type of Control as 'Significant Control'
    And I Validate 'Ultimate Beneficial Owner' check-box is not auto ticked and editable
    And I able to tick the check-box 'Ultimate Benifiial owner(UBO)' check-box
    #Verify ability to tick UBO checkbox manually for association types shareholder without giving shareholing%
    When I select Association Type as 'Shareholder', Type of Control as 'No Significant Control'
    And I Validate 'Ultimate Beneficial Owner' check-box is not auto ticked and editable
    And I able to tick the check-box 'Ultimate Benifiial owner(UBO)' check-box
    When I select Association Type as 'Shareholder', Type of Control as 'Significant Control' and 'Shareholding %' as '1'
    And I Validate 'Ultimate Beneficial Owner' check-box is not auto ticked and editable
    And I able to tick the check-box 'Ultimate Benifiial owner(UBO)' check-box
    #Verify 'Ultimate Beneficial Owner (UBO)' check box is auto ticked and not editable when Associaiton type is UBO
    When I select Association Type as 'Ultimate Benifiial owner(UBO)', Type of Control as 'NA'
    And I validate 'Ultimate Beneficial Owner' check-box is auto ticked and not editable
    #Verify 'Ultimate Beneficial Owner (UBO)' check box is auto ticked and read only when Associaiton type is Shareholder with shareholding % is greater than or equal to 25%
    When I select Association Type as 'Shareholder', Type of Control as 'No Significant Control' and 'Shareholding %' as '44'
    And I validate 'Ultimate Beneficial Owner' check-box is auto ticked and not editable
    When I select Association Type as 'Shareholder', Type of Control as 'Significant Control' and 'Shareholding %' as '9'
    And I validate 'Ultimate Beneficial Owner' check-box is auto ticked and not editable
    #Verify the UBO badge is available in graphical and tree view for the UBO ticked Associated parties
    When I navigate to graphical view
    And I validate UBO badge is available for UBO ticked Associated parties
    #Verify total no of UBO's are displayed in the UBO filter (for ex: if there are three UBO's added then the no should be 3)
    And I validate the no of UBO's in the UBO filter
    #Verify the ability to filter only UBO's from the graphical view
    And I filter the UBO from the graphical view
    #Verify the ability to filter only UBO's from the tree view
    And I filter the UBO from the tree view
