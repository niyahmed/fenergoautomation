#Test Case: TC_R2EPIC014PBI001_13
#PBI: R2EPIC014PBI001
#User Story ID: Corp/PCG-3, FIG-3
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC014PBI001_13

  Scenario: Validate auto-calculated "Review start date" and "Compliance Review Date" field as per DD for Client type 'Corporate' for DM workflow
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a new DM request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    And I assert that "AddressesGrid" is non-mondatory
    When I click on plus button displaying at the top of "Address" sub-flow
    Then I navigate to "AddAddress" screen
    Then I add Address Type as "Correspondence" with country as "AE-United Arab Emirates"
    When I click on "Save" button
    Then I Check that the newly added Address is visible
    #Test-data:Verify user is able to Add Product
    When I click on plus button displaying at the top of "Products" sub-flow
    When I navigate to "ProductInformation" screen
    Then I enter all the required details and click on save
    And I see  product is added to Product grid
    #Test-data:Verify user is able to Add Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    Then I enter all the required details and click on save
    Then I see relaionship is added to relationship grid
    And I click on 'save' button to save the details
    And I navigate to DM Screen-2 #using url
    And I provide value for "T24 CIF ID" field
    And I click on "SEARCH" button
    And I assert the migrated legal entity is fetched and shown in the result grid
    And I click on button "Add Selected"
    And I am redirected to "Capture Hierarchy Details" screen
    And I assert the migrated entity is visible in the hierarchy
    And I add "FircosoftScreening" for the entity
    #To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
    Then I navigate to "Assessment" screen
    When I complete 'Assessment' task
    And I assert case status is updated as 'closed'
    And I assert that the CaseStatus is "Closed"
    #Test-data: "regular Review" workflow is triggered automatically when "Review Start date is same as current date" for this case
    When I navigate to "LE360-LE details" screen
    When I validate "Review Start date is same as current date"
    When I navigate to 'Cases' tab
    Then I see "regular Review" workflow is triggered for this case
    And I see under Tasks grid 'Close Associated Cases' task is triggered with 'Open' status
    #Test-data: Validate auto-calculated "Review start date" (Compliance Review Date - 90 days) and "Compliance Review Date" (KYC Approval Date + 3 years)
    #field as per DD(Field Type, Visible, Editable, Mandatory, Field Defaults To) for Risk rating 'Medium-low'
    When I navigate to "LEDetails- LE overview" screen
    Then I see "Review start date" is auto-populated as "Compliance Review Date - 90 days" and "Compliance Review Date" as "KYC Approval Date + 3 years"
    Then I check that below labels as below
      | Label                  | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      | Review start date      | Date      | true    | False    | False     | Auto-populated    |
      | Compliance Review Date | Date      | true    | False    | False     | Auto-populated    |
    #Test-data: Validate auto-calculated "Review start date" (Compliance Review Date - 90 days) and "Compliance Review Date" (KYC Approval Date + 3 years)
    #field as per DD(Field Type, Visible, Editable, Mandatory, Field Defaults To) for Risk rating 'Medium-low'
    When I navigate to "Verified LE Details - LE Overview - Verified" screen
    Then I see "Review start date" is auto-populated as "Compliance Review Date - 90 days" and "Compliance Review Date" as "KYC Approval Date + 3 years"
    Then I check that below labels as below
      | Label                  | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      | Review start date      | Date      | true    | False    | False     | Auto-populated    |
      | Compliance Review Date | Date      | true    | False    | False     | Auto-populated    |
