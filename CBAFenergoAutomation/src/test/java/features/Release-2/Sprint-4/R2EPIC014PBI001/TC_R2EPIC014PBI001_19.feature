#Test Case: TC_R2EPIC014PBI001_19
#PBI: R2EPIC014PBI001
#User Story ID: Corp/PCG-3, FIG-3
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC014PBI001_19

  Scenario: 
    # Validate "Review Due Date" has been Removed on LE360-LE details screen for Client type 'Corporate' for DM Workflow
    #Validate "Review Due Date" has been Removed on LE360-LE details screen for Client type 'BBG' for DM Workflow
    #Validate "Review Due Date" has been Removed on LE360-LE details screen for Client type 'PCG entity' for DM Workflow
    #Validate "Review Due Date" has been Removed on LE360-LE details screen for Client type 'FI' for DM Workflow
    #Validate "Review Due Date" has been Removed on LE360-LE details screen for Client type 'NBFI' for DM Workflow
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a new DM request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    And I assert that "AddressesGrid" is non-mondatory
    When I click on plus button displaying at the top of "Address" sub-flow
    Then I navigate to "AddAddress" screen
    Then I add Address Type as "Correspondence" with country as "AE-United Arab Emirates"
    When I click on "Save" button
    Then I Check that the newly added Address is visible
    #Test-data:Verify user is able to Add Product
    When I click on plus button displaying at the top of "Products" sub-flow
    When I navigate to "ProductInformation" screen
    Then I enter all the required details and click on save
    And I see  product is added to Product grid
    #Test-data:Verify user is able to Add Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    Then I enter all the required details and click on save
    Then I see relaionship is added to relationship grid
    And I click on 'save' button to save the details
    And I navigate to DM Screen-2 #using url
    And I provide value for "T24 CIF ID" field
    And I click on "SEARCH" button
    And I assert the migrated legal entity is fetched and shown in the result grid
    And I click on button "Add Selected"
    And I am redirected to "Capture Hierarchy Details" screen
    And I assert the migrated entity is visible in the hierarchy
    And I add "FircosoftScreening" for the entity
    #To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
    Then I navigate to "Assessment" screen
    When I complete 'Assessment' task
    And I assert case status is updated as 'closed'
    And I assert that the CaseStatus is "Closed"
    When I navigate to "LE360-LE details" screen
    # Validate "Review Due Date" has been Removed on LE360-LE details screen
    And I validate Validate "Review Due Date" has been Removed on LE360-LE details screen
    # Validate "Review Due Date" has been Removed on "Verified LE Details - LE Overview - Verified" screen
    When I navigate to "Verified LE Details - LE Overview - Verified" screen
    And I validate Validate "Review Due Date" has been Removed on "Verified LE Details - LE Overview - Verified"
    #Validate "Review Due Date" has been Removed on LE360-LE details screen for Client type 'BBG' for DM Workflow
    When I create a new DM request with FABEntityType as "BBG" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    And I assert that "AddressesGrid" is non-mondatory
    When I click on plus button displaying at the top of "Address" sub-flow
    Then I navigate to "AddAddress" screen
    Then I add Address Type as "Correspondence" with country as "AE-United Arab Emirates"
    When I click on "Save" button
    Then I Check that the newly added Address is visible
    #Test-data:Verify user is able to Add Product
    When I click on plus button displaying at the top of "Products" sub-flow
    When I navigate to "ProductInformation" screen
    Then I enter all the required details and click on save
    And I see  product is added to Product grid
    #Test-data:Verify user is able to Add Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    Then I enter all the required details and click on save
    Then I see relaionship is added to relationship grid
    And I click on 'save' button to save the details
    And I navigate to DM Screen-2 #using url
    And I provide value for "T24 CIF ID" field
    And I click on "SEARCH" button
    And I assert the migrated legal entity is fetched and shown in the result grid
    And I click on button "Add Selected"
    And I am redirected to "Capture Hierarchy Details" screen
    And I assert the migrated entity is visible in the hierarchy
    And I add "FircosoftScreening" for the entity
    #To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
    Then I navigate to "Assessment" screen
    When I complete 'Assessment' task
    And I assert case status is updated as 'closed'
    And I assert that the CaseStatus is "Closed"
    When I navigate to "LE360-LE details" screen
    # Validate "Review Due Date" has been Removed on LE360-LE details screen
    And I validate Validate "Review Due Date" has been Removed on LE360-LE details screen
    # Validate "Review Due Date" has been Removed on "Verified LE Details - LE Overview - Verified" screen
    When I navigate to "Verified LE Details - LE Overview - Verified" screen
    And I validate Validate "Review Due Date" has been Removed on "Verified LE Details - LE Overview - Verified"
    #Validate "Review Due Date" has been Removed on LE360-LE details screen for Client type 'PCG-Entity' for DM Workflow
    When I create a new DM request with FABEntityType as "PCG-Entity" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    And I assert that "AddressesGrid" is non-mondatory
    When I click on plus button displaying at the top of "Address" sub-flow
    Then I navigate to "AddAddress" screen
    Then I add Address Type as "Correspondence" with country as "AE-United Arab Emirates"
    When I click on "Save" button
    Then I Check that the newly added Address is visible
    #Test-data:Verify user is able to Add Product
    When I click on plus button displaying at the top of "Products" sub-flow
    When I navigate to "ProductInformation" screen
    Then I enter all the required details and click on save
    And I see  product is added to Product grid
    #Test-data:Verify user is able to Add Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    Then I enter all the required details and click on save
    Then I see relaionship is added to relationship grid
    And I click on 'save' button to save the details
    And I navigate to DM Screen-2 #using url
    And I provide value for "T24 CIF ID" field
    And I click on "SEARCH" button
    And I assert the migrated legal entity is fetched and shown in the result grid
    And I click on button "Add Selected"
    And I am redirected to "Capture Hierarchy Details" screen
    And I assert the migrated entity is visible in the hierarchy
    And I add "FircosoftScreening" for the entity
    #To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
    Then I navigate to "Assessment" screen
    When I complete 'Assessment' task
    And I assert case status is updated as 'closed'
    And I assert that the CaseStatus is "Closed"
    When I navigate to "LE360-LE details" screen
    # Validate "Review Due Date" has been Removed on LE360-LE details screen
    And I validate Validate "Review Due Date" has been Removed on LE360-LE details screen
    # Validate "Review Due Date" has been Removed on "Verified LE Details - LE Overview - Verified" screen
    When I navigate to "Verified LE Details - LE Overview - Verified" screen
    And I validate Validate "Review Due Date" has been Removed on "Verified LE Details - LE Overview - Verified"
    #Validate "Review Due Date" has been Removed on LE360-LE details screen for Client type 'FI' for DM Workflow
    When I create a new DM request with FABEntityType as "FI" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    And I assert that "AddressesGrid" is non-mondatory
    When I click on plus button displaying at the top of "Address" sub-flow
    Then I navigate to "AddAddress" screen
    Then I add Address Type as "Correspondence" with country as "AE-United Arab Emirates"
    When I click on "Save" button
    Then I Check that the newly added Address is visible
    #Test-data:Verify user is able to Add Product
    When I click on plus button displaying at the top of "Products" sub-flow
    When I navigate to "ProductInformation" screen
    Then I enter all the required details and click on save
    And I see  product is added to Product grid
    #Test-data:Verify user is able to Add Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    Then I enter all the required details and click on save
    Then I see relaionship is added to relationship grid
    And I click on 'save' button to save the details
    And I navigate to DM Screen-2 #using url
    And I provide value for "T24 CIF ID" field
    And I click on "SEARCH" button
    And I assert the migrated legal entity is fetched and shown in the result grid
    And I click on button "Add Selected"
    And I am redirected to "Capture Hierarchy Details" screen
    And I assert the migrated entity is visible in the hierarchy
    And I add "FircosoftScreening" for the entity
    #To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
    Then I navigate to "Assessment" screen
    When I complete 'Assessment' task
    And I assert case status is updated as 'closed'
    And I assert that the CaseStatus is "Closed"
    When I navigate to "LE360-LE details" screen
    # Validate "Review Due Date" has been Removed on LE360-LE details screen
    And I validate Validate "Review Due Date" has been Removed on LE360-LE details screen
    # Validate "Review Due Date" has been Removed on "Verified LE Details - LE Overview - Verified" screen
    When I navigate to "Verified LE Details - LE Overview - Verified" screen
    And I validate Validate "Review Due Date" has been Removed on "Verified LE Details - LE Overview - Verified"
    #Validate "Review Due Date" has been Removed on LE360-LE details screen for Client type 'NBFI' for DM Workflow
    When I create a new DM request with FABEntityType as "NBFI" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    And I assert that "AddressesGrid" is non-mondatory
    When I click on plus button displaying at the top of "Address" sub-flow
    Then I navigate to "AddAddress" screen
    Then I add Address Type as "Correspondence" with country as "AE-United Arab Emirates"
    When I click on "Save" button
    Then I Check that the newly added Address is visible
    #Test-data:Verify user is able to Add Product
    When I click on plus button displaying at the top of "Products" sub-flow
    When I navigate to "ProductInformation" screen
    Then I enter all the required details and click on save
    And I see  product is added to Product grid
    #Test-data:Verify user is able to Add Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    Then I enter all the required details and click on save
    Then I see relaionship is added to relationship grid
    And I click on 'save' button to save the details
    And I navigate to DM Screen-2 #using url
    And I provide value for "T24 CIF ID" field
    And I click on "SEARCH" button
    And I assert the migrated legal entity is fetched and shown in the result grid
    And I click on button "Add Selected"
    And I am redirected to "Capture Hierarchy Details" screen
    And I assert the migrated entity is visible in the hierarchy
    And I add "FircosoftScreening" for the entity
    #To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
    Then I navigate to "Assessment" screen
    When I complete 'Assessment' task
    And I assert case status is updated as 'closed'
    And I assert that the CaseStatus is "Closed"
    When I navigate to "LE360-LE details" screen
    # Validate "Review Due Date" has been Removed on LE360-LE details screen
    And I validate Validate "Review Due Date" has been Removed on LE360-LE details screen
    # Validate "Review Due Date" has been Removed on "Verified LE Details - LE Overview - Verified" screen
    When I navigate to "Verified LE Details - LE Overview - Verified" screen
    And I validate Validate "Review Due Date" has been Removed on "Verified LE Details - LE Overview - Verified"
