#Test Case: TC_R2EPIC014PBI001_02
#PBI: R2EPIC014PBI001
#User Story ID: Corp/PCG-1,FIG-1, Corp/PCG-1.1, FIG-1.1
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC014PBI001_02

  @To_be_automated
  Scenario: Validate auto-calculated "Review start date" and "Compliance Review Date" field as per DD for Risk rating 'low' on the below screens for Client type 'Corporate'
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    #Given I login to Fenergo Application with "SuperUser"
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    #Then I login to Fenergo Application with "Onboarding Maker"
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Validate Risk category as 'Low'
    Then I Select Risk category as 'Low'
    And I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydKYC"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydAVP"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BusinessUnitHead"
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    #Test-data: Validate auto-calculated "Review start date" (Compliance Review Date - 90 days) and "Compliance Review Date" (KYC Approval Date + 5 years)
    #field as per DD(Field Type, Visible, Editable, Mandatory, Field Defaults To) for Risk rating 'low
    When I navigate to "LEDetails- LE overview" screen
    Then I see "Review start date" is auto-populated as "Compliance Review Date - 90 days" and "Compliance Review Date" as "KYC Approval Date + 5 years"
    Then I check that below labels as below
      | Label                  | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      | Review start date      | Date      | true    | False    | False     | Auto-populated    |
      | Compliance Review Date | Date      | true    | False    | False     | Auto-populated    |
    #Test-data: Validate auto-calculated "Review start date" (Compliance Review Date - 90 days) and "Compliance Review Date" (KYC Approval Date + 5 years)
    #field as per DD(Field Type, Visible, Editable, Mandatory, Field Defaults To) for Risk rating 'low
    When I navigate to "LE details- Customer details screen" screen
    Then I see "Review start date" is auto-populated as "Compliance Review Date - 90 days" and "Compliance Review Date" as "KYC Approval Date + 5 years"
    Then I check that below labels as below
      | Label                  | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      | Review start date      | Date      | true    | False    | False     | Auto-populated    |
      | Compliance Review Date | Date      | true    | False    | False     | Auto-populated    |
    #Test-data: Validate auto-calculated "Review start date" (Compliance Review Date - 90 days) and "Compliance Review Date" (KYC Approval Date + 5 years)
    #field as per DD(Field Type, Visible, Editable, Mandatory, Field Defaults To) for Risk rating 'low
    When I navigate to "Verified LE Details - LE Overview - Verified" screen
    Then I see "Review start date" is auto-populated as "Compliance Review Date - 90 days" and "Compliance Review Date" as "KYC Approval Date + 5 years"
    Then I check that below labels as below
      | Label                  | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      | Review start date      | Date      | true    | False    | False     | Auto-populated    |
      | Compliance Review Date | Date      | true    | False    | False     | Auto-populated    |
    #Test-data: Validate auto-calculated "Review start date" (Compliance Review Date - 90 days) and "Compliance Review Date" (KYC Approval Date + 5 years)
    #field as per DD(Field Type, Visible, Editable, Mandatory, Field Defaults To) for Risk rating 'low
    When I navigate to "Verified LE Details - Customer Details" screen
    Then I see "Review start date" is auto-populated as "Compliance Review Date - 90 days" and "Compliance Review Date" as "KYC Approval Date + 5 years"
    Then I check that below labels as below
      | Label                  | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      | Review start date      | Date      | true    | False    | False     | Auto-populated    |
      | Compliance Review Date | Date      | true    | False    | False     | Auto-populated    |
