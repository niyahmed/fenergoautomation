#Test Case: TC_R2EPIC024PBI067.1_11
  #PBI: R2EPIC024PBI067.1
  #User Story ID: BL_067_001
  #Designed by: Sasmita Pradhan
  #Last Edited by:
  @LEM
  Scenario: PCG - Validate the field behaviours in "Capture Proposed Changes" Screens in Maintenance Request stage:
  #Field behavior in Customer Details section (1 new field should be added)
  #Validate field"Purpose of Account/Relationship" should not be mandatory
  #Validate Field "Purpose of Account/Relationship"should be come between "Channel & Interface" and "Residential Status" fields
  #Validate Purpose of Account/Relationship LoVs are displayed in the order mentioned in the PBI-LOV tab
  #Validate the field "Details for Purpose of Account/Relationship" should be visible ,when Purpose of Account/Relationship type LOV is selected as Other  
  #Validate For Field "CIF Creation required" default value should be set to 'Yes'and greyed out  
  #Validate When referring back to New Request stage, updated values  should be retained
 ###################################################################################################
  #PreCondition: Create entity with client type as PCG and confidential as PCG.
  Given I login to Fenergo Application with "RM:PCG"
    When I complete "NewRequest" screen with key "PCG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:PCG"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:PCG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #Initiate LEM
    And I initiate "Legal Entity Maintenance" from action button
    #Select Area as 'LEdetails' and Area of change as 'KYC Data and Customer Details'
    When I navigate to "CaptureProposedChanges" task
    #Verify Field "Purpose of Account/Relationship"should be come between "Channel & Interface" and "Residential Status" fields
    And I Validate the field "Purpose of Account/Relationship" is present between field Channel & Interface and Residential Status
    #Verify the field "Details for Purpose of Account/Relationship" is not visible
    And I validate field "Details for Purpose of Account/Relationship" is not visible
    #Verify the field validation of 'Details Purpose of Account/Relationship' when Purpose of Account/Relationship type lov is selected as"Other"
    And I select "Other" for "Purpose of Account/Relationship" field
    And I validate "Details Purpose of Account/Relationship" field is visible
   
    #Verify the default value is set to No for the field "CIF Creation required"
    And I Validate the default value is set to "No" for the field "CIF Creation required"
     ##Validate if CIF Creation required field should be greyed out
    And I assert "CIF Creation required" fields is greyed out
    
    #verify field behavior for the fields Purpose of Account/Relationship and Details Purpose of Account/Relationship
    And I validate the following fields in "Customer Details" section
      | Label                                       | Field Type                | Visible | Editable | Mandatory | Field Defaults To |
      | Purpose of Account/Relationship             | Multiselect drop-down     | Yes     | Yes      | No        |                   |
      | Details Purpose of Account/Relationship     | Alphanumeric              | Yes     | Yes      | No        |                   |
      | CIF Creation required                       | Check box                 | Yes     |  No      | No        | No                |
    #Validate the Purpose of Account/Relationship LoVs are displayed in the order mentioned in the PBI-LOV tab
    And I validate LOVs of "Purpose of Account/Relationship" field
    #Refer PBI for LOV list  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    