  #Test Case: TC_R2EPICCRPBI001_01
  #PBI: R2EPICCRPBI001
  #User Story ID: Data_ADD_001
  #Designed by: Sasmita Pradhan
  #Last Edited by:
 Feature: TC_R2EPICCRPBI001_01
   
 Scenario: Corporate - Validate the maximum length and special characters for the fields "Address line 1", "Address line 2", and "Town/ city" under "Address Information section" in "New request"
   #Validate the maximum length for the fields "Address line 1", "Address line 2", and "Town/ city" should be 35 characters
   #Validate only "  " " ' ( ) + , - . / : ? "special characters are allowed for the fields "Address line 1", "Address line 2", and "Town/ city"
   #Validate the validation messages when user enters more than specified character limit and other than specified special characters for above mentioned fields
   #validate "save" button should not be enabled, when user enters more than specified character limit and other than specified special characters for above mentioned fields
   #Validate When referring back to New Request stage then data entered earlier  should be retained for the fields "Address line 1", "Address line 2", and "Town/ city"
	#Validate when the user refer back and edit the above mentioned fields 	with Length of the character is more than 35 & other than specified special characters, a validation message should be displayed.
   #Validate "Save and Add another" & "Save" button should not be enabled,unless the data is not trimmed/ corrected
   #Internal Booking Details section - No change (OOTB feature)
    #Customer Details section - No change (OOTB feature)
    #Relationship section - No change (OOTB feature)
    ##################################################################################################
    #PreCondition: Create entity with client type as Corporate and confidential as IBG-DNE.
    #######################################################################################
   
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I navigate to "AddAdresss" screen by clicking on "Plus" button from "CaptureRequestDetails"
    #Address screen
    When I select "Registered COI" for "MultiSelectDropdown" field "Address Type"
    When I select "36" for "TextBox" field "Address Line 1"
    And I validate the error messgage for "AddressLine1" as "You've reached the maximum length. Address Line 1 accepts 35 characters."
    When I select "" for "TextBox" field "Address Line 1"
    When I select "@@@" for "TextBox" field "Address Line 1"
    When I select "36" for "TextBox" field "Address Line 2"
     And I validate the error messgage for "AddressLine1" as "You've reached the maximum length. Address Line 2 accepts 35 characters."
     When I complete "Addresses" screen with key "CountryAsUAE"
     And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship | 
		| C1      | C1           | 
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
	When I complete "AddAddressFAB" task 
	Then I store the "CaseId" from LE360 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "ID&V" task 
	When I complete "EditID&V" task 
	When I complete "AddressAddition" in "Edit Verification" screen 
	When I complete "Documents" in "Edit Verification" screen 
	When I complete "TaxIdentifier" in "Edit Verification" screen 
	When I complete "LE Details" in "Edit Verification" screen 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	And I verify the populated risk rating is "Low" 
	When I complete "RiskAssessment" task 
	Then I login to Fenergo Application with "RM:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "BUH:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
   And I complete "Waiting for UID from GLCMS" task from Actions button 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferencesCIFId" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
    
    
     
#    #Verify the validation messages when user enters more than specified character limit and other than specified special characters for above mentioned fields
#    #verify "save" button should not be enabled, when user enters more than specified character limit and other than specified special characters for above mentioned fields
#    And I write "Address-12345678("Resident")####@@@@@Adresss234 for field "Address line 1"
#    And I validate the validation message "You've reached the maximum length. Address Line 1 accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen
#    And I write "Address-12345678("Resident")####@@@@@Adresss234 for field "Address line 2"
#    And I validate the validation message "You've reached the maximum length. Address Line 2 accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen
#    And I write "Australia-12345678("Resident")####@@@@@Adresss234 for field "Town/ City"
#    And I validate the validation message "You've reached the maximum length. Town/ City accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen
#    And I validate "save " buton is not enable 
#    
#    #Verify the maximum length and special characters for the fields "Address line 1", "Address line 2", and "Town/ city" under "Address Information "section
#    When I write "Address-12345678("Resident")" for field "Address line 1"
#    When I write "Address-12345678("Resident")" for field "Address line 2"
#    When I write "Address-12345678("Resident")" for field "Town/ city"
#    And I validate the maximum length and special characters for the fields "Address line 1", "Address line 2", and "Town/ city" 
#    
#    And I click on "Save " button
#    And I complete "CaptureRequestDetails" task by clicking "Continue" button
#    When I navigate to "ReviewRequest" task
#    And I Validate all the updated values are retained in "ReviewRequest" screen
#    When I complete "ReviewRequest" task
#    When I navigate to "ValidateKYCandRegulatoryGrid" task
#    When I complete "ValidateKYC" screen with key "C1"
#    And I click on "SaveandCompleteforValidateKYC" button
#    
#    
#    #Verify When referring back to New Request stage then data entered earlier  should be retained for the fields "Address line 1", "Address line 2", and "Town/ city"
#    When I select "Refer" from action button
#    When I select "New Request" for field "RefertoStage"
#    When I write "Test" for field "Referral Reason"
#    And I click on "Refer" button
#    Then I see "Capture Request Details" task is generated
#    When I navigate to "Capture Request Details" task
#    When I navigate to "Address Information "section
#    And I Validate all the updated values are retained in "Address" screen
#    #Validate "Save and Add another" & "Save" button should not be enabled,unless the data is not trimmed/ corrected
#    And I verify "Save and Add another" & "Save" button is not enabled
#    #Validate when the user refer back and edit the above mentioned fields 	with Length of the character is more than 35 & other than specified special characters, a validation message should be displayed.
#    And I write "Address-12345678("Resident")####@@@@@Adresss234 for field "Address line 1"
#    And I validate the validation message "You've reached the maximum length. Address Line 1 accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen
#    And I write "Address-12345678("Resident")####@@@@@Adresss234 for field "Address line 2"
#    And I validate the validation message "You've reached the maximum length. Address Line 2 accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen
#    And I write "Australia-12345678("Resident")####@@@@@Adresss234 for field "Town/ City"
#    And I validate the validation message "You've reached the maximum length.Town/ City accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen
#    And I validate "save " buton is not enable 
#    When I complete "Capture Request Details" task
#    When I navigate to "ReviewRequest" task
#    And I Validate all the updated values are retained in "ReviewRequest" screen
#    When I complete "ReviewRequest" task
#    Then I store the "CaseId" from LE360
#    
#    Given I login to Fenergo Application with "KYCMaker: Corporate"
#    When I search for the "CaseId"
#    Then I store the "CaseId" from LE360
#    When I navigate to "ValidateKYCandRegulatoryGrid" task
#    When I complete "ValidateKYC" screen with key "C1"
#    And I click on "SaveandCompleteforValidateKYC" button
#    When I navigate to "EnrichKYCProfileGrid" task
#    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
#    When I complete "AddAddressFAB" task
#    Then I store the "CaseId" from LE360
#    When I complete "EnrichKYC" screen with key "C1"
#    And I click on "SaveandCompleteforEnrichKYC" button
#    When I navigate to "CaptureHierarchyDetailsGrid" task
#    When I add AssociatedParty by right clicking
#    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
#    When I complete "AssociationDetails" screen with key "Director"
#    When I complete "CaptureHierarchyDetails" task
#    When I navigate to "KYCDocumentRequirementsGrid" task
#    When I add a "DocumentUpload" in KYCDocument
#    Then I complete "KYCDocumentRequirements" task
#    When I navigate to "CompleteAMLGrid" task
#    When I Initiate "Fircosoft" by rightclicking
#    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
#    Then I complete "CompleteAML" task
#    When I navigate to "CompleteID&VGrid" task
#    When I complete "ID&V" task
#    When I complete "EditID&V" task
#    When I complete "AddressAddition" in "Edit Verification" screen
#    When I complete "Documents" in "Edit Verification" screen
#    When I complete "TaxIdentifier" in "Edit Verification" screen
#    When I complete "LE Details" in "Edit Verification" screen
#    When I click on "SaveandCompleteforEditVerification" button
#    When I complete "CompleteID&V" task
#    When I navigate to "CompleteRiskAssessmentGrid" task
#    When I complete "RiskAssessment" task
#    Then I login to Fenergo Application with "RM:IBG-DNE"
#    When I search for the "CaseId"
#    When I navigate to "ReviewSignOffGrid" task
#    When I complete "ReviewSignOff" task
#    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
#    When I search for the "CaseId"
#    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
#    When I complete "ReviewSignOff" task
#    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
#    When I search for the "CaseId"
#    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
#    When I complete "ReviewSignOff" task
#    Then I login to Fenergo Application with "BUH:IBG-DNE"
#    When I search for the "CaseId"
#    When I navigate to "BHUReviewandSignOffGrid" task
#    When I complete "ReviewSignOff" task
#    Then I login to Fenergo Application with "KYCMaker: Corporate"
#    When I search for the "CaseId"
#    Then I store the "CaseId" from LE360
#    And I complete "Waiting for UID from GLCMS" task from Actions button
#    When I navigate to "CaptureFabReferencesGrid" task
#    When I complete "CaptureFABReferences" task
#    And I assert that the CaseStatus is "Closed"
#    
#    
    
    
    
    
   