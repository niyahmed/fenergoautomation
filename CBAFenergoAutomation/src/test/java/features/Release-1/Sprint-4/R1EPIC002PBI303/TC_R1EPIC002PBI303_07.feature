#Test Case: TC_R1EPIC002PBI303_07
#PBI: R1EPIC002PBI303
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: LE Details / Verified LE Details

  Scenario: To validate the following section and fields are renamed under LE360 screen - LE Details section for Client type - FI
    #Section1- Section "Anticipated Transaction Volumes"  is renamed as  "Anticipated Transaction Volumes (Annual in AED)" on "LE details"
    #Section2- Renamed field under section "Anticipated Transaction Volumes" on "LE details"
    #Section3- Renamed fields under "KYC conditions" section
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a new request with FABEntityType as "FI" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "3886"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    #Complete the COB flow
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddress" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "OnboardingChecker"
    When I search for the "CaseId"
    When I navigate to "QualityControlGrid" task
    When I complete "ReviewOnboarding" task
    Then I login to Fenergo Application with "RM"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "FLoydKYC"
    When I search for the "CaseId"
    When I navigate to "FLODKYCReviewandSign-OffGrid" task
    When I complete "FLODKYCReviewandSign-Off" task
    Then I login to Fenergo Application with "FLoydAVP"
    When I search for the "CaseId"
    When I navigate to "FLODAVPReviewandSign-Off Grid" task
    When I complete "FLODAVPReviewandSign-Off" task
    Then I login to Fenergo Application with "BusinessUnitHead"
    When I search for the "CaseId"
    When I navigate to "BUHReviewandSignOffGrid" task
    When I complete "BUHReviewandSignOff" task
    When I navigate to "LE360-LEdetails" task
    #Test-data: Verify section "Anticipated Transaction Volumes"  is renamed as  "Anticipated Transaction Volumes (Annual in AED)" on "LE details"
    Then I validate Section "Anticipated Transaction Volumes"  is renamed as  "Anticipated Transaction Volumes (Annual in AED)" on "LE details" task
    #Test-data: Validate the field "Mode of Transactions "  is renamed as  "Type/Mode of Transactions" under "Anticipated Transaction Volumes (Annual in AED)"
    Then I validate the field "Mode of Transactions "  is renamed as  "Type/Mode of Transactions" under "Anticipated Transaction Volumes (Annual in AED)"
    #Test-data: Validate following fields under "KYC conditions" section on "LE details" task
    Then I validate following fields under "KYC conditions" section on "LE details" task
      | Old Label                                                      | New Label                                       |
      | Anticipated Transaction Volumes                                | Anticipated Transaction Volumes (Annual in AED) |
      | Can the corporation issue bearer shares?                       | Types of Shares (Bearer/Registered)             |
      | Name of Exchange(s) the Entity is Listed On                    | Name of Stock Exchange                          |
      | Stock Exchange Domiciled in FABs Recognized List of Countries? | FABs Recognized Stock Exchange?                 |

  Scenario: To validate the following section and fields are renamed under LE360- LE Verified Details screen for Client type - FI
    #Section1- Section "Anticipated Transaction Volumes"  is renamed as  "Anticipated Transaction Volumes (Annual in AED)" on "LE verified details"
    #Section2- Renamed field under section "Anticipated Transaction Volumes" on "LE Verified details"
    #Section3- Renamed fields under "KYC conditions" section
    When I navigate to "LE-LEVerifieddetails" task
    #Test-data: Verify section "Anticipated Transaction Volumes"  is renamed as  "Anticipated Transaction Volumes (Annual in AED)" on "LE Verified details"
    Then I validate Section "Anticipated Transaction Volumes"  is renamed as  "Anticipated Transaction Volumes (Annual in AED)" on "LEVerifieddetails" task
    #Test-data: Validate the field "Mode of Transactions "  is renamed as  "Type/Mode of Transactions" under "Anticipated Transaction Volumes (Annual in AED)"
    Then I validate the field "Mode of Transactions "  is renamed as  "Type/Mode of Transactions" under "Anticipated Transaction Volumes (Annual in AED)"
    #Test-data: Validate following fields under "KYC conditions" section on "LE Verified details" task
    Then I validate following fields under "KYC conditions" section on "LEVerifieddetails" task
      | Old Label                                                      | New Label                                       |
      | Anticipated Transaction Volumes                                | Anticipated Transaction Volumes (Annual in AED) |
      | Can the corporation issue bearer shares?                       | Types of Shares (Bearer/Registered)             |
      | Name of Exchange(s) the Entity is Listed On                    | Name of Stock Exchange                          |
      | Stock Exchange Domiciled in FABs Recognized List of Countries? | FABs Recognized Stock Exchange?                 |
