#Test Case: TC_R1EPIC002PBI303_02
#PBI: R1EPIC002PBI303
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: LE Details / Verified LE Details

  @Tobeautomated
  Scenario: To validate if the following sections are not visible under LE360 screen - LE Details section for Client type - FI
    #Section1- "Pension Scheme details" section and the fields under "Pension Scheme details" section are not visible
    #Section2- "GLCMS" section and the fields under "GLCMS" section are not visible
    #Section3- "Financial Institution Due Diligence" section and the fields under "Financial Institution Due Diligence" section are not visible
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a new request with FABEntityType as "FI" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "3886"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    #Complete the COB flow
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddress" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "OnboardingChecker"
    When I search for the "CaseId"
    When I navigate to "QualityControlGrid" task
    When I complete "ReviewOnboarding" task
    Then I login to Fenergo Application with "RM"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "FLoydKYC"
    When I search for the "CaseId"
    When I navigate to "FLODKYCReviewandSign-OffGrid" task
    When I complete "FLODKYCReviewandSign-Off" task
    Then I login to Fenergo Application with "FLoydAVP"
    When I search for the "CaseId"
    When I navigate to "FLODAVPReviewandSign-Off Grid" task
    When I complete "FLODAVPReviewandSign-Off" task
    Then I login to Fenergo Application with "BusinessUnitHead"
    When I search for the "CaseId"
    When I navigate to "BUHReviewandSignOffGrid" task
    When I complete "BUHReviewandSignOff" task
    When I navigate to "LE360-LEdetails" task
    #Test-data: Verify following sections are not visible under LE360 screen - LE Details section
    Then I validate below sections are not visible under "LE360-LEdetails" task
      #Verify "Pension Scheme details" section following fields are not visible under "Pension Scheme details"
      | Pension Scheme details                                                                      |
      | Pension Scheme Registration Number                                                          |
      | Are contributions made by the employer setting up the scheme                                |
      | Name of Pension Scheme Administrator                                                        |
      | Name of Pension Scheme Promoter                                                             |
      | Name of Pension Scheme Sponsor                                                              |
      #Verify "GLCMS" section and following fields are not visible under GLCMS section
      | Website Address                                                                             |
      | Telephone Number                                                                            |
      | GLCMS Counterparty Type                                                                     |
      | GLCMS Legal Constitution Type                                                               |
      | Is UAE Licensed                                                                             |
      | GLCMS Industry of Operation                                                                 |
      | Relationship Manager                                                                        |
      #Verify "Financial Institution Due Diligence section" section and following fields are not visible under "Pension Scheme details"
      | Type of FI relationship                                                                     |
      | Does the client provide products or services to Shell Banks?                                |
      | Does the client provide direct access of the Correspondent Bank Accounts to their customers |
      | Does the FI permit downstream nesting                                                       |
      | Is nesting to third party banks or Group Affiliates                                         |
      | Does the client intend to use FAB for Downstream Nesting                                    |
      | Nesting Countries                                                                           |
      | List of Nested Entities                                                                     |

  Scenario: To validate if the following fields are not visible under LE360 screen - LE Details section for Client type - FI
    When I navigate to "LE360-LEdetails" task
    #Test-data: Verify following fields are not visible under LE360 screen - LE Details section
    Then I validate following fields under these sections are not visible under "LE360-LEdetails" task
      | Australian Company Number Issued                              |
      | Registered by Australian Securities and Investment Commission |
      | Nature of Funding                                             |
      | Structure Type                                                |
      | Partnership Type                                              |
      | Association Type                                              |
      | Relationship with Home State Authority                        |
      | Sovereign Wealth Categories                                   |
      | Member of the International Forum of Sovereign Wealth Funds   |
      | Has the SWF signed up to the Santiago Principles?             |
      | Membership with Professional Body                             |
      | Name of the Professional Body                                 |
      | ERISA Plan Investor                                           |
      | Objects                                                       |
      | Does the charity solicit donations?                           |
      | Redemption Cycle                                              |
      | Directors Identification Number                               |

  Scenario: To validate if the following sections and fields are not visible under LE360 screen - LE Verified Details screen for Client type - FI
    When I navigate to "LE360-LEdetails" task
    #Test-data: Verify following sections and fields under these sections are not visible under LE360- LE Verified Details section
    Then I validate below sections and fields under these sections are not visible under "LE360-LEVerifieddetails" task
      #Verify "Pension Scheme details" section and following fields are not visible under "Pension Scheme details"
      | Pension Scheme details                                                                      |
      | Pension Scheme Registration Number                                                          |
      | Are contributions made by the employer setting up the scheme                                |
      | Name of Pension Scheme Administrator                                                        |
      | Name of Pension Scheme Promoter                                                             |
      | Name of Pension Scheme Sponsor                                                              |
      #Verify "GLCMS" section and following fields are not visible under GLCMS section
      | GLCMS                                                                                       |
      | Website Address                                                                             |
      | Telephone Number                                                                            |
      | GLCMS Counterparty Type                                                                     |
      | GLCMS Legal Constitution Type                                                               |
      | Is UAE Licensed                                                                             |
      | GLCMS Industry of Operation                                                                 |
      | Relationship Manager                                                                        |
      | Financial Institution Due Diligence section                                                 |
      #Verify "Financial Institution Due Diligence section" section and following fields are not visible under "Pension Scheme details"                                  |
      | Financial Institution Due Diligence section - LE Details section                            |
      | Type of FI relationship                                                                     |
      | Does the client provide products or services to Shell Banks?                                |
      | Does the client provide direct access of the Correspondent Bank Accounts to their customers |
      | Does the FI permit downstream nesting                                                       |
      | Is nesting to third party banks or Group Affiliates                                         |
      | Does the client intend to use FAB for Downstream Nesting                                    |
      | Nesting Countries                                                                           |
      | List of Nested Entities                                                                     |

  Scenario: To validate if the following fields are not visible under LE360- LE Verified Details screen for Client type - FI
    Given I login to Fenergo Application with "SuperUser"
    When I navigate to "LE360-LEVerifieddetails" task
    #Test-data: Verify following fields are not visible under LE360- LE Verified Details section
    Then I validate following fields under these sections are not visible under "LE360-LEVerifieddetails" task
      | Australian Company Number Issued                              |
      | Registered by Australian Securities and Investment Commission |
      | Nature of Funding                                             |
      | Structure Type                                                |
      | Partnership Type                                              |
      | Association Type                                              |
      | Relationship with Home State Authority                        |
      | Sovereign Wealth Categories                                   |
      | Member of the International Forum of Sovereign Wealth Funds   |
      | Has the SWF signed up to the Santiago Principles?             |
      | Membership with Professional Body                             |
      | Name of the Professional Body                                 |
      | ERISA Plan Investor                                           |
      | Objects                                                       |
      | Does the charity solicit donations?                           |
      | Redemption Cycle                                              |
      | Directors Identification Number                               |
