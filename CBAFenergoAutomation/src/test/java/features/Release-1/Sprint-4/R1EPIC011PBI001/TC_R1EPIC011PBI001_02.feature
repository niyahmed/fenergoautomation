#Test Case: TC_R1EPIC011PBI001_02
#PBI: R1EPIC011PBI001
#User Story ID: US112
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC011PBI001_02

  @Automation
  Scenario: Verify creating end to end COB case for client type 'Business Banking Group'
    Given I login to Fenergo Application with "RM:BBG"
    	#Creating a legal entity with "Client Entity Type" as "Business Banking Group" and "Legal Entity Role" as "Client/Counterparty"
    When I complete "NewRequest" screen with key "BBG"
    And I complete "CaptureNewRequest" with Key "BBG" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Then I login to Fenergo Application with "RM:BBG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    Then I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    
    #Then I login to Fenergo Application with "KYCMaker: BBG"
    #When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task  
	  When I complete "RiskAssessment" task
    	
    	#Then I login to Fenergo Application with "Relationship Manager"
    Then I login to Fenergo Application with "RM:BBG"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    	#Then I login to Fenergo Application with "FLoydKYC"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    
    	#Then I login to Fenergo Application with "FLoydAVP"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    
    
    	#Then I login to Fenergo Application with "BusinessUnitHead"
    Then I login to Fenergo Application with "BUH:BBG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    And I complete "Waiting for UID from GLCMS" task from Actions button 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferencesCIFId" task 
	When I complete "CaptureFABReferences" task 

