#Test Case: TC_R1EPIC002PBI020_01
#PBI: R1EPIC002PBI020
#User Story ID: US009, US010, US068, US109, US111
#Designed by: Anusha PS
#Last Edited by: Anusha PS
#Note: The same test data can be used for all test cases of this PBI.
Feature:
Enrich Client Information (Capture Hierarchy Details>Associated Parties>Association Details) 

@Automation @TC_R1EPIC002PBI020_01 
Scenario:
Validate behaviour of fields in Capture Hierarchy Details>Associated Parties>Association Details screen for expressly added associated party - part 1 

	Given I login to Fenergo Application with "RM:IBG-DNE" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I complete "NewRequest" screen with key "Corporate" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		|Product|Relationship|
		|C1|C1|
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	#Perform below step by right clicking the legal entity from hierarchy and clicking "Add Associated Party"
	And I verify "Association Type " drop-down values 
	And I verify "Type of Control" drop-down values 
	And I check that below data is not visible 
		|Label|
		|Is BOD?|
		|Is Greater than 5% Shareholder?|
	When I complete "AssociationDetails" screen with key "Director" 
	
	
	
	
