#Test Case: TC_R1EPIC011PBI006_03
#PBI: R1EPIC011PBI006
#User Story ID: US075 (PRIMARY), US076 (PRIMARY), US077 (PRIMARY), US078 (PRIMARY), US079 (PRIMARY), US080 (PRIMARY), US081 (PRIMARY),
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC011PBI006_03

  @To_be_automated
  Scenario: Validate behaviour of below fields in Business Details section of Enrich KYC Profile screen
    #Anticipated Transactions Turnover (Annual in AED)
    #Value of Initial Deposit (AED)
    #Source of Initial Deposit & Country of Source
    #Value of Capital or Initial Investment in Business (AED)
    #Annual Business Turnover Annual Business Turnover (AED)
    #Projected Annual Business Turnover (AED)
    #Annual Business Turnover of Group (AED)
    Given I login to Fenergo Application with "RM"
    #Create entity with Country of Incorporation = UAE and client type = Business Banking Group
    When I create a new request with FABEntityType as "BusinessBankingGroup" and LegalEntityRole as "Client/Counterparty"
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    #Verify the below behaviour in Business Details section
    And I validate the following fields in "Business Details" section
      | Label                                                    | FieldType    | Visible | Editable | Mandatory |
      | Anticipated Transactions Turnover (Annual in AED)        | Dropdown     | true    | true     | true      |
      | Value of Initial Deposit (AED)                           | Numeric      | true    | true     | false     |
      | Value of Capital or Initial Investment in Business (AED) | Numeric      | true    | true     | false     |
      | Annual Business Turnover (AED)                           | Alphanumeric | true    | true     | false     |
      | Projected Annual Business Turnover (AED)                 | Numeric      | true    | true     | false     |
      | Annual Business Turnover of Group (AED)                  | Numeric      | true    | true     | false     |
      | Source of Initial Deposit & Country of Source            | String       | true    | true     | false     |
    #Validate 'Source of Initial Deposit & Country of Source' field is mandatory if value is entered in 'Value of Initital Deposit (AED) field
    #Enter value for Value of Initial Deposit (AED)
    And I validate the following fields in "Business Details" section
      | Label                                         | FieldType | Visible | Editable | Mandatory | DefaultsTo |
      | Source of Initial Deposit & Country of Source | String    | true    | true     | true      |            |
    #Verify the lovs for "Anticipated Transactions Turnover (Annual in AED)" field
    Then I validate the specific LOVs for "Anticipated Transactions Turnover (Annual in AED)"
      | Lovs                             |
      | Less < 5,000,000                 |
      | Between 5,000,000 - 10,000,000   |
      | Between 10,000,000 - 25,000,000  |
      | Between 25,000,000 - 100,000,000 |
      | More than 100,000,000            |
      | Not Applicable                   |
      | Withdrawals                      |
