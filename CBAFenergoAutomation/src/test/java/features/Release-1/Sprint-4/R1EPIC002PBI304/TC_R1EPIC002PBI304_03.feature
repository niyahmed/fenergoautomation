#Test Case: TC_R1EPIC002PBI304_03
#PBI: R1EPIC002PBI304
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC002PBI304_03

  @Automation
  Scenario: Validate the field behaviour in Enrich KYC Profile screen for Corporate Client type
   Validate the field behaviour in Enrich KYC Profile screen for Corporate client type 
	Given I login to Fenergo Application with "RM:IBG-DNE" 
	#Create entity with Client type = FI
	When I complete "NewRequest" screen with key "Corporate" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	And I store the "CaseId" from LE360 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	And I search for the "CaseId"
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	#Validate Financial Institition Due Diligence section is hidden from Enrich KYC Profile screen
	#Validate 'Source of Any Other Income' is hidden from Source of Funds And Welath Details section of Enrich KYC Profile screen
	And I check that below data is not visible 
      | FieldLabel                          | 
      | Source of Any Other Income          | 
      | Financial Institition Due Diligence | 
