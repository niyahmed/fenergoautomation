#Test Case: TC_R1EPIC002PBI304_04
#PBI: R1EPIC002PBI304
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC002PBI304_04 

@Automation 
Scenario:
Validate the field behaviour in Enrich KYC Profile screen for PCG-Entity Client type 
	Given I login to Fenergo Application with "RM:PCG" 
	#Create entity with Client type = PCG-Entity
	When I complete "NewRequest" screen with key "PCG" 
	And I complete "CaptureNewRequest" with Key "PCG-Entity" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	And I store the "CaseId" from LE360 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	And I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "PCG-Entity" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	#Validate Financial Institition Due Diligence section is hidden from Enrich KYC Profile screen
	And I check that below data is not visible 
      | FieldLabel                          | 
      | Source of Any Other Income          | 
      | Financial Institition Due Diligence | 
