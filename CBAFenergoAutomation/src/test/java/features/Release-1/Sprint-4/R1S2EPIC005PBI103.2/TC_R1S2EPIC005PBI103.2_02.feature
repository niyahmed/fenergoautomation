#Test Case: TC_R1S2EPIC005PBI103.2_02
#PBI: R1S2EPIC005PBI103.2
#User Story ID: FIG10, FIG11, FIG12, FIG13, FIG14, FIG15
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S2EPIC005PBI103.2_02

  @Automation
  Scenario: 
    Verify New added Fields and Grids added on "Edit Verification" task of "Complete ID&V" task for "AML"  stage

    Given I login to Fenergo Application with "RM:FI"
    When I create a new request with FABEntityType as "Financial Institution (FI)" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    When I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "FI"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "Financial Institution"
    And I click on "SaveandCompleteforEnrichKYC" button
    
		When I navigate to "CaptureHierarchyDetailsGrid" task 
		When I complete "CaptureHierarchyDetails" task 
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    When I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
	  When I add AssociatedParty by right clicking 
		When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
		When I complete "AssociationDetails" screen with key "Director" 
	  When I complete "CompleteAML" task
	    
	    When I navigate to "CompleteID&VGrid" task 
			When I complete "ID&V" task 
			When I complete "EditID&V" task 
			And I check that below data is visible
      | FieldLabel                               | 
      | Legal Entity Name                        | 
      | Client Type                              | 
      | Legal Entity Type                        | 
      | Country of Incorporation / Establishment | 
      | Country of Domicile / Physical Presence  | 
      | LEI                                      | 
      | Name of Registration Body                | 
      
      And I click on "EntityUnderVerification" button
    	And I check that below data is visible	
    	|FieldLabel			|
      | ID&V Category |
      
      Then I check that below subflow is visible
      | Subflow				 |
      | Addresses      |
	    | Documents      |
	    | Tax Identifier |
      
 
	    
	
	    
	    #=Below steps are written for Manual Testing.
#	    Then I can see already added association under "Associatedparties" section
#	    When I click on "Edit" button displaying corresponding to the added association
#	    When I navigate to "EditVerification" task
#	    Then I can see below mentioned Newly added Fields and grids on LE details on "EditVerification" task
#	    And I verify below fields under "LE details" section
#	      | Legal Entity Name                       |
#	      | Client Type                             |
#	      | Legal Entity Type                       |
#	      | Country of Incorporation/ Establishment |
#	      | Country of Domocile/ Physical Pesence   |
#	      | LEI                                     |
#	      | Name of Registration Body               |
#	    And I verify below fields under	"Entity Under Verification" section
#	      | ID&V Category |
#	    And I verify below fields underGrids
#	      | Addresses      |
#	      | Documents      |
#	      | Tax Identifier |
#	
