#Test Case: TC_R1S2EPIC005PBI103.2_08
#PBI: TC_R1S2EPIC005PBI103.2
#User Story ID: CORP5, CORP9, CORP10
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Additional Doc Requirements, View Documents which are pending upload, Upload Document

  @Automation
  Scenario: Verify the following functionality on Document details screen for Onboarding Maker
    
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddress" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    And I click on "SortDocuments" button and sort by "Mandatory"
    And I add a document for a requirement and verify status before upload is "Pending" and after upload is "Received"
    
    
    # Test-Data: Verify uploading a new document corresponding to document requirement
    #When I click on "AttachDocument" from options button displaying corresponding to document requirement
    #When I navigate to "DocumentDetails" screen
    #When I add document by browsing a document from "UploadDocument" button
    #When I add "DocumentName" and "Documentcategory" and click on "Save" button
    #Then I can see document is added successfully
    #When I navigate back to "KYCDocumentRequirementsGrid" task
    #Comment: Below 4 Steps are not applicable because once the document is added, another document can not be added further
    # Test-Data: Verify adding additional document corresponding to document requirement
    #-When I click on "AttachDocument" from options button displaying corresponding to document requirement having a document attached to it
    #-When I navigate to "DocumentDetails" screen
    #-When I add document by browsing a document from "UploadDocument" button
    #-When I add "DocumentName" and "Documentcategory" and click on "Save" button
    #-Then I can see document is added successfully
    #-When I navigate back to "KYCDocumentRequirementsGrid" task
    #-When I navigate to "DocumentDetails" screen
    #When I expand the Document requirement on "KYCDocumentRequirementsGrid" task
    # Test-Data: Verify User is able to view pending document corresponding to document requirement on "KYCDocumentRequirementsGrid" screen
    #Then I can see "Status" of document is displaying as "pending" in document grid
