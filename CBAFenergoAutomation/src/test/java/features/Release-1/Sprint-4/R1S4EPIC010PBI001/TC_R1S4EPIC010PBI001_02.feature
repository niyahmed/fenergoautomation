#Test Case: TC_R1S4EPIC010PBI001_02
#PBI: R1S4EPIC010PBI001
#User Story ID: Case Approval 01/02, Case Approval 03, Reject / Refer / Approve 08
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1S4EPIC010PBI001_02

  @To_be_automated
  Scenario: Verify the Manager Review and Approval workflow process for MEDIUM/LOW Risk rating COB case for Corporate client type
    #For Medium/Low risk rating 4 tasks should be trigerred in the Following order
    #Relationship Manager Review and Sign-Off
    #CIB R&C KYC Approver - KYC Manager Review and Sign-Off
    #CIB R&C KYC Approver - AVP Review and Sign-Off
    #Business Unit Head Review and Sign-Off
    Given I login to Fenergo Application with "RM"
    #Create an Enity with risk rating as  Medium/Low, client type as Corporate and COI as UAE
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
   And I complete "CaptureNewRequest" with Key "C1" and below data 
	|Product|Relationship|
	|C1|C1|
    And I click on "Continue" button
   When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "SuperUser"
    And I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
   	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button
	Given I login to Fenergo Application with "SuperUser"
    And I search for the "CaseId"
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddress" task 
	When I complete "EnrichKYC" screen with key "C1" 
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
     
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task

    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task
    #Ensure the risk rating is  Medium/Low
    Then I login to Fenergo Application with "OnboardingChecker"
	When I search for the "CaseId"
    When I navigate to "QualityControlGrid" task
    When I complete "ReviewOnboarding" task
    And I take a screenshot
    #Verify 'Relationship Manager Review and Sign-Off' task is generated and assigned to respective team (keep it commented )
	  Then I login to Fenergo Application with "RM"
	  When I search for the "CaseId"
	  And I validate "Relationship Manager Review and Sign-Off" field is visible in LHN
	
	When I navigate to "ReviewSignOffGrid" task
    #Verify 'Relationship Manager Review and Sign-Off' option (Hyperlink) is available under Review and Approval stage in Left hand menu
   When I complete "ReviewSignOff" screen with key "Refer"
    And I take the screenshot for "CaseDetails"
   # keep line 62-64 commented. NO need to test it for this case. 
    
#   And I validate the following fields in "RelationshipManagerReviewandSign" Sub Flow
#     | Label        | FieldType      | Visible | ReadOnly | Mandatory | DefaultsTo |
#      | Refer Reason | Dropdown | true    | false     | true      | Select...   |
#    And I verify "Refer Reason" drop-down values

   When I complete "ReviewSignOff" task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated and assigned to respective team
     And I take a screenshot
      Then I login to Fenergo Application with "FLoydKYC"
      When I search for the "CaseId"
     
    And I validate "CIBR&CKYCApprover" field is visible in LHN
   
    And I take a screenshot
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #Validate the Refer Reason Lov's
    When I complete "ReviewSignOff" screen with key "Refer"
    # And I validate the following fields in "RelationshipManagerReviewandSign" Sub Flow
    # | Label        | FieldType      | Visible | ReadOnly | Mandatory | DefaultsTo |
    #  | Refer Reason | Dropdown | true    | false     | true      | Select...   |
    # And I verify "Refer Reason" drop-down values
    When I complete "ReviewSignOff" task
    # Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated and assigned to respective team
    And I take a screenshot
      Then I login to Fenergo Application with "FlodAVP"
      When I search for the "CaseId"
      And I validate "CIBR&CKYCApproverAVPReview" field is visible in LHN
      When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
      When I complete "ReviewSignOff" screen with key "Refer"
       When I complete "ReviewSignOff" task
       Then I login to Fenergo Application with "BusinessUnitHead"
        When I search for the "CaseId"
       And I validate "BusinessUnitHead" field is visible in LHN
       When I navigate to "BHUReviewandSignOffGrid" task
       When I complete "ReviewSignOff" screen with key "Refer"
    #   And I validate the following fields in "RelationshipManagerReviewandSign" Sub Flow
    #	| Label        | FieldType      | Visible | ReadOnly | Mandatory | DefaultsTo |
    # | Refer Reason | Dropdown | true    | false     | true      | Select...   |
      When I complete "ReviewSignOff" task
      And I assert that the CaseStatus is "Closed"
#    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' option (Hyperlink) is available under Review and Approval stage in Left hand menu
#    And I validate the following fields in left hand menu
#      | Label                                          | FieldType |
#      | CIB R&C KYC Approver - AVP Review and Sign-Off | Hyperlink |
#    And I take the screenshot for "CaseDetails"
#    When I navigate to "CIBR&CKYCApprover-AVPReviewandSign-Off" task
#    #Validate the Refer Reason Lov's
#    When I select "Refer" option for "ReviewOutome" field
#    And I validate the following fields in "CIBR&CKYCApprover-AVPReviewandSign-Off" Screen
#      | Label        | FieldType      | Visible | Editable | Mandatory | DefaultsTo |
#      | Refer Reason | Multi-Dropdown | true    | true     | true      | Select..   |
#    And I validate LOVs of "ReferReason" field
#      | lovs                                            |
#      | Adverse Media Not Identified / Assessed         |
#      | PEP Not Identified / Assessed                   |
#      | Sanctions Nexus Not Identified / Assessed       |
#      | Ownership Structure Not Clear / Incomplete      |
#      | Name Screening Incorrect / Incomplete           |
#      | Incorrect Risk Rating                           |
#      | Insufficient / Incorrect Documentation Provided |
#      | Insufficient / Incorrect Information Provided   |
#      | Additional Information / EDD Required           |
#      | Others                                          |
#    When I complete "CIBR&CKYCApprover-AVPReviewandSign-Off" task
#    #Verify 'Business Unit Head Review and Sign-Off' task is generated and assigned to respective team
#    And I validate the following task is getting generated
#      | TaskName                               |
#      | Business Unit Head Review and Sign-Off |
#    #Verify 'Business Unit Head Review and Sign-Off' option (Hyperlink) is available under Review and Approval stage in Left hand menu
#    And I validate the following fields in left hand menu
#      | Label                                  | FieldType |
#      | Business Unit Head Review and Sign-Off | Hyperlink |
#    And I take the screenshot for "CaseDetails"
#    When I navigate to "BusinessUnitHeadReviewandSign-Off" task
#    When I select "Refer" option for "ReviewOutome" field
#    And I validate the following fields in "BusinessUnitHeadReviewandSign-Off" Screen
#      | Label        | FieldType      | Visible | Editable | Mandatory | DefaultsTo |
#      | Refer Reason | Multi-Dropdown | true    | true     | true      | Select..   |
#    #Validate the Refer Reason Lov's
#    And I validate LOVs of "ReferReason" field
#      | lovs                                            |
#      | Adverse Media Not Identified / Assessed         |
#      | PEP Not Identified / Assessed                   |
#      | Sanctions Nexus Not Identified / Assessed       |
#      | Ownership Structure Not Clear / Incomplete      |
#      | Name Screening Incorrect / Incomplete           |
#      | Incorrect Risk Rating                           |
#      | Insufficient / Incorrect Documentation Provided |
#      | Insufficient / Incorrect Information Provided   |
#      | Additional Information / EDD Required           |
#      | Others                                          |
#    When I complete "BusinessUnitHeadReviewandSign-Off" task
#    When I navigate to "CaptureFabReferencesGrid" task
#    #ADD External references as CIF ID and Counterparty UID
#    When I complete "CaptureFabReferences" task
#    #Validating that the case status is closed
#    And I assert that the CaseStatus is "Closed"
#    And I close the browser
