#Test Case: TC_R1S4EPIC010PBI001_06
#PBI: R1S4EPIC010PBI001
#User Story ID: Case Approval 01/02, Case Approval 04, Reject / Refer / Approve 08
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S4EPIC010PBI001_06

  @To_be_automated
  Scenario: Verify the Manager Review and Approval workflow process for HIGH Risk rating COB case for NBFI Client type
    #For High risk rating 6 tasks should be trigerred in the Following order
    #Relationship Manager Review and Sign-Off
    #CIB R&C KYC Approver - KYC Manager Review and Sign-Off
    #CIB R&C KYC Approver - AVP Review and Sign-Off
    #CIB R&C KYC Approver - VP Review and Sign-Off
    #Business Unit Head Review and Sign-Off
    #Business Head Review and Sign-Off

    Given I login to Fenergo Application with "RM"
    #Create an Enity with risk rating as  Medium/low, client type as NBFFI and COI is UAE
    When I create a new request with FABEntityType as "Non-Bank Financial Institution (NBFI)" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "NBFI" and below data
    |Product|Relationship|
	|C1|C1|
    And I click on "Continue" button
#    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddress" task
    When I complete "EnrichKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task
    #Ensure the risk rating is Medium/Low

    Then I login to Fenergo Application with "OnboardingChecker"
	When I search for the "CaseId"
    When I navigate to "QualityControlGrid" task
    When I complete "ReviewSignOff" task
    And  I take a screenshot
   And I take the screenshot for "CaseDetails"

    Then I login to Fenergo Application with "RM"
	When I search for the "CaseId"
    And I validate "Relationship Manager Review and Sign-Off" field is visible in LHN
     When I navigate to "ReviewSignOffGrid" task
    #Validate the Refer Reason Lov's
    When I complete "ReviewSignOff" screen with key "Refer"
    And I take the screenshot for "CaseDetails"
     And I validate the following fields in "RelationshipManagerReviewandSign" Sub Flow
     | Label        | FieldType      | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Refer Reason | MultiSelectDropdown| true    | false     | true      | Select...   |
    When I complete "ReviewSignOff" task
    And I take a screenshot

      Then I login to Fenergo Application with "FLoydKYC"
      When I search for the "CaseId"   
    And I validate "CIBR&CKYCApprover" field is visible in LHN
    And I take a screenshot
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
    When I complete "ReviewSignOff" screen with key "Refer"
   And I validate the following fields in "FlodKYCManagerReviewandSign" Sub Flow
     | Label        | FieldType      | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Refer Reason | MultiSelectDropdown | true    | false     | true      | Select...   |
#    And I verify "Refer Reason" drop-down values
    When I complete "ReviewSignOff" task
    And I take a screenshot

      Then I login to Fenergo Application with "FlodAVP"
      When I search for the "CaseId"
      And I validate "CIBR&CKYCApproverAVPReview" field is visible in LHN
      When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
      When I complete "ReviewSignOff" screen with key "Refer"
       And I validate the following fields in "FlodAVPManagerReviewandSign" Sub Flow
    | Label        | FieldType      | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Refer Reason | MultiSelectDropdown | true    | false     | true      | Select...   |
       When I complete "ReviewSignOff" task
	And I take a screenshot

      Then I login to Fenergo Application with "SuperUser"
      When I search for the "CaseId"
      And I validate "CIBR&CKYCApproverVPReview" field is visible in LHN
      When I navigate to "CIBR&CKYCApproverVPReviewGrid" task
      When I complete "ReviewSignOff" screen with key "Refer"
       And I validate the following fields in "FlodVPManagerReviewandSign" Sub Flow
    | Label        | FieldType      | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Refer Reason | MultiSelectDropdown | true    | false     | true      | Select...   |
       When I complete "ReviewSignOff" task
	And I take a screenshot

       Then I login to Fenergo Application with "BusinessUnitHead"
        When I search for the "CaseId"
       And I validate "BusinessUnitHead" field is visible in LHN
       When I navigate to "BHUReviewandSignOffGrid" task
       When I complete "ReviewSignOff" screen with key "Refer"
       And I validate the following fields in "BHUManagerReviewandSign" Sub Flow
    | Label        | FieldType      | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Refer Reason | MultiSelectDropdown | true    | false     | true      | Select...   |
      When I complete "ReviewSignOff" task
	And I take a screenshot

       Then I login to Fenergo Application with "SuperUser"
        When I search for the "CaseId"
       And I validate "BusinessHead" field is visible in LHN
       When I navigate to "BUReviewandSignOffGrid" task
       When I complete "ReviewSignOff" screen with key "Refer"
       And I validate the following fields in "BUManagerReviewandSign" Sub Flow
    | Label        | FieldType      | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Refer Reason | MultiSelectDropdown | true    | false     | true      | Select...   |
      When I complete "ReviewSignOff" task
	And I take a screenshot
	Then I login to Fenergo Application with "OnboardingMaker"
	When I search for the "CaseId"
	And I assert that the CaseStatus is "Closed"
    When I navigate to "CaptureFabReferencesGrid" task
    #ADD External references as CIF ID and Counterparty UID
    When I complete "CaptureFABReferences" task
    #Validating that the case status is closed  
   And I close the browser