#Test Case: TC_R1S4EPIC010PBI001_16
#PBI: R1S4EPIC010PBI001
#User Story ID: Case Approval 01/02, Case Approval 03, Reject / Refer / Approve 08
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S4EPIC010PBI001_16

  @To_be_automated
  Scenario: Verify the Manager Review and Approval workflow process when refer back and overriding the risk from LoW to High
    Given I login to Fenergo Application with "RM"
    #Create an Enity with risk rating as LOW, client type as BBG and COI as UAE
    When I create a new request with FABEntityType as "BusinessBankingGroup" and LegalEntityRole as "Client/Counterparty"
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddress" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task
    #Ensure the risk rating is LOW
    When I navigate to "QualityControlGrid" task
    When I complete "ReviewOnboarding" task
    #Verify 'Relationship Manager Review and Sign-Off' task is generated and assigned to respective team
    And I validate the following task is getting generated
      | TaskName                                 |
      | Relationship Manager Review and Sign-Off |
    #Verify 'Relationship Manager Review and Sign-Off' option (Hyperlink) is available under Review and Approval stage in Left hand menu
    And I validate the following fields in left hand menu
      | Label                                    | FieldType |
      | Relationship Manager Review and Sign-Off | Hyperlink |
    And I take the screenshot for "CaseDetails"
    When I navigate to "RelationshipManagerReviewandSign-Off" task
    When I complete "RelationshipManagerReviewandSign-Off" task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated and assigned to respective team
    And I validate the following task is getting generated
      | TaskName                                               |
      | CIB R&C KYC Approver - KYC Manager Review and Sign-Off |
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' option (Hyperlink) is available under Review and Approval stage in Left hand menu
    And I validate the following fields in left hand menu
      | Label                                                  | FieldType |
      | CIB R&C KYC Approver - KYC Manager Review and Sign-Off | Hyperlink |
    And I take the screenshot for "CaseDetails"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated and assigned to respective team
    And I validate the following task is getting generated
      | TaskName                                       |
      | CIB R&C KYC Approver - AVP Review and Sign-Off |
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' option (Hyperlink) is available under Review and Approval stage in Left hand menu
    And I validate the following fields in left hand menu
      | Label                                          | FieldType |
      | CIB R&C KYC Approver - AVP Review and Sign-Off | Hyperlink |
    And I take the screenshot for "CaseDetails"
    When I navigate to "CIBR&CKYCApprover-AVPReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-AVPReviewandSign-Off" task
    #Verify 'Business Unit Head Review and Sign-Off' task is generated and assigned to respective team
    And I validate the following task is getting generated
      | TaskName                               |
      | Business Unit Head Review and Sign-Off |
    #Verify 'Business Unit Head Review and Sign-Off' option (Hyperlink) is available under Review and Approval stage in Left hand menu
    And I validate the following fields in left hand menu
      | Label                                  | FieldType |
      | Business Unit Head Review and Sign-Off | Hyperlink |
    And I take the screenshot for "CaseDetails"
    When I navigate to "BusinessUnitHeadReviewandSign-Off" task
    #Refer back to Risk assessment task and change the risk to High
    When I select "Refer" option for "ReviewOutome" field
    When I select "RiskAssessment" option for "RefertoStage" field
    #select multiple refer reason
    When I select "Incorrect Risk Rating" option for "ReferReason" field
    When I select "Insufficient / Incorrect Information Provided" option for "ReferReason" field
    When I complete "BusinessUnitHeadReviewandSign-Off" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Change the risk to High
    When I complete "RiskAssessmentFAB" task
    When I navigate to "QualityControlGrid" task
    When I complete "ReviewOnboarding" task
    #Verify the High risk tasks are getting generated in Review and Approval stage
    #Verify 'Relationship Manager Review and Sign-Off' task is generated and assigned to respective team
    And I validate the following task is getting generated
      | TaskName                                 |
      | Relationship Manager Review and Sign-Off |
    #Verify 'Relationship Manager Review and Sign-Off' option (Hyperlink) is available under Review and Approval stage in Left hand menu
    And I validate the following fields in left hand menu
      | Label                                    | FieldType |
      | Relationship Manager Review and Sign-Off | Hyperlink |
    And I take the screenshot for "CaseDetails"
    When I navigate to "RelationshipManagerReviewandSign-Off" task
    When I complete "RelationshipManagerReviewandSign-Off" task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated and assigned to respective team
    And I validate the following task is getting generated
      | TaskName                                               |
      | CIB R&C KYC Approver - KYC Manager Review and Sign-Off |
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' option (Hyperlink) is available under Review and Approval stage in Left hand menu
    And I validate the following fields in left hand menu
      | Label                                                  | FieldType |
      | CIB R&C KYC Approver - KYC Manager Review and Sign-Off | Hyperlink |
    And I take the screenshot for "CaseDetails"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated and assigned to respective team
    And I validate the following task is getting generated
      | TaskName                                       |
      | CIB R&C KYC Approver - AVP Review and Sign-Off |
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' option (Hyperlink) is available under Review and Approval stage in Left hand menu
    And I validate the following fields in left hand menu
      | Label                                          | FieldType |
      | CIB R&C KYC Approver - AVP Review and Sign-Off | Hyperlink |
    And I take the screenshot for "CaseDetails"
    When I navigate to "CIBR&CKYCApprover-AVPReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-AVPReviewandSign-Off" task
    #Verify 'CIB R&C KYC Approver - VP Review and Sign-Off' task is generated and assigned to respective team
    And I validate the following task is getting generated
      | TaskName                                      |
      | CIB R&C KYC Approver - VP Review and Sign-Off |
    #Verify 'CIB R&C KYC Approver - VP Review and Sign-Off' option (Hyperlink) is available under Review and Approval stage in Left hand menu
    And I validate the following fields in left hand menu
      | Label                                         | FieldType |
      | CIB R&C KYC Approver - VP Review and Sign-Off | Hyperlink |
    And I take the screenshot for "CaseDetails"
    When I navigate to "CIBR&CKYCApprover-VPReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-VPReviewandSign-Off" task
    #Verify 'Business Head Review and Sign-Off' task is generated and assigned to respective team
    And I validate the following task is getting generated
      | TaskName                               |
      | Business Unit Head Review and Sign-Off |
    #Verify 'Business Unit Head Review and Sign-Off' option (Hyperlink) is available under Review and Approval stage in Left hand menu
    And I validate the following fields in left hand menu
      | Label                                  | FieldType |
      | Business Unit Head Review and Sign-Off | Hyperlink |
    And I take the screenshot for "CaseDetails"
    When I navigate to "BusinessUnitHeadReviewandSign-Off" task
    When I complete "BusinessUnitHeadReviewandSign-Off" task
    #Verify 'Business Head Review and Sign-Off' task is generated and assigned to respective team
    And I validate the following task is getting generated
      | TaskName                          |
      | Business Head Review and Sign-Off |
    #Verify 'Business Head Review and Sign-Off' option (Hyperlink) is available under Review and Approval stage in Left hand menu
    And I validate the following fields in left hand menu
      | Label                             | FieldType |
      | Business Head Review and Sign-Off | Hyperlink |
    And I take the screenshot for "CaseDetails"
    When I navigate to "BusinessHeadReviewandSign-Off" task
    When I complete "BusinessUnitHeadReviewandSign-Off" task
    When I navigate to "CaptureFabReferencesGrid" task
    #ADD External references as CIF ID and Counterparty UID
    When I complete "CaptureFabReferences" task
    #Validating that the case status is closed
    And I assert that the CaseStatus is "Closed"
    And I close the browser
