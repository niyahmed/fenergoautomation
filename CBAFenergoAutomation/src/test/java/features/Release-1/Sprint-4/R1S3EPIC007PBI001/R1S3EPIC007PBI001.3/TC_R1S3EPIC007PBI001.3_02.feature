#Test Case: TC_R1S3EPIC007PBI001.3_02
#PBI: R1S3EPIC007PBI001.3
#User Story ID: D1.13
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Update Documents(DMS)

  Scenario: Validate update service gets triggered (and content and structure of XML) when RM user update a document on Document details screen of Capture request details task of 'New request stage'.
  #Placeholder for DMS Testing 