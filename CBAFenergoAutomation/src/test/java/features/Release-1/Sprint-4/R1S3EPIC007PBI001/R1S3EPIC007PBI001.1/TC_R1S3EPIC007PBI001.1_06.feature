#Test Case: TC_R1S3EPIC007PBI001.1_06
#PBI: R1S3EPIC007PBI001.1
#User Story ID: D1.4
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Retrieve All Document versions (DMS)

  Scenario: Validate 'retreive all' service gets triggered (content and structure of XML) when RM user 'view' a document on Document details screen of Capture request details task of 'New request stage'.
  #Placeholder for DMS Testing 