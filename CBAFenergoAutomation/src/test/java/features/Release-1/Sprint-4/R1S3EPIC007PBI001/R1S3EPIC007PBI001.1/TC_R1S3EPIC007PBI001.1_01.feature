#Test Case: TC_R1S3EPIC007PBI001.1_01
#PBI: R1S3EPIC007PBI001.1
#User Story ID: D1.1, D1.2
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora 
Feature: Upload Documents and Drag and Drop Documents

  @Tobeautomated
  Scenario: Validate "RM User" is able to add document (by uploading)from local folder on document details screen for "Capture Request Details" task
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I complete to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I navigate to "KYCDocumentrequirement" task.
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    #Test-data: Add Document by browsing from local folder
    When I add document by browsing from Local folder and "save" the details
    Then I can see document is added under document requirement

  
  Scenario: Validate "RM User" is able to add document (by uploading)from Shared folder/Sharepoint on document details screen for "Capture Request Details" task
 	Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I complete to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I navigate to "KYCDocumentrequirement" task.
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    #Test-data: Add Document by browsing from shared folder
    When I add document by browsing from Shared folder and "save" the details
    Then I can see document is added under document requirement
 	
 	Scenario: Validate "RM User" is able to add document (by drag and drop)from local folder on document details screen for "Capture Request Details" task
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I complete to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I navigate to "KYCDocumentrequirement" task.
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    #Test-data: Add Document by drag and drop from local folder
    When I add document by drag and drop from Local folder and "save" the details
    Then I can see document is added under document requirement

  
  Scenario: Validate "RM User" is able to add document (by drag and drop)from Shared folder/Sharepoint on document details screen for "Capture Request Details" task
 	Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I complete to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I navigate to "KYCDocumentrequirement" task.
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    #Test-data: Add Document by drag and drop from shared folder
    When I add document by drag and drop from Shared folder and "save" the details
    Then I can see document is added under document requirement
    
    #Repeat the same validation for all the remaining 7 screens
    #New Request>Capture Request Details> Document Details
		#Enrich KYC Info> KYC Document Requirements > Document Details
		#AML>Complete ID&V>Edit Verification>Document Details
		#AML > Complete AML > Document Details
		#Capture Request Details > Product > Document Details
		#LE360> Documents
		#Enrich KYC Profile > Tax Identifier > Document Details
		#AML > Complete AML > Hierarchy > Add Fircosoft Screening > Assessment > Document Details
 	