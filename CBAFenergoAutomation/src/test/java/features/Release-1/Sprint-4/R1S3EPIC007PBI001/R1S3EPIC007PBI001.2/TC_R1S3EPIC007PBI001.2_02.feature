#Test Case: TC_R1S3EPIC007PBI001.2_02
#PBI: R1S3EPIC007PBI001.2
#User Story ID: D1.4
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Document Deletion (DMS)

  Scenario: Validate Delete service gets triggered (and content and structure of XML) when RM user uploads a document on Document details screen of Capture request details task of 'New request stage'.
  #Placeholder for DMS Testing 