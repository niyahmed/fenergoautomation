#Test Case: TC_R1S3EPIC002PBI301_02
#PBI: R1S3EPIC002PBI301
#User Story ID: OOTBF048, OOTBF049
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S3EPIC002PBI301_02

  @Automation
  Scenario: 
    Verify "Business Markets" section is displaying as hidden on "Enrich KYC Profile" task of "Enrich Client Profile" Stage for KYC Maker.

    #	Given I login to Fenergo Application with "RM"
    #	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    Given I login to Fenergo Application with "RM:IBG-DNE"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    And I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I click on "AddAddresss" button
    And I complete "Addresses" screen with key "CountryAsUAE"
    #Test-data: Verify "Business details" section is displaying as hidden under "Customer details" section
    #Then I see "Business" section is displaying as hidden under "Customer details" section
    Then I check that below data is not visible
      | FieldLabel |
      | Business   |
