#Test Case: TC_R1S5EPICDM001_05
#PBI: R1S5EPICDM001
#User Story ID: NA
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: DM screen-2 has "T24 CIF ID" as a search criteria

 
  Scenario: Validate if DM users have "T24 CIF ID" as a search criteria in DM screen-2
  #Placeholder test case
  #Precondition - Create a migrated entity using DM screen 1 and note the T24 CIF ID for the migrated entity
   		
  Given I login to Fenergo Application with "DM user"
  And I navigate to DM screen 2 #using url 
  And I assert that "T24 CIF ID" field is present under "Search Entity Details" section
  And I enter fill "T24 CIF ID" with T24 CIF ID value of the migrated entity
  And I click "SEARCH" button
  And I assert that the migrated entity is visible in the result grid