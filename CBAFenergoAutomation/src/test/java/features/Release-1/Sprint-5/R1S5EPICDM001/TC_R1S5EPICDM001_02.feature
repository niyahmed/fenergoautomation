#Test Case: TC_R1S5EPICDM001_02
#PBI: R1S5EPICDM001
#User Story ID: NA
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: DM user's ability to create a Data migration request

 
  Scenario: Validate if the DM user is able to create a Data migration request
  #Dev team to provide DM user 
  Given I login to Fenergo Application with "DM user"
  And I click on "+" button on the right top corner of the screen
  And I assert that "New DM Request" is available 
  And I click on  "New DM Request"
  And I'm taken to "DM Screen - 1"