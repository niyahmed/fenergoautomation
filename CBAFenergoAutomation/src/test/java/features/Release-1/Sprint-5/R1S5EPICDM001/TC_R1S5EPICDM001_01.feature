#Test Case: TC_R1S5EPICDM001_01
#PBI: R1S5EPICDM001
#User Story ID: NA
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: DM user's ability to access the application

 
  Scenario: Validate if the DM user is able to login to Fenergo application
  #Dev team to provide DM user 
  Given I login to Fenergo Application with "DM user"