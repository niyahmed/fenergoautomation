#Test Case: TC_R1S5EPICDM001_07
#PBI: R1S5EPICDM001
#User Story ID: NA
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Ability to store customer records (migrated entity and the corresponding associated parties and screening data)

 
  Scenario: Validate if data entered in DM screens 1 and 2 are getting saved.
  
  Given I login to Fenergo Application with "DM user"
  And I navigate to DM screen 1
  And I enter data for all the panels
  And I click "Save" button #intermediate button
  And I assert that data is saved successfully
  And complete all subflows
  And I click on "SAVE & ADD ANOTHER" button
  And I assert that data is saved successfully 
  
  And I navigate to DM screen 2 #using url 
  And I enter fill "T24 CIF ID" with T24 CIF ID value of the migrated entity
  And I click "SEARCH" button
  And I assert that the migrated entity is visible in the result grid
  And I click "Add Selected" button
  And I add associated parties
  And I add screenings
  And I click "COMPLETE" button
  And I assert that data is saved successfully
  
  