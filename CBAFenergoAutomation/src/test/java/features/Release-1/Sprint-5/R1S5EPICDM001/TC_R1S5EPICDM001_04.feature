#Test Case: TC_R1S5EPICDM001_04
#PBI: R1S5EPICDM001
#User Story ID: NA
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Security matrix for DM workflow

 
  Scenario: Validate if other users are not able to access "New DM Request" link to create migrated legal entities
  #Precondition - Create a migrated entity using DM screen 1 and 2 and note the T24 CIF ID of the migrated entity
  #Validate if DM user is able to view migrated entity using Legal entity search screen
  #Validate is other users (list given below) are not able to view the migrated entity
  		#Super user (without DM role group)
			#Relationship Manager
			#Onboarding Maker
			#Compliance
			#Fenergo Admin
			#FLOD KYC Manager
			#Business Unit Head (N3)
			#Business Head (N2)
			#FLOD SVP
			#FLOD AVP
			#FLOD VP
			#Onboarding Checker
  		
  Given I login to Fenergo Application with "DM user"
  And I click on "+" button on the right top corner of the screen
  And I assert that "New DM Request" is available 
  And I click on  "New DM Request"
  
  Given I login to Fenergo Application with "Super user (without DM role group)"
  And I click on "+" button on the right top corner of the screen
  And I assert that "New DM Request" is not available 
    
  ## Repeat the above validation for all the roles mentioned users