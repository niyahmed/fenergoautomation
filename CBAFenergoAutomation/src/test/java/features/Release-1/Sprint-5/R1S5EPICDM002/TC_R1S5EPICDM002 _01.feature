#Test Case: TC_R1S5EPICDM002_01
#PBI: R1S5EPICDM002
#User Story ID: US112, US114, US113, US106, US100, US029, US030, US068 (PRIMARY), OOTBF001a + b (PRIMARY), USR01, US055, US056, US057, US059,
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S5EPICDM002_01

  @Automation
  Scenario: Corporate:Validate the field behaviour in Enter Entity Details section & Internal Booking Details section of DM Screen-1 for Corporate Client type
    Given I login to Fenergo Application with "RM"
    #Create DM Request with Client type = Corporate
    #Click on + button and then click on New DM Request
    When I navigate to DM Request screen
    #Enter Entity Details Section
    #Validate field behaviours in Enter Entity Details section
    And I validate the following fields in "Enrich Entity Details" Sub Flow
      | Label                                    | FieldType             | Visible | Editable | Mandatory | Field Defaults To |
      | Client Type                              | Drop-down             | true    | true     | true      | Select...         |
      | Legal Entity Type                        | Drop-down             | true    | true     | true      | Select...         |
      | Legal Entity Name                        | Alphanumeric          | true    | true     | true      |                   |
      | Entity Type                              | Drop-down             | true    | true     | true      | Non Individual    |
      | Country of Incorporation / Establishment | Drop-down             | true    | true     | true      | Select...         |
      | GLCMS UID                                | Numeric               | true    | true     | false     |                   |
      | T24 CIF ID                               | Numeric               | true    | true     | true      |                   |
      | Legal Entity Category                    | Drop-down             | true    | true     | true      | Select...         |
      | Legal Entity Role                        | Drop-down             | true    | true     | true      | Select...         |
      | Entity of Onboarding                     | Drop-down             | true    | true     | true      | Select...         |
      | Resultant Jurisdiction(s)                | Multiselect drop-down | true    | false    | false     | UAE               |
    #Validate the Client type lovs
    Then I validate the specific LOVs for "Client Type"
      | Lovs                                  |
      | Business Banking Group                |
      | Corporate                             |
      | Financial Institution (FI)            |
      | Non-Bank Financial Institution (NBFI) |
      | PCG-Entity                            |
    #Verify 38 lovs are dispalying in Legal Entity Type field when client type is selected as Corporate
    #Refer LOV 'LuLeSubTp- Legal Entity Type' from PBI R1EPIC002PBI015 v1.1
    And I validate "Legal Entity Type" drop-down has following LOVs
    #Validate the Country of Incorporation/Establishment LOV
    #Refer PBI (countries)
    And I validate "Country of Incorporation / Establishment" drop-down has following LOVs
    #Verify only the below 13 lovs available in Legal Entity category for client type "Corporate"
    Then I validate the specific LOVs for "Legal Entity Catgory"
      | Lovs                                         |
      | Charities or NGOs                            |
      | General Partnership                          |
      | Sole Proprietorship                          |
      | Free Zone Entity                             |
      | Under formation Entity                       |
      | Branch of a Foreign Company                  |
      | Public Joint Stock Company                   |
      | Offshore/Non-Resident Entities               |
      | Government and Semi-Government Entities      |
      | Embassy/Diplomatic Mission                   |
      | Joint Ventures                               |
      | Trust Company                                |
      | Exchange Houses/Money-Service Bureaus’(MSBs) |
    #Verify only the below 4 lovs available in Legal Entity Role
    Then I validate the specific LOVs for "Legal Entity Role"
      | Lovs                |
      | Broker              |
      | Client/Counterparty |
      | Investment Manager  |
      | Prime Broker        |
    #Verify Legal Entity Name field NOT accepts special characters. Test Data:Special characters !@ # $ % ^ & * ( )} { ] [\ |'; ;/,~ `
    And I fill the data for "AssociationDetails" with key "Data1"
    #Verify Legal Entity Name field accepts lower case characters. Test Data: Lower case char a to z
    And I fill the data for "AssociationDetails" with key "Data2"
    #Verify Legal Entity Name field accepts Upper case characters. Test Data: Lower case char A to Z
    And I fill the data for "AssociationDetails" with key "Data3"
    #Verify Legal Entity Name field accepts numbers zero to nine. Test Data: 0 to 9
    And I fill the data for "AssociationDetails" with key "Data4"
    #Verify Legal Entity Name field accepts .(period/full stop)
    And I fill the data for "AssociationDetails" with key "Data5"
    #Verify Legal Entity Name field accepts Space ()
    And I fill the data for "AssociationDetails" with key "Data6"
    #Verify Legal Entity Name field accepts valid characters combination of upper case, lower case, numeric o to 9, . (full stop), space
    And I fill the data for "AssociationDetails" with key "Data7"
    #Verify Legal Entity Name field NOT accepts more than 255 characters. Test data : 256 characters
    And I fill the data for "AssociationDetails" with key "Data8"
    #Verify Legal Entity Name field accepts less than 255 characters. Test data : 255 characters
    And I fill the data for "AssociationDetails" with key "Data9"
    #GLCMS UID validation
    When I enter data in "GLCMS ID" as "ALPHABETICAL"
    Then I validate the error messgage for "GLCMS UID-Alphabetical" as "GLCMS UID can contain only digits. Only zero is invalid input."
    #Test Data: Enter values other than numeric values in GLCMS UID field
    #User should NOT be able to enter values other than numeric
    When I enter data in "GLCMS ID" as "NumericalLessThenSix"
    #Test Data: Enter numeric value less than or equal to 6 digits in GLCMS UID field
    #User should be able to enter numeric values less than or equal to 6 digits
    When I enter data in "GLCMS ID" as "NumericalMoreThenSix"
    #Test Data: Enter numeric value more than 6 digit in GLCMS UID Field
    #User should NOT be able to enter numeric value more than 6 digit
    When I enter data in "GLCMS ID" as "AllZeroes"
    #Test Data: Enter all zeros as 6 digit in GLCMS UID Field
    #User should NOT be able to enter only zeros in GLCMS UID field
    #T24 CIF ID Validation
    When I enter data in "T24 CIF ID" as "AllZeroes"
    #Test Data: Enter values other than numeric values in T24 CIF ID field
    #User should NOT be able to enter values other than numeric
    When I enter data in "T24 CIF ID" as "Lessthan10"
    #Test Data: Enter numeric value less than or equal to 10 digits in T24 CIF ID field
    #User should be able to enter numeric values less than or equal to 10 digits
    When I enter data in "T24 CIF ID" as "Morethan10"
    #Test Data: Enter numeric value more than 10 digit in T24 CIF ID Field
    #User should NOT be able to enter numeric value more than 10 digit
    When I enter data in "T24 CIF ID" as "AllZeroes"
    #Internal Booking Details section
    #Validate field behaviours in Internal Booking Details section
    And I validate the following fields in "Internal Booking Details" Sub Flow
      | Label                       | FieldType             | Visible | Editable | Mandatory | Field Defaults To |
      | Request Type                | Drop-down             | true    | false    | false     | New DM Request    |
      | Request Origin              | Drop-down             | true    | false    | false     | Todays Date       |
      | Booking Country             | Drop-down             | true    | true     | true      | Autopopulated     |
      | Jurisdiction                | Multiselect drop-down | true    | false    | false     | Autopopulated     |
      | Target Code                 | Drop-down             | true    | true     | true      | Select...         |
      | Sector Description          | Drop-down             | true    | true     | false     | Select...         |
      | UID Originating Branch      | Drop-down             | true    | true     | true      | Select...         |
      | Propagate To Target Systems | Drop-down             | true    | false    | true      | Autopopulated     |
    #Validate Target code LOV
    #Refer LOV 'Target Code' from PBI R1EPIC002PBI016
    And I validate "Target code" drop-down has following LOVs
    #Validate Sector Description LOV
    #Refer LOV 'T24 Sector' from PBI R1EPIC002PBI016
    And I validate "Sector Description" drop-down has following LOVs
    #Validate the Client type lovs
    Then I validate the specific LOVs for "Client Type"
      | Lovs                                  |
      | Business Banking Group                |
      | Corporate                             |
      | Financial Institution (FI)            |
      | Non-Bank Financial Institution (NBFI) |
      | PCG-Entity                            |
		#Verify the ability to enter / select all mandatory and non-mandatory values in Enter Entity Details section & Internal Booking Details section