#Test Case: TC_R1S5EPICDM002_05
#PBI: R1S5EPICDM002
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S5EPICDM002_05

  @Automation
  Scenario: Validate the ability to Save the DM Request and verify the data in LE360 screen
    Given I login to Fenergo Application with "RM"
    #Create DM Request with Client type = Corporate/PCG-Entity/FI/NBFI/BBG and Country of Incorporation / Establishment = UAE or any other country
    #Click on + button and then click on New DM Request
    When I navigate to DM Request screen
    #Fill in all the mandatory and non-mandatory fields and click on Save button
    Then I navigate to LE360 screen
    #Verify the DM data attributes are displayed in existing LE360 screen and Field which are not applicable for DM should be dispalyed as blank or grayed out
		#Verify Legal Entity ID is created
