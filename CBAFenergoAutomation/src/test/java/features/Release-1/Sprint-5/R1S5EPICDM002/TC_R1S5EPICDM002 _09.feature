#Test Case: TC_R1S5EPICDM002_09
#PBI: R1S5EPICDM002
#User Story ID: US072 (PRIMARY), US075 (PRIMARY), US076 (PRIMARY), US077 (PRIMARY), US078 (PRIMARY), US079 (PRIMARY), US080 (PRIMARY), US081 (PRIMARY), US083 (PRIMARY), US084 (PRIMARY), US085 (PRIMARY), US074 (PRIMARY), US073 (PRIMARY)
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S5EPICDM002_09

  @Automation
  Scenario: NBFI:Validate the field behaviour of below sections in DM Screen-1 for NBFI Client type
    #Business Details
    #Source of Funds and Wealth Details
    #Industry Code Details
    #Risk Assessment
    Given I login to Fenergo Application with "RM"
    #Create DM Request with Client type = NBFI and Country of Incorporation / Establishment = UAE
    #Click on + button and then click on New DM Request
    When I navigate to DM Request screen
    #Business Detail section
    #Validate field behaviours in Business Details section
    And I validate the following fields in "Business details" Sub Flow
      | Label                                                    | FieldType    | Visible | Editable | Mandatory | Field Defaults To |
      | Nature of Business Activity                              | Alphanumeric | true    | true     | true      |                   |
      | Anticipated Transactions Turnover (Annual in AED)        | Drop-down    | true    | true     | true      |                   |
      | Value of Initial Deposit (AED)                           |              | false   |          |           |                   |
      | Source of Initial Deposit & Country of Source            |              | false   |          |           |                   |
      | Value of Capital or Initial Investment in Business (AED) |              | false   |          |           |                   |
      | Annual Business Turnover (AED)                           | Numeric      | true    | true     | false     |                   |
      | Projected Annual Business Turnover (AED)                 |              | false   |          |           |                   |
      | Annual Business Turnover of Group (AED)                  |              | false   |          |           |                   |
      | Active Presence in Sanctioned Countries/Territories      |   Drop-down  | true    |  true    |  false    |                   |
      | If Yes, Specify the Sanctioned Countries/Territories     |              | false   |          |           |                   |
      | Offshore Banking License                                 |              | false   |          |           |                   |
    #Verify 'If Yes, Specify the Sanctioned Countries/Territories' field is visible and non- mandatory if yes is selected for 'Active Presence in Sanctioned Countries/Territories'
    And I validate the following fields in "KYC condition" Sub Flow
      | Label                                                 | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      | If Yes, Specify the Sanctioned Countries/Territories  | Drop-down | true    | true     | No        |                   |
    #Verify the lovs for "Anticipated Transactions Turnover (Annual in AED)" field
    Then I validate the specific LOVs for "Anticipated Transactions Turnover (Annual in AED)"
      | Lovs                             |
      | Less < 5,000,000                 |
      | Between 5,000,000 - 10,000,000   |
      | Between 10,000,000 - 25,000,000  |
      | Between 25,000,000 - 100,000,000 |
      | More than 100,000,000            |
      | Not Applicable                   |
      | Withdrawals                      |
    #Source of Funds and Wealth Details section
    #Validate field behaviours in Source of Funds and Wealth Details section
    And I validate the following fields in "Source of Funds and Wealth Details" Sub Flow
      | Label                                  | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      | Legal Entity Source of Income & Wealth | String    | true    | true     | false     |                   |
      | Legal Entity Source of Funds           |           | false   |          |           |                   |
    #Validate 'Legal Entity Source of Funds' is visible and non-mandatory if Product=Call Account or Savings Account
    When I add product type "Call Account" or "Savings account" and save
    And I validate the following fields in "Source of Funds and Wealth Details" Sub Flow
      | Label                                  | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      | Legal Entity Source of Funds           | String    | true    | true     | false     |                   |
    #Industry Code Details section
    #Validate field behaviours in Industry Code Details section
    And I validate the following fields in "Industry Code Details" Sub Flow
      | Fenergo Label Name                    | Field Type            | Visible | Editable | Mandatory | Field Defaults To |
      | Primary Industry of Operation         | Drop-down             | true    | true     | true      | Select...         |
      | Secondary Industry of Operation       | Multiselect drop-down | true    | true     | false     | Please Select...  |
      | Primary Industry of Operation Islamic | Drop-down             | true    | false    | true      | Autopopulated     |
      | Primary Industry of Operation UAE     | Drop-down             | true    | true     | true      | Select...         |
    #Validate 'Secondary Industry of Operation' field value is defaulted to 'Primary Industry of Operation' value  when UID Originating Branch = Head Office _ ISB-100 and Country of Incorporation = UAE
    #Select 'Dummy 1' value for 'Primary Industry of Operation' field and enter UID Originating Branch = Head Office_ISB-100
    #Verify 'Primary Industry of Operation UAE' field value is defaulting as 'Primary Industry of Operation'
    And I validate the following fields in "Industry Code Details" Sub Flow
      | Fenergo Label Name                    | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      | Primary Industry of Operation Islamic | Drop-down  | true    | false    | true      | Dummy 1           |
      | Primary Industry of Operation UAE     | Drop-down  | true    | true     | true      | Dummy 1           |
    #Validate the 'Primary Industry of Operation' lovs
    Then I validate the specific LOVs for "Primary Industry of Operation"
      | Lovs    |
      | Dummy 1 |
      | Dummy 2 |
      | Dummy 3 |
    #Validate the 'Secondary Industry of Operation' lovs
    Then I validate the specific LOVs for "Secondary Industry of Operation"
      | Lovs    |
      | Dummy 1 |
      | Dummy 2 |
      | Dummy 3 |
    #Risk Assessment
    #Validate field behaviours in Risk Assessment section
    And I validate the following fields in "Risk Assessment" Sub Flow
      | Label         | FieldType | Visible | Editable | Mandatory | Field Defaults To |
      | Risk Category | Drop-down | true    | true     | true      |                   |
		#Verify the ability to enter / select all mandatory and non-mandatory values in the below sections
		#Business Details
    #Source of Funds and Wealth Details
    #Industry Code Details
    #Risk Assessment
    