#Test Case: TC_R1S5EPICDM003_05
#PBI: R1S5EPICDM003
#User Story ID: US047 US042, US043
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S5EPICDM003_05

  @Automation
  Scenario: FI:Validate the field behaviour in Document Details screen of DM Screen-1 for FI Client type
    Given I login to Fenergo Application with "RM"
    #Create DM Request with Client type = FI
    #Click on + button and then click on New DM Request
    When I navigate to DM Request screen
    #Fill in all mandatory and non-mandatory data in all the sections and save the record
    And I navigate to "Documents" screen
    #Validate field behaviours in Document Details screen
    And I validate the only following fields in "Document Details" section
      | Label Name                     | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
      | Document Source                | Drop-down    | true    | true     | true      | Select...         |
      | Document Category              | Drop-down    | true    | true     | true      | Select...         |
      | Document Type                  | Drop-down    | true    | true     | true      | Select...         |
      | Document Name                  | Alphanumeric | true    | false    | false     |                   |
      | Date of Expiration             | Date         | true    | true     | false     | DD/ MM/ YY        |
      | Effective Date                 | Date         | true    | true     | false     | DD/ MM/ YY        |
      | Date of Issuance               | Date         | true    | true     | false     | DD/ MM/ YY        |
      | Document Identification Number | Alphanumeric | true    | true     | false     |                   |
    #Verify the 'Document name' field is editable when Document type is selected as Others
    And I select "MISC" for "Document Category" field
    And I select "Others" for "Document Type" field
    And I validate "Document Name" field is defaulted to blank
    And I validate "Document Name" field is editable
    #Verify the 'Document name' field is NOT editable when Docuemnt type is not selected as Others
    And I select "AOF" for "Document Category" field
    And I select "Account Opening Form" for "Document Type" field
    And I validate "Document Name" field is defaulted to "Account Opening Form"
    And I validate "Document Name" field is not-editable
    #Field validation of Document Name and Document Identification Number
    And I assert that "Document Name" accepts less than or equal to 100 characters
    And I assert that "Document Name" does not accept more than 100 characters
    And I assert that "Document Identification Number" accepts less than or equal to 100 characters
    And I assert that "Document Identification Number" does not accept more than 100 characters
    #Verify 'Effective Date' and 'Date of Issuance' fields does not accepts future date.
    And I fill the data for "DMRequest" with key "Data4"
    #Verify the ability of Saving single fileless document meta details by clicking on Save button
    And I select "Fileless" for "Document Source" field
    And I select "MISC" for "Document Category" field
    And I select "Others" for "Document Type" field
    And I fill "Document Name" field
    And I select "Date" for "Date of Expiration " field
    And I select "Date" for "Effective Date" field
    And I select "Date" for "Date of Issuance" field
    And I enter valid value for "Document Identification Number" field
    And I click on 'Save' button
    And I asser that data is saved in the grid
    #Verify the ability of Saving MULTIPLE fileless document meta details by clicking on Save and add another button
    And I select "Fileless" for "Document Source" field
    And I select "MISC" for "Document Category" field
    And I select "Others" for "Document Type" field
    And I fill "Document Name" field
    And I select "Date" for "Date of Expiration " field
    And I select "Date" for "Effective Date" field
    And I select "Date" for "Date of Issuance" field
    And I enter valid value for "Document Identification Number" field
    And I click on 'SaveandAddAnother' button
    And I asser that data is saved in the grid
    And I select "Fileless" for "Document Source" field
    And I select "AOF" for "Document Category" field
    And I select "Account Opening Form" for "Document Type" field
    And I validate "Document Name" field is defaulted to "Account Opening Form"
    And I click on 'Save' button
    And I asser that data is saved in the grid
    #Validate the warning message for duplicate document identification number
    And I select "Fileless" for "Document Source" field
    And I select "AOF" for "Document Category" field
    And I select "Account Opening Form" for "Document Type" field
    And I enter duplicate value for "Document Identification Number" field
    And I click on 'Save' button
    And I assert that the following message is diplayed along with reference to the details and location of the other (duplicate) document
      | This Document Identification Number already exists with DocumentId:No, Please enter another Document Identification Number |
    #Validate if only the follows LOVs are there for "Document Source"
    Then I validate the specific LOVs for "Document Source"
      | Lovs     |
      | Upload   |
      | Fileless |
      | Existing |
    #Validate if only the follows LOVs are there for "Document Category"
    Then I validate the specific LOVs for "Document Category"
      | Lovs                   |
      | AOF                    |
      | Constitutive           |
      | Authorized Signatories |
      | MISC                   |
    #Validate the Document Type Lovs
    And I select "AOF" for "Document Category" field
    And I validate LOVs of "Document Type" field
    #Refer PBI for LOV list (12 values)
    And I select "Authorized Signatories" for "Document Category" field
    And I validate LOVs of "Document Type" field
    #Refer PBI for LOV list (8 values)
    And I select "Constitutive" for "Document Category" field
    And I validate LOVs of "Document Type" field
    #Refer PBI for LOV list (18 values)
    And I select "MISC" for "Document Category" field
    And I validate LOVs of "Document Type" field
    #Refer PBI for LOV list (12 values)
    #Verify Inability of Saving the record without entering Mandatory values and appropirate warning message should be displayed
    And I select "blank" for "Document Source" field
    And I select "blank" for "Document Category" field
    And I select "blank" for "Document Type" field
    And I click on 'Save' button
