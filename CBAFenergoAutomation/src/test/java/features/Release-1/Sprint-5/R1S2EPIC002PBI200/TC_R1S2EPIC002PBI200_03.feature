#Test Case: TC_R1S2EPIC002PBI200_03
#PBI: R1S2EPIC002PBI200
#User Story ID: US012
#Designed by: Priyanka Arora
Feature: TC_R1S2EPIC002PBI200_03 

@Automation 
Scenario: 
	Validate conditionally mandatory fields on "LE details" screen for the Associated LE(for "individual" relationship) for the following Association types and client type "Corporate" 
		Given I login to Fenergo Application with "RM:IBG-DNE" 
		When I complete "NewRequest" screen with key "Corporate" 
		And I complete "CaptureNewRequest" with Key "C1" and below data 
			|Product|Relationship|
			|C1|C1|
		And I click on "Continue" button 
		When I complete "ReviewRequest" task 
		Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
		When I navigate to "ValidateKYCandRegulatoryGrid" task 
		When I complete "ValidateKYC" screen with key "C1" 
		And I click on "SaveandCompleteforValidateKYC" button 
		When I navigate to "EnrichKYCProfileGrid" task 
		When I complete "AddAddressFAB" task 
		When I complete "EnrichKYC" screen with key "BBG" 
		And I click on "SaveandCompleteforEnrichKYC" button
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "DirectorIndividual" 
	And I navigate to "LEDetails" screen of added AssociatedParty
	#Testdata: Verify below mentioned fields as mandatory for relationship as "Authorised Signatory" on "LE details" screen
	And I check that below data is mandatory 
		| FieldLabel          | 
		| Date of Birth  | 
		| Nationality    |
		| Place of Birth | 
		| Occupation     | 
    When I search for the "CaseId" 
    When I navigate to "CaptureHierarchyDetailsGrid" task 
    When I remove the existing "RemoveAssociatedParty" from AssociatedPartiesGrid
    When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "Custodian"
		And I navigate to "LEDetails" screen of added AssociatedParty
		And I check that below data is mandatory 
		| FieldLabel          | 
		| Date of Birth  | 
		| Nationality    |
		| Place of Birth | 
		| Occupation     | 
	 When I search for the "CaseId" 
    When I navigate to "CaptureHierarchyDetailsGrid" task 
    When I remove the existing "RemoveAssociatedParty" from AssociatedPartiesGrid
    When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "Investor"
	And I navigate to "LEDetails" screen of added AssociatedParty
	And I check that below data is mandatory 
		| FieldLabel          | 
		| Date of Birth  | 
		| Nationality    |
		| Place of Birth | 
		| Occupation     | 
		When I search for the "CaseId" 
	 When I navigate to "CaptureHierarchyDetailsGrid" task 
    When I remove the existing "RemoveAssociatedParty" from AssociatedPartiesGrid
    When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "Power of Attorney"
		And I navigate to "LEDetails" screen of added AssociatedParty
		And I check that below data is mandatory 
		| FieldLabel          | 
		| Date of Birth  | 
		| Nationality    |
		| Place of Birth | 
		| Occupation     | 	
		When I search for the "CaseId" 
	 When I navigate to "CaptureHierarchyDetailsGrid" task 
    When I remove the existing "RemoveAssociatedParty" from AssociatedPartiesGrid
    When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "Settlor"
		And I navigate to "LEDetails" screen of added AssociatedParty
		And I check that below data is mandatory 
		| FieldLabel          | 
		| Date of Birth  | 
		| Nationality    |
		| Place of Birth | 
		| Occupation     | 	
			
		
