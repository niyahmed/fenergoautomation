#Test Case: TC_R1S2EPIC002PBI200_05
#PBI: R1S2EPIC002PBI200
#User Story ID: US012
#Designed by: Priyanka Arora
Feature: TC_R1S2EPIC002PBI200_05

  @To_be_automated
  Scenario: Validate conditionally mandatory fields on "LE details" screen for the Associated LE(for "individual" relationship) for the following Association types
    Given I login to Fenergo Application with "RM"
    #Create entity with Country of Incorporation = Andora (other than UAE) and client type = Business Banking Group
    When I create a new request with FABEntityType as "Corporate", Country o and LegalEntityRole as "Client/Counterparty"
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    When I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    #Enter Value in "Passport number" field
    When I Complete "EnrichKYCProfileGrid" task
    When I navigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Ultimate Beneficial Owner (UBO)" and complete the task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Ultimate Beneficial Owner (UBO)" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
      | Passport number |
      | Shareholding %  |
    When I nvaigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Investment Manager" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Investment Manager" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
      | Passport number |
      | Shareholding %  |
    When I nvaigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Investor" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Investor" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
      | Passport number |
      | Shareholding %  |
    When I nvaigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Shareholder" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Shareholder" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
      | Passport number |
      | Shareholding %  |
    When I nvaigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Settlor" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Settlor" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
      | Passport number |
      | Shareholding %  |

  Scenario: Validate conditionally mandatory fields on "LE details" screen for the Associated LE.
    Given I login to Fenergo Application with "RM"
    #Create entity with Country of Incorporation = Andora (other than UAE) and client type = Business Banking Group
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    When I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I Complete "EnrichKYCProfileGrid" task
    When I navigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task
    When I select Legal Entity type as "Individual" and having value in "OtherNationality" field on LE details screen
    When I navigate to "Associationdetails" task
    Then I select relationship as "Ultimate Beneficial Owner (UBO)" and complete the task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as conditionally mandatory on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
      | Passport Number (other Nationality) |
