#Test Case: TC_R1S2EPIC002PBI200_01
#PBI: R1S2EPIC002PBI200
#User Story ID: US012
#Designed by: Priyanka Arora
Feature: TC_R1S2EPIC002PBI200_01

  @Automation
  Scenario: Validate the renamed fields on "LE details" screen for the Associated LE added via Express addition for "individual" relationship on "Capture Hierarchy details" screen     
    
    Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "Corporate" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship |
		| C1      | C1           |
		
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	#Given I login to Fenergo Application with "SuperUser"
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
 
	When I navigate to "EnrichKYCProfileGrid" task 
	And I take a screenshot 
	When I complete "AddAddress" task 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
#	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId"	
	When I navigate to "CaptureHierarchyDetailsGrid" task
    


	When I add AssociatedParty by right clicking
	#When I add "AssociatedParty" via express addition
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual"	
	#When I select Legal Entity type as "Individual" and complete the task 	
	When I complete "AssociationDetails" screen with key "DirectorIndividual"	


	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId"	
	When I navigate to "CaptureHierarchyDetailsGrid" task  	
	When I "Navigate to Legal Entity" by right clicking

    #Testdata: Verify below mentioned fields are renamed  on "LE details" screen
    And I can see "Citizenship" label is renamed as "Other Nationality" on "LE Details" screen
    And I can see "Number of" label is renamed as "National Identification Number" on "LE Details" screen
    And I can see "National Insurance Number" label is renamed as "EID Number" on "LE Details" screen
    
#    And I verify below renamed fields on "LE details" screen:
#      | Previous Label           | Renamed Label                  | Field Type   | Visible | editable | Mandatory |
#      | Citizenship              | Other Nationality              | Numeric      | True    | True     | False     |
#      | Number of                | National Identification Number | AlphaNumeric | True    | True     | False     |
#      | National Insurance Numbe | EID Number                     | AlphaNumeric | True    | True     | False     |
      
      
      
