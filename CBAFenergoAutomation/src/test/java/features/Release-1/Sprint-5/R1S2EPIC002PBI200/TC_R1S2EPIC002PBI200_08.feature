#Test Case: TC_R1S2EPIC002PBI200_08
#PBI: R1S2EPIC002PBI200
#User Story ID: US011
#Designed by: Priyanka Arora
Feature: TC_R1S2EPIC002PBI200_08 

@Automation 
Scenario:
Validate the hidden fields on "LE details" screen for the Associated LE(for "client" relationship) added via Express addition on "Capture Hierarchy details" screen 
	Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "Corporate" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship |
		| C1      | C1           |
		
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1"
	And I click on "SaveandCompleteforValidateKYC" button
	 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddress" task 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "DirectorIndividual" 
	And I navigate to "LEDetails" screen of added AssociatedParty 
	
	#Testdata: Verify below mentioned fields as hidden  on "LE details" screen
	And I take a screenshot 
	And I check that below data is not visible 
		|FieldLabel|
		| "Customer details" section                                    |
		| Australian Company Number Issued                              |
		| Registered by Australian Securities and Investment Commission |
		| Legal Status                                                  |
		| Anticipated Activity of Account                               |
		| Date of Establishment                                         |
		| Location of Business/Activity                                 |
		| Industry codes details section                              |
		| SIC                                                           |
		| NAIC                                                          |
		| ISC                                                           |
		| ISIN                                                          |
		| NACE 2 Code                                                   |
		| Stock Exchange Code                                           |
		| Central Index Key                                             |
		| SWIFT BIC                                                     |
