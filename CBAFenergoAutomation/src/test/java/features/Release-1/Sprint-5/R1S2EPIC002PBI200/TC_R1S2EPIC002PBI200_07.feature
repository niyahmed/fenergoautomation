#Test Case: TC_R1S2EPIC002PBI200_07
#PBI: R1S2EPIC002PBI200
#User Story ID: US011
#Designed by: Priyanka Arora
Feature: TC_R1S2EPIC002PBI200_07

  @Automation
  Scenario: Validate the renamed fields on "LE details" screen for the Associated LE (for "client" relationship) added via Express addition on "Capture Hierarchy details" screen
    Given I login to Fenergo Application with "RM:IBG-DNE"
    # Select value in "Client type" as per the updated List of Values ( FI, NBFI, Corporate, PCG-Entity)
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship |
		| C1      | C1           |
    And I click on "Continue" button
  When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddress" task 
    When I Complete "EnrichKYCProfileGrid" task
    And I click on "SaveandCompleteforEnrichKYC" button
   When I navigate to "CaptureHierarchyDetailsGrid" task
     When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Individual"
    When I "Navigate to Legal Entity" by right clicking
    #Testdata: Verify below mentioned fields are renamed  on "LE details" screen
    And I take a screenshot
    
     And I can see "Citizenship" label is renamed as "Other Nationality" on "LE Details" screen
      And I can see "Citizenship" label is renamed as "Other Nationality" on "LE Details" screen
    And I can see "Number of" label is renamed as "National Identification Number" on "LE Details" screen
    And I can see "National Insurance Number" label is renamed as "EID Number" on "LE Details" screen
#    And I verify below renamed fields on "LE details" screen:
#      | Pevious Label               | Renamed label                                    |
#      | Principal Place of Business | Country of Business Operations/Economic Activity |
#      | Registration Body           | Name of Registration Body                        |
#      | Regulated By                | Name of Regulatory Body                          |
#      | Date of Establishment       | Date of Incorporation/Establishment              |
#      | ISIC                        | Primary Industry of Operation                    |
    #Testdata: Verify "Legal entity name" field as per the updated format (No special characters allowed. A-Z, a-z and 0-9  and . (period/full stop) allowed) on "LE details" screen
