#Test Case: TC_R1S5EPICDM006_05
#PBI: R1S5EPICDM006
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Ability to add Address subflow (DM screen - 1)

  @Tobeautomated
  Scenario: Validate RM user is able to add  Address via Address subflow along with Fields validation on "DM screen-1 (Capture LE details)" for "PCG-Entity" client Type
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a new DM request with FABEntityType as "PCG-Entity" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    #Test-data: Verify Address sub-flow is a non-mandatory sub-flow
    And I assert that "AddressesGrid" is non-mondatory
    When I click on plus button displaying at the top of "Address" sub-flow
    Then I navigate to "AddAddress" screen
    #Test-data: Verify the available fields on Add Address screen
    Then I check that below data is available
      | Label           | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Address Type    | MultiSelectDropdown | true    | false    | true      | Select...  |
      | Address Line 1  | Alphanumeric        | true    | false    | true      | NA         |
      | Address Line 2  | Alphanumeric        | true    | false    | false     | NA         |
      | Town / City     | Alphanumeric        | true    | false    | false     | NA         |
      | ZIP Code        | Alphanumeric        | true    | false    | false     | NA         |
      | PO Box          | Alphanumeric        | true    | false    | false     | NA         |
      | Country         | Dropdown            | true    | false    | true      | Select...  |
      | State / Emirate | Dropdown            | true    | false    | false     | Select...  |
    Then I add Address Type as "Correspondence" with country as "AE-United Arab Emirates"
    And I verify that below data is mandatory
      | Label           |
      | PO Box          |
      | State / Emirate |
    #Test-data: Verify LOVs for State/Emirate field on Add Address screen
    Then I validate LOVs for State/Emirate field
    Then I can see Save button is enabled
    When I click on "Save" button
    #Test-data: Verify Address is added under Address sub-flow on "CaptureLEdetails" task
    Then I Check that the newly added Address is visible
    #Test-data:Verify user is able to Add another Address
    When I click on plus button displaying at the top of "Address" sub-flow
    Then I navigate to "AddAddress" screen
    Then I enter all the required details and click on save
    And I see another Address is added to Address grid
