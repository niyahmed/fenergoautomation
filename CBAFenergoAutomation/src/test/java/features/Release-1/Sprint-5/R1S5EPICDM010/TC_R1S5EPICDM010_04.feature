#Test Case: TC_R1S5EPICDM010_01
#PBI: R1S5EPICDM010
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Ability to add a Anticipated Transactional Activity (Per Month) subflow (DM screen - 1)

  Scenario: Validate RM user is able to add details to "Anticipated Transactional Activity (Per Month)" subflow along with Fields validation on "DM screen-1 (Capture LE details)" for "BBG" client Type
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a new DM request with FABEntityType as "BBG" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    #Test-data: Verify "Anticipated Transactional Activity (Per Month)" sub-flow is a non-mandatory sub-flow
    And I assert that "Anticipated Transactional Activity (Per Month)" is non-mondatory
    When I click on plus button displaying at the top of "AnticipatedTransactionalActivity(Per Month)" sub-flow
    Then I navigate to "AnticipatedTransactionalActivity(Per Month)" task screen
    #Test-data: Verify the available fields on Anticipated Transactional Activity (Per Month) screen
    Then I check that below data is available
      | Fenergo Label Name                    | Field Type            | Visible | Editable | Mandatory | Field Defaults To |
      | Value of Projected Transactions (AED) | Numeric               | Yes     | Yes      | No        | NA                |
      | Number of Transactions                | Numeric               | Yes     | Yes      | No        | NA                |
      | Type/Mode of Transactions             | Alphanumeric          | Yes     | Yes      | No        | NA                |
      | Transaction Type                      | Drop down             | Yes     | Yes      | No        | Select...         |
      | Currencies Involved                   | Multiselect drop-down | Yes     | Yes      | No        | Select...         |
      | Countries Involved                    | Multiselect drop-down | Yes     | Yes      | No        | Select...         |
    #Test-data: Validate "Transaction Type" drop-down has following LOVs
    And I validate "Transaction Type" drop-down has following LOVs
      | Values      |
      | Deposits    |
      | Withdrawals |
    #Test-data: Validate LOVs for "Countries involved" drop-down
    And I validate LOVs for "Countries involved" drop-down
    Then I add the details "Anticipated Transactional Activity (Per Month)" task screen and complete the task
    And I assert values are displaying under Anticipated Transactional Activity (Per Month) grid
    #Test-data: Verify user is able to add another record for "Anticipated Transactional Activity (Per Month)" sub-flow
    When I click on plus button displaying at the top of "AnticipatedTransactionalActivity(Per Month)" sub-flow
    Then I navigate to "AnticipatedTransactionalActivity(Per Month)" task screen
    Then I enter all required details and click on save button
    And I validate record is added successfully
