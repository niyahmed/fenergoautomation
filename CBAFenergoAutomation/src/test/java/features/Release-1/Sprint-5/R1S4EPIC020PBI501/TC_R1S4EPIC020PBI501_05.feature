#Test Case: TC_R1S4EPIC020PBI501_05
#PBI: R1S4EPIC020PBI501
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Quality Control task at Risk Assessment stage

  @Tobeautomated
  Scenario: Validate "Quality control" task is removed under Risk Assessment stage for risk rating "Very High" and for client type "PCG Entity"
 		#validate following task gets trigerred for "Very High" risk rating
 		#Relationship Manager Review and Sign-Off
    #CIB R&C KYC Approver - KYC Manager Review and Sign-Off
    #CIB R&C KYC Approver - AVP Review and Sign-Off
    #CIB R&C KYC Approver - VP Review and Sign-Off
    #CIB R&C KYC Approver - SVP Review and Sign-Off
    #Group Compliance (CDD) Review and Sign-Off
    #Business Unit Head Review and Sign-Off
    #Business Head Review and Sign-Off
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a new request with FABEntityType as "PCG Entity" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseID"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Test-data: Select the Risk category as "Very High" and complete "CaptureRiskCategoryGrid" task
    Then I select risk category as "Very High"
    And I complete "CaptureRiskCategoryGrid" task
    #verify "Quality control" task is not generated under Risk Assessment stage
    And I see "Quality control" task is not generated under Risk Assessment stage
    #Verify 'Relationship Manager Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete 'Relationship Manager Review and Sign-Off' task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    When I select "Refer" option for "ReviewOutome" field and refer stage as "RiskAssessmentFAB"
    #Validate the case is referred to "RiskAssessmentFAB" stage
    Then I see the case is referred to "RiskAssessmentFAB" stage
    Then I navigate to "RiskAssessmentFAB" task and kept the Risk rating "Low"
    And I complete "RiskAssessmentFAB" task
    #verify "Quality control" task is not generated under Risk Assessment stage
    And I see "Quality control" task is not generated under Risk Assessment stage            
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    When I select "Refer" option for "ReviewOutome" field and refer stage as "RiskAssessmentFAB"
    #Validate the case is referred to "RiskAssessmentFAB" stage
    Then I see the case is referred to "RiskAssessmentFAB" stage
    Then I navigate to "RiskAssessmentFAB" task and kept the Risk rating "Medium"            
    And I complete "RiskAssessmentFAB" task
    #verify "Quality control" task is not generated under Risk Assessment stage
    And I see "Quality control" task is not generated under Risk Assessment stage
    #Verify 'Relationship Manager Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete 'Relationship Manager Review and Sign-Off' task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
    And I validate the following task is getting generated   
    When I select "Refer" option for "ReviewOutome" field and refer stage as "RiskAssessmentFAB"
    Then I see the case is referred to "RiskAssessmentFAB" stage
    Then I navigate to "RiskAssessmentFAB" task and kept the Risk rating "Medium/Low"
    And I complete "RiskAssessmentFAB" task
    #verify "Quality control" task is not generated under Risk Assessment stage
    And I see "Quality control" task is not generated under Risk Assessment stage
    #Verify 'Relationship Manager Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete 'Relationship Manager Review and Sign-Off' task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
    And I validate the following task is getting generated    
    When I select "Refer" option for "ReviewOutome" field and refer stage as "RiskAssessmentFAB"
    #Validate the case is referred to "RiskAssessmentFAB" stage
    Then I see the case is referred to "RiskAssessmentFAB" stage
    Then I navigate to "RiskAssessmentFAB" task and kept the Risk rating "High"
    And I complete "RiskAssessmentFAB" task    
    #verify "Quality control" task is not generated under Risk Assessment stage
    And I see "Quality control" task is not generated under Risk Assessment stage
    #Verify 'Relationship Manager Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete 'Relationship Manager Review and Sign-Off' task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete 'CIB R&C KYC Approver - AVP Review and Sign-Off' task
    #Verify 'CIB R&C KYC Approver - VP Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    And I complete'CIB R&C KYC Approver - VP Review and Sign-Off' task
    #Verify 'Business Unit Head Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete 'Business Unit Head Review and Sign-Off' task    
    When I select "Refer" option for "ReviewOutome" field and refer stage as "RiskAssessmentFAB"
    #Validate the case is referred to "RiskAssessmentFAB" stage
    Then I see the case is referred to "RiskAssessmentFAB" stage
    Then I navigate to "RiskAssessmentFAB" task as "very High"    
    And I complete "RiskAssessmentFAB" task
    #verify "Quality control" task is not generated under Risk Assessment stage
    And I see "Quality control" task is not generated under Risk Assessment stage
    #Verify 'Relationship Manager Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete 'Relationship Manager Review and Sign-Off' task
    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete "CIB R&C KYC Approver - KYC Manager Review and Sign-Off" task
    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete 'CIB R&C KYC Approver - AVP Review and Sign-Off' task
    #Verify 'CIB R&C KYC Approver - VP Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    And I complete'CIB R&C KYC Approver - VP Review and Sign-Off' task
    #Verify 'CIB R&C KYC Approver - SVP Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete 'CIB R&C KYC Approver - SVP Review and Sign-Off' task
    #Verify 'Group Compliance (CDD) Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete 'Group Compliance (CDD) Review and Sign-Off' task
    #Verify 'Business Unit Head Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete "Business Unit Head Review and Sign-Off" task
    #Verify 'Business Head Review and Sign-Off' task is generated
    And I validate the following task is getting generated
    Then I complete 'Business Head Review and Sign-Off' task
