#Test Case: TC_R1S4EPIC020PBI501_03
#PBI: R1S4EPIC020PBI501
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Quality Control task at Risk Assessment stage 

@Automation 
Scenario: 
	Validate "Quality control" task is removed under Risk Assessment stage for risk rating "Medium/Low" and for client type "NBFI" 
	#validate following task gets trigerred for "Medium/Low" risk rating
	#Relationship Manager Review and Sign-Off
	#CIB R&C KYC Approver - KYC Manager Review and Sign-Off
	#CIB R&C KYC Approver - AVP Review and Sign-Off
	#Business Unit Head Review and Sign-Off
	Given I login to Fenergo Application with "RM:NBFI" 
	#Creating a legal entity with legal entity role as Client/Counterparty
	When I complete "NewRequest" screen with key "NBFI" 
	And I complete "CaptureNewRequest" with Key "NBFI" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseID" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "NBFI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddress" task 
	When I complete "EnrichKYC" screen with key "NBFI" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CaptureRiskCategoryGrid" task 
	#Test-data: Select the Risk category as "Medium" and complete "CaptureRiskCategoryGrid" task
	
	When I complete "RiskAssessmentFAB" task 
	#verify "Quality control" task is not generated under Risk Assessment stage
	And I see "QualityControlTask" task is not generated under Risk Assessment stage 
	#Verify 'Relationship Manager Review and Sign-Off' task is generated
	Then I login to Fenergo Application with "RM:NBFI" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" screen with key "RefertoRiskAssessment" 
	And I click on "Submit" button 
	#Validate the case is referred to "RiskAssessmentFAB" stage
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentMedium" task 
	And I see "QualityControlTask" task is not generated under Risk Assessment stage 
	Then I login to Fenergo Application with "RM:NBFI" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	#Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" screen with key "RefertoRiskAssessment" 
	And I click on "Submit" button 
	#	Validate the case is referred to "RiskAssessmentFAB" stage
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessment" screen with key "Medium" 
	
	#verify "Quality control" task is not generated under Risk Assessment stage
	And I see "QualityControlTask" task is not generated under Risk Assessment stage 
	#Verify 'Relationship Manager Review and Sign-Off' task is generated
	
	Then I login to Fenergo Application with "RM:NBFI" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	#Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" screen with key "RefertoRiskAssessment" 
	And I click on "Submit" button 
	#Validate the case is referred to "RiskAssessmentFAB" stage 
	
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessment" screen with key "High" 
	
	
	
	And I see "QualityControlTask" task is not generated under Risk Assessment stage 
	Then I login to Fenergo Application with "RM:NBFI" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	#Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I store the "CaseId" from LE360 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task