#Test Case: TC_R1S5EPICDM005_03
#PBI: R1S5EPICDM005
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Ability to add Relationships subflow (DM screen - 1)

  Scenario: Validate Relationship subflow(by searching using DAO code)along with Field validations for RM user on "DM screen-1 (Capture LE details)" for "NBFI" client Type
    #Section: Add Relationship by searching user record and add relationship subflow on "DM screen-1 (Capture LE details)"
    #Section: Can add customer relationship details on Relationship screen and save the record on "DM screen-1 (Capture LE details)"
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a New DM request with FABEntityType as "NBFI" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    #Test-data: Verify below mentioned fields on Add Relationship screen
    Then I validate that below data is available
      | FieldLabel    | FieldType    | Mandatory | Visible | Editable |
      | User Login    | Alphanumeric | False     | True    | True     |
      | First name    | Alphanumeric | False     | True    | True     |
      | Last Name     | Alphanumeric | False     | True    | True     |
      | Email Address | Alphanumeric | False     | True    | True     |
      | Phone Number  | Alphanumeric | False     | True    | True     |
      | DAO Code      | Drop-down    | False     | True    | True     |
    When I select a value from "DAOcode" field and click on search button
    Then I select a record from the results
    #Test-data: Verify below mentioned fields on Add Relationship screen
    Then I validate that below data is available
      | FieldLabel                                              | FieldType | Mandatory | Visible | Editable |
      | Relationship Type                                       | Drop-down | True      | True    | True     |
      | Relationship status                                     | Drop-down | True      | True    | True     |
      | Is this the primary relationship for this legal entity? | Check-box | False     | True    | True     |
    And I add Relationship by filling in the data
    Then I click on "Save" button
    #Test-data: Verify Relationship is added under Relationship sub-flow
    And I verify that product is added under "relationship" sub-flow
    #Test-data:Verify user is able to Add another Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    Then I enter all the required details and click on save
    And I see another relaionship is added to relationship grid

  Scenario: Validate Relationship subflow along with Field validations for RM user on "DM screen-1 (Capture LE details)" for "NBFI" client Type
    #Section: Add Relationship by searching user record and add relationship subflow on "DM screen-1 (Capture LE details)"
    #Section: Can add customer relationship details on Relationship screen and save the record on "DM screen-1 (Capture LE details)"
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a New DM request with FABEntityType as "NBFI" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    #Test-data: Verify below mentioned fields on Add Relationship screen
    Then I validate that below data is available
      | FieldLabel    | FieldType    | Mandatory | Visible | Editable |
      | User Login    | Alphanumeric | False     | True    | True     |
      | First name    | Alphanumeric | False     | True    | True     |
      | Last Name     | Alphanumeric | False     | True    | True     |
      | Email Address | Alphanumeric | False     | True    | True     |
      | Phone Number  | Alphanumeric | False     | True    | True     |
      | DAO Code      | Drop-down    | False     | True    | True     |
    When I select a record from the results
    #Test-data: Verify below mentioned fields on Add Relationship screen
    Then I validate that below data is available
      | FieldLabel                                              | FieldType | Mandatory | Visible | Editable |
      | Relationship Type                                       | Drop-down | True      | True    | True     |
      | Relationship status                                     | Drop-down | True      | True    | True     |
      | Is this the primary relationship for this legal entity? | Check-box | False     | True    | True     |
    And I add Relationship by filling in the data
    Then I click on "Save" button
    #Test-data: Verify Relationship is added under Relatoinship sub-flow
    And I verify that product is added under "relationship" sub-flow
