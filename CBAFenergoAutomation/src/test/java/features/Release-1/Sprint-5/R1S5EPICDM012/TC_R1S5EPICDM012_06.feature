#Test Case: TC_R1S5EPICDM012_06
#PBI: R1S5EPICDM012
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S5EPICDM012_06

    Scenario: Verify Anticipated Transaction Activity (per month) subflow is hidden only for FI Client Type
    Given I login to Fenergo Application with "DMuser"
    #Create DM Request with Client type = FI and Country of Incorporation / Establishment = UAE or any other country
    #Click on + button and then click on New DM Request
    When I navigate to DM Request screen
    #Fill in all the mandatory and non-mandatory fields and click on Save button
    #Add all the subflows 
    #Search existing DM Entity
    #Click on + button and then click on New DM Request and select 'Existing' in DM Request type
    #Enter T24 CIF ID (for the entiy created with adding all the sub flows)
    #Validate the entity is available in search result grid and all the below sub flows are editable
    #Documents
    #Products
    #Relationships
    #Addresses
    #Contacts
    #Comments
    #Validate other grids from Enter Entity Details to Risk Assessment are Not available or disabled