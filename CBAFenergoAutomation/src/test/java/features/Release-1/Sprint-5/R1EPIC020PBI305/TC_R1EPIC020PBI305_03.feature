#Test Case: TC_R1EPIC020PBI305_03
#PBI: R1EPIC020PBI305
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Enrich KYC Profile / LE Details / Verified LE Details - Anticipated Transaction Volumes (Annual in AED)

  @Automation
  Scenario: Verify "Anticipated Transaction Volumes (Annual in AED)" section is not visible on "Enrich Client information" task, "LE360- LE details" , "LE360-LE Verified details" task for Onboarding maker for client Type "NBFI"
    
      Given I login to Fenergo Application with "RM:NBFI" 
	When I complete "NewRequest" screen with key "Non FI" 
	And I complete "CaptureNewRequest" with Key "NBFI" and below data 
		| Product | Relationship |
		| C1      | C1           |

 	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	#Given I login to Fenergo Application with "SuperUser"
	Given I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId"    
    When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "NBFI"
	And I click on "SaveandCompleteforValidateKYC" button
	
	Given I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
    When I navigate to "EnrichKYCProfileGrid" task 
    And I take a screenshot  
    #Test-data: Verify "Anticipated Transaction Volumes (Annual in AED)" section is not visible
    #And I validate "Anticipated Transaction Volumes (Annual in AED)" section is not visible
    Then I check that below data is not visible 
		| FieldLabel                      |
		| Anticipated Transaction Volumes (Annual in AED) |
	When I complete "AddAddress" task 
    When I complete "EnrichKYC" screen with key "NBFI"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    
    #Then I login to Fenergo Application with "Onboarding Maker"
    Given I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId"   
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument
	Then I complete "KYCDocumentRequirements" task
    
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
    
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task
    
	Then I login to Fenergo Application with "RM:NBFI" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
    
    #Then I login to Fenergo Application with "FLoydKYC"
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 	
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task  
	When I complete "ReviewSignOff" task
    
    #Then I login to Fenergo Application with "FLoydAVP"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
	When I search for the "CaseId"
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task
    
    #Then I login to Fenergo Application with "BusinessUnitHead"
	Then I login to Fenergo Application with "BUH:NBFI" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task  
	When I complete "ReviewSignOff" task
	
	 
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	When I navigate to "CaptureFabReferencesGrid" task
	When I complete "CaptureFABReferences" task
	
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	When I navigate to "CaptureFabReferencesGrid" task
	And I click on "SaveFABReferencesButton" button
    
    #When I navigate to "LE360-LEdetails" task
    #Test-data: Verify "Anticipated Transaction Volumes (Annual in AED)" section is not visible
    Then I login to Fenergo Application with "SuperUser" 
	When I search for the "CaseId"
    Then I navigate to "LE360overview" screen 
	Then I navigate to "LEDetails" screen
	And I take a screenshot
    Then I check that below data is not visible 
		| FieldLabel                      |
		| Anticipated Transaction Volumes (Annual in AED) |
    #And I validate "Anticipated Transaction Volumes (Annual in AED)" section is not visible
    Then I login to Fenergo Application with "SuperUser" 
	When I search for the "CaseId"
    Then I navigate to "LE360overview" screen
    When I navigate to "VerifiedLEDetails" task
    And I take a screenshot
    #Test-data: Verify "Anticipated Transaction Volumes (Annual in AED)" section is not visible
    #And I validate "Anticipated Transaction Volumes (Annual in AED)" section is not visible
    Then I check that below data is not visible 
		| FieldLabel                      |
		| Anticipated Transaction Volumes (Annual in AED) |
    
