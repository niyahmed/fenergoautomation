#Test Case: TC_R1S3EPIC007PBI002_01
#PBI: R1S3EPIC007PBI002
#User Story ID: D1.7
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Document Format

  @Tobeautomated
  Scenario: Validate for below  non-acceptable formats of file and corresponding error message that will be displayed when "Onboarding Maker" user uploads document on document details screen of 'KYC document requirement' task of "Enrich KYC profile" stage:
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I complete to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I navigate to "KYCDocumentrequirement" task.
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    #Test-data: Validate document with .exe format is not acceptable in system when user try to upload a document againsta a particular requirement.
    When I add a document with .exe format
    Then I can see document is not acceptable on Document details screen
    And I can see error 'Invalid file format - Following extensions not allowed: exe config zip'
    #Test-data: Validate document with .zip format is not acceptable in system when user try to upload a document againsta a particular requirement.
    When I add a document with .zip format
    Then I can see document is not acceptable on Document details screen
    And I can see error 'Invalid file format - Following extensions not allowed: exe config zip'
    #Test-data: Validate document with .config format is not acceptable in system when user try to upload a document againsta a particular requirement.
    When I add a document with .config format
    Then I can see document is not acceptable on Document details screen
    And I can see error 'Invalid file format - Following extensions not allowed: exe config zip'
    #Test-data: Validate document with .doc format is acceptable in system when user try to upload a document againsta a particular requirement.
    When I add a document with .doc format
    Then I can see document is acceptable on Document details screen
    And added document is saved successfully
    When I navigate to "KYCDocumentrequirement" task.
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    #Test-data: Validate document with .Xlsx format is acceptable in system when user try to upload a document againsta a particular requirement.
    When I add a document with .xlsx format
    Then I can see document is acceptable on Document details screen
    And added document is saved successfully
    When I navigate to "KYCDocumentrequirement" task.
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    #Test-data: Validate document with .png format is acceptable in system when user try to upload a document againsta a particular requirement.
    When I add a document with .png format
    Then I can see document is acceptable on Document details screen
    And added document is saved successfully
