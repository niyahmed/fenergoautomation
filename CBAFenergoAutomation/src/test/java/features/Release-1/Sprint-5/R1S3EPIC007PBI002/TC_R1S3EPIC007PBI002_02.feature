#Test Case: TC_R1S3EPIC007PBI002_02
#PBI: R1S3EPIC007PBI002
#User Story ID: D1.7
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Document Format

  @Tobeautomated
  Scenario: Validate document with size less than or equal to 5 MB is acceptable in system otherwise it should through an error
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I navigate to "Documentdetails" task by clicking on plus sign displaying at the top of document section
    #Test-data: Validate document with size more than 5 MB
    When I upload a document of size more than 5 mb
    Then I can see document is not acceptable on Document details screen
    And error message "The file cannot be saved because it exceeds 5 MB" appearing on document details screen
    #Test-data: Validate document with size less than 5 MB
    When I upload a document of size less than 5 mb
    Then I can see document is acceptable and details are saved successfully on Document details screen
    #Test-data: Validate document with size 5 MB
    When I upload a document of size 5 mb
    Then I can see document is acceptable and details are saved successfully on Document details screen
