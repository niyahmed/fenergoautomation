#Test Case: TC_R1S5EPICDM007_05
#PBI: R1S5EPICDM007
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Ability to add a Contacts subflow (DM screen - 1)

  Scenario: Validate RM user is able to add Contacts via contacts subflow along with Fields validation on "DM screen-1 (Capture LE details)" for "PCG Entity" client Type
    #Section: "Home Phone" field became mandatory when user selects "Home Phone" as value in "Primary Phone Numer" field 
    #Section: "Work Phone" field became mandatory when user selects "Work Phone" as value in "Primary Phone Numer" field 
    #Section: "Mobile" field became mandatory when user selects "Mobile" as value in "Primary Phone Numer" field 
    #section: Format of "Home Phone, Work Phone, Mobile" fields
    #Section: Add another contact record
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a New DM request with FABEntityType as "PCG Entity" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    When I click on plus button displaying at the top of "Contacts" sub-flow
    When I navigate to "Contactdetails" task
    #Test-data: Verify below mentioned fields on "Contact details" screen
    Then I validate that below data is available
      | FieldLabel                      | FieldType    | Mandatory               | Visible | Editable |
      | Contact Type                    | Drop-down    | True                    | True    | True     |
      | Legal Basis for Data Processing | Multi-select | True                    | True    | True     |
      | Title                           | Drop-down    | True                    | True    | True     |
      | First Name                      | Alphanumeric | True                    | True    | True     |
      | Last name                       | Alphanumeric | True                    | True    | True     |
      | Status                          | Drop-down    | True                    | True    | True     |
      | Primary Phone Number            | Drop-down    | True                    | True    | True     |
      | Work Phone                      | Numeric      | Conditionally mandatory | True    | True     |
      | Home Phone                      | Numeric      | Conditionally mandatory | True    | True     |
      | Mobile                          | Numeric      | Conditionally mandatory | True    | True     |
      | Fax                             | Alphanumeric | False                   | True    | True     |
      | Email Address                   | Alphanumeric | True                    | True    | True     |
      | Is Primary contact              | Check box    | False                   | True    | True     |
    #Test-data: Verify "Home Phone" field became mandatory when user selects "Home Phone" as value in "Primary Phone Numer" field 
    When I select "Home Phone" from "Primary Phone Numer" field 
    Then I see  "Home Phone" field became mandatory     
    #Test-data: Verify "Work Phone" field became mandatory when user selects "Work Phone" as value in "Primary Phone Numer" field 
    When I select "Work Phone" from "Primary Phone Numer" field 
    Then I see  "Work Phone" field became mandatory 
    #Test-data: Verify "Mobile" field became mandatory when user selects "Mobile" as value in "Primary Phone Numer" field 
    When I select "Mobile" from "Primary Phone Numer" field 
    Then I see  "Mobile" field became mandatory 
    #Test Data: Validate format of "Home Phone, Work Phone, Mobile" fields
    And I enter value in "Home Phone, Work Phone, Mobile" and I can see error message "Telephone number should be in the format - CCCNNNNNNNNNNN" 
	  Then I enter values in "Home Phone, Work Phone, Mobile" fields in format "CCCNNNNNNNNNNN max 14 numbers" 
	  Then I Verify data is saved successfully in "CCCNNNNNNNNNNN max 14 numbers" format 
	  #Test-data: Verify user is able to add another contact record 
	  When I click on plus button displaying at the top of "Contacts" sub-flow
    When I navigate to "Contactdetails" task
    Then I enter all required details and click on save button
    And I assert record is added successfully
	  
    
