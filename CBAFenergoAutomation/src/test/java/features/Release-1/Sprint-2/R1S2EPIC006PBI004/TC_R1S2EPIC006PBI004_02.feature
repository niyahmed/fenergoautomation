#Test Case: TC_R1S2EPIC006PBI004_02
#PBI: R1S2EPIC006PBI004
#User Story ID: 13
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: TC_R1S2EPIC006PBI004_02-Screening 

@Automation 
Scenario: 
	Validate if system is automatically pick up the values that are recorded in both Fircosoft and Google screening whereby there is a 'Positive Match'

	Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "Corporate" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddress" task 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	#=And Ficrosoft screening for the entity with "Positive" match for "Adverse Media"
	#=   And Google screening for the entity with "Positive" match for "Adverse Media"
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
  #=And I click on "SaveandCompleteforAssessmentScreen1" button
	 
	When I Initiate "Google" by rightclicking 
	And I complete "Google" from assessment grid with Key "GoogleScreeningData" 
  #=And I click on "SaveandCompleteforAssessmentScreen1" button 
	And validate if confirmed matches number populates correctly in Complete AML screen according to below table 
		| FieldLabel    | Number |
		| Adverse Media |      1 |
		
		
