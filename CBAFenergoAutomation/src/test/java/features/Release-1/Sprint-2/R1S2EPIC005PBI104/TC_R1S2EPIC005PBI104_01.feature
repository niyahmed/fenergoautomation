#Test Case: TC_R1S2EPIC005PBI104_01
#PBI: R1S2EPIC005PBI104
#User Story ID: CORP2, CORP5, CORP3, CORP5, CORP6, CORP7, CORP8,CORP10, CORP14, Corp15, 
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S2EPIC005PBI104_01

@Tobeautomated

Scenario: Verify the following functionality on Document details screen for Onboarding Maker (refer latest PBI for reference)
	Given I login to Fenergo Application with "RM"
	When I create new request with LegalEntityrole as "Client/Counterparty" and "ClientType" as "Corporate"
	When I navigate to "CaptureRequestDetailsFAB" task 
	When I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker"
	When I search for "CaseID" 
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	
	# Test-Data: Verify uploading document corresponding to document requirement
	When I click on "AttachDocument" from options button displaying corresponding to document requirement
	When I navigate to "DocumentDetails" screen
	When I add document by dragging a document
	When I add "DocumentName", "Documentcategory", "ID" and "Expiration date" and click on "Save" button
	Then I can see document is added successfully
	
	When I navigate back to "KYCDocumentRequirementsGrid" task
	# Test-Data: Verify adding additional document corresponding to document requiremen and Saving it
	When I click on "AttachDocument" from options button displaying corresponding to document requirement having a document attached to it
	When I navigate to "DocumentDetails" screen
	When I add document by browsing a document from "UploadDocument" button
	When I add "DocumentName" and "Documentcategory" and click on "Save" button
	Then I can see document is added successfully
	
	When I navigate back to "KYCDocumentRequirementsGrid" task
	# Test-Data: Verify adding "Document Collection from RM - Email approval" corresponding to document requirement
	When I click on "AttachDocument" from options button displaying corresponding to document requirement having a document attached to it
	When I navigate to "DocumentDetails" screen
	When I add document by browsing a document from "Requestviaemail" button
	When I add "contact" in the Contact grid.
	And I select added contact and click on "EditEmail" button 
	When I navigate to "EditEmail" screen and complete the task 
	Then I can see Email is sent successfully
	
	When I navigate back to "KYCDocumentRequirementsGrid" task
	# Test-Data: Verify User is able to view attached document corresponding to document requirement on "Document details" screen
	When I click on "View" from options button displaying corresponding to document requirement
	When I navigate to "DocumentDetails" screen
	Then I can see document is already attached on "DocumentDetails" screen along with "DocumentName" and "Documentcategory"
	When I navigate back to "KYCDocumentRequirementsGrid" task
	# Test-Data: Verify User is able to view attached document corresponding to document requirement on "KYCDocumentRequirementsGrid" screen
	Then I can see attached document is displaying in document grid as well corresponding to document requirement
	
	When I expand the Document requirement on "KYCDocumentRequirementsGrid" task
	# Test-Data: Verify User is able to remove attached document corresponding to document requirement on "Document details" screen
	When I click on "remove" from options button displaying corresponding to document requirement having a document attached to it
	Then I can see attached document is removed which was attached to the document requirement displaying under document grid
	
	 #Test-data: Verify "Others" as Non-mandatory document requirement on requirement grid of "KYC document requirement" screen
    When I navigate back to "KYCDocumentRequirementsGrid" task
    Then I can see "Others" as document requirement on requirement grid
	
	#Test-data: Verify after uploading the documents once, display all the documents in a grid with unique ID for each document on "KYC document requirement" screen
    When I navigate back to "KYCDocumentRequirementsGrid" task
    When I add document corresponding to any document requirement.	
    When I add another document corresponding to any document requirement.
    Then I can see all the documents in a grid with unique ID for each document
	
	 # Test-Data: Verify searching an existing document and adding it corresponding to document requirement
    When I navigate back to "KYCDocumentRequirementsGrid" task
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    When I select "Existingdocument" from "Document source" drop-down
    Then I can see "existingdocuments" in the results grid
    When I select a document from the results and click on "save" button
    When I expand the grid corresponding to the same document requirement
    Then I can see existing document is added successfully to the document requirement
	
	 # Test-Data: Verify User is able to view pending document corresponding to document requirement on "KYCDocumentRequirementsGrid" screen
    Then I can see "Status" of document is displaying as "pending" in document grid
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
