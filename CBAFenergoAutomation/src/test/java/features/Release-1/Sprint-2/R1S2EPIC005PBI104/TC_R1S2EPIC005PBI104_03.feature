#Test Case: TC_R1S2EPIC005PBI104_03
#PBI: R1S2EPIC005PBI104
#User Story ID: CORP0, CORP1, CORP4
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S2EPIC005PBI104_04
@Tobeautomated

Scenario: Verify the document Type corresponding to the document category  drop-down along with mandatory and Non-mandatory flag on "document details screen" for "KYC document Requirements" task :
	Given I login to Fenergo Application with "RM"
	When I create new request with LegalEntityrole as "Client/Counterparty" and "ClientType" as "Corporate"
	When I navigate to "CaptureRequestDetailsFAB" task 
	When I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker"
	When I search for "CaseID" 
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	
	# Test-Data:Verify document Type corresponding to the document category drop-down along with mandatory and Non-mandatory flag
	And I Verify the document Type corresponding to the document category drop-down along with mandatory and Non-mandatory flag(refer PBI doc for reference)
	
	|Requirement (List of documents to be shown            |Doc Category|             Doc type|                          Mandatory|
  |on KYC Document Requirement Section)|
   
   | Account Opening Form | 															| AOF |                     Account Opening Form |         	   	False|
   |Swift Message MT 199/299|            																							|Swift Message MT 199/299|   				False|
   |KYC Form    |                        																							|KYC Form    |                      True| 
   | Annual Report|                                                                   | Annual Report|                    True|
   | Proof of Address (Client) |         																							| Proof of Address (Client) |       True|
	
	 | Passport | 										                   | Authorized Signatories |      Passport |         	                False|
   |Passport (Dual National|            																							|Passport (Dual Nationality) |   			False|
   |Emirates ID |                        																							|Emirates ID   |                      False| 
   | Residence Visa|                                                                  |Residence Visa|                      True|
   | GCC ID  |         																																|  GCC ID   |                         True|
	 |Diplomat ID |																																			|Diplomat ID |	                      False|
	
	 |Passport | 										                   | MISC |                          Passport |         	                False|
   |Passport (Dual National|            																							|Passport (Dual Nationality) |   			False|
   |Emirates ID |                        																							|Emirates ID   |                      False| 
   | Residence Visa|                                                                  |Residence Visa|                      Flase|
   | GCC ID  |         																																|  GCC ID   |                         False|
	 |Diplomat ID |																																			|Diplomat ID |	                      False|
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
