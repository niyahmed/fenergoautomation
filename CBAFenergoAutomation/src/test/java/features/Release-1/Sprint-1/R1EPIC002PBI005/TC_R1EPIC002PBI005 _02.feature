#Test Case: TC_R1EPIC002PBI005_02
#PBI: R1EPIC002PBI005
#User Story ID: US112, US113, US112, US068, US101, US100, US029, US099, US027
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: Enrich KYC profile task -Customer Details
  @TobeAutomated
  Scenario: Verify the field behaviors of modified fields in Customer Details section of Enrich KYC Profile Screen
    #Client Type
    #Legal Entity Name
    #Legal Entity Type
    #Entity Type
    #Legal Entity Category
    #Country of Domicile/Physical Presence
    #Country of Incorporation / Establishment
    #Date of Incorporation
    #Name of Registration Body
    #Trading/Operation Name
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create new request with LegalEntityrole as "Client/Counterparty"
    ###	And I fill the data for "CaptureNewRequest" with key "C1"
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    And I check that below data is available
      | FieldLabel                               | Field Type | Mandatory | Editable | Field Defaults to |
      | Client Type                              | Dropdown   | Yes       | No       | Select...         |
      | Legal Entity Name                        | Textbox    | Yes       | No       |                   |
      | Legal Entity Type                        | Dropdown   | Yes       | No       |                   |
      | Entity Type                              | Dropdown   | No        | No       |                   |
      | Legal Entity Category                    | Dropdown   | Yes       | No       |                   |
      | Country of Domicile/Physical Presence    | Dropdown   | Yes       | Yes      | Select...         |
      | Country of Incorporation / Establishment | Dropdown   | Yes       | Yes      | Select...         |
      | Date of Incorporation                    | Date       | Yes       | Yes      |                   |
      | Name of Registration Body                | Textbox    | Yes       | Yes      |                   |
      | Trading/Operation Name                   | Textbox    | No        | Yes      |                   |
    #Validate the Country of Domicile/Physical Presence and country of incorporation lovs (Countries-Refer to LOV tab in the PBI)
    And I validate the LOV of "CountryofDomicile" with key "countrieslov"
    #Verify Trading/Operation Name field NOT accepts special characters. Test Data:Special characters
		And I fill the data for "enrichkycprofile" with key "Data1"
		#Verify Trading/Operation Name field NOT accepts lower case characters. Test Data: Lower case char
		And I fill the data for "enrichkycprofile" with key "Data2"
		#Verify Trading/Operation Name field accepts valid characters other than special and lowercase. 
		And I fill the data for "enrichkycprofile" with key "Data3"
		#Verify Date of Incorporation field does not accepts future date. 
		And I fill the data for "enrichkycprofile" with key "Data4"
		
