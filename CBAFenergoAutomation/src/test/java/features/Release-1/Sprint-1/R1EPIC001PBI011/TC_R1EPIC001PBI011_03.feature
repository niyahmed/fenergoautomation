#Test Case: TC_R1EPIC01PBI011_03
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC01PBI011_03

  @Automation @R1EPIC01PBI011_03
  Scenario: "Accounts" subflow is displaying as hidden on "Capture request details" task on "New Request" stage
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    Then I can see "Accounts" subflow is hidden
