#Test Case: TC_R1EPIC01PBI011_05
#PBI: R1EPIC01PBI011
#User Story ID: US031
Feature: TC_R1EPIC01PBI011_05 

@Automation
Scenario: "Accounts" subflow is displaying as hidden on "Add Product" screen for "Capture Request details" task for "new request" stage
	Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	And I complete "CaptureNewRequest" with Key "C1" and below data 
	| Product | Relationship |
  | C1      | C1           |
#	And I click on "Continue" button
	When I navigate to "AddProduct" screen on "CaptureRequestDetails" task
	Then I can see "Accounts" subflow is hidden
	