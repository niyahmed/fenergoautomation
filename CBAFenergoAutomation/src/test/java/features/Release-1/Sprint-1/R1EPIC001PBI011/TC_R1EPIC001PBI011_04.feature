#Test Case: TC_R1EPIC01PBI011_04
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC01PBI011_04 

 @TC_R1EPIC01PBI001_04 @Automation
Scenario: "Accounts" subflow is displaying as hidden on "Review Request" task on "NewRequest" stage
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	And I complete "CaptureNewRequest" with Key "C1" and below data 
	| Product | Relationship |
  | C1      | C1           |
	And I click on "Continue" button
	Then I can see "Accounts" subflow is hidden