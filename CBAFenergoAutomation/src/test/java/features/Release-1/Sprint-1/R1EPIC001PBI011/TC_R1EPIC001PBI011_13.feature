#Test Case: TC_R1EPIC01PBI011_13
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: <<Priyanka Arora>>
#Last Edited by: <<Priyanka Arora>>
Feature: TC_R1EPIC01PBI011_13 

@TC_R1EPIC01PBI011_13
Scenario: Validate "Accounts" subflow is displaying as hidden for following users on "LE360 overview" screen
	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	And I complete "CaptureNewRequest" with Key "C1" and below data 
	|Product|Relationship|
	|C1|C1|
	And I click on "Continue" button
	When I navigate to "LE360overview" screen
	Then I can see "Accounts" subflow is hidden
	And I store the "CaseID" from LE360
	Given I login to Fenergo Application with "OnboardingMaker" 
	When I search for the "CaseID"
	When I navigate to "LE360 overview" screen
	Then I can see "Accounts" subflow is hidden
    Given I login to Fenergo Application with "FlodVP" 
	When I search for the "CaseID"
	When I navigate to "LE360 overview" screen
	Given I login to Fenergo Application with "FlodSVP" 
	When I search for the "CaseID"
	When I navigate to "LE360 overview" screen
	Then I can see "Accounts" subflow is hidden
	Given I login to Fenergo Application with "Business Head (N2)" 
	When I search for the "CaseID"
	When I navigate to "LE360 overview" screen
	Then I can see "Accounts" subflow is hidden
	Given I login to Fenergo Application with "BusinessUnitHead" 
	When I search for the "CaseID"
	When I navigate to "LE360 overview" screen
	Then I can see "Accounts" subflow is hidden
	