#Test Case: TC_R1EPIC001PBI013_02
#PBI: R1EPIC001PBI013
#User Story ID: US034
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC001PBI013_02 - Enrich KYC Profile screen-"Anticipated Activity of Account" field

  @Automation
  Scenario: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section of "Enrich client Profile screen" stage for onboarding maker.
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    #Test Data:Verify "Anticipated Activity of Account" field is not visible under "Customer details" section and screenshot is taken
    Then I check that below data is not visible
      | FieldLabel                      |
      | Anticipated Activity of Account |
    Then I click on "ID" button to take screenshot
