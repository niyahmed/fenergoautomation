#Test Case: TC_R1EPIC001PBI002 _08
#PBI: R1EPIC001PBI002
#User Story ID: US029, US030
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: GLCMS UID validation, CIF validation 

Scenario:
Verify the RM user is able to search entity using GLCMS UID and T24 CIF ID in New Request-Search for duplicate screen. 

#Precondition:Entity creation by adding GLCMS UID and T24 CIF ID in Capture FAB References screen
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	###	And I fill the data for "CaptureNewRequest" with key "C1"
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYCandRegulatoryFAB" task 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "EnrichKYCProfileFAB" task 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	When I navigate to "QualityControlGrid" task 
	When I complete "ReviewOnboarding" task 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
	When I complete "FLODKYCReviewandSign-Off" task 
	When I navigate to "FLODAVPReviewandSign-OffGrid" task 
	When I complete "FLODAVPReviewandSign-Off" task 
	When I navigate to "BUHReviewandSignOffGrid" task 
	When I complete "BUHReviewandSignOff" task 
	When I navigate to "CaptureFabReferencesGrid" task 
	#ADD External references as CIF ID and Counterparty UID
	When I complete "CaptureFabReferences" task 
	#Validating that the case status is closed
	And I assert that the CaseStatus is "Closed" 
	#Test Case objective starts here 
	Given I login to Fenergo Application with "RMUser" 
	When I navigate to "Enterentitydetails" screen 
	When I complete "Enterentitydetails" screen 
	#Searching entity using GLCMS UID
	#Enter valid 6 digit GLCMS UID. Example test dat: GLCMS UID:123456 (Refer CaptureFabReferences screen for valid id)
	And I fill in data in New Request-Search for duplicate screen 
	When I click on Search button in New Request-Search for duplicate screen 
	#User should be able to view the entity in the search grid corresponding to the GLCMS ID entered in the search criteria
	Then I validate the search grid data 
	#Searching entity using T24 CIF ID
	#Enter valid 10 digit T24 CIF ID. Example test dat: T24 CIF ID :1234567891 (Refer CaptureFabReferences screen for valid id)
	And I fill in data in New Request-Search for duplicate screen 
	When I click on Search button in New Request-Search for duplicate screen 
	#User should be able to view the entity in the search grid corresponding to the T24 CIF ID entered in the search criteria
	Then I validate the search grid data 
	#Combination of GLCMS UID and T24 CIF ID
	#Enter valid GLCMS UID and T24 CIF ID (Refer CaptureFabReferences screen for valid id)
	And I fill in data in New Request-Search for duplicate screen 
	When I click on Search button in New Request-Search for duplicate screen 
	#User should be able to view the entity in the search grid corresponding to the GLCMS UID and T24 CIF ID entered in the search criteria
	Then I validate the search grid data 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
