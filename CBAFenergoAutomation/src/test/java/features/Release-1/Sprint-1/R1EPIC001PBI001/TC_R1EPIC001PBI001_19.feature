#Test Case: TC_R1EPIC01PBI001_19
#PBI: R1EPIC01PBI001
#User Story ID: US015
#Designed by: Anusha PS
#Last Edited by: Anusha PS

Feature: Products Section- Grid View - Cancelled products

@COB 
Scenario: Verify that the Cancelled Product is visible/not visible for various users in LE360 screen

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	And I navigate to "CaptureRequestDetails" task 
	
	#Adding product with status as "New"
	And I add a Product with Product Status as "New"
	Then I click on Cancel option from action button 
	Then I can see the product status is shown as "Cancelled" 
	And I complete "CaptureRequestDetails" task 
	When I complete "ReviewRequest" task 
	#Complete all tasks of Onboarding Manager and Onbording checker and make sure case is at Review and approval stage
	Then I store the "CaseId" from LE360
	
	Then I navigate to LE360 screen
	Then I can see the Cancelled product is visible in the product grid
	
	Given I login to Fenergo Application with "Onboarding Maker" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	Then I can see the Cancelled product is visible in the product grid
	
	Given I login to Fenergo Application with "Onboarding Checker" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	Then I cannot see the Cancelled product is visible in the product grid
	
	Given I login to Fenergo Application with "FLOD KYC Manager" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	Then I cannot see the Cancelled product is visible in the product grid

	Given I login to Fenergo Application with "FLOD AVP" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	Then I cannot see the Cancelled product is visible in the product grid
	
	Given I login to Fenergo Application with "Business Unit Head (N3)" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	Then I cannot see the Cancelled product is visible in the product grid
	
	Given I login to Fenergo Application with "FLOD VP" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	Then I cannot see the Cancelled product is visible in the product grid
	
	Given I login to Fenergo Application with "FLOD SVP" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	Then I cannot see the Cancelled product is visible in the product grid
	
	
	Given I login to Fenergo Application with "Business Head (N2)" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	Then I cannot see the Cancelled product is visible in the product grid
	
	Given I login to Fenergo Application with "Compliance" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	Then I cannot see the Cancelled product is visible in the product grid
	
	Given I login to Fenergo Application with "Fenergo - Admin" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	Then I cannot see the Cancelled product is visible in the product grid
	