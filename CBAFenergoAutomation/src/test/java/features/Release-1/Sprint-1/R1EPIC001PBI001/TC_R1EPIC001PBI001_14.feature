#Test Case: TC_R1EPIC01PBI001_14
#PBI: R1EPIC01PBI001
#User Story ID: US015
#Designed by: Sanjeet Singh
#Last Edited by: Anusha PS
Feature: Products Section- Grid View - Cancelled products 


Scenario:
Verify that the Cancelled Product is available in "Capture Request Details", "Review Request" screens for RM user once the task moved to review and sign off 
	Given I login to Fenergo Application with "RelationshipManager" 
	#Creating a legal entity with legal entity role as Client/Counterparty 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	When I add a product in capture request details page 
	Then I click on Cancel option from action button 
	Then I can see the product status is shown as "Cancelled" 
	And I complete "CaptureRequestDetails" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Then I login to Fenergo Application with "KYCManager" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYCandRegulatory" task 
	Then I navigate to "EnrichKYCProfileSection" task 
	When I cancel a product in EnrichKYCProfile Screen 
	Then I can see the product is visible in the product grid 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "RiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	Then I login to Fenergo Application with "RelationshipManager" 
	Then I navigate to "CaptureRequestDetails" task 
	Then I can see the Cancelled product is visible in the product grid 
	Then I navigate to "ReviewRequest" task 
	Then I can see the Cancelled product is visible in the product grid 
	
	
	
