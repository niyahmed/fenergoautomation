#Test Case: TC_R1EPIC01PBI001_18
#PBI: R1EPIC01PBI001
#User Story ID: US014
#Designed by: Anusha PS
#Last Edited by: Anusha PS


Feature: Products Section- Grid View 

Scenario: Validate if Onboarding Maker is able to see "Product Status" as a column on the "Products grid view" in "ValidateKYCandRegulatory" screen after adding/editing the Product 

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	And I complete "CaptureRequestDetails" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360
	
	When I navigate to "ValidateKYCandRegulatory" task as "Onboarding Maker"
	#Adding product with status as "New"
	And I add a Product with Product Status as "New"
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	#Editing product with status as "Active"
	And I edit a Product with Product Status as "Active"
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	#Editing product with status as "Approved"
	And I edit a Product with Product Status as "Approved"
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	#Editing product with status as "Pending Approval"
	And I edit a Product with Product Status as "Pending Approval"
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	#Editing product with status as "Rejected"
	And I edit a Product with Product Status as "Rejected"
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	#Editing product with status as "New"
	And I edit a Product with Product Status as "New"
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	#Editing product with status as "Cancelled"
	And I edit a Product with Product Status as "Cancelled"
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	#Deleting the added product
	And I delete the product
	
	#Validate if product section is empty
	And I assert "Product Section" is empty