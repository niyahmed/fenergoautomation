#Test Case: TC_R1EPIC01PBI001_13
#PBI: R1EPIC01PBI001
#User Story ID: US013
#Designed by: Sanjeet Singh
#Last Edited by: Anusha PS
Feature: Products Section- Product Grid Remove Products

@Onboarding 
Scenario: Verify Remove option is not visible for Product Grid in action button  in LE360 screen

	Given I login to Fenergo Application with "RelationshipManager" 
 	#Creating a legal entity with legal entity role as Client/Counterparty 
	When I create new request with ClientEntityType as "FI"  and ClientEntityRole as "Client/Counterparty"
	When I add a product in "CaptureNewRequest" screen
	And I complete "CaptureRequestDetails" task 
	When I complete "ReviewRequest" task 
	Then I validate the specific LOVs for "DAO Code"
	|Lovs|
	|BNK-103330734|
    |BNK-103330735|
    |BNK-103330733|
	Then I navigate to LE360 screen
	Then I click on action button for "ProductGrid"
	Then I see the "Remove" option is not present in action button
	