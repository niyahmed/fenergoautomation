#Test Case: TC_R1EPIC01PBI001_01
#PBI: R1EPIC01PBI001
#User Story ID: US014
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: TC_R1EPIC01PBI001_01- Products Section- Grid View

  @R1EPIC01PBI001 @Automation @TC_R1EPIC01PBI001_01
  Scenario: 
    Validate if RM is able to see "Product Status" as a column on the "Products grid view" in "Capture Request Details" and "Review Request" screen after adding the Product

    Given I login to Fenergo Application with "RM:IBG-DNE"
    #=Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    #=Adding product with status as "New"
    And I add a Product from "CaptureRequestDetails"
    When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "CaptureRequestDetails"
    And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails"
    And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "ReviewRequest"
    And I assert "Status" column is visible in the Product Grid for "ReviewRequest"
    And I assert "Status" column is not present as a field below the grid for "ReviewRequest"
	#    When I complete "ReviewRequest" task
	#    Then I store the "CaseId" from LE360
	#    Then I login to Fenergo Application with "OnboardingMaker"
	#    When I search for the "CaseId"
	#    When I navigate to "ValidateKYCandRegulatoryGrid" task 
	#    When I complete "ValidateKYCandRegulatoryFAB" task 
	#    When I navigate to "EnrichKYCProfileGrid" task 
	#    When I expand "ProductGridExpandEnrichKYC" and zoom out so that grid gets fully visible
	#	And I assert "Status" column is visible in the Product Grid for "EnrichKYCProfile" 
	#	And I assert "Status" column is not present as a field below the grid for "EnrichKYCProfile"
	##     
	#	When I navigate to  "ReviewRequest" task 
	#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	#	And I assert "Status" column is not present as a field below the grid 
	#	
