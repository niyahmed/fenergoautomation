#Test Case: TC_R1EPIC002PBI030_02
#PBI: R1EPIC002PBI030
#User Story ID: US073
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI030_02

  @To_be_automated
  Scenario: Validate behaviour of "Legal Entity Source of Funds" field under "Source Of Funds And Wealth Details" section for Onboarding Maker user
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    When I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    # est data- I validate "Legal Entity Source of Funds" field is visible under "Source Of Funds And Wealth Details" section
    And I check that below data is visible
      | FieldLabel                   |
      | Legal Entity Source of Funds |
    And I click on "AddAddresss" button
    When I complete "Addresses" screen with key "CountryAsUAE"
    And I click on "SaveAddress" button
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task
    
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    #Validating that the case status is closed
    When I navigate to "LE360-overview" task
    When I navigate to "Source Of Funds And Wealth Details" section by clicking on "LEdetails" grid
    # est data- I validate "Legal Entity Source of Funds" field is visible under "Source Of Funds And Wealth Details" section
    Then I can see "Legal Entity Source of Funds" is visible as per DD(sequence, mandatory, editable, Visible) under "Source Of Funds And Wealth Details" section
    When I navigate to "LEVerifieddetails" task by navigating through "LE360-overview" stage
    When I navigate to "Source Of Funds And Wealth Details" section
    # est data- I validate "Legal Entity Source of Funds" field is visible under "Source Of Funds And Wealth Details" section
    Then I can see "Legal Entity Source of Funds" is visible as per DD(sequence, mandatory, editable, Visible) under "Source Of Funds And Wealth Details" section
    # Validating "Legal Entity Source of Funds" for client type "FI" or "NBFI" and Product is "Call Account" or "Savings account"
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty" and Client Type is "FI" or "NBFI"
    When I navigate to "CaptureRequestDetailsFAB" task
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "Onboardingmaker"
    When I search for "CaseID"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I add product type "Call Account" or "Savings account" and save
    # Test data- I validate "Legal Entity Source of Funds" field is visible under "Source Of Funds And Wealth Details" section
    Then I can see "Legal Entity Source of Funds" is visible as per DD(sequence, mandatory, editable, Visible) under "Source Of Funds And Wealth Details" section
    When I complete "EnrichKYCProfileFAB" task
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task
    When I navigate to "QualityControlGrid" task
    When I complete "ReviewOnboarding" task
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    When I navigate to "FLODKYCReviewandSign-OffGrid" task
    When I complete "FLODKYCReviewandSign-Off" task
    When I navigate to "FLODAVPReviewandSign-OffGrid" task
    When I complete "FLODAVPReviewandSign-Off" task
    When I navigate to "BUHReviewandSignOffGrid" task
    When I complete "BUHReviewandSignOff" task
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFabReferences" task
    #Validating that the case status is closed
    When I navigate to "LE360-overview" task
    When I navigate to "Source Of Funds And Wealth Details" section by clicking on "LEdetails" grid
    # Test data- I validate "Legal Entity Source of Funds" field is visible under "Source Of Funds And Wealth Details" section
    Then I can see "Legal Entity Source of Funds" is visible as per DD(sequence, mandatory, editable, Visible) under "Source Of Funds And Wealth Details" section
    When I navigate to "LEVerifieddetails" task by navigating through "LE360-overview" stage
    When I navigate to "Source Of Funds And Wealth Details" section
    # Test data- I validate "Legal Entity Source of Funds" field is visible under "Source Of Funds And Wealth Details" section
    Then I can see "Legal Entity Source of Funds" is visible as per DD(sequence, mandatory, editable, Visible) under "Source Of Funds And Wealth Details" section
