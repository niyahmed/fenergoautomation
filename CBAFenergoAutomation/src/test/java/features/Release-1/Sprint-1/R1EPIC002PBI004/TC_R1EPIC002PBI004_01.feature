#Test Case: TC_R1EPIC002PBI004_01
#PBI: R1EPIC002PBI004
#User Story ID: US068, US024, US025, US087, US040, US039, US033, US041
#Designed by: Niyaz Ahmed
#Last Edited by: Vibhav Kumar.
Feature: TC_R1EPIC002PBI004_01 - Capture New Request Details - Customer Details.

  @Automation @TC_R1EPIC002PBI004_01
  Scenario: 
    Verify the field behaviour of below new fields added in Customer Details section of Capture Request Details Screen
#Create entity with Country of Incorporation = UAE and client type = corporate
#Legal Entity Category is not visible in screen after Release 2,sprint 4.(@Regression run 12/04/21).
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I validate the following fields in "CaptureRequestDetails" Sub Flow
      | Label                           | FieldType           | Mandatory | ReadOnly | DefaultsTo | Visible | 
      | Channel & Interface             | Dropdown            | true      | false    | Select...  | true    | 
      | Purpose of Account/Relationship | MultiSelectDropdown | NA        | false    | NA         | true    | 
      | Residential Status              | Dropdown            | NA        | true     | NA         | true    | 
      | Customer Tier                   | Dropdown            | true      | false    | Select...  | true    | 
      | Relationship with bank          | Dropdown            | true      | false    | Select...  | true    | 
      | CIF Creation Required?          | CheckBox            | NA        | true     | Yes        | true    | 
      | Emirate                         | Dropdown            | true      | false    | Select...  | true    | 
      | Legal Entity Category           | NA                  | NA        | NA       | NA         | false   | 
      #=| Legal Entity Category           | Dropdown  | NA        | true     | NA         | true    |
    #=And I click on "Legal Entity Category" button to take screenshot
    #=Verify Residential status is set to Resident when country of Incorporation = UAE
    And I check that below data is available
      | Label              | Value    |
      | Residential Status | Resident |
    #		#=Validate the Legal Category Type lovs (LegalEntityCategory-Refer to LOV tab in the PBI)
    #	And I validate the LOV of "LegalEntityCategory" with key "Legalentiycatlov"
    #=Validate the 'Channel & Interface' lovs
    And I verify "Channel & Interface" drop-down values
    And I verify "Customer Tier" drop-down values
    And I verify "Relationship with bank" drop-down values
    And I verify "Emirate" drop-down values
    #		#Validate the Residential Status lovs (Residency Status-Refer to LOV tab in the PBI)
    #Then I validate the specific LOVs for "Customer Tier"
    #	And I validate the LOV of "CustomerTier" with key "custierlov"
    #Then I validate the specific LOVs for "Relationship with bank"
    #Then I validate the specific LOVs for "Emirate"
