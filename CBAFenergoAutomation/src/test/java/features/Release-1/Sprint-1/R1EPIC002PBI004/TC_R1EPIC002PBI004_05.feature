#Test Case: TC_R1EPIC002PBI004_05
#PBI: R1EPIC002PBI004
#User Story ID: US112, US113, US114, US106, US100, US101, US029
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1EPIC002PBI004_05
  @TobeAutomated
  Scenario: Verify the field behaviors of modified fields in Customer Details section of Capture request details screen
    #Client Type
    #Legal Entity Name
    #Legal Entity Type
    #Entity Type
    #Country of Domicile/Physical Presence
    #Country of Incorporation / Establishment
    #Date of Incorporation
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create new request with LegalEntityrole as "Client/Counterparty"
    And I check that below data is available
      | FieldLabel                               | Field Type | Mandatory | Editable | Field Defaults to |
      | Client Type                              | Dropdown   | Yes       | No       | Select...         |
      | Legal Entity Name                        | Textbox    | Yes       | Yes      |                   |
      | Legal Entity Type                        | Dropdown   | Yes       | No       |                   |
      | Entity Type                              | Dropdown   | Yes       | No       |                   |
      | Country of Domicile/Physical Presence    | Dropdown   | No        | Yes      | Select...         |
      | Country of Incorporation / Establishment | Dropdown   | No        | Yes      | Select...         |
      | Date of Incorporation                    | Date       | Yes       | Yes      |                   |
    #Validate the Country of Domicile/Physical Presence and country of incorporation lovs (Countries-Refer to LOV tab in the PBI)
    And I validate the LOV of "CountryofDomicile" with key "countrieslov"
    #Verify Date of Incorporation field does not accepts future date.
    And I fill the data for "enrichkycprofile" with key "Data4"
    #Verify Legal Entity Name field NOT accepts more than 255 characters. Test Data:256 characters
    And I fill the data for "capturerequestdetails" with key "Data1"
    #Verify Legal Entity Name field accepts 255 characters. Test Data: 255 characters
    And I fill the data for "capturerequestdetails" with key "Data2"
    #Verify the field behavior of new fields in review request screen
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    And I complete "CaptureRequestDetailsFAB" task
    And I check that below data is available
      | FieldLabel                               | Field Type | Mandatory | Editable |
      | Client Type                              | Dropdown   | Yes       | No       |
      | Legal Entity Name                        | Textbox    | Yes       | No       |
      | Legal Entity Type                        | Dropdown   | Yes       | No       |
      | Entity Type                              | Dropdown   | Yes       | No       |
      | Country of Domicile/Physical Presence    | Dropdown   | No        | No       |
      | Country of Incorporation / Establishment | Dropdown   | No        | No       |
      | Date of Incorporation                    | Date       | Yes       | No       |
