#Test Case: TC_R1EPIC002PBI004_02
#PBI: R1EPIC002PBI004
#User Story ID: US068, US024, US025, US087, US026, US040, US039, US033, US041
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1EPIC002PBI004_02- Capture New Request Details - Customer Details

  @Automation
  Scenario: 
    Verify the conditionally available field behaviour of new fields added in Customer Details section of Capture Request Details Screen

    Given I login to Fenergo Application with "RM:PCG"
    #Create entity with Country of Incorporation = Andora (other than UAE) and client type = PCG Entity
    When I create a new request with FABEntityType as "PCG-Entity" and LegalEntityRole as "Client/Counterparty"
    And I validate the following fields in "CaptureRequestDetails" Sub Flow
      | Label                           | FieldType           | Mandatory | ReadOnly | DefaultsTo | Visible | 
     #| Legal Entity Category           | Dropdown            | NA        | true     | NA         | true    | 
      | Channel & Interface             | Dropdown            | true      | false    | Select...  | true    | 
      | Purpose of Account/Relationship | MultiSelectDropdown | NA        | false    | NA         | true    | 
      | Residential Status              | Dropdown            | NA        | true     | NA         | true    | 
      | Relationship with bank          | Dropdown            | true      | false    | Select...  | true    | 
      | CIF Creation Required?          | CheckBox            | NA        | true     | Yes        | true    | 
    #Verify Residential status is set to Resident when country of Incorporation = Andora (other than UAE)
    When I select "AE-UNITED ARAB EMIRATES" for "Dropdown" field "Country of Incorporation / Establishment"
    And I check that below data is available
      | Label              | Value        |
      | Residential Status | Non Resident |
    And I take a screenshot  
