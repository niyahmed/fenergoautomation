#Test Case: TC_R1EPIC002PBI014_03
#PBI: R1EPIC002PBI014
#User Story ID: Remove/Hide
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC002PBI014_03
  Scenario: Verify the below fields are removed in add product screen, add prodcut-trading entity screen, Product search screen and Verified LE details screen
    #Purpose of Account
    #Trading Location
    #Selling Location
    Given I login to Fenergo Application with "RM"
    #Validate in Add product screen
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureNewRequest" task
    When I navigate to "ProductInformation" screen
    And I check that below data is available
      | FieldLabel         | Visible |
      | Purpose of Account | NO      |
      | Trading Location   | No      |
      | Selling Location   | No      |
    #Verify the Booking entity value is defaulted to 'Bank - UAE Branch'
    And I check that below data is available
      | FieldLabel     | Value             |
      | Booking Entity | Bank - UAE Branch |
    When I navigate to "tradingentity" screen
    And I check that below data is available
      | FieldLabel         | Visible |
      | Purpose of Account | NO      |
      | Trading Location   | No      |
      | Selling Location   | No      |
    #Verify the Booking entity value is defaulted to 'Bank - UAE Branch'
    And I check that below data is available
      | FieldLabel     | Value             |
      | Booking Entity | Bank - UAE Branch |
    #Validate in Product search screen
    When I navigate to "ProductSearch" screen
    And I check that below data is available
      | FieldLabel         | Visible |
      | Purpose of Account | NO      |
      | Trading Location   | No      |
      | Selling Location   | No      |
    #Validate in Verified LE Details screen in LE360 screen
    When I navigate to LE360 screen
    And I check that below data is available
      | FieldLabel         | Visible |
      | Purpose of Account | NO      |
      | Trading Location   | No      |
      | Selling Location   | No      |
