#Test Case: TC_R1EPIC001PBI020_04
#PBI: R1EPIC001PBI020
#User Story ID: US126/SR10
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: create contact screen-"Primary Phone number" field

Scenario: Validate "Primary Phone Number" field is defaults to "Work Phone" on" Create contact" screen of "Capture request details" task for RM user.
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I click on "+" sign displaying at the top of contact section to create contact
	When I navigate to "CreateContact" task
	#Test Data: Validate "Primary Phone Number" field is defaults to "Work Phone" on" Create contact" screen
	Then I can see "Primary Phone Number" field is defaults to "Work Phone" on" Create contact" screen
