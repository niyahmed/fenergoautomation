#Test Case: TC_R1EPIC001PBI020_09
#PBI: R1EPIC001PBI020
#User Story ID: US126/SR10
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: create contact screen-"Home phone" field

Scenario: Validate "Home phone" field becomes mandatory when "Primary Phone Number" is set to "Home phone" on "Create contact" screen
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I click on "+" sign displaying at the top of contact section to create contact
	When I navigate to "CreateContact" task
	#Test Data: Validate "Home Phone" field becomes mandatory when "Primary Phone Number" field is set to "Home Phone" on" Create contact" screen
	When I set "Primary Phone Number" field to "Home Phone" on" Create contact" screen
	Then I can see "Home Phone" field becomes mandatory
