#Test Case: TC_R1EPIC002PBI017.1_06
#PBI: R1EPIC002PBI017.1
#User Story ID: US091, US024, US025, US087, US026, US040, US039, US033, US041, US086, US088, US089, US030, US036, US090, US034, US035, US038, US037, US092, US090
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI017.1_06

  Scenario: Validate new auto-populated fields under "Customer details" section on "LEVerified details" screen for RM user
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    Then I fill values in all mentioned fields and complete "CaptureRequestDetailsFAB" task
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYCProfileFAB" task
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task
    When I navigate to "QualityControlGrid" task
    When I complete "ReviewOnboarding" task
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    When I navigate to "FLODKYCReviewandSign-OffGrid" task
    When I complete "FLODKYCReviewandSign-Off" task
    When I navigate to "FLODAVPReviewandSign-OffGrid" task
    When I complete "FLODAVPReviewandSign-Off" task
    When I navigate to "BUHReviewandSignOffGrid" task
    When I complete "BUHReviewandSignOff" task
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFabReferences" task
    #Validating that the case status is closed
    When I navigate to "LE360-overview" task
    When I navigate to "customer details" section by clicking on "LEverifieddetails" grid
    #Test Data:Verify renamed fields under  "customer details" section on "LEverifieddetails" screen
    And I validate the following Auto-populated fields under "customer details" section
       | FAB Segment                                                |
      | Channel & Interface                                        |
      | Residential Status                                         |
      | Customer Tier                                              |
      | Relationship with bank                                     |
      | CIF Creation Required?                                     |
      | Emirate                                                    |
      | Customer Relationship Status                               |
      | Length of Relationship                                     |
      | Legal Counter party type                                   |
      | Legal Constitution Type                                    |
      | Purpose of Account/Relationship                            |
      | Justification for opening/maintaining non-resident account |
      | Legal Entity Name (Parent)                                 |
      | Website Address                                            |
      | Real Name                                                  |
      | Original Name                                              |
      | Group name                                                 |
