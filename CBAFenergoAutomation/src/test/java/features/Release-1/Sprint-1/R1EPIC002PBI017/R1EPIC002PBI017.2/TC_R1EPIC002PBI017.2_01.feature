#Test Case: TC_R1EPIC002PBI017.2_01
#PBI: R1EPIC002PBI017.2
#User Story ID:USR01
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI017.2_01
@Automation
  Scenario: 
    Validate the renamed fields under "Internal Booking details" section on "LE360-overview" screen for RM user

    Given I login to Fenergo Application with "RM"
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    Then I navigate to "LE360overview" screen
    #Test Data:Verify renamed fields under "Internal Booking details" section
    And I can see "Entity of Onboarding" label is renamed as "Booking Country" on "CaptureRequestDetails" screen
