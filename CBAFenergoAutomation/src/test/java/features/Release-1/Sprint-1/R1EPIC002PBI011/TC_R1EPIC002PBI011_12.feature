#Test Case: TC_R1EPIC002PBI011_12
#PBI: R1EPIC002PBI011
#User Story ID: US108
#Designed by: Priyanka Arora (as part of R1EPIC001PBI008)
#Last Edited by: Anusha PS
Feature: COB 

@TC_R1EPIC002PBI011_12
Scenario: Validate the behavior of "TaxIdentifierValue" field while trying to Edit/view it.
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	 And I complete "CaptureNewRequest" with Key "C1" and below data 
	|Product|Relationship|
	|C1|C1|
	And I click on "Continue" button
	Then I store the "CaseId" from LE360
	And I complete "ReviewRequest" task
	Given I login to Fenergo Application with "OnboardingMaker"
	When I search for the "CaseId"
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	And I complete "ValidateKYC" screen with key "C1"
	And I click on "SaveandCompleteforValidateKYC" button 
	Then I navigate to "EnrichKYCProfileGrid" task
	#Test Data: Tax Type: VAT ID
	Then I navigate to "TaxIdentifier" screen by clicking on "Plus" button from "EnrichKYCProfile"
	And I check that "Tax Type" is Mandatory
	When I complete "TaxIdentifier" with TaxType as "VAT ID" and Country as "AE-UNITED ARAB EMIRATES"
	And I assert "TaxIdentifier" column is visible in the Product Grid for "CaptureRequestDetails" 
	When I navigate to "TaxIdentifier" link by clicking on "Edit" link under options
	And I check that "Tax Type" is Mandatory	
