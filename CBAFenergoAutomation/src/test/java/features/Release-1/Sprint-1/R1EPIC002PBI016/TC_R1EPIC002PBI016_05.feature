#Test Case: TC_R1EPIC002PBI016_05
#PBI: R1EPIC002PBI016
#User Story ID: USR01
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI016_05-Internal Booking Entity section

  @Automation @TC_R1EPIC002PBI016_05
  Scenario: Validate "Entity of Onboarding" dropdown is renamed as "Booking Country" drop-down under "Internal Booking details" section on "Capture request details" screen for RM user
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    #Test-data: Verify "Entity of Onboarding" dropdown should be renamed as "Booking Country" drop-down
    Then I can see "EntityofOnboarding" label is renamed as "BookingCountry" on "EnterEntitydetails" screen
    #Test Data:Verify "Booking Country"  drop-down is displaying as editable, Visible and mandatory on 7 sequence under "Internal Booking details" section on "Capture request details" screen
    #	Then I can see "Booking Country"  drop-down is displaying as editable, Visible and mandatory on 7 sequence under "Internal Booking details" section
    And I check that "Booking Country" is Mandatory
