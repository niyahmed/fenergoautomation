#Test Case: TC_R1EPIC002PBI016_01
#PBI: R1EPIC002PBI016
#User Story ID: USR01
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI016_01 - Internal Booking Entity section

  @Automation @TC_R1EPIC002PBI016_01
  Scenario: Validate the removed fields under "Internal Booking details" section on "Capture request details" screen for RM user
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    #  Test Data:Verify "Priority","From Office Area","On Behalf Of","Internal desk" drop-doenfields should be display as removed on "Capture request details" screen
    #	Then I can see "Priority","From Office Area","On Behalf Of","Internal desk" drop-down fields displaying as removed
    And I check that below data is not visible
      | FieldLabel       |
      | Priority         |
      | From Office Area |
      | On Behalf Of     |
      | Internal desk    |
