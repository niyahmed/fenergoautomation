#Test Case: TC_R1EPIC002PBI016_07
#PBI: R1EPIC002PBI016
#User Story ID:US055,US056,US057, US058, US060
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: Internal Booking Entity section

@To_be_automated
Scenario: Validate LOVs for "Target Code", "Sector Description" drop-down under "Internal Booking details" section on "Capture request details" screen for RM user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I input values "Target Code", "Sector Description","UID Originating Branch", "Country of Domicile/Physical Presence" drop-down
	When I complete "CaptureRequestDetailsFAB" task 	
	When I navigate to "ReviewRequest" task
	#Test-data: verify values for fields"Target Code", "Sector Description","UID Originating Branch", "Country of Domicile/Physical Presence" drop-down are autopopulated as same.
	Then I can see values for "Target Code", "Sector Description","UID Originating Branch", "Country of Domicile/Physical Presence" drop-down are autopopulated as same from "CaptureRequestDetailsFAB" task 