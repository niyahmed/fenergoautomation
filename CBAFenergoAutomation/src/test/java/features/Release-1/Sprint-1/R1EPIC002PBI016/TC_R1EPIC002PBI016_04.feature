#Test Case: TC_R1EPIC002PBI016_04
#PBI: R1EPIC002PBI016
#User Story ID: US055,US056,US057, US058, US059
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI016_04-Internal Booking Entity section

  @TC_R1EPIC002PBI016_04 @Automation
  Scenario: Validate new fields are added under "Internal Booking details" section on "Capture request details" screen for RM user
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    #Test Data:Verify "Target Code","Sector Description","UID Originating Branch","Propagate To Target Systems" drop-down fields are getting displayed on "Review request"" screen
    #	Then I can see "Target Code","Sector Description","UID Originating Branch","Propagate To Target Systems" drop-down fields are getting displayed
    And I check that below data is visible
      | FieldLabel                 |
      | Target Code                |
      | Sector Description         |
      | UID Originating Branch     |
      | Propagate To Target System |
    And I take a screenshot
