#Test Case: TC_R1EPIC002PBI015_09
#PBI: R1EPIC002PBI015
#User Story ID: US113
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: COB 

Scenario: Validate input for "Legal Entity Name" is as per DD on "Enter Entity details" screen of "New request" stage
	Given I login to Fenergo application with "RM" user
	When I click on "+" sign to create new request
	When I navigate to "Enter Entity details" screen
	#Test Data:validate input for "Legal entity Name" field for 255 characters
	When I enter value in "LegalEntityName" upto 255 characters
	Then I see value in "LegalEntityName" field value is saved successfully
	#validate input for "Legal entity Name" field for Less than 255 characters
	When I enter value in "LegalEntityName" less than 255 characters
	Then I see value in "LegalEntityName" field value is saved successfully
	#validate input for "Legal entity Name" field for more than 255 characters
	When I enter value in "LegalEntityName" more than 255 characters
	Then I see value in "LegalEntityName" field value is not saved and user gets error message to enter value upto 255 characters.
	
	