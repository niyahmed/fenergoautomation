Feature: TC_R1EPIC001PBI006 _09

Scenario: Verify the  FLOD SVP user is able to search a case using 'Assigned To'.
	
	Given I login to Fenergo Application with "FLOD SVPUser" 
	When I navigate to "CaseSearch" screen
	#Test Data: Assigned To = Any FLOD SVP user
	And I fill in data in CaseSearch screen
	When I click on Search button in CaseSearch screen
	#User should be able to view all cases assigned to the FLOD SVP user
	Then I validate the search grid data
