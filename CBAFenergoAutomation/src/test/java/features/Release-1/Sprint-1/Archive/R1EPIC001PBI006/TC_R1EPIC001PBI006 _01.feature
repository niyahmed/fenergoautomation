Feature: TC_R1EPIC001PBI006 _01


@Automation
Scenario: Verify new field 'Assigned To' is available in Case Search screen.
	
	Given I login to Fenergo Application with "RM" 
	When I navigate to "CaseSearch" screen
	And I check that below data is available 
	|FieldLabel|
	|Assigned To|
