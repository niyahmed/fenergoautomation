#Test Case: TC_R1EPIC002PBI009_04
#PBI: R1EPIC002PBI009
#User Story ID: US055
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: Capture New Request Details/Review Request - Add Relationship -Relationship Type

  @Automation @TC_R1EPIC002PBI009_04
  Scenario: Verify Relationship section is mandatory in Capture request details screen
    Given I login to Fenergo Application with "RM"
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    #check the Relationship grid is mandatory
    And I assert that "RelationshipsGrid" grid is mandatory
