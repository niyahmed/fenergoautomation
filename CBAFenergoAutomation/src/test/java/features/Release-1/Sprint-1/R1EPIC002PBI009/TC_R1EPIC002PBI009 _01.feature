#Test Case: TC_R1EPIC002PBI009_01
#PBI: R1EPIC002PBI009
#User Story ID: US055
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: Capture New Request Details/Review Request - Add Relationship -Relationship Type

@TobeAutomated @TC_R1EPIC002PBI009_01
Scenario: Validate the dropdown values of the field 'Relationship Type' in Add Relationship screen of Capture request details screen (Refer 'Relationship Type' lov in the PBI)
	
	Given I login to Fenergo Application with "RM" 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	When I navigate to "AddRelationship" screen on "CaptureRequestDetails" task
	Then I verify "Relationship Type" drop-down values
	